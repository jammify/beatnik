package pages;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RoomPage extends PartnerDetailPage {
    private Map<String, String> data;
    private static WebDriver driver;
    private int timeout = 15;
    private static WebElement element = null;

    @FindBy(id = "capacityMax")
    private WebElement capacityRangeMaximum;

    @FindBy(id = "capacityMin")
    private WebElement capacityRangeMinimum;

    @FindBy(id = "drumkitBrand")
    private WebElement drumkitBrand;

    @FindBy(id = "drumkitPieces")
    private WebElement drumkitPieces;

    @FindBy(css = "a[href='#!dashboard/']")
    private WebElement jammateTest;

    @FindBy(css = "a[href='#!login']")
    private WebElement logout;

    @FindBy(id = "noOfAmplifiers")
    private WebElement noOfAmplifiers1;

    @FindBy(css = "#_md-chips-wrapper-380 div.md-chip-input-container.ng-scope div input.md-input.ng-pristine.ng-untouched.ng-valid.ng-scope.ng-empty")
    private WebElement noOfAmplifiers2;

    @FindBy(id = "input_381")
    private WebElement noOfMics1;

    @FindBy(css = "#_md-chips-wrapper-382 div.md-chip-input-container.ng-scope div input.md-input.ng-pristine.ng-untouched.ng-valid.ng-scope.ng-empty")
    private WebElement noOfMics2;

    @FindBy(id = "noOfMixerBoardInputs")
    private WebElement noOfMixerBoardInputs;
   
    @FindBy(id = "noOfMixerBoardInputs")
    private WebElement hasCondenser;

    @FindBy(id = "noOfStools")
    private WebElement noOfStools;

    @FindBy(xpath = "\"_md-chips-wrapper-380\"")
    private WebElement otherAmenities;
    
    @FindBy(css = "a[href='#!setup/']")
    private WebElement orgSetup;
    
    @FindBy(xpath = "//*[@id=\"hasJamming\"]/div[1]")
    private WebElement hasJamming;	

    private final String pageLoadedText = "No of Mixer Board Inputs";

    private final String pageUrl = "/partner/#!/createroom/undefined";

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required section div:nth-of-type(1) div:nth-of-type(1) div.button-wrapper button.btn.ng-pristine.ng-untouched.ng-valid.ng-empty")
    @CacheLookup
    private WebElement selectImage1;

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required section div:nth-of-type(1) div:nth-of-type(2) div.button-wrapper button.btn.ng-pristine.ng-untouched.ng-valid.ng-empty")
    private WebElement selectImage2;

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required section div:nth-of-type(1) div:nth-of-type(3) div.button-wrapper button.btn.ng-pristine.ng-untouched.ng-valid.ng-empty")
    private WebElement selectImage3;

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required section div:nth-of-type(1) div:nth-of-type(4) div.button-wrapper button.btn.ng-pristine.ng-untouched.ng-valid.ng-empty")
    private WebElement selectImage4;

    @FindBy(css = "a[href='#!settlements/']")
    private WebElement settlements;

    @FindBy(id = "speakerBrand")
    private WebElement speakerBrand;

    @FindBy(css = "button.md-raised.md-primary.md-button.md-ink-ripple.md-button")
    private WebElement submit;

    @FindBy(css = "a[href='#!transactions/']")
    private WebElement transactions;

    @FindBy(css = "label:nth-of-type(1) input[type='file']")
    private WebElement upload1;

    @FindBy(css = "label:nth-of-type(2) input[type='file']")
    private WebElement upload2;

    @FindBy(css = "label:nth-of-type(3) input[type='file']")
    private WebElement upload3;

    @FindBy(css = "label:nth-of-type(4) input[type='file']")
    private WebElement upload4;

    @FindBy(id = "xHourPackage")
    private WebElement xHourPackage;
    
   

    public  RoomPage() {
    	
        super(driver);
    
    }

    public RoomPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public RoomPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public RoomPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }
    
    public static  WebElement txtBoxCapacityMin(WebDriver driver) {
    	element = scrollElementIntoMiddle("#capacityMin");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static  WebElement txtBoxCapacityMax(WebDriver driver) {
    	element = scrollElementIntoMiddle("#capacityMax");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxNoOfStools(WebDriver driver) {
    	element = scrollElementIntoMiddle("#noOfStools");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxNoOfMixerBoardInputs(WebDriver driver) {
    	element = scrollElementIntoMiddle("#noOfMixerBoardInputs");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxDrumkitPieces(WebDriver driver) {
    	element = scrollElementIntoMiddle("#drumkitPieces");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxDrumkitBrand(WebDriver driver) {
    	element = driver.findElement(By.id("drumkitBrand"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxSpeakerBrand(WebDriver driver) {
    	element = driver.findElement(By.id("speakerBrand"));
    	element.click();
    	element.clear();
    	return element;
    }
    public static WebElement chkBoxSoundProofed(WebDriver driver) {
    	element = driver.findElement(By.cssSelector("div.md-container.md-ink-ripple"));
    	element.click();
    	return element;
    }
    
    public static WebElement txtBoxNoOfAmplifiers(WebDriver driver) {
    	element = driver.findElement(By.id("noOfAmplifiers"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxOtherAmenities(WebDriver driver) {
    	element = scrollElementIntoMiddle("#_md-chips-wrapper-380 > div > div > input");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxNoOfMics(WebDriver driver) {
    	element = scrollElementToBottom("#input_381");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxAmplifierBrands(WebDriver driver) {
    	element = scrollElementIntoMiddle("#_md-chips-wrapper-382 > div > div > input");
    	element.click();
    	element.clear();
    	return element;
    }
    
    
    public static WebElement chkBoxHasCondenser(WebDriver driver) {
    	element = driver.findElement(By.xpath("//*[@id=\"hasCondenser\"]"));
    	element.click();
    	return element;
    }
    
    public static WebElement chkBoxHasJamming(WebDriver driver) {
    	element = scrollElementIntoMiddle("#hasJamming");
    	element.click();
    	return element;
    }
    
    public static WebElement txtBoxXHourPackage(WebDriver driver) {
    	element = driver.findElement(By.id("xHourPackage"));
    	element.click();
    	element.clear();
    	return element;
    }
    public static void buttonSubmit(WebDriver driver) {
    	element = scrollElementToBottom("body > div > div.outr > div.ng-scope > div > div > form > button");
    	element.click();
    	
    }
    
    public static void buttonAddSlab(WebDriver driver) {
    	element = scrollElementIntoMiddle("#addHourlySlab");
    	element.click();
    }
    
    public static WebElement firstSlab(WebDriver driver) {
    	 element = scrollElementIntoMiddle("body > div:nth-child(1) > div.outr > div.ng-scope > div > div > form > section > div.ng-scope > md-table-container > table > tbody > tr > td:nth-child(1)");//Monday Dropdown arrow
    	 element.click();
    	 element = scrollElementToBottomXpath("//*[@id=\"select_option_9\"]");//select Monday
    	 element.click();
    	 element = scrollElementToBottom("#select_value_label_6 > span.md-select-icon");//clicking on the End Day drowdown arrow
    	 element.click();
    	 element = driver.findElement(By.cssSelector("#select_option_20"));//Selecting Friday
    	 element.click();
//    	 element = scrollElementIntoMiddle("");
//    	 element.click();
    	 element = driver.findElement(By.cssSelector("span.ng-binding.ng-scope"));
    	 element.sendKeys("20");
    	 element = driver.findElement(By.xpath("//div[@class='moment-picker-specific-views']//td[.='09:00']"));
    	 element.click();
    	 element = driver.findElement(By.xpath("//div[@class='outr']//span[.='Select Hour...']"));
    	 element.click();
    	 element = driver.findElement(By.xpath("//div[@class='moment-picker-specific-views']//td[.='22:00']"));
    	 element.click();
    	 element = driver.findElement(By.id("price"));
    	 element.click();
    	 element.clear();
    	 
    	 return element;
    }
    
    public static WebElement txtBoxJammingHourPackageWeekDay(WebDriver driver) {
    	element = scrollElementToBottom("#jammingXWeekday");
    	element.click();
    	element.clear();
    	return element;
    }
    public static WebElement txtBoxJammingHourPackageWeekEnd(WebDriver driver) {
    	element = scrollElementToBottom("#jammingXWeekend");
    	element.click();
    	element.clear();
    	return element;
    }
    public static WebElement txtBoxJammingFulldayPackageWeekDay(WebDriver driver) {
    	element = scrollElementToBottom("#jammingFullWeekday");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxJammingFulldayPackageWeekEnd(WebDriver driver) {
    	element = scrollElementToBottom("#jammingFullWeekend");
    	element.click();
    	element.clear();
    	return element;
    }
    
    
    
    
    /**
     * Click on Jammate Test Link.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickJammateTestLink() {
        jammateTest.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Org Setup Link.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickOrgSetupLink() {
        orgSetup.click();
        return this;
    }

    /**
     * Click on Select Image Button.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSelectImage1Button() {
        selectImage1.click();
        return this;
    }

    /**
     * Click on Select Image Button.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSelectImage2Button() {
        selectImage2.click();
        return this;
    }

    /**
     * Click on Select Image Button.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSelectImage3Button() {
        selectImage3.click();
        return this;
    }

    /**
     * Click on Select Image Button.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSelectImage4Button() {
        selectImage4.click();
        return this;
    }

    /**
     * Click on Settlements Link.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSettlementsLink() {
        settlements.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickSubmitButton() {
        submit.click();
        return this;
    }

    /**
     * Click on Transactions Link.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage clickTransactionsLink() {
        transactions.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage fill() {
        setCapacityRangeMinimumNumberField();
        setCapacityRangeMaximumNumberField();
        setNoOfStoolsNumberField();
        setNoOfMixerBoardInputsNumberField();
        setDrumkitBrandTextField();
        setDrumkitPiecesNumberField();
        setSpeakerBrandTextField();
        setNoOfAmplifiers1TextField();
        setNoOfAmplifiers2TextField();
        setNoOfMics1TextField();
        setNoOfMics2TextField();
        setXHourPackageNumberField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Capacity Range Maximum Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setCapacityRangeMaximumNumberField() {
        return setCapacityRangeMaximumNumberField(data.get("CAPACITY_RANGE_MAXIMUM"));
    }

    /**
     * Set value to Capacity Range Maximum Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setCapacityRangeMaximumNumberField(String capacityRangeMaximumValue) {
        capacityRangeMaximum.sendKeys(capacityRangeMaximumValue);
        return this;
    }

    /**
     * Set default value to Capacity Range Minimum Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setCapacityRangeMinimumNumberField() {
        return setCapacityRangeMinimumNumberField(data.get("CAPACITY_RANGE_MINIMUM"));
    }

    /**
     * Set value to Capacity Range Minimum Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setCapacityRangeMinimumNumberField(String capacityRangeMinimumValue) {
        capacityRangeMinimum.sendKeys(capacityRangeMinimumValue);
        return this;
    }

    /**
     * Set default value to Drumkit Brand Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setDrumkitBrandTextField() {
        return setDrumkitBrandTextField(data.get("DRUMKIT_BRAND"));
    }

    /**
     * Set value to Drumkit Brand Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setDrumkitBrandTextField(String drumkitBrandValue) {
        drumkitBrand.sendKeys(drumkitBrandValue);
        return this;
    }

    /**
     * Set default value to Drumkit Pieces Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setDrumkitPiecesNumberField() {
        return setDrumkitPiecesNumberField(data.get("DRUMKIT_PIECES"));
    }

    /**
     * Set value to Drumkit Pieces Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setDrumkitPiecesNumberField(String drumkitPiecesValue) {
        drumkitPieces.sendKeys(drumkitPiecesValue);
        return this;
    }

    /**
     * Set default value to No Of Amplifiers Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfAmplifiers1TextField() {
        return setNoOfAmplifiers1TextField(data.get("NO_OF_AMPLIFIERS_1"));
    }

    /**
     * Set value to No Of Amplifiers Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfAmplifiers1TextField(String noOfAmplifiers1Value) {
        noOfAmplifiers1.sendKeys(noOfAmplifiers1Value);
        return this;
    }

    /**
     * Set default value to No Of Amplifiers Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfAmplifiers2TextField() {
        return setNoOfAmplifiers2TextField(data.get("NO_OF_AMPLIFIERS_2"));
    }

    /**
     * Set value to No Of Amplifiers Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfAmplifiers2TextField(String noOfAmplifiers2Value) {
        noOfAmplifiers2.sendKeys(noOfAmplifiers2Value);
        return this;
    }

    /**
     * Set default value to No Of Mics Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMics1TextField() {
        return setNoOfMics1TextField(data.get("NO_OF_MICS_1"));
    }

    /**
     * Set value to No Of Mics Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMics1TextField(String noOfMics1Value) {
        noOfMics1.sendKeys(noOfMics1Value);
        return this;
    }

    /**
     * Set default value to No Of Mics Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMics2TextField() {
        return setNoOfMics2TextField(data.get("NO_OF_MICS_2"));
    }

    /**
     * Set value to No Of Mics Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMics2TextField(String noOfMics2Value) {
        noOfMics2.sendKeys(noOfMics2Value);
        return this;
    }

    /**
     * Set default value to No Of Mixer Board Inputs Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMixerBoardInputsNumberField() {
        return setNoOfMixerBoardInputsNumberField(data.get("NO_OF_MIXER_BOARD_INPUTS"));
    }

    /**
     * Set value to No Of Mixer Board Inputs Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfMixerBoardInputsNumberField(String noOfMixerBoardInputsValue) {
        noOfMixerBoardInputs.sendKeys(noOfMixerBoardInputsValue);
        return this;
    }

    /**
     * Set default value to No Of Stools Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfStoolsNumberField() {
        return setNoOfStoolsNumberField(data.get("NO_OF_STOOLS"));
    }

    /**
     * Set value to No Of Stools Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setNoOfStoolsNumberField(String noOfStoolsValue) {
        noOfStools.sendKeys(noOfStoolsValue);
        return this;
    }

    /**
     * Set default value to Speaker Brand Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setSpeakerBrandTextField() {
        return setSpeakerBrandTextField(data.get("SPEAKER_BRAND"));
    }

    /**
     * Set value to Speaker Brand Text field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setSpeakerBrandTextField(String speakerBrandValue) {
        speakerBrand.sendKeys(speakerBrandValue);
        return this;
    }

    /**
     * Set Upload File field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setUpload1FileField() {
        return this;
    }

    /**
     * Set Upload File field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setUpload2FileField() {
        return this;
    }

    /**
     * Set Upload File field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setUpload3FileField() {
        return this;
    }

    /**
     * Set Upload File field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setUpload4FileField() {
        return this;
    }

    /**
     * Set default value to X Hour Package Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setXHourPackageNumberField() {
        return setXHourPackageNumberField(data.get("X_HOUR_PACKAGE"));
    }

    /**
     * Set value to X Hour Package Number field.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage setXHourPackageNumberField(String xHourPackageValue) {
        xHourPackage.sendKeys(xHourPackageValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage submit() {
        clickSubmitButton();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the RoomPage class instance.
     */
    public RoomPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
    
}
    


