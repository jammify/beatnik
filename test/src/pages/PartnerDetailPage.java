package pages;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import jammateweb.BasePage;

public class PartnerDetailPage extends BasePage {
    private Map<String, String> data;
    private int timeout = 15;
    private static WebElement element = null;
    
    
//    By username = By.id("identifierId");
//    By password = By.name("password");
//    By login_button = By.cssSelector("content.CwaK9");

   

	@FindBy(id = "accountNo")
    private WebElement accountNumber;

    @FindBy(id = "accountHolderName")
    private WebElement accountholderName;

    @FindBy(id = "addCancellationSlab")
    private WebElement addSlab;

    @FindBy(id = "area")
    private WebElement area;

    @FindBy(id = "branchName")
    private WebElement branchName1;

    @FindBy(id = "branchName")
    private WebElement branchName2;

    @FindBy(id = "contact")
    private WebElement contactNo;

    @FindBy(id = "deleteButton")
    private WebElement delete;

    @FindBy(id = "email")
    private WebElement emailAddress;

    @FindBy(id = "slabEnd")
    private WebElement end;

    @FindBy(id = "fbPage")
    private WebElement facebookPage;

    @FindBy(css = "a[title='Click to see this area on Google Maps']")
    private WebElement google4hdpi;

    @FindBy(id = "houseNo")
    private WebElement houseNumber;

    @FindBy(id = "ifscCode")
    private WebElement ifscCode;

    @FindBy(id = "landmark")
    private WebElement landmark;

    @FindBy(css = "a[href='#!login']")
    private WebElement logout;

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required.ng-valid-email ng-map div div div.gm-style div:nth-of-type(4) div.gm-style-cc div:nth-of-type(2) a")
    private WebElement mapData;

    @FindBy(css = "a[href='#!setup/']")
    private WebElement orgSetup;

    @FindBy(id = "orgName")
    private WebElement organizationName;

    @FindBy(id = "ownerName")
    private WebElement ownerName;

    private final String pageLoadedText = "Bank Name (Enter bank and branch name OR IFSC code)";

    private final String pageUrl = "/partner/#!/createhost/53";

    @FindBy(id = "percent")
    private WebElement percent;

    @FindBy(id = "pinCode")
    private WebElement pincode;

    @FindBy(css = "a[href='#!settlements/']")
    private WebElement settlements;

    @FindBy(id = "slabStart")
    private WebElement start;

    @FindBy(id = "street")
    private WebElement street;
    
    @FindBy(id = "orgType")
    private WebElement orgType;

    @FindBy(css = "div div:nth-of-type(2) div:nth-of-type(1) div.innr.ng-scope div.layout-margin.layout-padding.layout-column form.ng-pristine.ng-invalid.ng-invalid-required.ng-valid-email button.md-raised.md-primary.md-button.md-ink-ripple.md-button")
    private WebElement submit;

    @FindBy(css = "a[href='https://www.google.com/intl/en-US_US/help/terms_maps.html']")
    private WebElement termsOfUse;

    @FindBy(css = "a[href='#!transactions/']")
    private WebElement transactions;

    @FindBy(id = "website")
    private WebElement website;
	private WebElement branchName;

//    public LoginPage(String uid, String pass) {
//    	username.
//    	
//    }

    public PartnerDetailPage(WebDriver driver) {
    	
        super(driver);
    }
    
    public static WebElement txtBoxOwnerName(WebDriver driver){
    	element = driver.findElement(By.id("ownerName"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxOrgName(WebDriver driver) {
    	element = driver.findElement(By.id("orgName"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxEmail(WebDriver driver) {
    	element = driver.findElement(By.id("email"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    
    public static WebElement txtBoxContact(WebDriver driver) {
    	element = driver.findElement(By.id("contact"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxFbPage(WebDriver driver) {
    	element = driver.findElement(By.id("fbPage"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxWebsite(WebDriver driver) {
    	element = driver.findElement(By.id("website"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxArea(WebDriver driver) {
    	element = driver.findElement(By.id("area"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement scrollElementIntoMiddle(String cssSelector) {
    	element = wait.until(
        	    ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    	element = driver.findElement(By.cssSelector(cssSelector));
    	String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
    	((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
        SleepPlease();
        return element;
    }
    
    
    private static WebElement scrollElementToTop(String cssSelector) {
    	element = wait.until(
        	    ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    	element = driver.findElement(By.cssSelector(cssSelector));
    	String scrollElementToTop = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-viewPortHeight);";
    	((JavascriptExecutor) driver).executeScript(scrollElementToTop, element);
        SleepPlease();
        return element;
    }
    
    protected static WebElement scrollElementToBottom(String cssSelector) {
    	element = wait.until(
        	    ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    	element = driver.findElement(By.cssSelector(cssSelector));
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        SleepPlease();
        return element;
    }
    
    protected static WebElement scrollElementToBottomXpath(String xPath) {
    	element = wait.until(
        	    ExpectedConditions.visibilityOfElementLocated(By.cssSelector(xPath)));
    	element = driver.findElement(By.xpath(xPath));
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        SleepPlease();
        return element;
    }
    
    
    
    public static  WebElement drpDownOrgType(WebDriver driver) {
    	
    	element = scrollElementIntoMiddle("#orgType");
        element.click();
        element = scrollElementIntoMiddle("#select_option_88");
        element.click();
        
    	return element;
    	
//    	Logger.info;
    }
    
    private static void SleepPlease() {
    	 try {
 			Thread.sleep(500);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		} 
    }
    
    public static WebElement txtBoxHouseNo(WebDriver driver) {
    	element = scrollElementIntoMiddle("#houseNo");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxStreet(WebDriver driver) {
    	element = driver.findElement(By.id("street"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxLandmark(WebDriver driver) {
    	element = driver.findElement(By.id("landmark"));
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxPincode(WebDriver driver) {
    	element = scrollElementIntoMiddle("#pinCode");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement txtBoxAccNo(WebDriver driver) {
    	element = scrollElementIntoMiddle("#accountNo");
    	element.click();
    	element.clear();
    	return element;
    }
    
    public static WebElement drpDownState(WebDriver driver) {
    	element = scrollElementIntoMiddle("#state");
    	element.click();
    	element = scrollElementIntoMiddle("#select_option_110");
    	element.click();
    	return element;
    }
    
    
    public static WebElement clickMap(WebDriver driver) {
    	element = scrollElementIntoMiddle("body > div > div.outr > div.ng-scope > div > div > form > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3)");
//    	WebElement area = element.findElement(By.id("reports"));

    	JavascriptExecutor executor = (JavascriptExecutor)driver;
    	executor.executeScript("arguments[0].click();", element);
    	
    	return element;
    }

    public static WebElement drpDownCity(WebDriver driver) {
    	element = scrollElementIntoMiddle("#city");
    	element.click();
    	element = scrollElementIntoMiddle("#select_option_377");
    	element.click();
    	return element;
    }
    
//    public static WebElement drpDownBankName(WebDriver driver) {
//    	element = scrollElementIntoMiddle("#bankName");
//    	element.click();
//    	element = scrollElementIntoMiddle("#select_option_140");
//    	element.click();
//    	return element;
//    }
//    
//    public static WebElement drpDownBranch(WebDriver driver) {
//    	element = scrollElementIntoMiddle("#branchName_chosen > a > span");
//    	element.click();
//    	element = scrollElementIntoMiddle("");
//    	return element;
//    }
    
    public static WebElement txtBoxIFSC(WebDriver driver) {
    	element = driver.findElement(By.cssSelector("#ifscCode"));
    	element.click();
    	element.clear();
    	return element;
    	
    	
    }
    
    public static WebElement drpDownAccType(WebDriver driver) {
    	element = scrollElementIntoMiddle("#accountType");
    	element.click();
    	element = scrollElementIntoMiddle("body > div.md-select-menu-container.md-active.md-clickable > md-select-menu > md-content > md-option:nth-child(1)");
    	element.click();
    	return element;
    }
    
    public static WebElement buttonAddRoom(WebDriver driver) {
    	element = scrollElementToTop("body > div > div.outr > div.ng-scope > div > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:nth-child(2) > button");
    	element.click();
    	return element;
    }
    
    public static void buttonSubmit(WebDriver driver) {
    	element = scrollElementIntoMiddle("body > div:nth-child(1) > div.outr > div.ng-scope > div > div > form > button");
    	element.click();
    	
    }
    
   public static WebElement buttonUploadImage1(WebDriver driver) {
	 	WebElement element = driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div/form/section/div[1]/div[1]/div/button"));
       	String modelValue = element.getAttribute("ng-model");
       	element = driver.findElement(By.cssSelector("input[ngf-select][ng-model=\"" + modelValue + "\"]"));
       	element.sendKeys("/Users/Apple/Desktop/VL/MIUdHZNbog.jpg");
       	return element;
   }
   
   public static WebElement buttonUploadImage2(WebDriver driver) {
	 	WebElement element = driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div/form/section/div[1]/div[1]/div/button"));
      	String modelValue = element.getAttribute("ng-model");
      	element = driver.findElement(By.cssSelector("input[ngf-select][ng-model=\"" + modelValue + "\"]"));
      	element.sendKeys("/Users/Apple/Desktop/VL/MIUdHZNboh.jpg");
      	return element;
  }
   public static WebElement buttonUploadImage3(WebDriver driver) {
	 	WebElement element = driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div/form/section/div[1]/div[1]/div/button"));
     	String modelValue = element.getAttribute("ng-model");
     	element = driver.findElement(By.cssSelector("input[ngf-select][ng-model=\"" + modelValue + "\"]"));
     	element.sendKeys("/Users/Apple/Desktop/VL/MIUdHZNboi.jpg");
     	return element;
 }

   public static WebElement buttonUploadImage4(WebDriver driver) {
	 	WebElement element = driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div/form/section/div[1]/div[1]/div/button"));
    	String modelValue = element.getAttribute("ng-model");
    	element = driver.findElement(By.cssSelector("input[ngf-select][ng-model=\"" + modelValue + "\"]"));
    	element.sendKeys("/Users/Apple/Desktop/VL/MIUdHZNboj.jpg");
    	return element;
}

 
  

    public PartnerDetailPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public PartnerDetailPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Add Slab Button.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickAddSlabButton() {
        addSlab.click();
        return this;
    }

    /**
     * Click on Delete Button.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickDeleteButton() {
        delete.click();
        return this;
    }

    /**
     * Click on Google4hdpi Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickGoogle4hdpiLink() {
        google4hdpi.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Map Data Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickMapDataLink() {
        mapData.click();
        return this;
    }

    /**
     * Click on Org Setup Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickOrgSetupLink() {
        orgSetup.click();
        return this;
    }

    /**
     * Click on Settlements Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickSettlementsLink() {
        settlements.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickSubmitButton() {
        submit.click();
        return this;
    }

    /**
     * Click on Terms Of Use Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickTermsOfUseLink() {
        termsOfUse.click();
        return this;
    }

    /**
     * Click on Transactions Link.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage clickTransactionsLink() {
        transactions.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage fill() {
        setOwnerNameTextField();
        setOrganizationNameTextField();
        setEmailAddressEmailField();
        setContactNoTelField();
        setFacebookPageTextField();
        setWebsiteTextField();
        setHouseNumberTextField();
        setStreetTextField();
        setLandmarkTextField();
        setAreaTextField();
        setPincodeNumberField();
        setAccountNumberTextField();
        setAccountholderNameTextField();
        setBranchName1TextField();
        setBranchName2TextField();
        setIfscCodeTextField();
        setStartNumberField();
        setEndNumberField();
        setPercentNumberField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Account Number Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAccountNumberTextField() {
        return setAccountNumberTextField(data.get("ACCOUNT_NUMBER"));
    }

    /**
     * Set value to Account Number Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAccountNumberTextField(String accountNumberValue) {
    	accountNumber.clear();
        accountNumber.sendKeys(accountNumberValue);
        return this;
    }

    /**
     * Set default value to Accountholder Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAccountholderNameTextField() {
        return setAccountholderNameTextField(data.get("ACCOUNTHOLDER_NAME"));
    }

    /**
     * Set value to Accountholder Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAccountholderNameTextField(String accountholderNameValue) {
        accountholderName.sendKeys(accountholderNameValue);
        return this;
    }

    /**
     * Set default value to Area Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAreaTextField() {
        return setAreaTextField(data.get("AREA"));
    }

    /**
     * Set value to Area Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setAreaTextField(String areaValue) {
    	area.clear();
        area.sendKeys(areaValue);
        return this;
    }

    /**
     * Set default value to Branch Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setBranchName1TextField() {
        return setBranchName1TextField(data.get("BRANCH_NAME_1"));
    }

    /**
     * Set value to Branch Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setBranchName1TextField(String branchName1Value) {
        new Select(branchName1).selectByVisibleText(branchName1Value);
        return this;
    }

    /**
     * Set default value to Branch Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setBranchName2TextField() {
        return setBranchName2TextField(data.get("BRANCH_NAME_2"));
    }

    /**
     * Set value to Branch Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setBranchName2TextField(String branchName2Value) {
    	branchName2.clear();
        branchName2.sendKeys(branchName2Value);
        return this;
    }

    /**
     * Set default value to Contact No Tel field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setContactNoTelField() {
        return setContactNoTelField(data.get("CONTACT_NO"));
    }

    /**
     * Set value to Contact No Tel field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setContactNoTelField(String contactNoValue) {
    	contactNo.clear();
        contactNo.sendKeys(contactNoValue);
        return this;
    }

    /**
     * Set default value to Email Address Email field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setEmailAddressEmailField() {
        return setEmailAddressEmailField(data.get("EMAIL_ADDRESS"));
    }

    /**
     * Set value to Email Address Email field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setEmailAddressEmailField(String emailAddressValue) {
    	emailAddress.clear();
        emailAddress.sendKeys(emailAddressValue);
        return this;
    }

    /**
     * Set default value to End Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setEndNumberField() {
        return setEndNumberField(data.get("END"));
    }

    /**
     * Set value to End Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setEndNumberField(String endValue) {
    	end.clear();
        end.sendKeys(endValue);
        return this;
    }

    /**
     * Set default value to Facebook Page Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setFacebookPageTextField() {
        return setFacebookPageTextField(data.get("FACEBOOK_PAGE"));
    }

    /**
     * Set value to Facebook Page Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setFacebookPageTextField(String facebookPageValue) {
    	facebookPage.clear();
        facebookPage.sendKeys(facebookPageValue);
        return this;
    }

    /**
     * Set default value to House Number Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setHouseNumberTextField() {
        return setHouseNumberTextField(data.get("HOUSE_NUMBER"));
    }

    /**
     * Set value to House Number Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setHouseNumberTextField(String houseNumberValue) {
    	houseNumber.clear();
        houseNumber.sendKeys(houseNumberValue);
        return this;
    }

    /**
     * Set default value to Ifsc Code Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setIfscCodeTextField() {
        return setIfscCodeTextField(data.get("IFSC_CODE"));
    }

    /**
     * Set value to Ifsc Code Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setIfscCodeTextField(String ifscCodeValue) {
    	ifscCode.clear();
        ifscCode.sendKeys(ifscCodeValue);
        return this;
    }

    /**
     * Set default value to Landmark Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setLandmarkTextField() {
        return setLandmarkTextField(data.get("LANDMARK"));
    }

    /**
     * Set value to Landmark Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setLandmarkTextField(String landmarkValue) {
    	landmark.clear();
        landmark.sendKeys(landmarkValue);
        return this;
    }

    /**
     * Set default value to Organization Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setOrganizationNameTextField() {
        return setOrganizationNameTextField(data.get("ORGANIZATION_NAME"));
    }

    /**
     * Set value to Organization Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setOrganizationNameTextField(String organizationNameValue) {
    	organizationName.clear();
        organizationName.sendKeys(organizationNameValue);
        return this;
    }

    /**
     * Set default value to Owner Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setOwnerNameTextField() {
        return setOwnerNameTextField(data.get("OWNER_NAME"));
    }

    /**
     * Set value to Owner Name Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setOwnerNameTextField(String ownerNameValue) {
    	ownerName.clear();
        ownerName.sendKeys(ownerNameValue);
        return this;
    }

    /**
     * Set default value to Percent Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setPercentNumberField() {
        return setPercentNumberField(data.get("PERCENT"));
    }

    /**
     * Set value to Percent Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setPercentNumberField(String percentValue) {
    	percent.clear();
        percent.sendKeys(percentValue);
        return this;
    }

    /**
     * Set default value to Pincode Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setPincodeNumberField() {
        return setPincodeNumberField(data.get("PINCODE"));
    }

    /**
     * Set value to Pincode Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setPincodeNumberField(String pincodeValue) {
    	pincode.clear();
        pincode.sendKeys(pincodeValue);
        return this;
    }

    /**
     * Set default value to Start Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setStartNumberField() {
        return setStartNumberField(data.get("START"));
    }

    /**
     * Set value to Start Number field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setStartNumberField(String startValue) {
    	start.clear();
        start.sendKeys(startValue);
        return this;
    }

    /**
     * Set default value to Street Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setStreetTextField() {
        return setStreetTextField(data.get("STREET"));
    }

    /**
     * Set value to Street Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setStreetTextField(String streetValue) {
    	street.clear();
        street.sendKeys(streetValue);
        return this;
    }

    /**
     * Set default value to Website Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setWebsiteTextField() {
        return setWebsiteTextField(data.get("WEBSITE"));
    }

    /**
     * Set value to Website Text field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage setWebsiteTextField(String websiteValue) {
    	website.clear();
        website.sendKeys(websiteValue);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage submit() {
        clickSubmitButton();
        return this;
    }

    /**
     * Unset default value from Branch Name Drop Down List field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage unsetBranchNameDropDownListField() {
        return unsetBranchNameDropDownListField(data.get("BRANCH_NAME"));
    }

    /**
     * Unset value from Branch Name Drop Down List field.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage unsetBranchNameDropDownListField(String branchNameValue) {
        new Select(branchName).deselectByVisibleText(branchNameValue);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the JammatePO class instance.
     */
    public PartnerDetailPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
