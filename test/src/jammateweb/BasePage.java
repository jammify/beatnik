package jammateweb;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	WebDriver wd = null;

    protected static WebDriver driver;
    protected static WebDriverWait wait;
    
    public BasePage() {
    	
    }

    public BasePage(WebDriver wd2) {
		// TODO Auto-generated constructor stub
    	wd = wd2;
	}

	public static WebDriver startdriver(){
//	    System.setProperty("webdriver.chrome.driver", "/Users/Apple/Downloads/chromedriver 3");
	    System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\Downloads\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("https://jammate.in/partner/");
	    wait = new WebDriverWait(driver, 50);
	    return driver;
    }

    protected static void loadPage() {
		  
	  }
    
//     protected void randomEmail() {
//    	 wd = BasePage.driver;
//    	 PartnerDetailPage.txtBox_Email(wd).click();  
//    	 Random randomGenerator = new Random();  
//    	 int randomInt = randomGenerator.nextInt(1000);  
//    	 PartnerDetailPage.txtBox_Email(wd).sendKeys("username"+ randomInt +"@gmail.com");  
//     }
     
     public class GenerateData {

    	 public String generateRandomString(int length){
    	  return RandomStringUtils.randomAlphabetic(length);
    	 }
    	 
    	 public String generateRandomNumber(int length){
    	  return RandomStringUtils.randomNumeric(length);
    	 }
    	 
    	 public String generateRandomAlphaNumeric(int length){
    	  return RandomStringUtils.randomAlphanumeric(length);
    	 }
    	 
    	 public String generateStringWithAllobedSplChars(int length,String allowdSplChrs){
    	  String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
    	    "1234567890" +   //numbers
    	    allowdSplChrs;
    	  return RandomStringUtils.random(length, allowedChars);
    	 }
    	 
    	 public String generateRandomEmail(int length) {
    	  String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
    	    "1234567890"; 
    	  String email="";
    	  String temp=RandomStringUtils.random(length,allowedChars);
    	  email=temp.substring(0,temp.length()-9)+"@gmail.com";
    	  return email;
    	 }
    	 
    	 
    	 public String generateRandomFacebook(int length) {
       	  String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
       	    "1234567890" +   //numbers
       	    "_-.";   //special characters
       	  String fbName="";
       	  String temp=RandomStringUtils.random(length,allowedChars);
       	  fbName=temp.substring(0,temp.length()-9)+"@facebook.com";
       	  return fbName;
       	 }
    	 
    	 public String generateRandomWebsite(int length) {
          	  String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
          	    "1234567890" +   //numbers
          	    "_-.";   //special characters
          	  String website="";
          	  String temp=RandomStringUtils.random(length,allowedChars);
          	  website=temp.substring(0,temp.length()-9)+".com";
          	  return website;
          	 }
    	 
    	 public String generateUrl(int length) {
    	  String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
    	    "1234567890" +   //numbers
    	    "_-.";   //special characters
    	  String url="";
    	  String temp=RandomStringUtils.random(length,allowedChars);
    	  url=temp.substring(0,3)+"."+temp.substring(4,temp.length()-4)+"."+temp.substring(temp.length()-3);
    	  return url;
    	 }
    	}
    
	  

	  protected  static void tearDownAfterTestMethod() {
	    driver.quit();
	  }

//	  protected tearDownAfterTestClass() {
//	    // close connections, close browser as needed
//	  }

}