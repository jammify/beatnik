package jammateweb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.PartnerDetailPage;
import pages.RoomPage;

public class PartnerRegistration extends BasePage{
	
	public PartnerRegistration() {
		
	}
        
		public PartnerRegistration(WebDriver wd2) {
		super(wd2);
		// TODO Auto-generated constructor stub
	}

		WebDriver wd = null;
		GenerateData genData;
		
        
		@BeforeMethod
        public void setUp() throws Exception {
	    	
	    	wd = BasePage.startdriver();
	        wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        genData = new GenerateData();
		}
    
//    @Test
//     public void checkMapRefresh() {
//    	WebDriverWait wait = new WebDriverWait(wd, 40);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div > div.outr > div.ng-scope > div > div > form > ng-map > div > div > div > div.gmnoscreen")));
//    }
//    
////    @Test
////     public void executePreInput()  {
////    	
////    	
////    }
		
    @Test
    public void Registration() {
        PartnerWebLoginTest login = new PartnerWebLoginTest(wd);
        login.loginTestNG();
//        wd.manage().window().maximize();
//        wd.getTitle();
//       this.getOwnerName().click();
       	PartnerDetailPage.txtBoxOwnerName(wd).sendKeys(genData.generateRandomString(10));
       	PartnerDetailPage.txtBoxOrgName(wd).sendKeys(genData.generateRandomString(10));
       	PartnerDetailPage.txtBoxEmail(wd).sendKeys(genData.generateRandomEmail(30));
       	PartnerDetailPage.txtBoxContact(wd).sendKeys(genData.generateRandomNumber(10));
       	PartnerDetailPage.txtBoxFbPage(wd).sendKeys(genData.generateRandomFacebook(15));
       	PartnerDetailPage.txtBoxWebsite(wd).sendKeys(genData.generateRandomWebsite(20));
       	PartnerDetailPage.drpDownOrgType(wd);
       	PartnerDetailPage.txtBoxHouseNo(wd).sendKeys(genData.generateRandomAlphaNumeric(30));
       	PartnerDetailPage.txtBoxStreet(wd).sendKeys(genData.generateRandomAlphaNumeric(20));
       	PartnerDetailPage.txtBoxLandmark(wd).sendKeys(genData.generateRandomAlphaNumeric(60));
       	PartnerDetailPage.txtBoxArea(wd).sendKeys(genData.generateRandomAlphaNumeric(30));
       	PartnerDetailPage.drpDownState(wd);
       	PartnerDetailPage.drpDownCity(wd);
       	PartnerDetailPage.txtBoxPincode(wd).sendKeys(genData.generateRandomNumber(6));
       	PartnerDetailPage.txtBoxAccNo(wd).sendKeys(genData.generateRandomNumber(16));
       	PartnerDetailPage.txtBoxIFSC(wd).sendKeys("ALLA0210853");
    	PartnerDetailPage.clickMap(wd);
       	PartnerDetailPage.drpDownAccType(wd);
       	PartnerDetailPage.buttonSubmit(wd);
       	
       	//Room details input
    	PartnerDetailPage.buttonAddRoom(wd);
       	PartnerDetailPage.buttonUploadImage1(wd);
       	PartnerDetailPage.buttonUploadImage2(wd);
       	PartnerDetailPage.buttonUploadImage3(wd);
       	PartnerDetailPage.buttonUploadImage4(wd);
    	RoomPage.txtBoxCapacityMin(wd).sendKeys(genData.generateRandomNumber(3));
    	RoomPage.txtBoxCapacityMax(wd).sendKeys(genData.generateRandomNumber(3));
    	RoomPage.txtBoxNoOfStools(wd).sendKeys(genData.generateRandomNumber(3));
    	RoomPage.txtBoxNoOfMixerBoardInputs(wd).sendKeys(genData.generateRandomNumber(3));
    	RoomPage.txtBoxDrumkitBrand(wd).sendKeys(genData.generateRandomString(20));
    	RoomPage.txtBoxDrumkitPieces(wd).sendKeys(genData.generateRandomNumber(2));
    	RoomPage.txtBoxSpeakerBrand(wd).sendKeys(genData.generateRandomString(15));
    	RoomPage.chkBoxSoundProofed(wd);
    	RoomPage.txtBoxNoOfAmplifiers(wd).sendKeys(genData.generateRandomNumber(1));
    	RoomPage.txtBoxOtherAmenities(wd).sendKeys(genData.generateRandomString(10));
    	RoomPage.txtBoxNoOfMics(wd).sendKeys("1");
    	RoomPage.txtBoxAmplifierBrands(wd).sendKeys(genData.generateRandomString(10));
    	RoomPage.chkBoxHasCondenser(wd);
    	RoomPage.chkBoxHasJamming(wd);
    	RoomPage.txtBoxXHourPackage(wd).sendKeys("3");
//    	RoomPage.firstSlab(wd).sendKeys("400");
    	
    	RoomPage.txtBoxJammingHourPackageWeekDay(wd).sendKeys("1000");
    	RoomPage.txtBoxJammingHourPackageWeekEnd(wd).sendKeys("1200");
    	RoomPage.txtBoxJammingFulldayPackageWeekDay(wd).sendKeys("4000");
    	RoomPage.txtBoxJammingFulldayPackageWeekEnd(wd).sendKeys("5000");
    	RoomPage.buttonSubmit(wd);
    }
    
    @AfterMethod
    public void tearDown() {
        wd.quit();
    }
    
//    public static boolean isAlertPresent(WebDriver wd) {
//        try {
//            wd.switchTo().alert();
//            return true;
//        } catch (NoAlertPresentException e) {
//            return false;
//        }
  //  }
}
