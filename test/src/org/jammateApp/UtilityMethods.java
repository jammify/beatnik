package org.jammateApp;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public  class UtilityMethods{
	
	public UtilityMethods(MobileDriver driver) {
		// TODO Auto-generated constructor stub
	}




	public static AppiumDriver<MobileElement> testApp() throws MalformedURLException, InterruptedException {
	    String apkpath = "D:\\Jammate_in.jammate.consumer.apk";
	    File app = new File (apkpath);
	    DesiredCapabilities capabilities = new DesiredCapabilities();
	    capabilities.setCapability("platformName", "Android");
	    capabilities.setCapability("deviceName", "ZY223HJNFW");
	    capabilities.setCapability("platformVersion", "7.0");
	    capabilities.setCapability("app", "D:\\Jammate_in.jammate.consumer.apk");
//	    capabilities.setCapability("browserName", "");
	    capabilities.setCapability("deviceOrientation", "portrait");
	    capabilities.setCapability("appiumVersion", "1.9.1");
	    capabilities.setCapability("no-reset", true);
	    capabilities.setCapability("full-reset", false);
	    capabilities.setCapability("autoGrantPermissions", true);
	    AppiumDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    return driver;
	}

	
	public boolean swipeFromUpToBottom() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		AppiumDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
     try {
          JavascriptExecutor js = (JavascriptExecutor) driver;
          HashMap<String, String> scrollObject = new HashMap<String, String>();
          scrollObject.put("direction", "up");
          js.executeScript("mobile: scroll", scrollObject);
        }
           catch (Exception e) {
            }   
        return false;               
    }

	
	public boolean swipeBottomFromTop() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		AppiumDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
     try {
          JavascriptExecutor js = (JavascriptExecutor) driver;
          HashMap<String, String> scrollObject = new HashMap<String, String>();
          scrollObject.put("direction", "down");
          js.executeScript("mobile: scroll", scrollObject);
        }
           catch (Exception e) {
            }   
        return false;               
    }

	
	
	
	
	
}
