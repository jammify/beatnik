package org.jammateApp;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.host.URL;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class JustTryng {
	
	
	@Test
	public void iCanSeeTheLoginErrorMessage() {

	    // Define capabilities
	    DesiredCapabilities capabilities = new DesiredCapabilities();
	    capabilities.setCapability("platformName", "Android");
	    capabilities.setCapability("deviceName", "ZY223HJNFW");
	    capabilities.setCapability("platformVersion", "7.0");
	    capabilities.setCapability("app", "D:\\Jammate_in.jammate.consumer.apk");
//	    capabilities.setCapability("browserName", "");
	    capabilities.setCapability("deviceOrientation", "portrait");
	    capabilities.setCapability("appiumVersion", "1.9.1");
	    capabilities.setCapability("no-reset", true);
	    capabilities.setCapability("full-reset", false);
	    capabilities.setCapability("autoGrantPermissions", true);
	    // Define Selenium Grid URL
	    URL seleniumGridUrl = null;
	    seleniumGridUrl = new URL("http://127.0.0.1:4723/wd/hub", seleniumGridUrl);

	    // Start up an AppiumDriver session
	    AppiumDriver ad = new AndroidDriver((Capabilities) seleniumGridUrl);
	    ad.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	    // Perform the test steps
	    ad.findElementById("org.wordpress.android:id/nux_username").sendKeys("myusername@email.com");
	    ad.findElementById("org.wordpress.android:id/nux_password").sendKeys("password123");
	    ad.findElementById("org.wordpress.android:id/nux_sign_in_button").click();
//	    Assert.assertEquals(
//	        ad.findElementById("org.wordpress.android:id/nux_dialog_title")).getText(),
//	        "We can't log you in");

	    // Close the WebDriver session
	    ad.quit();
	}

}
