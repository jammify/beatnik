package org.jammateApp;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
 
public class BookAStudio  extends UtilityMethods{
	public BookAStudio(MobileDriver driver1) {
		super(driver1);
	}

	private static  MobileElement element;
	static MobileDriver driver = null;	
	
	
	@BeforeMethod
	public  void setUp() throws Exception {
		
		    	driver = UtilityMethods.testApp();
		        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}
	    
	@Test
	public void bookStudio() throws Exception {
		
		UtilityMethods startDriver = new UtilityMethods(driver);
		startDriver.testApp();
		setUp();
		UtilityMethods swipe = new UtilityMethods(driver);
		element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ImageView"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/check_availability_button"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/radio_button"));
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RadioButton"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.xpath("//android.view.View[@content-desc=\"31 July 2018\"]"));
	    element.click();
	    swipe.swipeFromUpToBottom();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/frame_21"));//Start picking the time
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/book_now_button"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/book_now_button"));// The booking amount is 1000
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/action_button"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("contact"));
	    element.sendKeys("9051888497");
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[2]"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View[3]/android.view.View[2]/android.view.View\n" + 
	    		""));
	    element.click();
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.view.View[2]/android.view.View[2]"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[8]/android.widget.Button[1]"));
	    element.click();
	    element = (MobileElement) driver.findElement(By.id("in.jammate.consumer:id/done_button"));
	    element.click();

	}

	@AfterMethod
	public  void tearDown() throws Exception {
	  driver.removeApp("in.jammate.consumer");
	  driver.quit();
	}

 
  
}
