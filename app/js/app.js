'use strict';

angular.module('app', ['ngRoute', 'ngMaterial', 'app.controllers']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
    $routeProvider.when('/home', {templateUrl: 'views/home.html'});
    $routeProvider.when('/partners', {templateUrl: 'views/partners.html'});
    $routeProvider.when('/aboutus', {templateUrl: 'views/aboutus.html'});
    $routeProvider.when('/partnertos', {templateUrl: 'views/partnertos.html'});
    $routeProvider.when('/consumerterms', {templateUrl: 'views/usertos.html'});
    $routeProvider.when('/privacypolicy', {templateUrl: 'views/privacypolicy.html'});
    $routeProvider.when('/refundpolicy', {templateUrl: 'views/refundandcancellation.html'});
    $routeProvider.when('/pricing', {templateUrl: 'views/pricingpolicy.html'});
    $routeProvider.when('/contactus', {templateUrl: 'views/contactus.html'});
	$routeProvider.otherwise({redirectTo: '/home'});
	
  }]);





