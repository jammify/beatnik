'use strict';
angular.module('gis', ['ngRoute', 'restangular', 'ngMaterial', 'gis.controllers', 'gis.services']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
//	  $locationProvider.html5Mode(true);
    $routeProvider.when('/home', {templateUrl: 'views/home.html'});
	$routeProvider.otherwise({redirectTo: '/home'});
	
  }]).
  config(['RestangularProvider', '$httpProvider', function(RestangularProvider, $httpProvider) {
      RestangularProvider.setBaseUrl('http://localhost:8080/beatnik-core/'); 
	  RestangularProvider.setDefaultHeaders({ 'Accept': 'text/html,application/json;q=0.9,*/*;q=0.8'});
	  delete $httpProvider.defaults.headers.common['X-Requested-With'];
      RestangularProvider.setRestangularFields({
        id: '_id.$oid'
      });
      
      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      });
  }]);