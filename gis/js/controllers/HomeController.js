angular.module('gis.controllers').controller('HomeController', ['$scope', '$rootScope', 'Restangular', '$location', '$mdSidenav', function($scope, $rootScope, Restangular,  $location, $mdSidenav) {
	
	$scope.openSideNavPanel = function() {
        $mdSidenav('left').open();
    };
    $scope.closeSideNavPanel = function() {
        $mdSidenav('left').close();
    };
	
	var mymap = L.map('mapid').setView([12.9665617, 77.6008548], 12);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	    maxZoom: 18
	}).addTo(mymap);
	
	var redMarker = L.VectorMarkers.icon({
	    icon: 'flag',
	    markerColor: 'red'
	  });
	
	var greenMarker = L.VectorMarkers.icon({
	    icon: 'flag',
	    markerColor: 'green'
	  });
	
	var blueMarker = L.VectorMarkers.icon({
	    icon: 'flag',
	    markerColor: 'blue'
	  });
	
	$scope.jamroomMarkers = [];
	$scope.studioMarkers = [];
	$scope.schoolMarkers = [];
	
	Restangular.all('gis').getList()  
    .then(function(locations) {
    	for(var i=0; i<locations.length; i++) {
    		if(locations[i].locationType == 1) {
    			$scope.jamroomMarkers.push(L.marker([locations[i].latitude, locations[i].longitude], {icon: greenMarker})
    		    		.bindPopup(locations[i].name));
    		} else if(locations[i].locationType == 2){
    			$scope.studioMarkers.push(L.marker([locations[i].latitude, locations[i].longitude], {icon: redMarker})
	    		.bindPopup(locations[i].name));
    		} else if(locations[i].locationType == 3){
    			$scope.schoolMarkers.push(L.marker([locations[i].latitude, locations[i].longitude], {icon: blueMarker})
	    		.bindPopup(locations[i].name));
    		}
    	}
    	$scope.jamrooms = L.layerGroup($scope.jamroomMarkers);
    	$scope.studios = L.layerGroup($scope.studioMarkers);
    	$scope.schools = L.layerGroup($scope.schoolMarkers);
    	var jamroomOverlay = { "Jamrooms": 	$scope.jamrooms};
    	var studioOverlay = { "Studios": 	$scope.studios};
    	var schoolOverlay = { "Schools": 	$scope.schools};
    	L.control.layers(null, jamroomOverlay).addTo(mymap);	
    	L.control.layers(null, studioOverlay).addTo(mymap);	
    	L.control.layers(null, schoolOverlay).addTo(mymap);	
    });
}]);