'use strict';

module.exports = function (grunt) {
	 // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Project settings
        beatnik: {
            // configurable paths
            app: require('./bower.json').appPath || 'gis',
            dist: 'dist',
            target: 'beatnik'
        },
        concat: {
            css: {
                src: [
                    'css/*.css'
                ],
                dest: 'dist/beatnik.css'
            },
            js: {
                src: [
                    'js/*.js',
                    'js/controllers/*.js'
                ],
                dest: 'dist/beatnik.js'
            }
        },
        cssmin: {
            css: {
                src: 'dist/beatnik.css',
                dest: 'dist/beatnik.min.css'
            }
        },
        uglify: {
            js: {
                files: {
                    'dist/beatnik.min.js': ['dist/beatnik.js']
                }
            }
        },
        watch: {
          files: ['/*'],
          tasks: ['concat', 'cssmin', 'uglify']
       },
       // Empties folders to start fresh
       clean: {
           dist: {
               files: [{
                   dot: true,
                   src: [
                       '.tmp',
                       '<%= beatnik.dist %>/*',
                       '!<%= beatnik.dist %>/.git*'
                   ]
               }]
           },
           server: '.tmp'
       },
       connect: {
           options: {
               port:  9005,
               hostname: 'localhost',
               livereload: 35729,
               open:'http://<%= connect.options.hostname %>:<%= connect.options.port %>/'
           },
           livereload: {
               options: {
                   base: [
//                       '.tmp',
                       '<%= beatnik.gis %>'
                   ]
               }
           }
       },
       copy: {
    	   		server: {
               expand: true,
               dot: true,
//               cwd: '<%= beatnik.test %>',
//               dest: '.tmp/test',
               src: '**/**'
           }
       }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['concat:css', 'cssmin:css', 'concat:js', 'uglify:js']);
    grunt.registerTask('serve', ['clean', 'concat:js', 'connect:livereload', 'watch']);
    grunt.registerTask('prod', ['clean', 'concat:js', 'uglify:js']);
};