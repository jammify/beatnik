CREATE TABLE `j_jamhost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `latitude` decimal(19,6) NOT NULL,
  `longitude` decimal(19,6) NOT NULL,
  `locationType` TINYINT(3) NOT NULL,
  `verified` TINYINT(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);

CREATE TABLE `j_room` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`hostId` bigint(20) NOT NULL,
	`minCapacity` TINYINT(5) NULL DEFAULT NULL,
  	`maxCapacity` TINYINT(5) NULL DEFAULT NULL,
  	`slotCost` INT(10) NULL DEFAULT NULL,
  	`hourlyCost` INT(10) NULL DEFAULT NULL,
  	`soundProofed` TINYINT(2) NULL DEFAULT 0,
  	`stools` TINYINT(2) NULL DEFAULT 0,
  	`noOfStools` TINYINT(3) NULL DEFAULT 0,
  	`mixerBoardInputs` TINYINT(3) NULL DEFAULT 0,
  	`noOfMics` TINYINT(3) NULL DEFAULT 0,
  	`drumkitBrandCvId` INT(11) NULL DEFAULT NULL,
  	`drumkitBrand` VARCHAR(100) NULL DEFAULT NULL,
  	`amplifierBrandCvId` INT(11) NULL DEFAULT NULL,
  	`amplifierBrand` VARCHAR(11) NULL DEFAULT NULL,
  	`speakerBrandCvId` INT(11) NULL DEFAULT NULL,
  	`speakerBrand` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `j_mic` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`roomId` bigint(20) NOT NULL,
	`condenser` TINYINT(2) NULL DEFAULT 0,
	`wireless` TINYINT(2) NULL DEFAULT 0,
	`instrument` TINYINT(2) NULL DEFAULT 0,
	PRIMARY KEY (`id`)
);

INSERT INTO `j_code` (`name`, `isSystemDefined`, `label`, `parentId`)
VALUES
	('drumkit', 1, 'Drumkit', NULL),
	('amplifier', 1, 'Amplifier', NULL),
	('speaker', 1, 'Speaker', NULL);