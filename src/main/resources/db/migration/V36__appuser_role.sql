CREATE TABLE `appuser_role` (
  `appuserId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`appuserId`,`roleId`),
  KEY `FK7662CE59B4100309` (`appuserId`),
  KEY `FK7662CE5915CEC7AB` (`roleId`)
);