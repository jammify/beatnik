alter table j_appuser
add column `uid` VARCHAR(100) NULL DEFAULT NULL,
add column `name` VARCHAR(100) NOT NULL DEFAULT '',
add column `picture` VARCHAR(300) NULL DEFAULT NULL,
add column `signInProvider` VARCHAR(100) NULL DEFAULT NULL,
add column `email` VARCHAR(100) NULL DEFAULT NULL,
add UNIQUE KEY `uid` (`uid`);