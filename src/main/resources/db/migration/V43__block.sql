create table `block` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`roomId` bigint(20) NOT NULL,
`date` date DEFAULT NULL,
`start` varchar(20) NULL DEFAULT NULL,
`end` varchar(20) NULL DEFAULT NULL,
`createdById` bigint(20) DEFAULT NULL,
`createdDate` datetime DEFAULT NULL,
`lastModifiedById` bigint(20) DEFAULT NULL,
`lastModifiedDate` datetime DEFAULT NULL,
PRIMARY KEY (`id`)
);
