alter table j_cancellation_policy modify column `end` int(6) NULL DEFAULT NULL;
alter table j_rescheduling_policy modify column `end` int(6) NULL DEFAULT NULL;