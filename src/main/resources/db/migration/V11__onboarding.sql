alter table j_jamhost
add column addressLine VARCHAR(100) NULL DEFAULT NULL,
add column cityId bigint(20) NULL DEFAULT NULL,
add column pinCode bigint(7) NULL DEFAULT NULL,
add column ownerName VARCHAR(100) NULL DEFAULT NULL,
add column email VARCHAR(100) NULL DEFAULT NULL,
add column contact VARCHAR(20) NULL DEFAULT NULL,
add column website VARCHAR(100) NULL DEFAULT NULL,
add column fbPage VARCHAR(100) NULL DEFAULT NULL,
add column noOfRooms TINYINT(2) NULL DEFAULT 1,
add column openHours TINYINT(3) NULL DEFAULT NULL,
add column openMins TINYINT(3) NULL DEFAULT NULL,
add column closeHours TINYINT(3) NULL DEFAULT NULL,
add column closeMins TINYINT(3) NULL DEFAULT NULL,
add column startTimeMultiples TINYINT(3) NULL DEFAULT NULL,
add column slotDurationMultiples TINYINT(3) NULL DEFAULT NULL,
add column imageId BIGINT(20) NULL DEFAULT NULL,
add column accountHolderName VARCHAR(100) NULL DEFAULT NULL,
add column accountNo VARCHAR(25) NULL DEFAULT NULL,
add column ifscCode VARCHAR(20) NULL DEFAULT NULL,
modify column `latitude` decimal(15,10) NOT NULL,
modify column `longitude` decimal(15,10) NOT NULL;

alter table j_room
drop column `stools`,
drop column `amplifierBrandCvId`,
drop column `amplifierBrand`;

alter table j_room
add column `drumkitPieces` TINYINT(3) NULL DEFAULT NULL,
add column `hasRecording` TINYINT(2) NULL DEFAULT 0,
add column `hasMasterRecording` TINYINT(2) NULL DEFAULT 0,
add column `hourlyRecordingRate` int(10) NULL DEFAULT NULL,
add column `hourlyMasterRecordingRate` int(10) NULL DEFAULT NULL,
add column `noOfAmplifiers` TINYINT(3) NULL DEFAULT 0,
add column `amplifierBrands` VARCHAR(100) NULL DEFAULT 0,
add column `otherItems` VARCHAR(200) NULL DEFAULT 0;

CREATE TABLE `j_cancellation_policy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hostId` bigint(20) NOT NULL,
  `start` int(6) NOT NULL,
  `end` int(6) NOT NULL,
  `refund_percent` TINYINT(3) NULL DEFAULT NULL,
  `refund_flat` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hostIdRef` (`hostId`)
);

CREATE TABLE `j_rescheduling_policy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hostId` bigint(20) NOT NULL,
  `start` int(6) NOT NULL,
  `end` int(6) NOT NULL,
  `penalty_percent` TINYINT(3) NULL DEFAULT NULL,
  `penalty_flat` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hostIdRef2` (`hostId`)
);