CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `message` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `notification_generator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entityType` text,
  `entityId` bigint(20) DEFAULT NULL,
  `action` text,
  `actor` bigint(20) DEFAULT NULL,
  `isSystemGenerated` tinyint(1) DEFAULT '0',
  `content` text,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `notification_mapper` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notificationId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_mapper` (`userId`),
  KEY `notification_id` (`notificationId`)
);