CREATE TABLE `j_configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(150) DEFAULT NULL,
  `valueType` int(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_c_conf_name` (`name`)
);

CREATE TABLE `j_document` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parentEntityType` varchar(50) NOT NULL,
  `parentEntityId` int(20) NOT NULL DEFAULT '0',
  `subEntityType` varchar(50) DEFAULT NULL,
  `subEntityId` int(20) DEFAULT NULL,
  `documentStageIdentifier` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `fileName` varchar(250) NOT NULL,
  `size` int(20) DEFAULT '0',
  `type` varchar(500) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `location` varchar(500) NOT NULL DEFAULT '0',
  `storageTypeEnum` smallint(5) DEFAULT NULL,
  `documentTagCvIid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_parent_entity_type` (`parentEntityType`),
  KEY `idx_parent_entity_id` (`parentEntityId`)
);