INSERT INTO `configuration` (`name`, `value`, `valueType`, `enabled`)
VALUES
	('razorpay-gateway-fee', '2', 0, 1),
	('razorpay-transfer-fee', '0.25', 0, 1),
	('default-commission', '0', 0, 1);
