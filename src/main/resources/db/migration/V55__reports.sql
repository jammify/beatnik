CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `category` VARCHAR(100) DEFAULT NULL,
  `reportSQL` TEXT NULL DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `type` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`)
  );
  
  
    CREATE TABLE `parameter_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  `label` VARCHAR(150) DEFAULT NULL,
  `parameterSQL` VARCHAR(1000),
  PRIMARY KEY (`id`)
  );
  
  
  
    CREATE TABLE `parameter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportId` bigint(20) NOT NULL,
  `parameterId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
  );
  
  
  
insert into report(`name`, category, reportSQL, `type`) VALUES
('TotalHoursByOpenHours', 'Studio Order', "select *, (TIME_TO_SEC(innr.booked)/TIME_TO_SEC(innr.hoursInWeek))*100 as percentOfWeek from
(select  h.name as studio, -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo,
e.enum_value as service, SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom))) AS booked,
SEC_TO_TIME(TIMESTAMPDIFF(SECOND, STR_TO_DATE(CONCAT(CURDATE(), ' ', h.openHours,':', h.openMins, ':00'), '%Y-%m-%d %H:%i:%s'), 
STR_TO_DATE(CONCAT(CURDATE(), ' ', h.`closeHours`,':', h.`closeMins`, ':00'), '%Y-%m-%d %H:%i:%s')) * 7) as hoursInWeek
from `order` o inner join room r on r.id = o.roomId 
inner join jamhost h on h.id = r.hostId
inner join enum e on e.enum_id = o.serviceType and e.enum_name='Service Type'
where h.id = ${studioId} and o.`timeTo` < CURDATE() and o.timeFrom > CURDATE() - INTERVAL  28 DAY
and o.`orderStatus` not in (100, 500, 600)
group by o.`serviceType`, h.`name`, weekNo order by h.name, weekNo, o.serviceType) as innr", 'table'),
('TotalBookingsWithPercentage', 'Studio Order', "select a.studio, a.weekNo, a.service, a.noOfOrders, b.totalOrders, (a.noOfOrders/b.totalOrders)*100 as percentOrders from 
(select h.id as sid, h.name as studio, -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo, 
e.enum_value as service, count(o.id) as noOfOrders
from `order` o inner join room r on r.id = o.roomId 
inner join jamhost h on h.id = r.hostId
inner join enum e on e.enum_id = o.serviceType and e.enum_name='Service Type'
where o.`timeFrom` < CURDATE() and o.timeFrom > CURDATE() - INTERVAL  28 DAY
and h.id = ${studioId}
and o.`orderStatus` not in (100, 500, 600)
group by o.`serviceType`, h.`name`, weekNo  order by h.name, weekNo, o.serviceType
) a inner join
(select  h.id as sid, h.name as studio, -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo,
count(o.id) as totalOrders
from `order` o inner join room r on r.id = o.roomId 
inner join jamhost h on h.id = r.hostId
inner join enum e on e.enum_id = o.serviceType and e.enum_name='Service Type'
where o.`timeTo` < CURDATE() and o.timeFrom > CURDATE() - INTERVAL  28 DAY
and h.id = ${studioId}
and o.`orderStatus` not in (100, 500, 600)
group by h.`name`, weekNo order by h.name, weekNo
) b on a.weekNo=b.weekNo and a.sid=b.sid", 'table'),
('RatingsAndFeedback', 'Studio Order', "select ra.message as feedback,
ra.rating as rating, ra.createdDate from `order` o
inner join room r on r.id=o.roomid inner join jamhost h on h.id=r.hostId
inner join rating ra on ra.orderId=o.id and h.id = ${studioId}
order by ra.createdDate desc limit 10", 'table'),
('CurrentWeekBookings', 'Studio Order', "select h.name, DAYNAME(o.timeFrom), count(o.id) as noOfBookings, 
CONCAT(HOUR(SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom)))), '.', IF(MINUTE(SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom)))) = 0, 0, 5)) as bookedHrs from `order` o inner join room r on o.roomId = r.id
inner join jamhost h on h.id=r.hostId
where o.timeFrom > CURDATE() - INTERVAL  7 DAY and o.timeTo < CURDATE();
and h.id = ${studioId}
group by DAY(o.timeTo)", 'table'),
('BookingRevenue', 'Studio Order', "select frst.weekNo, frst.service, frst.totalRevenue, (frst.totalRevenue/scnd.totalRevenue)*100 as percentRevenue from 
(select -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo, e.`enum_value` as service, sum(o.`partnerReceivedAmount`) as totalRevenue
from 
`order` o inner join room r on o.roomId = r.id
inner join jamhost h on h.id = r.hostId
inner join `enum` e on o.`serviceType`=e.enum_id and e.`enum_name`='Service Type' 
where h.id = ${studioId} and o.`timeFrom` > CURDATE() - INTERVAL 28 DAY  and  o.timeFrom < CURDATE()
group by o.`serviceType`, WEEK(o.timeFrom)
order by WEEK(o.timeFrom)) frst inner join 
(
select -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo, sum(o.`partnerReceivedAmount`) as totalRevenue
from 
`order` o inner join room r on o.roomId = r.id
inner join jamhost h on h.id = r.hostId 
where h.id = ${studioId} and o.`timeFrom` > CURDATE() - INTERVAL 28 DAY  and  o.timeFrom < CURDATE()
group by WEEK(o.timeFrom)
order by WEEK(o.timeFrom)
) scnd on frst.weekNo = scnd.weekNo", 'table');


insert into parameter_master(`name`, label, parameterSQL) values
('studioId', 'Studio', 'select id from jamhost');

insert into parameter(reportId, parameterId) values
((select id from report where name='TotalHoursByOpenHours'), (select id from parameter_master where name='studioId')),
((select id from report where name='TotalBookingsWithPercentage'), (select id from parameter_master where name='studioId')),
((select id from report where name='RatingsAndFeedback'), (select id from parameter_master where name='studioId')),
((select id from report where name='CurrentWeekBookings'), (select id from parameter_master where name='studioId')),
((select id from report where name='BookingRevenue'), (select id from parameter_master where name='studioId'));