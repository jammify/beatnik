CREATE TABLE `j_appuser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firebaseUserId` varchar(100) NOT NULL,
  `vpa` varchar(100) NULL DEFAULT NULL,
  `mobileNo` varchar(30) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `firebaseUserId` (`firebaseUserId`)
);

CREATE TABLE `j_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isSystemDefined` tinyint(1) NOT NULL DEFAULT '1',
  `label` varchar(100) NULL DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);

CREATE TABLE `j_code_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codeId` int(11) NOT NULL,
  `codeValue` varchar(150) DEFAULT NULL,
  `orderPosition` int(11) NOT NULL DEFAULT '0',
  `label` varchar(150) NOT NULL,
  `isSystemDefined` tinyint(1) NOT NULL DEFAULT '1',
  `context` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_value` (`codeId`,`codeValue`)
);