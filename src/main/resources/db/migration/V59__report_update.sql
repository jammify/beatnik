update report set reportSQL = "select innr.studio, innr.weekNo*-1 as weekNo, innr.service, CONVERT(innr.booked, char(20)) as booked,
CONVERT(innr.hoursInWeek, char(20)) as hoursInWeek, (TIME_TO_SEC(innr.booked)/TIME_TO_SEC(innr.hoursInWeek))*100 as percentOfWeek from
(select  h.name as studio, -1*(WEEK(CURDATE()) - WEEK(o.timeFrom)) as weekNo,
e.enum_value as service, SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom))) AS booked,
SEC_TO_TIME(TIMESTAMPDIFF(SECOND, STR_TO_DATE(CONCAT(CURDATE(), ' ', h.openHours,':', h.openMins, ':00'), '%Y-%m-%d %H:%i:%s'), 
STR_TO_DATE(CONCAT(CURDATE(), ' ', h.`closeHours`,':', h.`closeMins`, ':00'), '%Y-%m-%d %H:%i:%s')) * 7) as hoursInWeek
from `order` o inner join room r on r.id = o.roomId 
inner join jamhost h on h.id = r.hostId
inner join enum e on e.enum_id = o.serviceType and e.enum_name='Service Type'
where h.id = ${studioId} and o.`timeTo` < CURDATE() and o.timeFrom > CURDATE() - INTERVAL  28 DAY
and o.`orderStatus` not in (100, 500, 600)
group by o.`serviceType`, h.`name`, weekNo order by h.name, weekNo, o.serviceType) as innr"
where name = 'TotalHoursByOpenHours';

update report set reportSQL = "select h.name, DAYNAME(o.timeFrom), count(o.id) as noOfBookings, 
CONCAT(HOUR(SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom)))), '.', IF(MINUTE(SEC_TO_TIME(SUM(TO_SECONDS(o.timeto) - TO_SECONDS(o.timefrom)))) = 0, 0, 5)) as bookedHrs from `order` o inner join room r on o.roomId = r.id
inner join jamhost h on h.id=r.hostId
where o.timeFrom > CURDATE() - INTERVAL  7 DAY and o.timeTo < CURDATE()
and h.id = ${studioId}
group by DAY(o.timeTo);" 
where name = 'CurrentWeekBookings';



