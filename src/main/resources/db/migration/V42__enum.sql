INSERT INTO `enum` (`enum_name`, `enum_id`, `enum_value`)
VALUES
	('GIS', 0, 'Invalid'),
	('GIS', 1, 'Country'),
	('GIS', 2, 'State'),
	('GIS', 3, 'District'),
	('Global', 0, 'Invalid'),
	('Global', 1, 'Studio'),
	('Global', 2, 'Consumer'),
	('Global', 3, 'Band'),
	('Global', 4, 'Event'),
	('Global', 5, 'User'),
	('Global', 6, 'Jobs'),
	('Global', 7, 'SMS'),
	('Global', 8, 'Documents'),
	('Global', 9, 'Notes'),
	('Global', 10, 'Calendar'),
	('Global', 11, 'Meetings'),
	('Global', 12, 'Holidays'),
	('Global', 13, 'Code Value'),
	('Global', 14, 'Code'),
	('Global', 15, 'Room'),
	('File System', 1, 'File System'),
	('File System', 2, 'S3'),
	('Service Type', 100, 'Just Jam'),
	('Service Type', 200, 'Jamming with Recording'),
	('Service Type', 300, 'Recording with Mixing and Mastering'),
	('Service Type', 400, 'Voice Over Recording'),
	('Location Type', 0, 'General'),
	('Location Type', 100, 'Studio'),
	('Location Type', 200, 'Residence'),
	('Quote Status', 100, 'Requested'),
	('Quote Status', 200, 'Responded'),
	('Time Range', 100, 'Morning (Till 12 noon)'),
	('Time Range', 200, 'Afternoon (12 noon - 4 pm)'),
	('Time Range', 300, 'Evening (4 pm - 7 pm)'),
	('Time Range', 400, 'Night (8 pm - Closing Hours)'),
	('Order Status', 100, 'Initiated' ),
	('Order Status', 200, 'Authorized' ),
	('Order Status', 300, 'Captured' ),
	('Order Status', 400, 'Transferred' ),
	('Order Status', 500, 'Cancelled' ),
	('Order Status', 600, 'Refunded' ),
	('Order Status', 700, 'Completed'),
	('Payment Mode', 0, 'Razorpay'),
	('Payment Mode', 1, 'Paytm'),
	('Payment Mode', 2, 'Simpl' );