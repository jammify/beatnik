CREATE TABLE `enum` (
  `enum_name` varchar(100) NOT NULL,
  `enum_id` int(11) NOT NULL,
  `enum_value` varchar(100) NOT NULL,
  PRIMARY KEY (`enum_name`,`enum_id`),
  UNIQUE KEY `enum_value` (`enum_name`,`enum_value`)
);

alter table `order`
add column `paymentType` int(5) NULL DEFAULT 1;

