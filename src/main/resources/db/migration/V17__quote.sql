CREATE TABLE `j_quote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roomId` bigint(20) NOT NULL,
  `noOfBandMembers` tinyint(4) DEFAULT NULL,
  `noOfSongs` tinyint(4) DEFAULT NULL,
  `avgSongLength` tinyint(4) DEFAULT NULL,
  `avgNoOfTracksPerSong` tinyint(4) NULL DEFAULT NULL,
  `message` VARCHAR(400) NULL DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedById` bigint(20) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);