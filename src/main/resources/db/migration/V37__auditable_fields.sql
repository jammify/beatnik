alter table document
add column `createdById` bigint(20) DEFAULT NULL,
add column  `createdDate` datetime DEFAULT NULL,
add column  `lastModifiedById` bigint(20) DEFAULT NULL,
add column  `lastModifiedDate` datetime DEFAULT NULL;

alter table jamhost
add column `createdById` bigint(20) DEFAULT NULL,
add column  `createdDate` datetime DEFAULT NULL,
add column  `lastModifiedById` bigint(20) DEFAULT NULL,
add column  `lastModifiedDate` datetime DEFAULT NULL;

alter table room
add column `createdById` bigint(20) DEFAULT NULL,
add column  `createdDate` datetime DEFAULT NULL,
add column  `lastModifiedById` bigint(20) DEFAULT NULL,
add column  `lastModifiedDate` datetime DEFAULT NULL;