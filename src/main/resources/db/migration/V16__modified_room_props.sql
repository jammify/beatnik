alter table j_room 
add column `name` VARCHAR(100) NULL DEFAULT NULL AFTER `hostId`,
add column `overallAverage` VARCHAR(100) NULL DEFAULT NULL AFTER `name`,
add column `primaryImageId` bigint(20) NOT NULL,
add column `hasJamming` tinyint(2) DEFAULT '0' after `drumkitPieces`,
change `hourlyCost` `avgHourly` int(10) DEFAULT NULL,
change `hourlyRecordingRate` `avgRecordingHourly` int(10) DEFAULT NULL,
change `hourlyMasterRecordingRate` `avgMixingMasteringHourly` int(10) DEFAULT NULL;

alter table j_jamhost
change `addressLine` `houseNo` VARCHAR(100) NULL DEFAULT NULL,
add column `street` VARCHAR(100) NULL DEFAULT NULL AFTER `houseNo`,
add column `landmark` VARCHAR(100) NULL DEFAULT NULL AFTER `street`,
add column `area` VARCHAR(100) NULL DEFAULT NULL AFTER `landmark`;