INSERT INTO `role` (`id`, `name`, `description`)
VALUES
	(1, 'Artist', 'Artist'),
	(2, 'Studio Partner', 'Studio Partner');


INSERT INTO `permission` (`id`, `grouping`, `code`, `entityName`, `actionName`)
VALUES
	(1, 'ADMIN', 'READ_ALL', 'ALL', 'READ');
	
INSERT INTO `role_permission` (`roleId`, `permissionId`)
VALUES
	(1, 1),
	(2, 1);
	
insert into `appuser_role` (appuserId, roleId) 
select id, 2 from appuser;