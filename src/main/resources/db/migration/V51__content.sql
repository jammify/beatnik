create table `content` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`dateRange` VARCHAR(50) NULL DEFAULT NULL,
	`timeRange` VARCHAR(50) NULL DEFAULT NULL,
	`author` VARCHAR(50) NULL DEFAULT NULL,
	`type` INT(6) NOT NULL,
	`articleType` INT(6) NULL DEFAULT NULL,
	`tags` VARCHAR(100) NULL DEFAULT NULL,
	`likes` int(11) NULL DEFAULT NULL,
	`views` int(11) NULL DEFAULT NULL,
	`contact` VARCHAR(20) NULL DEFAULT NULL,
	`content` TEXT NOT NULL,
	PRIMARY KEY (`id`),
	KEY `FKB3D587CE0DD567A` (`type`)
);

create table `page` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`contentId` bigint(20) NOT NULL,
	`content` text NOT NULL,
	PRIMARY KEY (`id`)
);