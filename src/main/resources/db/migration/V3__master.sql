INSERT INTO `j_code` (`id`, `name`, `isSystemDefined`, `label`, `parentId`)
VALUES
	(1, 'skills', 1, 'Skills', NULL),
	(2, 'genres', 1, 'Genres', NULL),
	(3, 'locations', 1, 'Locations', NULL);
	
CREATE TABLE `j_skills` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appuserId` int(11) NOT NULL,
  `skillsCvId` int(11) NOT NULL,
  `selfRating` TINYINT(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skillsCvId` (`skillsCvId`)
) ;

CREATE TABLE `j_genres` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appuserId` int(11) NOT NULL,
  `genresCvId` int(11) NOT NULL,
  `selfRating` TINYINT(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `genresCvId` (`genresCvId`)
);
	

	



