insert into j_code(`name`, `isSystemDefined`, `label`, `parentId`) values
('roomService', 1, 'Services', null),
('timeRange', 1, 'Time Range', null);


insert into j_code_value(`codeId`, `codeValue`, `orderPosition`, `label`, `isSystemDefined`) values
((select id from j_code where name='roomService'), 'justJam', 0, 'Just Jam', 1),
((select id from j_code where name='roomService'), 'jamWithRecording', 0, 'Jamming with recording of the session', 1),
((select id from j_code where name='roomService'), 'mixingAndMastering', 0, 'Recording with Mixing and Mastering)', 1),
((select id from j_code where name='roomService'), 'voiceOver', 0, 'Voice-Over', 1),
((select id from j_code where name='timeRange'), 'morning', 0, 'Morning    (8 am - 12 noon)', 1),
((select id from j_code where name='timeRange'), 'afternoon', 0, 'Afternoon  (12 noon-4 pm)', 1),
((select id from j_code where name='timeRange'), 'evening', 0, 'Evening    (4 pm - 8 pm)', 1),
((select id from j_code where name='timeRange'), 'night', 0, 'Night      (8 pm - 12 midnight)', 1);