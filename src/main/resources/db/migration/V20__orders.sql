CREATE TABLE `order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roomId` bigint(20) NOT NULL,
  `serviceType`int(11) NOT NULL,
  `from` datetime not null,
  `to` datetime not null,
  `status` int(5) NOT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  `refundedAmount` int(11) NULL DEFAULT NULL,
  `razorpayOrderId` varchar(100) NULL DEFAULT NULL,
  `razorpayPaymentId` varchar(100) NULL DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedById` bigint(20) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

alter table jamhost add column `razorpayAccountId` VARCHAR(100) NULL DEFAULT NULL;

alter table `order` add column cancelledByClient tinyint(2) NULL DEFAULT NULL after `razorpayPaymentId`;

insert into external_service values ('razorpaySecret', null);

alter table `order` change `from` `timeFrom` datetime NOT NULL;
alter table `order`  change `to` `timeTo` datetime NOT NULL;
alter table `order`  change `status` `orderStatus` int(5) NOT NULL;