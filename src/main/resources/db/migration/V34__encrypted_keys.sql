CREATE TABLE `encrypted_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `types` varchar(100) NOT NULL,
  `key_type` varchar(100) NOT NULL,
  `key_value` blob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_m_encrypted_keys_types` (`types`,`key_type`)
);