alter table j_jamhost 
modify column `longitude` decimal(17,14) NOT NULL;

alter table j_jamhost 
modify column `latitude` decimal(17,14) NOT NULL;

alter table j_jamhost drop key name;