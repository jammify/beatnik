alter table j_appuser drop index firebaseUserId;
alter table j_appuser modify column `firebaseUserId` TEXT NOT NULL;