delete from email_template_config where templateName='ORDER_CONFIRMATION';
INSERT into email_template_config(`templateName`, `fromName`, `to`, `subject`, `body`) values
('ORDER_CONFIRMATION', 'arnab@jammate.in', '{{address}}', 'Order Confirmed!', '<div>
  

<div>
  

<div style="background-color: #000; height: 180px;">
<div id="image" style="float: left; margin-left: 450px; margin-top: 30px;"><img src="https://s3.ap-south-1.amazonaws.com/jammatestatic/site/jammate-logo.png" width="60" height="70" /></div>
<br /><br />

<div class="texts" style="float: left; margin-top: -80px; margin-left:50px; font-size: 40px; "><span style="color: #FFD54D;  font-family: "Sans Serif"; font-size: 30px; ">Thank You</span></div>
  <head>

<body>
<div class="texts" style="float: left; margin-top: -25px; margin-left: 50px; font-size: 25px; "><span style="color: #ffffff;  font-family: "Sans Serif"; font-size: 30px; ">for booking with us!</span></div>
<div class="jammate" style="float: left;  margin-left: 440px; font-size: 10px; "><span style="color: #ffffff; font-family: "Sacramento"; font-size: 26px; font-weight:200; ">Jammate</span></div>
 
</div>

<div id="texts" style="float: left; margin-top: 20px; margin-left: 120px; font-size: 16px; font-family: Arial; "><span style="color: #fffafe; ">&nbsp;  <span style="color: #000000; ">&lt; {{customerName}}&gt; , You are jamming at {{studioName}}!</span></span></div>
<p>&nbsp;</p>
<hr width="350/" />
<div id="texts" style="float: left; margin-top: 0px; margin-left: 200px; font-size: 16px; font-family: Arial;"><span style="color: #000000;">&nbsp; Your order details: </span></div>
<div id="d2">
<div style="background-color: #DCDCDC; height: 300px; margin-top: 100px; margin-left: 70px; margin-right: 70px; ">&nbsp;
<div id="texts" style="float: left; margin-top: 30px; margin-left: 40px; font-size: 13px; font-family: Candara; "><span style="color: #000000; ">&nbsp; Jamming Location: https://www.google.co.in/maps/@{{lat}}, {{lng}}, 17z</span></div>
<div id="texts" style="float: left; margin-top: 100px; margin-left: -1px; font-size: 14px; font-family: Candara;"><span style="color: #000000; ">&nbsp; Begins at: {{startDateTime}}</span></div>
<div id="texts" style="float: left; margin-top: 120px; margin-left: -170px; font-size: 14px; font-family: Candara;"><span style="color: #000000; ">&nbsp; Ends at: {{endDateTime}}</span></div>
<div id="texts" style="float: left; margin-top: 140px; margin-left: -170px; font-size: 14px; font-family: Candara;"><span style="color: #000000; ">&nbsp; Total Amount: {{orderAmount}}</span></div>
</div>
</div>
  </div>');