alter table room 
add column `totalReviews` int(11) NULL DEFAULT NULL AFTER avgVoiceoverHourly,
add column `avgRating` double(4,1) AFTER avgVoiceoverHourly;