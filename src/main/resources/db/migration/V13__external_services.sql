CREATE TABLE `j_external_service` (
  `name` varchar(150) NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
);

INSERT INTO `j_external_service` (`name`, `value`)
VALUES
	('s3_access_key', NULL),
	('s3_bucket_name', NULL),
	('s3_secret_key', NULL),
	('firebase_key', NULL);
	
INSERT INTO `j_configuration` (`name`, `value`, `valueType`, `enabled`, `description`)
VALUES
	('amazon-S3', NULL, 0, 0, NULL);