alter table `order` 
add column `receivedAmount` decimal(19,6) NULL DEFAULT NULL,
add column `partnerReceivedAmount` int(11) NULL DEFAULT NULL,
add column `transferredAmount` decimal(19,6) NULL DEFAULT NULL,
add column `margin` decimal(19,6) NULL DEFAULT NULL;