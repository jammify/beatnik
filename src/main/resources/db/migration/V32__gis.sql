CREATE TABLE `gis` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `latitude` decimal(17,14) NOT NULL,
  `longitude` decimal(17,14) NOT NULL,
  `locationType` integer(4) NOT NULL,
  `live` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
);