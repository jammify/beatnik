alter table rating
add column `createdById` bigint(20) DEFAULT NULL,
add column  `createdDate` datetime DEFAULT NULL,
add column  `lastModifiedById` bigint(20) DEFAULT NULL,
add column  `lastModifiedDate` datetime DEFAULT NULL;

alter table `order`
add column `feedbackFormSeen` TINYINT(3) DEFAULT 0;