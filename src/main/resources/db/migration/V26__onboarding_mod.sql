INSERT INTO `code` (`name`, `isSystemDefined`, `label`, `parentId`)
VALUES
	('orgType', 1, 'Organization Type', NULL),
	('accountType', 1, 'Account Type', NULL);	

INSERT INTO `code_value` (`codeId`, `codeValue`, `orderPosition`, `label`, `isSystemDefined`, `context`)
VALUES
	((select id from `code` where name='orgType'), 'propietorship', 0, 'Propietorship', 1, NULL),
	((select id from `code` where name='orgType'), 'partnership', 0, 'Partnership', 1, NULL),
	((select id from `code` where name='orgType'), 'individual', 0, 'Individual', 1, NULL),
	((select id from `code` where name='orgType'), 'privateLimited', 0, 'Private Limited', 1, NULL),
	((select id from `code` where name='orgType'), 'publicLimited', 0, 'Public Limited', 1, NULL),
	((select id from `code` where name='orgType'), 'llp', 0, 'LLP', 1, NULL),
	((select id from `code` where name='orgType'), 'ngo', 0, 'NGO', 1, NULL),
	((select id from `code` where name='orgType'), 'educationalInstitutes', 0, 'Educational Institutes', 1, NULL),
	((select id from `code` where name='orgType'), 'trust', 0, 'Trust', 1, NULL),
	((select id from `code` where name='orgType'), 'society', 0, 'Society', 1, NULL),
	((select id from `code` where name='orgType'), 'notYetRegistered', 0, 'Not yet registered', 1, NULL),
	((select id from `code` where name='orgType'), 'other', 0, 'Other', 1, NULL),
	((select id from `code` where name='accountType'), 'savings', 0, 'Savings', 1, NULL),
	((select id from `code` where name='accountType'), 'current', 0, 'Current', 1, NULL);
	
alter table jamhost 
add column live TINYINT(3) DEFAULT 0,
add column orgTypeId INT(11) NOT NULL AFTER `ownerName`,
add column accountTypeId INT(11) NOT NULL AFTER `ifscCode`;

alter table room 
add column hasVoiceover TINYINT(3) DEFAULT 0,
add column xHourPackage INT(11) NULL DEFAULT NULL, 
add column avgVoiceoverHourly INT(11) NULL DEFAULT NULL;

CREATE TABLE `room_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roomId` bigint(20) NOT NULL,
  `jammingHourlyWeekdayMorning` int(10) DEFAULT NULL,
  `jammingHourlyWeekdayAfternoon` int(10) DEFAULT NULL,
  `jammingHourlyWeekdayEvening` int(10) DEFAULT NULL,
  `jammingHourlyWeekdayNight` int(10) DEFAULT NULL,
  `jammingHourlyWeekendMorning` int(10) DEFAULT NULL,
  `jammingHourlyWeekendAfternoon` int(10) DEFAULT NULL,
  `jammingHourlyWeekendEvening` int(10) DEFAULT NULL,
  `jammingHourlyWeekendNight` int(10) DEFAULT NULL,
  `jammingFullWeekday` int(10) DEFAULT NULL,
  `jammingFullWeekend` int(10) DEFAULT NULL,
  `jammingXHourWeekday` int(10) DEFAULT NULL,
  `jammingXHourWeekend` int(10) DEFAULT NULL,
  `recordingHourlyWeekdayMorning` int(10) DEFAULT NULL,
  `recordingHourlyWeekdayAfternoon` int(10) DEFAULT NULL,
  `recordingHourlyWeekdayEvening` int(10) DEFAULT NULL,
  `recordingHourlyWeekdayNight` int(10) DEFAULT NULL,
  `recordingHourlyWeekendMorning` int(10) DEFAULT NULL,
  `recordingHourlyWeekendAfternoon` int(10) DEFAULT NULL,
  `recordingHourlyWeekendEvening` int(10) DEFAULT NULL,
  `recordingHourlyWeekendNight` int(10) DEFAULT NULL,
  `recordingFullWeekday` int(10) DEFAULT NULL,
  `recordingFullWeekend` int(10) DEFAULT NULL,
  `recordingXHourWeekday` int(10) DEFAULT NULL,
  `recordingXHourWeekend` int(10) DEFAULT NULL,
  `masteringHourlyWeekdayMorning` int(10) DEFAULT NULL,
  `masteringHourlyWeekdayAfternoon` int(10) DEFAULT NULL,
  `masteringHourlyWeekdayEvening` int(10) DEFAULT NULL,
  `masteringHourlyWeekdayNight` int(10) DEFAULT NULL,
  `masteringHourlyWeekendMorning` int(10) DEFAULT NULL,
  `masteringHourlyWeekendAfternoon` int(10) DEFAULT NULL,
  `masteringHourlyWeekendEvening` int(10) DEFAULT NULL,
  `masteringHourlyWeekendNight` int(10) DEFAULT NULL,
  `masteringFullWeekday` int(10) DEFAULT NULL,
  `masteringFullWeekend` int(10) DEFAULT NULL,
  `masteringXHourWeekday` int(10) DEFAULT NULL,
  `masteringXHourWeekend` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekdayMorning` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekdayAfternoon` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekdayEvening` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekdayNight` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekendMorning` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekendAfternoon` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekendEvening` int(10) DEFAULT NULL,
  `voiceoverHourlyWeekendNight` int(10) DEFAULT NULL,
  `voiceoverFullWeekday` int(10) DEFAULT NULL,
  `voiceoverFullWeekend` int(10) DEFAULT NULL,
  `voiceoverXHourWeekday` int(10) DEFAULT NULL,
  `voiceoverXHourWeekend` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
);	

alter table room modify column `primaryImageId` bigint(20) DEFAULT NULL;