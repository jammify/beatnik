package org.jammate.platform;

import javax.servlet.Filter;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.service.AuditorAwareImpl;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories("org.jammate.platform.*")
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@ComponentScan(basePackages = { "org.jammate.platform.*" })
@EnableAutoConfiguration
@ImportResource({ "classpath*:META-INF/spring/securityContext.xml" })
public class Configuration  {
	
	@Bean
    public AuditorAware<AppUser> auditorAware() {
        return new AuditorAwareImpl();
    }
	
	@Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
        tomcat.setContextPath(getContextPath());
        return tomcat;
    }
	
	private String getContextPath() {
        return "/beatnik-core";
    }
	
	@Bean   
    public DispatcherServlet dispatcherServlet() {
        DispatcherServlet ds = new DispatcherServlet();
        ds.setThrowExceptionIfNoHandlerFound(true);
        return ds;
    }
	
	@Bean
    public Filter springSecurityFilterChain() {
        return new DelegatingFilterProxy();
    }

	

}
