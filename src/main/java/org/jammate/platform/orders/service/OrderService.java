package org.jammate.platform.orders.service;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.RandomStringUtils;
import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.calendar.domain.BlockRepository;
import org.jammate.platform.configuration.domain.ConfigurationDomainService;
import org.jammate.platform.configuration.service.ExternalServicesReadPlatformService;
import org.jammate.platform.core.email.service.MailService;
import org.jammate.platform.core.exception.GeneralPlatformException;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.core.infra.StringUtil;
import org.jammate.platform.jamhosts.data.QuoteData;
import org.jammate.platform.jamhosts.data.QuoteStatus;
import org.jammate.platform.jamhosts.domain.JamHost;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.jamhosts.domain.JamRoomRepository;
import org.jammate.platform.jamhosts.domain.Rating;
import org.jammate.platform.jamhosts.domain.RatingRepository;
import org.jammate.platform.jamhosts.service.JamHostPolicyService;
import org.jammate.platform.jamhosts.service.JamHostsService;
import org.jammate.platform.notifications.service.NotificationSenderService;
import org.jammate.platform.orders.api.OrdersApiConstants;
import org.jammate.platform.orders.data.OrderStatus;
import org.jammate.platform.orders.data.SettlementData;
import org.jammate.platform.orders.domain.Order;
import org.jammate.platform.orders.domain.OrderRepository;
import org.jammate.platform.profile.data.BookingData;
import org.jammate.platform.profile.data.OrderData;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.CollectionUtils;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Refund;
import com.razorpay.Transfer;

@Service
public class OrderService {

	private final static Logger logger = LoggerFactory.getLogger(OrderService.class);
	private final OrderAssembler orderAssembler;
	private final JdbcTemplate jdbcTemplate;
	private final OrderRepository orderRepository;
	private final ExternalServicesReadPlatformService externalReadService;
	private final RatingRepository ratingRepository;
	private final JamHostsService hostsService;
	private final JamHostPolicyService policyService;
	private final ConfigurationDomainService configService;
	private final PlatformSecurityContext context;
	private final JamRoomRepository roomRepository;
	private final NotificationSenderService notificationService;
	private final MailService mailService;
	private final BlockRepository blockRepository;

	@Autowired
	public OrderService(final DataSource dataSource, final OrderAssembler orderAssembler,
			final OrderRepository orderRepository, final ExternalServicesReadPlatformService externalReadService,
			final RatingRepository ratingRepository, final JamHostsService hostsServiceImpl,
			final JamHostPolicyService policyService, final ConfigurationDomainService configService,
			final PlatformSecurityContext context,
			final JamRoomRepository roomRepository, final NotificationSenderService notificationService,
			final MailService mailService, final BlockRepository blockRepository) {
		this.context = context;
		this.jdbcTemplate = new JdbcTemplate();
		this.jdbcTemplate.setDataSource(dataSource);
		this.orderAssembler = orderAssembler;
		this.orderRepository = orderRepository;
		this.externalReadService = externalReadService;
		this.ratingRepository = ratingRepository;
		this.hostsService = hostsServiceImpl;
		this.policyService = policyService;
		this.configService = configService;
		this.roomRepository = roomRepository;
		this.notificationService = notificationService;
		this.mailService = mailService;
		this.blockRepository = blockRepository;
	}

	@Transactional
	public CommandProcessingResult createOrder(final JsonCommand command) {
		final Order order = this.orderAssembler.assembleFrom(command.parsedJson());
		// Call Razorpay order API
		try {
			final String razorpaySecret = this.externalReadService.getRazorpaySecret();
			final String apiKey = this.externalReadService.getRazorpayApiKey();
			RazorpayClient razorpay = new RazorpayClient(apiKey, razorpaySecret);
			final String timeFrom = order.getTimeFrom("ddMMMyyyy HH:mm");
			final String timeTo = order.getTimeTo("ddMMMyyyy HH:mm");

			String dateTo = null;
			if (timeTo != null)
				dateTo = timeTo.split(" ")[0];
			String dateFrom = null;
			if (timeFrom != null)
				dateFrom = timeFrom.split(" ")[0];
			String exactTimeFrom = null;
			String exactTimeTo = null;
			if (timeFrom != null && timeFrom.length() > 10)
				exactTimeFrom = timeFrom.split(" ")[1];
			if (timeTo != null && timeTo.length() > 10)
				exactTimeTo = timeTo.split(" ")[1];
			final Double defaultCommission = this.configService.getDefaultCommission();
			final Double studioCommission = order.getRoom().getHost().getCommission();
			Double actualCommission = studioCommission != null ? studioCommission  : defaultCommission ;
			final Integer price = this.hostsService.retrievePrice(order.getRoom().getId(), order.getServiceType(),
					dateFrom, dateTo, exactTimeFrom, exactTimeTo);
			final Integer priceWithoutCommission = (int) Math.round(price - (price * (actualCommission / 100)));
			order.setPrice(price);
			order.setPartnerReceivedAmount(priceWithoutCommission);
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", price * 100); // amount in paise
			orderRequest.put("currency", "INR");
			orderRequest.put("receipt", RandomStringUtils.randomAlphabetic(6)); // Change to sequence
			orderRequest.put("payment_capture", false);
			
			validateTimeRange(order);
			
			com.razorpay.Order razorpayOrder = razorpay.Orders.create(orderRequest);
			order.setRazorpayOrderId(razorpayOrder.get("id"));
			this.orderRepository.save(order);
		} catch (RazorpayException re) {
			re.printStackTrace();
			System.out.println(re.getMessage());
		}
		final Map<String, Object> changes = new HashMap<>();
		changes.put(OrdersApiConstants.razorpayOrderIdParamName, order.getRazorpayOrderId());
		logger.info(order.getRazorpayOrderId());
		return CommandProcessingResult.fromDetails(order.getId(), changes);
	}
	
	//Probably bullshit code
	private void validateTimeRange(final Order order) {
		//Use OrderRepository and BlockRepository to validate overlapping slots
		final String dateTimeFrom = order.getTimeFrom("yyyy-MM-dd");
		final String dateTimeTo = order.getTimeTo("yyyy-MM-dd");
		final String timeFrom = order.getTimeFrom("hh:mm:ss");
		final String timeTo = order.getTimeTo("hh:mm:ss");
		final String timeFromDate = order.getTimeFrom("yyyy-MM-dd");
		BigInteger isOverlappingInt =  this.orderRepository.isOverlappingOrder(dateTimeFrom,
				dateTimeTo, order.getRoom().getId());
		Boolean isOverlapping = isOverlappingInt.compareTo(BigInteger.ZERO) != 0;
		if(!isOverlapping) {
			isOverlappingInt = this.blockRepository.isOverlappingOrder(timeFrom, timeTo,
					order.getRoom().getId(), timeFromDate);
			isOverlapping = isOverlappingInt.compareTo(BigInteger.ZERO) != 0;
		}
		if(isOverlapping)
			throw new GeneralPlatformException("error.msg.overlapping.timerange",
					"Time range overlaps with another booking.");
	}
     
	
	
	@Transactional
	public CommandProcessingResult captureOrder(final Long orderId, final JsonCommand captureOrder){
		final Order order = this.orderRepository.findOne(orderId);
		setDerivedFields(order);
		final String paymentId = this.orderAssembler.fetchPaymentId(captureOrder.parsedJson());
		final HashMap<String, Object> changes = new HashMap<>();

		try {
			RazorpayClient razorpay = getRazorpayClient();
			// Call Razorpay Capture API
			captureRequest(order, changes, razorpay, paymentId);

			// Call Razorpay Transfer API based on cancellation policy or use on hold options
			transferRequest(order, changes, razorpay, paymentId);
		} catch (RazorpayException re) {
			re.printStackTrace();
			System.out.println(re.getMessage());
		}
		
		this.orderRepository.save(order);
	    sendNotifications(order);
		sendEmail(order);
		
		return CommandProcessingResult.fromDetails(orderId, changes);
	}
	
	private void sendNotifications(final Order order) {
		String customerMessage = order.getRoom().getHost().getName() + " has confirmed your booking for " 
				+ order.getTimeFrom("dd MMM HH:mm");
		String partnerMessage = order.getCreatedBy().getName() + " has placed a booking for " 
				+ order.getTimeFrom("dd MMM HH:mm");
		if(order.getTimeTo("HH:mm") != null) {
			partnerMessage += " to " + order.getTimeTo("HH:mm") + ".";
			customerMessage += " to " + order.getTimeTo("HH:mm") + ".";
		} else {
			partnerMessage += ".";
			customerMessage += ".";
		}
		this.notificationService.sendNotification(this.context.authenticatedUser().getId(),
				customerMessage, "Order Confirmed!");
		
		this.notificationService.sendNotification(order.getRoom().getHost().getUserId(),
				partnerMessage, "Order Placed!");
	}
	
	private void sendEmail(final Order order) {
		final Map<String, String> templateParams = new HashMap<>();
		
		templateParams.put("customerName", this.context.authenticatedUser().getFirstName());
		final JamHost host = order.getRoom().getHost();
		templateParams.put("studioName", host.getName());
		templateParams.put("lat" , host.getLatitude().toString() );
		templateParams.put("lng", host.getLongitude().toString());
		templateParams.put("startDateTime", order.getTimeFrom("dd MMM HH:mm"));
		templateParams.put("endDateTime", order.getTimeTo("dd MMM HH:mm"));
		templateParams.put("orderAmount", order.getAmount().toString());
		
		final String emailAddress = this.context.authenticatedUser().getEmail();
		this.mailService.sendEmail(emailAddress, "ORDER_CONFIRMATION", templateParams);
	}
	
	private void setDerivedFields(final Order order) {
		final Double razorpayGatewayCharge = this.configService.getRazorpayGatewayCharge();
		final Double razorpayTransferCharge = this.configService.getRazorpayTransferCharge();
		final Double receivedAmount = order.getAmount() - (order.getAmount() * (razorpayGatewayCharge / 100) * 1.18);
		final Integer partnerReceivedAmount = order.getPartnerReceivedAmount();
		final Double transferredAmount = partnerReceivedAmount
				+ (partnerReceivedAmount * (razorpayTransferCharge / 100) * 1.18);
		final Double margin = receivedAmount - transferredAmount;
		order.setReceivedAmount(receivedAmount);
		order.setTransferredAmount(transferredAmount);
		order.setMargin(margin);
	}
	
	private void transferRequest(final Order order, final HashMap<String, Object> changes, final RazorpayClient razorpay,
			final String paymentId) throws RazorpayException {
		final String accountId = order.getRoom().getHost().getRazorpayAccountId();
		JSONObject transferRequest = new JSONObject();
		JSONArray transfers = new JSONArray();
		JSONObject transfer = new JSONObject();
		transfer.put("amount", new Double(order.getTransferAmount() * 100).intValue()); // The amount should be in paise.
		transfer.put("currency", "INR");
		transfer.put("account", accountId);

		// Will settle next day of booked date
		final LocalDateTime timeFrom = order.getLocalDateTimeFrom();
		LocalDateTime afterEndOfBooking = timeFrom.plusMinutes(15);
		ZonedDateTime zdt = afterEndOfBooking.atZone(ZoneId.systemDefault());
		transfer.put("on_hold", true);
		transfer.put("on_hold_until", zdt.toInstant().toEpochMilli() / 1000);
		transfers.put(transfer);
		transferRequest.put("transfers", transfers);
		List<Transfer> transfersResponse = razorpay.Payments.transfer(paymentId, transferRequest);
		if(!CollectionUtils.isNullOrEmpty(transfersResponse)) {
			transfersResponse.get(0).get("id");
			final String transferId = transfersResponse.get(0).get("id");
			 order.setTransferId(transferId);
		}
		order.capturePayment(paymentId);
	}
	

	private void captureRequest(final Order order, final HashMap<String, Object> changes, final RazorpayClient razorpay,
			final String paymentId) throws RazorpayException {
		JSONObject captureRequest = new JSONObject();
		captureRequest.put("amount", order.getAmount() * 100);
		Payment payment = razorpay.Payments.capture(paymentId, captureRequest);

		changes.put("mobileNo", payment.get("contact"));
		// Save contact in appuser
		changes.put("email", payment.get("email"));
	}

	@Transactional
	public CommandProcessingResult refund(final Long orderId, final Boolean isPartner) {
		final HashMap<String, Object>  changes = new HashMap<>();
		final Order order = this.orderRepository.findOne(orderId);
		if(order.getLocalDateTimeTo().isBefore(LocalDateTime.now())) 
			throw new GeneralPlatformException("error.msg.non.refundable", "Booking time has elapsed.");
		if(order.isUncancellable())
			throw new GeneralPlatformException("error.msg.uncancellable", "Order is already cancelled.");
		// Call Razorpay refund API
		try {
			RazorpayClient razorpay = getRazorpayClient();
			// Full Refund
			if (isPartner) {
				fullRefund(order, razorpay, OrderStatus.REFUNDED);
				sendCancellationNotifications(order, true, true, null);
			} else {
				JSONObject refundRequest = new JSONObject();
				Boolean isRefunded = false;
				// Partial refund
				final Integer refundPercentage = getRefundPercentage(order.getRoom().getId(),
						order.getLocalDateTimeFrom());
				final Integer refundAmount = ((Double) (order.getAmount() * ((double) refundPercentage / 100)))
						.intValue();
				if (refundAmount.equals(order.getAmount())) {
					fullRefund(order, razorpay, OrderStatus.CANCELLED);
					isRefunded = true;
				} else if (refundAmount <= 0) {
					noRefund(order, changes);
				} else if (refundAmount < (order.getAmount()) && refundAmount > 0) {
					partialRefund(order, refundRequest, refundAmount, refundPercentage, razorpay);
					isRefunded = true;
				}
				order.setOrderStatus(OrderStatus.CANCELLED.getValue());
				order.setCancelledByClient(true);
				sendCancellationNotifications(order, isRefunded, false, refundAmount);
			}
			this.orderRepository.save(order);
		} catch (RazorpayException re) {
			re.printStackTrace();
			System.out.println(re.getMessage());
			throw new GeneralPlatformException("payment.gateway.exception", "Payment Gateway Exception");
		} 
		return CommandProcessingResult.fromDetails(orderId, changes);
	}
	
	private void sendCancellationNotifications(final Order order, final Boolean isRefunded, final Boolean isPartner, final Integer refundAmount) {
		String notificationMessage = null;
		Long notifyUserId = null;
		if(isPartner) {
			notifyUserId = order.getCreatedBy().getId();
			notificationMessage = "Your payment amount has been refunded to you. We apologize for the inconvenience caused.";
		} else {
			notifyUserId = this.context.authenticatedUser().getId();
			if(isRefunded) {
				notificationMessage =  "As per " + order.getRoom().getHost().getName()
						+ " policy, an amount of Rs. " + (refundAmount / 100) + " has been refunded.";
			} else {
				notificationMessage = "Your cancellation was successful but amount could not be refunded as per studio cancellation policy.";
			}
			this.notificationService.sendNotification(notifyUserId, notificationMessage, "Order Cancelled!");
			notifyUserId = order.getRoom().getHost().getUserId();
			notificationMessage = "Customer has cancelled booking for " + order.getTimeFrom("dd MMM HH:mm") + " to "
					+ order.getTimeTo("HH:mm") + ".";
			this.notificationService.sendNotification(notifyUserId, notificationMessage, "Order Cancelled!");
		}
			
	}
	
	private RazorpayClient getRazorpayClient() throws RazorpayException {
		final String razorpaySecret = this.externalReadService.getRazorpaySecret();
		final String razorpayApiKey = this.externalReadService.getRazorpayApiKey();
		return new RazorpayClient(razorpayApiKey, razorpaySecret);
	}
	
	private void fullRefund(final Order order, final RazorpayClient razorpay, final OrderStatus orderStatus) throws RazorpayException {
		JSONObject refundRequest = new JSONObject();
		refundRequest.put("amount", order.getAmount() * 100);
		refundRequest.put("reverse_all", true);
		razorpay.Payments.refund(order.getRazorpayPaymentId(), refundRequest);
		order.setRefundAmount(order.getAmount() * 100);
		order.setOrderStatus(orderStatus.getValue());
	}
	
	private void noRefund(final Order order, final HashMap<String, Object> changes) {
		order.setRefundAmount(0);
		order.setOrderStatus(OrderStatus.CANCELLED.getValue());
		changes.put("defaultUserMessage", "Your cancellation was successful but amount could not be refunded as per studio cancellation policy.");
	}
	
	private void partialRefund(final Order order, final JSONObject refundRequest, final Integer refundAmount,
			final Integer refundPercentage, final RazorpayClient razorpay) throws RazorpayException {
		
		final Double razorpayTransferCharge = this.configService.getRazorpayTransferCharge();
		refundRequest.put("amount", refundAmount*100); // Amount should be in paise
		JSONObject transferRequest = new JSONObject();
		final Double actualRefundPercentage = (double)refundPercentage / 100;
		final Double newPartnerReceivedAmount = order.getPartnerReceivedAmount().doubleValue()
				- (order.getPartnerReceivedAmount().doubleValue() * actualRefundPercentage);
		final Double newTransferredAmount = newPartnerReceivedAmount
				+ (newPartnerReceivedAmount * (razorpayTransferCharge / 100) * 1.18);
		final Double reversalAmount = order.getTransferAmount() - newTransferredAmount;
		transferRequest.put("amount", (int)(reversalAmount*100));
		final Double newPartnerRefundedAmount = order.getPartnerReceivedAmount() - newPartnerReceivedAmount;
		final Double newMargin = order.getMargin() - (refundAmount - newPartnerRefundedAmount);
		

		order.setTransferredAmount(newTransferredAmount);
	
		order.setPartnerReceivedAmount(newPartnerReceivedAmount.intValue());
		order.setMargin(newMargin);
		razorpay.Transfers.reversal(order.getRazorpayTransferId(), transferRequest);
		Refund refund = razorpay.Payments.refund(order.getRazorpayPaymentId(), refundRequest);
		order.setRefundAmount(refundAmount / 100);
	}

	private Integer getRefundPercentage(final Long roomId, final LocalDateTime startTime) {
		final LocalDateTime now = LocalDateTime.now();
		final long hours = Math.abs(ChronoUnit.HOURS.between(startTime, now));
		final Integer refundPercentage = this.policyService.retrievePolicies(roomId).getRefundPercentage(hours);
		return refundPercentage;
	}

	@Transactional
	public CommandProcessingResult captureFeedback(final Long orderId, final JsonCommand command) {
		final Order order = this.orderRepository.findOne(orderId);
		final Rating rating = this.orderAssembler.fetchRating(order, command.parsedJson());
		this.ratingRepository.save(rating);
		final JamRoom room = order.getRoom();
		if (room.getTotalReviews() == null) {
			room.setTotalReviews(1);
			room.setAvgRating(rating.getRating());
		} else {
			room.setTotalReviews(room.getTotalReviews() + 1);
			room.setAvgRating(
					(room.getAvgRating() * room.getTotalReviews() + rating.getRating()) / (room.getTotalReviews() + 1));
		}
		this.roomRepository.save(room);
		return CommandProcessingResult.fromDetails(orderId, null);
	}

	public List<SettlementData> retrieveSettlements(final String dateFrom, final String dateTo) {
		final SettlementDataMapper dm = new SettlementDataMapper();
		final AppUser user = this.context.authenticatedUser();
		if (!user.isPartner())
			return null;
		final String parsedDateFrom = StringUtil.formatDate(dateFrom, "yyyy-MM-dd");
		final String parsedDateTo = StringUtil.formatDate(dateTo, "yyyy-MM-dd");
		String settlementSql = "select " + dm.settlementSchema()
				+ " where DATE(o.`timeTo`) > ? and DATE(o.timeFrom) < ? and u.id= ? group by DATE(o.timeTo), o.`serviceType`, o.`paymentType`) d ";
		final List<SettlementData> settlements = this.jdbcTemplate.query(settlementSql, dm,
				new Object[] { parsedDateTo, parsedDateFrom, user.getId() });
		return settlements;
	}

	public List<BookingData> retrieveOrders(final String dateFrom, final String dateTo) {
		final OrderDataMapper dm = new OrderDataMapper();
		final AppUser user = this.context.authenticatedUser();
		if (!user.isPartner())
			return null;
		final String parsedDateFrom = StringUtil.formatDate(dateFrom, "yyyy-MM-dd hh:mm:ss");
		final String parsedDateTo = StringUtil.formatDate(dateTo, "yyyy-MM-dd hh:mm:ss");
		String orderSql = "select " + dm.bookingSchema()
				+ " inner join appuser u on u.id = h.userId where u.id = ? and o.timeTo <= ? and o.timeFrom >= ? ";
		final List<BookingData> orders = this.jdbcTemplate.query(orderSql, dm,
				new Object[] { user.getId(), parsedDateTo, parsedDateFrom });
		return orders;
	}

	public OrderData retrieveOrders(final Long userId) {
		final QuotationDataMapper rm = new QuotationDataMapper();
		final String sql = "select " + rm.quotationSchema() + " where q.createdById = ?";
		final List<QuoteData> quotes = this.jdbcTemplate.query(sql, rm, new Object[] { userId });
		final OrderDataMapper dm = new OrderDataMapper();
		String orderSql = "select " + dm.bookingSchema() + " where o.createdById = ? ";
		String pastOrderSql = orderSql + " and o.timeFrom < NOW() order by o.timeFrom desc";
		final List<BookingData> pastBookings = this.jdbcTemplate.query(pastOrderSql, dm, new Object[] { userId });
		String upcomingOrderSql = orderSql + " and o.timeFrom > NOW() order by o.timeFrom asc";
		final List<BookingData> upcomingBookings = this.jdbcTemplate.query(upcomingOrderSql, dm,
				new Object[] { userId });
		
		for (BookingData upcomingBooking : upcomingBookings) {
			if (!OrderStatus.isCancelled(OrderStatus.fromString(upcomingBooking.getStatus()))) {
				final Integer refundPercentage = getRefundPercentage(upcomingBooking.getRoomId(),
						upcomingBooking.getTimeFrom());
				final Double actualRefundPercentage =  ((refundPercentage * 1.0)/ 100);
				final Integer amountPaid = upcomingBooking.getAmountPaid();
				final Integer refundAmount = ((Double) (amountPaid * actualRefundPercentage)).intValue();
				upcomingBooking.setRefundAmount(refundAmount);
				
			}
			
		}
		 
	 
			
		return OrderData.instance(pastBookings, upcomingBookings, quotes);
	}

	private static final class QuotationDataMapper implements RowMapper<QuoteData> {

		public QuotationDataMapper() {

		}

		public String quotationSchema() {
			final StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append(
					" r.id as roomId, h.`name` as studioName, h.area as area, q.status_id as 'status', q.response as response ")
					.append(" from quote q inner join room r on q.roomId = r.id ")
					.append(" inner join jamhost h on r.hostId=h.id ");
			return sqlBuilder.toString();

		}

		@Override
		public QuoteData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
			final Long roomId = rs.getLong("roomId");
			final String studioName = rs.getString("studioName");
			final String area = rs.getString("area");
			final String status = QuoteStatus.fromInt(rs.getInt("status")).getCode();
			final String response = rs.getString("response");

			return QuoteData.instance(roomId, studioName, area, status, response);
		}
	}

	private static final class OrderDataMapper implements RowMapper<BookingData> {

		public OrderDataMapper() {

		}

		public String bookingSchema() {
			final StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append(
					" o.id as orderId, r.id as roomId, h.area as area, h.name as studioName, o.partnerReceivedAmount as partnerAmount, ")
					.append(" r.primaryImageId as primaryImageId, DATE(o.timeFrom) as dateFrom, DATE(o.timeTo) as dateTo, o.createdDate as createdDate, ")
					.append(" TIME(o.timeFrom) as timeFrom, TIME(o.timeTo) as timeTo, o.lastModifiedDate as lastModifiedDate, ")
					.append(" TIMESTAMPDIFF(MINUTE, o.timeFrom, o.timeTo) as minutes, ")
					.append(" o.amount as amount, o.orderStatus as orderStatus, h.latitude as latitude, h.longitude as longitude, o.`refundedAmount` as refundAmount ")
					.append(" from `order` o inner join room r on o.roomId=r.id and o.orderStatus <> 100 ")
					.append(" inner join jamhost h on r.hostId=h.id ");
			return sqlBuilder.toString();
		}

		@Override
		public BookingData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum)
				throws SQLException {
			final Long orderId = rs.getLong("orderId");
			final Long roomId = rs.getLong("roomId");
			final String area = rs.getString("area");
			final String studioName = rs.getString("studioName");
			final Long primaryImageId = rs.getLong("primaryImageId");
			final String dateFrom = rs.getString("dateFrom");
			final String dateTo = rs.getString("dateTo");
			final String timeFrom = rs.getString("timeFrom");
			final String timeTo = rs.getString("timeTo");
			final Integer duration = rs.getInt("minutes");
			final Integer amount = rs.getInt("amount");
			final String orderStatus = OrderStatus.fromInt(rs.getInt("orderStatus"));
			final String latitude = rs.getString("latitude");
			final String longitude = rs.getString("longitude");
			final Integer refundedAmount = rs.getInt("refundAmount");

			// TODO: Fix when multi room studio gets onboarded
			// final Long roomNo = rs.getLong("");
			final Integer partnerReceivedAmount = rs.getInt("partnerAmount");
			final String bookedOn = rs.getString("createdDate");
			final String cancelledOn = rs.getString("lastModifiedDate");
			// Replace first null with roomNo
			return BookingData.instance(orderId, roomId, area, studioName, primaryImageId, dateFrom, dateTo, timeFrom,
					timeTo, duration, amount, latitude, longitude, refundedAmount, orderStatus, null,
					partnerReceivedAmount, bookedOn, cancelledOn);
		}
	}

	private static final class SettlementDataMapper implements RowMapper<SettlementData> {

		public SettlementDataMapper() {

		}

		public String settlementSchema() {
			final StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append(
					" settlementInitiatedDate, DATE_ADD(DATE(settlementInitiatedDate), INTERVAL 3 DAY) as settlementDate, serviceType, paymentMode, amount from ")
					.append(" (select DATE(o.timeTo) as settlementInitiatedDate, e.`enum_value` as serviceType, n.`enum_value` as paymentMode, sum(o.partnerReceivedAmount) as amount ")
					.append(" from `order` o inner join `enum` e on e.`enum_name`='Service Type' and o.`serviceType` = e.`enum_id` ")
					.append(" inner join `enum` n on n.`enum_name`='Payment Mode' and n.`enum_id`=o.paymentType ")
					.append(" inner join `room` r on r.id = o.`roomId` ")
					.append(" inner join jamhost h on h.id = r.`hostId` inner join appuser u on u.id = h.userId ");
			return sqlBuilder.toString();

		}

		@Override
		public SettlementData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum)
				throws SQLException {
			final String settlementInitiatedDate = rs.getString("settlementInitiatedDate");
			final String settlementDate = rs.getString("settlementDate");
			final String serviceType = rs.getString("serviceType");
			final String paymentMode = rs.getString("paymentMode");
			final Integer amount = rs.getInt("amount");

			return SettlementData.instance(settlementInitiatedDate, settlementDate, serviceType, paymentMode, amount);
		}
	}

}
