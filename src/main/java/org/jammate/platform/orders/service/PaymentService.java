package org.jammate.platform.orders.service;

import com.google.gson.JsonObject;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;

public interface PaymentService {
	
	final static String API_KEY_HEADER = "API-Key";
	
	@POST("/orders")
	Response createOrder(@Header(API_KEY_HEADER) String apiKeyHeader,
			@Body JsonObject request);
	
	@POST("/payments/{paymentId}/capture")
	Response capturePayment(@Header(API_KEY_HEADER) String apiKeyHeader,
			@Path("paymentId") String paymentId,
			@Body JsonObject request);
	
	@POST("/payments/{paymentId}/refund")
	Response refundPayment(@Header(API_KEY_HEADER) String apiKeyHeader,
			@Path("paymentId") String paymentId,
			@Body JsonObject request);
	
	@POST("/payments/{paymentId}/transfers")
	Response transferPayment(@Header(API_KEY_HEADER) String apiKeyHeader,
			@Path("paymentId") String paymentId,
			@Body JsonObject request);

}
