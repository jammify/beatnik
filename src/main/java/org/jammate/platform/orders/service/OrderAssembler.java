package org.jammate.platform.orders.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.jamhosts.domain.JamRoomRepository;
import org.jammate.platform.jamhosts.domain.Rating;
import org.jammate.platform.orders.api.OrdersApiConstants;
import org.jammate.platform.core.email.api.EmailApiConstants;
import org.jammate.platform.core.email.api.EmailApiResource;
import org.jammate.platform.orders.data.OrderStatus;
import org.jammate.platform.orders.domain.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;

@Service
public class OrderAssembler {
	
	private final static Logger logger = LoggerFactory
			.getLogger(OrderAssembler.class);
	private final JamRoomRepository roomRepository;
	private final FromJsonHelper fromApiJsonHelper;
	private final PlatformSecurityContext context;
	
	@Autowired
	public OrderAssembler(final JamRoomRepository roomRepository,
			final FromJsonHelper fromApiJsonHelper,
			final PlatformSecurityContext context) {
		this.roomRepository = roomRepository;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.context = context;
	}
	
	public Order assembleFrom(final JsonElement element) {
		 //Assembling host
		 final Long roomId = this.fromApiJsonHelper.extractLongNamed(OrdersApiConstants.roomIdParamName, element);
		 final JamRoom room = this.roomRepository.findOne(roomId);
		 final Integer serviceType = this.fromApiJsonHelper.extractIntegerNamed(OrdersApiConstants.serviceTypeParamName, element);
		 final String to = this.fromApiJsonHelper.extractStringNamed(OrdersApiConstants.toParamName, element);
		 final String from = this.fromApiJsonHelper.extractStringNamed(OrdersApiConstants.fromParamName, element);
		 DateTimeFormatter fullDateTimeformatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
		 DateTimeFormatter partDateTimeformatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
		 DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
		 
		 LocalDateTime toDateTime = null;
		 if(to != null && to.length() > 11)
			 toDateTime = LocalDateTime.parse(to, fullDateTimeformatter);
		 else if(to != null)
			 toDateTime = LocalDate.parse(to, partDateTimeformatter).atStartOfDay();
		 LocalDateTime fromDateTime = null;
		 if(from != null && from.length() > 11)
			 fromDateTime = LocalDateTime.parse(from, fullDateTimeformatter);
		 else if(from != null)
			 fromDateTime = LocalDate.parse(from, partDateTimeformatter).atStartOfDay();
		 
		 final Integer status = OrderStatus.INITIATED.getValue();
		return Order.instance(room, serviceType, fromDateTime, toDateTime, status, null, null,
				null, null, null);
	}
	
	public String fetchPaymentId(final JsonElement element) {
		final String paymentId = this.fromApiJsonHelper
				.extractStringNamed(OrdersApiConstants.paymentIdParamName, element);
		return paymentId;
	}
	public void fetchEmailAddress(final JsonElement element) {
		final String emailAddress = this.fromApiJsonHelper.extractStringNamed(EmailApiConstants.REFFERAL_EMAIL, element);
	}
	
	
	
	public Rating fetchRating(final Order order,
			final JsonElement element) {
		final String message = this.fromApiJsonHelper
				.extractStringNamed(OrdersApiConstants.messageParamName, element);
		final String rating = this.fromApiJsonHelper
				.extractStringNamed(OrdersApiConstants.ratingParamName, element);
		return Rating.instance(order, message, rating);
	}

}
