package org.jammate.platform.orders.api;

import java.util.List;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.orders.data.SettlementData;
import org.jammate.platform.orders.service.OrderService;
import org.jammate.platform.profile.data.BookingData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/orders") 
public class OrdersApiResource {
	
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final OrderService orderService;
	private final static Logger logger = LoggerFactory
			.getLogger(OrdersApiResource.class);
	
	@Autowired
    public OrdersApiResource(final OrderService orderService,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper) {
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.orderService = orderService;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String createOrder(@RequestBody final String order) {
		final CommandProcessingResult response = this.orderService
				.createOrder(JsonCommand.from(order,
						new JsonParser().parse(order), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(value="/{orderId}/capture", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String captureOrder(@PathVariable Long orderId, @RequestBody final String captureOrder) {
		final CommandProcessingResult response = this.orderService
				.captureOrder(orderId, JsonCommand.from(captureOrder,
						new JsonParser().parse(captureOrder), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(value="/{orderId}/refund", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String refundOrder(@PathVariable Long orderId) {
		final CommandProcessingResult response = this.orderService
				.refund(orderId, false);
		return this.jsonSerializer.serialize(response);
	}
	
	//Ensure this API cannot be called by users
	@RequestMapping(value="/{orderId}/cancellation", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String partnerCancelOrder(@PathVariable Long orderId) {
		final CommandProcessingResult response = this.orderService
				.refund(orderId, true);
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(value="/{orderId}/feedback", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String captureFeedback(@PathVariable Long orderId,
			@RequestBody final String command) {
		final CommandProcessingResult response = this.orderService
				.captureFeedback(orderId, JsonCommand.from(command,
						new JsonParser().parse(command), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrievePartnerOrders(@RequestParam(value="dateFrom", required=true) String dateFrom,
    		@RequestParam(value="dateTo", required=true) String dateTo) {
		final List<BookingData> bookings = this.orderService.retrieveOrders(dateFrom, dateTo);
		return this.jsonSerializer.serialize(bookings);
    }

	@RequestMapping(value="/settlements", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrievePartnerSettlements(@RequestParam(value="dateFrom", required=true) String dateFrom,
    		@RequestParam(value="dateTo", required=true) String dateTo) {
		final List<SettlementData> bookings = this.orderService.retrieveSettlements(dateFrom, dateTo);
		return this.jsonSerializer.serialize(bookings);
    }

}
