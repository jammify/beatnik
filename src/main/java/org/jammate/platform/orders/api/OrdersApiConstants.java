package org.jammate.platform.orders.api;

public interface OrdersApiConstants {
	
	public static final String roomIdParamName = "roomId";
	public static final String serviceTypeParamName = "serviceType";
	public static final String fromParamName = "from";
	public static final String toParamName = "to";
	
	public static final String paymentIdParamName = "paymentId";
	public static final String transferIdParamName = "transferId";
	
	public static final String messageParamName = "message";
	public static final String ratingParamName = "rating";
	
	public static final String razorpayOrderIdParamName = "razorpayOrderId";

}
