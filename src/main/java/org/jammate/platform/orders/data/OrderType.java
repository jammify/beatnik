package org.jammate.platform.orders.data;

import java.util.HashMap;
import java.util.Map;


public enum OrderType {
    RAZORPAY(1, "razorpay"), PAYTM(2, "paytm"), SIMPL(3, "simpl");

    private final Integer value;
    private final String code;

    private OrderType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, OrderType> intToEnumMap = new HashMap<>();
    static {
        for (final OrderType type : OrderType.values()) {
            intToEnumMap.put(type.value, type);
        }
    }

    public static OrderType fromInt(final int i) {
        final OrderType type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }
}

