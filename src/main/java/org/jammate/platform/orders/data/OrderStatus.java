package org.jammate.platform.orders.data;

import java.util.HashMap;
import java.util.Map;

public enum OrderStatus {
	
	INITIATED(100, "Initiated"), AUTHORIZED(200, "Authorized"),
	CAPTURED(300, "Captured"), TRANSFERRED(400, "Transferred"),
	CANCELLED(500, "Cancelled"), REFUNDED(600, "Refunded"),
	COMPLETED(700, "Completed"), 
	//translated status
	CONFIRMED(800, "Confirmed");

    private final Integer value;
    private final String code;

    private OrderStatus(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, OrderStatus> intToEnumMap = new HashMap<>();
    private static final Map<String, OrderStatus> stringToEnumMap = new HashMap<>();
    static {
        for (final OrderStatus type : OrderStatus.values()) {
            intToEnumMap.put(type.value, type);
            stringToEnumMap.put(type.code, type);
        }
    }
    
    public static String fromInt(final int i) {
        final OrderStatus type = intToEnumMap.get(Integer.valueOf(i));
        if(type.equals(AUTHORIZED) || type.equals(CAPTURED) || type.equals(COMPLETED) || type.equals(TRANSFERRED))
        		return "Confirmed";
        return type.getCode();
    }
    
    public static OrderStatus fromString(final String status) {
        final OrderStatus type = stringToEnumMap.get(status);
        return type;
    }
    
    public static Boolean isCancelled(OrderStatus status) {
    		if(status.equals(REFUNDED) || status.equals(CANCELLED))
    			return true;
    		else
    			return false;
    					
    }
    
    public static Boolean isUncancellable(final int i) {
    	final OrderStatus type = intToEnumMap.get(Integer.valueOf(i));
    	if(type.equals(REFUNDED) || type.equals(CANCELLED) || type.equals(TRANSFERRED) 
    			|| type.equals(COMPLETED))
			return true;
		else
			return false;
    }

    @Override
    public String toString() {
        return name().toString();
    }

}
