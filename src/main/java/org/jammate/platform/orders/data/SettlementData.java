package org.jammate.platform.orders.data;

public class SettlementData {
	
	private final String settlementInitiatedDate;
	private final String settlementDate;
	private final String serviceType;
	private final String paymentMode;
	private final Integer amount;
	
	public static SettlementData instance(final String settlementInitiatedDate, final String settlementDate,
			final String serviceType, final String paymentMode, final Integer amount) {
		return new SettlementData(settlementInitiatedDate, settlementDate, serviceType,
				paymentMode, amount);
	}
	
	private SettlementData(final String settlementInitiatedDate, final String settlementDate,
			final String serviceType, final String paymentMode, final Integer amount) {
		this.settlementInitiatedDate = settlementInitiatedDate;
		this.settlementDate = settlementDate;
		this.serviceType = serviceType;
		this.paymentMode = paymentMode;
		this.amount = amount;
	}

}
