package org.jammate.platform.orders.domain;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.orders.data.OrderStatus;

@Entity
@Table(name = "`order`")
public class Order extends AbstractAuditableCustom<AppUser, Long>  {
	
	@ManyToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;
	
	@Column(name = "serviceType")
	private Integer serviceType;
	
	@Column(name = "timeFrom")
	private Date timeFrom;
	
	@Column(name = "timeTo")
	private Date timeTo;
	
	@Column(name = "orderStatus")
	private Integer orderStatus;
	
	@Column(name = "amount")
	private Integer amount;
	
	
	@Column(name = "partnerReceivedAmount")
	private Integer partnerReceivedAmount;
	
	@Column(name = "receivedAmount")
	private Double receivedAmount;
	
	@Column(name = "transferredAmount")
	private Double transferredAmount;

	
	@Column(name = "margin")
	private Double margin;
	
	
	
	@Column(name = "refundedAmount")
	private Integer refundedAmount;
	
	@Column(name = "razorpayOrderId")
	private String razorpayOrderId;
	

	@Column(name = "razorpayPaymentId")
	private String razorpayPaymentId;

	@Column(name = "razorpayTransferId")
	private String razorpayTransferId;
	
	@Column(name = "cancelledByClient")
	private Boolean cancelledByClient;
	
	@Column(name = "feedbackFormSeen")
	private Boolean feedbackFormSeen;
	
	protected Order() {
		//
	}
	
	public static Order instance(final JamRoom room, final Integer serviceType,
			final LocalDateTime from, final LocalDateTime to, final Integer status,
			final Integer amount, final Integer refundedAmount,
			final String razorpayOrderId, final String razorpayPaymentId,
			final Boolean cancelledByClient) {
		return new Order(room, serviceType, from, to, status, amount,
				refundedAmount, razorpayOrderId, razorpayPaymentId,
				cancelledByClient);
	}
	
	private Order(final JamRoom room, final Integer serviceType,
			final LocalDateTime from, final LocalDateTime to, final Integer status,
			final Integer amount, final Integer refundedAmount,
			final String razorpayOrderId, final String razorpayPaymentId,
			final Boolean cancelledByClient) {
		this.room = room;
		this.serviceType = serviceType;
		if(from != null)
			this.timeFrom = Date.from(from.atZone(ZoneId.systemDefault()).toInstant());
		if(to != null)
			this.timeTo = Date.from(to.atZone(ZoneId.systemDefault()).toInstant());
		this.orderStatus = status;
		this.amount = amount;
		this.refundedAmount = refundedAmount;
		this.razorpayOrderId = razorpayOrderId;
		this.razorpayPaymentId = razorpayPaymentId;
		this.cancelledByClient = cancelledByClient;
	}
	
	public Boolean isUncancellable() {
		return OrderStatus.isUncancellable(this.orderStatus);
	}
	
	public void capturePayment(final String paymentId) {
		this.razorpayPaymentId = paymentId;
		this.orderStatus = OrderStatus.CAPTURED.getValue();
	}
	
	
	
	public String getRazorpayOrderId() {
		return this.razorpayOrderId;
	}
	
	public void setRazorpayOrderId(final String razorpayOrderId) {
		this.razorpayOrderId = razorpayOrderId;
	}
	
	public void setRefundAmount(Integer refundAmount) {
		this.refundedAmount = refundAmount;
	}
	
	public String getRazorpayPaymentId() {
		return this.razorpayPaymentId;
	}
	

	public JamRoom getRoom() {
		return this.room;
	}
	
	public Integer getServiceType() {
		return this.serviceType;
	}

	public String getTimeTo(String format) {
		if(this.timeTo == null)
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return this.timeTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(formatter);
	}
	
	public LocalDateTime getLocalDateTimeTo() {
		if(this.timeTo != null)
			return this.timeFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		else
			return null;
	}
	
	public String getTimeFrom(String format) {
		if(this.timeFrom == null)
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return this.timeFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(formatter);
	}
	
	public LocalDateTime getLocalDateTimeFrom() {
		return this.timeFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
	
	public void setPrice(Integer price) {
		this.amount = price;;
	}
	
	public Integer getAmount() {
		return this.amount;
	}
	
	public Boolean feedbackFormSeen() {
		return this.feedbackFormSeen;
	}
	
	public void setFeedbackFormSeen() {
		this.feedbackFormSeen = true;
	}
	
	public void setPartnerReceivedAmount(Integer partnerReceivedAmount) {
		this.partnerReceivedAmount = partnerReceivedAmount;
	}
	
	public Integer getPartnerReceivedAmount() {
		return this.partnerReceivedAmount;
	}
	
	public void setReceivedAmount(Double receivedAmount) {
		this.receivedAmount = receivedAmount;
	}
	
	public void setTransferId(String transferId) {
		this.razorpayTransferId = transferId;
	}
	
	
	public Double getTransferAmount() {
		return this.transferredAmount;
	}

    public void setRazorpayTransferId(String RazorpayTransferId) {
    	this.razorpayTransferId = razorpayTransferId;
    }
    public String getRazorpayTransferId() {
    	return this.razorpayTransferId;
    }
	
	public void setOrderStatus(final Integer status) {
		this.orderStatus = status;
	}
	
	public void setTransferredAmount(final Double transferredAmount) {
		this.transferredAmount = transferredAmount;
	}
	
	public void setMargin(final Double margin) {
		this.margin = margin;
	}

	public Double getMargin() {
		return this.margin;
	}
	
	
	
	public void setCancelledByClient(final Boolean cancelled) {
		this.cancelledByClient = cancelledByClient;
	}
}
