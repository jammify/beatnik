package org.jammate.platform.orders.domain;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderRepository extends JpaRepository<Order, Long>,
	JpaSpecificationExecutor<Order> {
	
	@Query(value = "select h.`razorpayAccountId` from room r inner join "
			+ " `order` o on r.id=o.roomId " 
			+ "inner join jamhost h on h.id=r.hostId where o.id = :orderId", nativeQuery = true)
	String fetchAccountId(@Param("orderId") final Long orderId);
	
	@Query(value = "select * from `order` o where o.`feedbackFormSeen`=0 and "
			+ " timediff(o.`timeFrom`, now()) < 0 and o.`createdById` = :userId", nativeQuery=true)
	Order findNonSeenLapsedOrder(@Param("userId") final Long orderId);
	
	@Query(value = "select if(count(o.id) > 0, true, false) from `order`  o "
			+ "where o.timeFrom < :endB  and o.timeTo > :startB and o.roomId = :roomId and o.orderStatus in (200,300,400,700)", nativeQuery=true)
	BigInteger isOverlappingOrder(@Param("startB") final String startB, @Param("endB") final String endB,
			@Param("roomId") final Long roomId);
	

}
