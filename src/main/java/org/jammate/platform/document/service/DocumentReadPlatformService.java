package org.jammate.platform.document.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.jammate.platform.core.infra.JdbcSupport;
import org.jammate.platform.document.contentrepository.ContentRepository;
import org.jammate.platform.document.contentrepository.ContentRepositoryFactory;
import org.jammate.platform.document.data.DocumentData;
import org.jammate.platform.document.data.FileData;
import org.jammate.platform.document.domain.DocumentRepository;
import org.jammate.platform.document.exceptions.DocumentNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class DocumentReadPlatformService {
	
	private final static Logger logger = LoggerFactory.getLogger(DocumentReadPlatformService.class);

	private final JdbcTemplate jdbcTemplate;
    private final DocumentRepository documentRepository;
    private final ContentRepositoryFactory contentRepositoryFactory;

    @Autowired
    public DocumentReadPlatformService(final DataSource dataSource,
    		final DocumentRepository documentRepository,
    		final ContentRepositoryFactory documentStoreFactory) {
    		this.jdbcTemplate = new JdbcTemplate();
	    this.jdbcTemplate.setDataSource(dataSource);
        this.documentRepository = documentRepository;
        this.contentRepositoryFactory = documentStoreFactory;
    }
	
	
	
	public FileData retrieveFileData(final Long documentId) {
        try {
            final DocumentMapper mapper = new DocumentMapper(false, false);
            final DocumentData documentData = fetchDocumentDetails(documentId, mapper);
            final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository(documentData.storageType());
            return contentRepository.fetchFile(documentData);
        } catch (final EmptyResultDataAccessException e) {
            throw new DocumentNotFoundException(documentId);
        }
    }
	
	private DocumentData fetchDocumentDetails(final Long documentId,
            final DocumentMapper mapper) {
        final String sql = "select " + mapper.schema() + " where d.id=? ";
        return this.jdbcTemplate.queryForObject(sql, mapper, new Object[] {documentId });
    }
	
	private static final class DocumentMapper implements RowMapper<DocumentData> {

        private final boolean hideLocation;
        private final boolean hideStorageType;

        public DocumentMapper(final boolean hideLocation, final boolean hideStorageType) {
            this.hideLocation = hideLocation;
            this.hideStorageType = hideStorageType;
        }

        public String schema() {
            return "d.id as id, d.parentEntityType as parentEntityType, d.parentEntityId as parentEntityId, d.name as name, "
                    + " d.fileName as fileName, d.size as fileSize, d.type as fileType, "
                    + " d.description as description, d.location as location, d.storageTypeEnum as storageType"
                    + " from document d ";
        }

        @Override
        public DocumentData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {

            final Long id = JdbcSupport.getLong(rs, "id");
            final Long parentEntityId = JdbcSupport.getLong(rs, "parentEntityId");
            final Long fileSize = JdbcSupport.getLong(rs, "fileSize");
            final String parentEntityType = rs.getString("parentEntityType");
            final String name = rs.getString("name");
            final String fileName = rs.getString("fileName");
            final String fileType = rs.getString("fileType");
            final String description = rs.getString("description");
            String location = null;
            Integer storageType = null;
            if (!this.hideLocation) {
                location = rs.getString("location");
            }
            if (!this.hideStorageType) {
                storageType = rs.getInt("storageType");
            }
            return new DocumentData(id, parentEntityType, parentEntityId, name, fileName,
            		fileSize, fileType, description, location, storageType, null, null, null, null);
        }
	}

}
