package org.jammate.platform.document.service;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.contentrepository.ContentRepository;
import org.jammate.platform.document.contentrepository.ContentRepositoryFactory;
import org.jammate.platform.document.domain.Document;
import org.jammate.platform.document.domain.DocumentRepository;
import org.jammate.platform.document.domain.StorageType;
import org.jammate.platform.document.exceptions.ContentManagementException;
import org.jammate.platform.document.exceptions.DocumentNotFoundException;
import org.jammate.platform.document.exceptions.InvalidEntityTypeForDocumentManagementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.Files;

@Service
public class DocumentWritePlatformService  {

    private final static Logger logger = LoggerFactory.getLogger(DocumentWritePlatformService.class);
    private final String SystemEntityType = "DOCUMENT";
    private final List<String> REPORT_LANGUAGES = Arrays.asList("Telugu","Bengali","Assamese","Hindi","Konkani","Gujarati","Kashmiri","Kannada","Malayalam","Marathi","Manipuri","Oriya","Punjabi","Nepali","Tamil");

    private final DocumentRepository documentRepository;
    private final ContentRepositoryFactory contentRepositoryFactory;

    @Autowired
    public DocumentWritePlatformService(final DocumentRepository documentRepository,
            final ContentRepositoryFactory documentStoreFactory) {
        this.documentRepository = documentRepository;
        this.contentRepositoryFactory = documentStoreFactory;
    }

    @Transactional
    public Long createDocument(final DocumentCommand documentCommand, final InputStream inputStream) {
        try {
            // this.context.authenticatedUser();

            validateParentEntityType(documentCommand);

//            this.validator.validateForCreate(documentCommand);

            final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository();

            final String fileLocation = contentRepository.saveFile(inputStream, documentCommand);

            final Document document = Document.createNew(documentCommand.getParentEntityType(), documentCommand.getParentEntityId(),
                    documentCommand.getName(), documentCommand.getFileName(), documentCommand.getSize(), documentCommand.getType(),
                    documentCommand.getDescription(), fileLocation, contentRepository.getStorageType(), 
                    documentCommand.getSubEntityType(), documentCommand.getSubEntityId(),
                    documentCommand.getDocumentStageIdentifier());

            this.documentRepository.save(document);
            logger.info(document.getId() + "-");
            return document.getId();
        } catch (final DataIntegrityViolationException dve) {
            logger.error(dve.getMessage(), dve);
            dve.printStackTrace();
            return null;
//            throw new PlatformDataIntegrityException("error.msg.document.unknown.data.integrity.issue",
//                    "Unknown data integrity issue with resource.");
        }
    }

    @Transactional
    public CommandProcessingResult updateDocument(final DocumentCommand documentCommand, final InputStream inputStream) {
        try {
            // this.context.authenticatedUser();

            String oldLocation = null;
//            validator.validateForUpdate(documentCommand);
            // TODO check if entity id is valid and within data scope for the
            // user
            final Document documentForUpdate = this.documentRepository.findOne(documentCommand.getId());
            if (documentForUpdate == null) { throw new DocumentNotFoundException(documentCommand.getId()); }
            final StorageType documentStoreType = documentForUpdate.storageType();
            oldLocation = documentForUpdate.getLocation();
            if (inputStream != null && documentCommand.isFileNameChanged()) {
                final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository();
                documentCommand.setLocation(contentRepository.saveFile(inputStream, documentCommand));
                documentCommand.setStorageType(contentRepository.getStorageType().getValue());
            }

            documentForUpdate.update(documentCommand);

            if (inputStream != null && documentCommand.isFileNameChanged()) {
                final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository(documentStoreType);
                contentRepository.deleteFile(documentCommand.getName(), oldLocation);
            }

            this.documentRepository.saveAndFlush(documentForUpdate);

            return  CommandProcessingResult.fromDetails(documentForUpdate.getId(), null);
        } catch (final DataIntegrityViolationException dve) {
            logger.error(dve.getMessage(), dve);
            dve.printStackTrace();
//            throw new PlatformDataIntegrityException("error.msg.document.unknown.data.integrity.issue",
//                    "Unknown data integrity issue with resource.");
            return CommandProcessingResult.empty();
        } catch (final ContentManagementException cme) {
            logger.error(cme.getMessage(), cme);
            throw new ContentManagementException(documentCommand.getName(), cme.getMessage());
        }
    }

    @Transactional
    public CommandProcessingResult deleteDocument(final DocumentCommand documentCommand) {
        // this.context.authenticatedUser();
        try {
            validateParentEntityType(documentCommand);
            final Document document = this.documentRepository.findOne(documentCommand.getId());
            if (document == null) { throw new DocumentNotFoundException(documentCommand.getId()); }
            this.documentRepository.delete(document);
            this.documentRepository.flush();

            final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository(document.storageType());
            contentRepository.deleteFile(document.getName(), document.getLocation());
            return CommandProcessingResult.fromDetails(document.getId(), null);
        } catch (final DataIntegrityViolationException dataException) {
            logger.error(dataException.getMessage(), dataException);
            dataException.printStackTrace();
//            handleDataIntegrityIssues(dataException);
            return CommandProcessingResult.empty();
        }
    }

    @Transactional
    public Long createDocument(final InputStream inputStream, String fileName, String entityType, Long entityId, String contentType, String loanDocumentsLocation) {
        try {

        	String fileLocation = null;
        	final ContentRepository contentRepository = this.contentRepositoryFactory.getRepository();
        	if(inputStream != null) {

                fileLocation = contentRepository.saveFile(inputStream, fileName, entityType, entityId);
        	}
        	else {
        		fileLocation = loanDocumentsLocation;
        	}

            final String docName = Files.getNameWithoutExtension(fileName);
            final Long fileSize = 00l;
            final String description = null;

            Document document = this.documentRepository.findByParentEntityTypeAndParentEntityIdAndFileName(entityType, entityId, fileName);
            if (document == null) {
                document = Document.createNew(entityType, entityId, docName, fileName, fileSize, contentType, description, fileLocation,
                        contentRepository.getStorageType(), null, null, null);
            } else {
                document.update(fileLocation);
            }

            this.documentRepository.save(document);

            return document.getId();
        } catch (final DataIntegrityViolationException dve) {
            logger.error(dve.getMessage(), dve);
            dve.printStackTrace();
            return null;
        }
    }




    private void validateParentEntityType(final DocumentCommand documentCommand) {
        if (!checkValidEntityType(documentCommand.getParentEntityType())) { throw new InvalidEntityTypeForDocumentManagementException(
                documentCommand.getParentEntityType()); }
    }

    private static boolean checkValidEntityType(final String entityType) {
        for (final DOCUMENT_MANAGEMENT_ENTITY entities : DOCUMENT_MANAGEMENT_ENTITY.values()) {
            if (entities.name().equalsIgnoreCase(entityType)) { return true; }
        }
        return false;
    }

    /*** Entities for document Management **/
    public static enum DOCUMENT_MANAGEMENT_ENTITY {
        STUDIOS, ROOMS, CONSUMERS, BANDS, EVENTS, QUOTES, CONTENT, PAGE;

        @Override
        public String toString() {
            return name().toString().toLowerCase();
        }
    }

}