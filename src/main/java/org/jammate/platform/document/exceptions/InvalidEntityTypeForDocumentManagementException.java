package org.jammate.platform.document.exceptions;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

/**
 * A {@link RuntimeException} thrown when document management functionality is
 * invoked for invalid Entity Types
 */
public class InvalidEntityTypeForDocumentManagementException extends AbstractResourceNotFoundException {

    public InvalidEntityTypeForDocumentManagementException(final String entityType) {
        super("error.documentmanagement.entitytype.invalid", "Document Management is not support for the Entity Type: " + entityType,
                entityType);
    }
}