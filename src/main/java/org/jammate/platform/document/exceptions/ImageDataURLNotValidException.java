package org.jammate.platform.document.exceptions;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class ImageDataURLNotValidException extends AbstractRuntimeException {

    public ImageDataURLNotValidException() {
        super("error.msg.dataURL.save", "Only GIF, PNG and JPEG Data URL's are allowed");
    }
}
