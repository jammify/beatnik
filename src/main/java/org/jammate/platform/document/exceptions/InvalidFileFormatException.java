package org.jammate.platform.document.exceptions;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class InvalidFileFormatException extends AbstractRuntimeException {

    public InvalidFileFormatException(final String fileExtension) {
        super("error.msg.document.unsupported.file.format", "File with '." + fileExtension + "' extension, is not supported", fileExtension);
    }
    
    public InvalidFileFormatException(){
    	super("error.msg.document.unsupported.file.format.or.content.type", "File format or content type is invalid");
    }
    
    public InvalidFileFormatException(final String fileName, final String fileType ){
    	super("error.msg.document.unsupported.file.format.or.content.type", "File["+fileName+"] format or content type ["+fileType+"] is invalid"
    			, fileName,fileType);
    }
    
	public InvalidFileFormatException(final String fileName,
			final String fileExtension, final String fileType) {
		super("error.msg.document.unsupported.file.extension.do.not.macth.with.content.type",
				"File[" + fileName + "] format or content type [" + fileType
						+ "] doesnt match with extention ["+fileExtension +"]", fileName, fileType, fileExtension);
	}

}