package org.jammate.platform.document.exceptions;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class ImageUploadException extends AbstractRuntimeException {

    public ImageUploadException() {
        super("error.msg.image.type.upload", "Only image files of type GIF,PNG and JPG are allowed ");
    }
}
