package org.jammate.platform.document.exceptions;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

public class DocumentNotFoundException extends AbstractResourceNotFoundException {

    public DocumentNotFoundException(final Long id) {
        super("error.msg.document.id.invalid", "Document with identifier " + id + " does not exist", id);
    }
}
