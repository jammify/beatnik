package org.jammate.platform.document.api;


import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.document.data.DocumentData;
import org.jammate.platform.document.data.FileData;
import org.jammate.platform.document.service.DocumentReadPlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@RequestMapping("/documents")
public class DocumentApiResource {
	
	private final ApiSerializer<DocumentData> jsonSerializer;
	private final static Logger logger = LoggerFactory
			.getLogger(DocumentApiResource.class);
	private final DocumentReadPlatformService documentReadService;
	
	@Autowired
    public DocumentApiResource(final DocumentReadPlatformService documentReadService,
    		final ApiSerializer<DocumentData> jsonSerializer) {
		this.documentReadService = documentReadService;
		this.jsonSerializer = jsonSerializer;
	}
	
    @RequestMapping(value="/{documentId}/attachment", method = RequestMethod.GET)  
    public void downloadFile(HttpServletResponse response,
        @PathVariable final Long documentId) {

        final FileData fileData = this.documentReadService.retrieveFileData(documentId);
        logger.info(documentId.toString());
        response.setContentType("application/octet-stream");
        try {
	        byte[] file = IOUtils.toByteArray(fileData.file());
	        response.setContentLength(file.length);
	        IOUtils.write(file, response.getOutputStream());
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileData.name() + "\"");
	        response.setHeader("Content-Type", fileData.contentType());
	        response.flushBuffer();
        } catch (IOException e) {
        		e.printStackTrace();
        }
    }

}
