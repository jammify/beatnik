package org.jammate.platform.document.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.document.command.DocumentCommand;

@Entity
@Table(name = "document")
public class Document extends AbstractAuditableCustom<AppUser, Long> {

    @Column(name = "parentEntityType", length = 50)
    private String parentEntityType;

    @Column(name = "parentEntityId", length = 1000)
    private Long parentEntityId;
    
    @Column(name = "subEntityType", length = 50, nullable = true)
    private String subEntityType;

    @Column(name = "subEntityId", nullable = true)
    private Long subEntityId;
    
    @Column(name = "documentStageIdentifier", length = 250, nullable = true)
    private String documentStageIdentifier;

    @Column(name = "name", length = 250)
    private String name;

    @Column(name = "fileName", length = 250)
    private String fileName;

    @Column(name = "size")
    private Long size;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "location", length = 500)
    private String location;

    @Column(name = "storageTypeEnum")
    private Integer storageType;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "document_tag_cv_id", nullable = true)
//    private CodeValue tag;
    
    public Document() {}

    public static Document createNew(final String parentEntityType, final Long parentEntityId, final String name, final String fileName,
            final Long size, final String type, final String description, final String location, final StorageType storageType,
            final String subEntityType, final Long subEntityId, final String documentStageIdentifier) {
        return new Document(parentEntityType, parentEntityId, name, fileName, size, type, description, 
        		location, storageType, subEntityType, subEntityId, documentStageIdentifier);
    }


    private Document(final String parentEntityType, final Long parentEntityId, final String name, final String fileName, final Long size,
            final String type, final String description, final String location, final StorageType storageType, 
            final String subEntityType, final Long subEntityId, final String documentStageIdentifier) {
        this.parentEntityType = StringUtils.defaultIfEmpty(parentEntityType, null);
        this.parentEntityId = parentEntityId;
        this.name = StringUtils.defaultIfEmpty(name, null);
        this.fileName = StringUtils.defaultIfEmpty(fileName, null);
        this.size = size;
        this.type = StringUtils.defaultIfEmpty(type, null);
        this.description = StringUtils.defaultIfEmpty(description, null);
        this.location = StringUtils.defaultIfEmpty(location, null);
        this.storageType = storageType.getValue();
        this.subEntityId = subEntityId;
        this.subEntityType = subEntityType;
        this.documentStageIdentifier = documentStageIdentifier;
    }

    public static Document createFromId(final Long id) {
        Document document = new Document();
        document.setId(id);
        return document;
    }

    public void update(final DocumentCommand command) {
        if (command.isDescriptionChanged()) {
            this.description = command.getDescription();
        }
        if (command.isFileNameChanged()) {
            this.fileName = command.getFileName();
        }
        if (command.isFileTypeChanged()) {
            this.type = command.getType();
        }
        if (command.isLocationChanged()) {
            this.location = command.getLocation();
        }
        if (command.isNameChanged()) {
            this.name = command.getName();
        }
        if (command.isSizeChanged()) {
            this.size = command.getSize();
        }
    }

    public String getParentEntityType() {
        return this.parentEntityType;
    }

    public void setParentEntityType(final String parentEntityType) {
        this.parentEntityType = parentEntityType;
    }

    public Long getParentEntityId() {
        return this.parentEntityId;
    }

    public void setParentEntityId(final Long parentEntityId) {
        this.parentEntityId = parentEntityId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public Long getSize() {
        return this.size;
    }

    public void setSize(final Long size) {
        this.size = size;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public StorageType storageType() {
        return StorageType.fromInt(this.storageType);
    }

    
    public void setStorageType(Integer storageType) {
        this.storageType = storageType;
    }

    
    public void update(String location) {
        this.location = StringUtils.defaultIfEmpty(location, null);
    }


	public String getSubEntityType() {
		return this.subEntityType;
	}

	public Long getSubEntityId() {
		return this.subEntityId;
	}

	public String getDocumentStageIdentifier() {
		return this.documentStageIdentifier;
	}

}