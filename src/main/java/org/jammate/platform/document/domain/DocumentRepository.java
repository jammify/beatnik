package org.jammate.platform.document.domain;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DocumentRepository extends JpaRepository<Document, Long>, JpaSpecificationExecutor<Document> {

    @Query("select document.id from Document document where document.parentEntityType = :parentEntityType and document.parentEntityId = :parentEntityId")
    Collection<Long> findDocumentIDsByParentEntityTypeAndParentEntityId(@Param("parentEntityType") String parentEntityType,
            @Param("parentEntityId") Long parentEntityId);

    Document findByParentEntityTypeAndParentEntityIdAndFileName(String entityType, Long entityId, String fileName);
    
	List<Document> findByParentEntityTypeAndParentEntityIdAndTypeAndStorageType(final String parentEntityType,
			final Long parentEntityId, final String type, final Integer storageType);
}
