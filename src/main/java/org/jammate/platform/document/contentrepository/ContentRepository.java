package org.jammate.platform.document.contentrepository;

import java.io.InputStream;

import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.data.Base64EncodedImage;
import org.jammate.platform.document.data.DocumentData;
import org.jammate.platform.document.data.FileData;
import org.jammate.platform.document.data.ImageData;
import org.jammate.platform.document.domain.StorageType;

public interface ContentRepository {

    public StorageType type = null;

    // TODO:Vishwas Need to move these settings to the Database
    public static final Integer MAX_FILE_UPLOAD_SIZE_IN_MB = 50;

    // TODO:Vishwas Need to move these settings to the Database
    public static final Integer MAX_IMAGE_UPLOAD_SIZE_IN_MB = 2;

    public abstract String saveFile(InputStream uploadedInputStream, DocumentCommand documentCommand);

    public abstract void deleteFile(String fileName, String documentPath);

    public abstract FileData fetchFile(DocumentData documentData);

    public abstract String saveImage(InputStream uploadedInputStream, Long resourceId, String imageName, Long fileSize);

    public abstract String saveImage(Base64EncodedImage base64EncodedImage, Long resourceId, String imageName);

    public abstract void deleteImage(final Long resourceId, final String location);

    public abstract ImageData fetchImage(ImageData imageData);

    public abstract StorageType getStorageType();

    public abstract String saveFile(InputStream uploadedInputStream, String fileName, String entityType, Long entityId);

	String saveResourceImage(InputStream uploadedInputStream, String resource,
			Long resourceId, String imageName, Long fileSize);
	
	public abstract String generateDocumentReportsParentDirectory(final String entityType, final Long entityId);

}
