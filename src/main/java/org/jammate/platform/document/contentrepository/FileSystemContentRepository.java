package org.jammate.platform.document.contentrepository;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.jammate.platform.core.exception.GeneralPlatformException;
import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.data.Base64EncodedImage;
import org.jammate.platform.document.data.DocumentData;
import org.jammate.platform.document.data.FileData;
import org.jammate.platform.document.data.ImageData;
import org.jammate.platform.document.domain.StorageType;
import org.jammate.platform.document.exceptions.ContentManagementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FileSystemContentRepository implements ContentRepository {
	

    private final static Logger logger = LoggerFactory.getLogger(FileSystemContentRepository.class);
    

    public static final String JAMMATE_BASE_DIR = System.getProperty("user.home") + File.separator + ".jammate";

    @Override
    public String saveFile(final InputStream uploadedInputStream, final DocumentCommand documentCommand) {
        final String fileName = documentCommand.getFileName();
        final String uploadDocumentLocation = generateFileParentDirectory(documentCommand.getParentEntityType(),
                documentCommand.getParentEntityId());

        final String fileLocation = uploadDocumentLocation + File.separator + fileName;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
		try {
			IOUtils.copy(uploadedInputStream, baos);
			byte[] bytes = baos.toByteArray();
			final InputStream newUploadedInputStream = new ByteArrayInputStream(bytes);
	        
	        //this.fileFormatValidationService.validateFileTypeByExtension(fileLocation);
	        //this.fileFormatValidationService.validateFileTypeByContentType(fileName, uploadedInputStream);
//	        this.fileFormatValidationService.validateFileTypeByContentTypeAndExtension(fileLocation, fileName, newUploadedInputStream);
//	        ContentRepositoryUtils.validateFileSizeWithinPermissibleRange(documentCommand.getSize(), fileName);
	        makeDirectories(uploadDocumentLocation);
	        
	        newUploadedInputStream.reset();
	        writeFileToFileSystem(fileName, newUploadedInputStream, fileLocation);
	        return fileLocation;
		} catch (final IOException ioe) {
			throw new ContentManagementException(fileName, ioe.getMessage());
		}
    }

    @Override
    public String saveImage(final InputStream uploadedInputStream, final Long resourceId, final String imageName, final Long fileSize) {
        final String uploadImageLocation = generateClientImageParentDirectory(resourceId);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        try {
                       IOUtils.copy(uploadedInputStream, baos);
                       byte[] bytes = baos.toByteArray();
                      final InputStream newUploadedInputStream = new ByteArrayInputStream(bytes);
               
        
        //this.fileFormatValidationService.validateFileTypeByExtension(imageName);
        //this.fileFormatValidationService.validateFileTypeByContentType(imageName, newUploadedInputStream);
//        this.fileFormatValidationService.validateFileTypeByContentTypeAndExtension(imageName, imageName, newUploadedInputStream);
       ContentRepositoryUtils.validateFileSizeWithinPermissibleRange(fileSize, imageName);
        makeDirectories(uploadImageLocation);

        final String fileLocation = uploadImageLocation + File.separator + imageName;
        newUploadedInputStream.reset();
        writeFileToFileSystem(imageName, newUploadedInputStream, fileLocation);
        return fileLocation;
               } catch (final IOException ioe) {
                       throw new ContentManagementException(imageName, ioe.getMessage());
               }
   }
   
    
    @Override
    public String saveResourceImage(final InputStream uploadedInputStream,final String resource, final Long resourceId, final String imageName, final Long fileSize) {
        final String uploadImageLocation = generateResourceImageParentDirectory(resource, resourceId);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
		try {
			IOUtils.copy(uploadedInputStream, baos);
			byte[] bytes = baos.toByteArray();
			final InputStream newUploadedInputStream = new ByteArrayInputStream(bytes);
		
        
        //this.fileFormatValidationService.validateFileTypeByExtension(imageName);
        //this.fileFormatValidationService.validateFileTypeByContentType(imageName, newUploadedInputStream);
//        this.fileFormatValidationService.validateFileTypeByContentTypeAndExtension(imageName, imageName, newUploadedInputStream);
        ContentRepositoryUtils.validateFileSizeWithinPermissibleRange(fileSize, imageName);
        makeDirectories(uploadImageLocation);

        final String fileLocation = uploadImageLocation + File.separator + imageName;
        newUploadedInputStream.reset();
        writeFileToFileSystem(imageName, newUploadedInputStream, fileLocation);
        return fileLocation;
		} catch (final IOException ioe) {
			throw new ContentManagementException(imageName, ioe.getMessage());
		}
    }

    @Override
    public String saveImage(final Base64EncodedImage base64EncodedImage, final Long resourceId, final String imageName) {
        final String uploadImageLocation = generateClientImageParentDirectory(resourceId);

        makeDirectories(uploadImageLocation);

        final String fileLocation = uploadImageLocation + File.separator + imageName + base64EncodedImage.getFileExtension();
        try {
        	final byte[] imgBytes = Base64.decodeBase64(base64EncodedImage.getBase64EncodedString());
        	ContentRepositoryUtils.validateFileContentType(imgBytes);
            final OutputStream out = new FileOutputStream(new File(fileLocation));
            out.write(imgBytes);
            out.flush();
            out.close();
        } catch (final IOException ioe) {
            throw new ContentManagementException(imageName, ioe.getMessage());
        }
        return fileLocation;
    }

    @Override
    public void deleteImage(final Long resourceId, final String location) {
        final boolean fileDeleted = deleteFile(location);
        if (!fileDeleted) {
            // no need to throw an Error, simply log a warning
            logger.warn("Unable to delete image associated with clients with Id " + resourceId);
        }
    }

    @Override
    public void deleteFile(final String fileName, final String documentPath) {
        final boolean fileDeleted = deleteFile(documentPath);
        if (!fileDeleted) { throw new ContentManagementException(fileName, null); }
    }

    private boolean deleteFile(final String documentPath) {
        final File fileToBeDeleted = new File(documentPath);
        if (fileToBeDeleted.exists()){
            return fileToBeDeleted.delete();
        }
        return true;
    }

    @Override
    public StorageType getStorageType() {
        return StorageType.FILE_SYSTEM;
    }

    @Override
    public FileData fetchFile(final DocumentData documentData) {
        final File file = new File(documentData.fileLocation());
        return new FileData(file, documentData.fileName(), documentData.getName(), documentData.contentType());
    }
    
    public FileData fetchReleaseFile(final String fileLocationFolder, final String fileName, final String contentType) {
    	final String location = FileSystemContentRepository.JAMMATE_BASE_DIR + File.separator + "AppReleases" + File.separator + fileLocationFolder + File.separator + fileName + ".zip";
        final File file = new File(location);
        if(file.exists() && !file.isDirectory()) { 
        	return new FileData(file, fileName, fileName, contentType);
        }
        throw new GeneralPlatformException("error.msg.file.not.found", "File Not Found");
    }

    @Override
    public ImageData fetchImage(final ImageData imageData) {
        final File file = new File(imageData.location());
        imageData.updateContent(file);
        return imageData;
    }

    /**
     * Generate the directory path for storing the new document
     * 
     * @param entityType
     * @param entityId
     * @return
     */
    private String generateFileParentDirectory(final String entityType, final Long entityId) {
        return FileSystemContentRepository.JAMMATE_BASE_DIR + File.separator
                //+ ThreadLocalContextUtil.getTenant().getName().replaceAll(" ", "").trim() 
        			+ "jammate"
                + File.separator + "documents" + File.separator
                + entityType + File.separator + entityId;
    }

    /**
     * Generate directory path for storing new Image
     */
    private String generateClientImageParentDirectory(final Long resourceId) {
        return FileSystemContentRepository.JAMMATE_BASE_DIR + File.separator
                //+ ThreadLocalContextUtil.getTenant().getName().replaceAll(" ", "").trim() 
        		    + "jammate"
                + File.separator + "images" + File.separator
                + "clients" + File.separator + resourceId;
    }
    
    /**
     * Generate directory path for storing new Image for  a Resource
     */
    private String generateResourceImageParentDirectory(final String resource, final Long resourceId) {
        return FileSystemContentRepository.JAMMATE_BASE_DIR + File.separator
                //+ ThreadLocalContextUtil.getTenant().getName().replaceAll(" ", "").trim() 
        		    + "jammate"
                + File.separator + "images" + File.separator
                + resource + File.separator + resourceId;
    }

    /**
     * Generate the directory path for storing the new document
     * 
     * @param entityType
     * @param entityId
     * @return
     */
    @Override
    public String generateDocumentReportsParentDirectory(final String entityType, final Long entityId) {
        return FileSystemContentRepository.JAMMATE_BASE_DIR + File.separator
                //+ ThreadLocalContextUtil.getTenant().getName().replaceAll(" ", "").trim() 
        		    + "jammate"
                + File.separator + "documents" + File.separator
                + entityType + File.separator + entityId + File.separator + "reports";
    }

    /**
     * Recursively create the directory if it does not exist *
     */
    private void makeDirectories(final String uploadDocumentLocation) {
        if (!new File(uploadDocumentLocation).isDirectory()) {
            new File(uploadDocumentLocation).mkdirs();
        }
    }

    private void writeFileToFileSystem(final String fileName, final InputStream uploadedInputStream, final String fileLocation) {
        try {
            final OutputStream out = new FileOutputStream(new File(fileLocation));
            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (final IOException ioException) {
            throw new ContentManagementException(fileName, ioException.getMessage());
        }
    }

    @Override
    public String saveFile(final InputStream uploadedInputStream, final String fileName, final String entityType, final Long entityId) {
        final String uploadDocumentLocation = generateDocumentReportsParentDirectory(entityType, entityId);

        final String fileLocation = uploadDocumentLocation + File.separator + fileName;
        ContentRepositoryUtils.validateFileType(fileLocation);
        
        makeDirectories(uploadDocumentLocation);

        writeFileToFileSystem(fileName, uploadedInputStream, fileLocation);
        return fileLocation;
    }
}