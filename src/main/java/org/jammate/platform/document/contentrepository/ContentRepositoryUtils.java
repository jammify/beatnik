package org.jammate.platform.document.contentrepository;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.core.exception.ApiParameterError;
import org.jammate.platform.core.exception.PlatformApiDataValidationException;
import org.jammate.platform.document.data.Base64EncodedImage;
import org.jammate.platform.document.exceptions.ContentManagementException;
import org.jammate.platform.document.exceptions.ImageDataURLNotValidException;
import org.jammate.platform.document.exceptions.ImageUploadException;
import org.jammate.platform.document.exceptions.InvalidFileFormatException;

import com.google.common.io.Files;

public class ContentRepositoryUtils {

    public static Random random = new Random();

    public static enum IMAGE_MIME_TYPE {
        GIF("image/gif"), JPEG("image/jpeg"), PNG("image/png");

        private final String value;

        private IMAGE_MIME_TYPE(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static IMAGE_MIME_TYPE fromFileExtension(IMAGE_FILE_EXTENSION fileExtension) {
            switch (fileExtension) {
                case GIF:
                    return IMAGE_MIME_TYPE.GIF;
                case JPG:
                case JPEG:
                    return IMAGE_MIME_TYPE.JPEG;
                case PNG:
                    return IMAGE_MIME_TYPE.PNG;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    public static enum IMAGE_FILE_EXTENSION {
        GIF(".gif"), JPEG(".jpeg"), JPG(".jpg"), PNG(".png");

        private final String value;

        private IMAGE_FILE_EXTENSION(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public String getValueWithoutDot() {
            return this.value.substring(1);
        }

        public IMAGE_FILE_EXTENSION getFileExtension() {
            switch (this) {
                case GIF:
                    return IMAGE_FILE_EXTENSION.GIF;
                case JPEG:
                    return IMAGE_FILE_EXTENSION.JPEG;
                case PNG:
                    return IMAGE_FILE_EXTENSION.PNG;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    public static enum IMAGE_DATA_URI_SUFFIX {
        GIF("data:" + IMAGE_MIME_TYPE.GIF.getValue() + ";base64,"), JPEG("data:" + IMAGE_MIME_TYPE.JPEG.getValue() + ";base64,"), PNG(
                "data:" + IMAGE_MIME_TYPE.PNG.getValue() + ";base64,");

        private final String value;

        private IMAGE_DATA_URI_SUFFIX(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }
    
    public static enum VALID_DOCUMENT_FILE_EXTENSION {
        PDF("pdf"), DOC("doc"), DOCX("docx"), PPT("ppt"), PPTX("pptx"), PPS("pps"), PPSX("ppsx"), ODT("odt"), XLS("xls"), XLSX("xlsx"), ZIP(
                "zip"), TEXT("txt"), MP3("mp3"), M4A("m4a"), OGG("ogg"), WAV("wav"), MP4("mp4"), M4V("m4v"), MOV("mov"), WMV("wmv"), AVI(
                "avi"), MPG("mpg"), OGV("ogv"), GPP3("3gp"), GPP2("3g2");

        private final String value;

        private VALID_DOCUMENT_FILE_EXTENSION(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    /**
     * Validates that passed in Mime type maps to known image mime types
     * 
     * @param mimeType
     */
    public static void validateImageMimeType(final String mimeType) {
        if (!(mimeType.equalsIgnoreCase(IMAGE_MIME_TYPE.GIF.getValue()) || mimeType.equalsIgnoreCase(IMAGE_MIME_TYPE.JPEG.getValue()) || mimeType
                .equalsIgnoreCase(IMAGE_MIME_TYPE.PNG.getValue()))) { throw new ImageUploadException(); }
    }

    /**
     * Extracts Image from a Data URL
     * 
     * @param mimeType
     */
    public static Base64EncodedImage extractImageFromDataURL(final String dataURL) {
        String fileExtension = "";
        String base64EncodedString = null;
        if (StringUtils.startsWith(dataURL, IMAGE_DATA_URI_SUFFIX.GIF.getValue())) {
            base64EncodedString = dataURL.replaceAll(IMAGE_DATA_URI_SUFFIX.GIF.getValue(), "");
            fileExtension = IMAGE_FILE_EXTENSION.GIF.getValue();
        } else if (StringUtils.startsWith(dataURL, IMAGE_DATA_URI_SUFFIX.PNG.getValue())) {
            base64EncodedString = dataURL.replaceAll(IMAGE_DATA_URI_SUFFIX.PNG.getValue(), "");
            fileExtension = IMAGE_FILE_EXTENSION.PNG.getValue();
        } else if (StringUtils.startsWith(dataURL, IMAGE_DATA_URI_SUFFIX.JPEG.getValue())) {
            base64EncodedString = dataURL.replaceAll(IMAGE_DATA_URI_SUFFIX.JPEG.getValue(), "");
            fileExtension = IMAGE_FILE_EXTENSION.JPEG.getValue();
        } else {
            throw new ImageDataURLNotValidException();
        }

        return new Base64EncodedImage(base64EncodedString, fileExtension);
    }

    public static void validateFileSizeWithinPermissibleRange(final Long fileSize, final String name) {
        /**
         * Using Content-Length gives me size of the entire request, which is
         * good enough for now for a fast fail as the length of the rest of the
         * content i.e name and description while compared to the uploaded file
         * size is negligible
         **/
        if (fileSize != null && ((fileSize / (1024 * 1024)) >= ContentRepository.MAX_FILE_UPLOAD_SIZE_IN_MB)) { throw new ContentManagementException(
                name, fileSize, ContentRepository.MAX_FILE_UPLOAD_SIZE_IN_MB); }
    }
    
    public static void validateFileType(final String fileLocation) {
        /*final Set<Object> SUPPORTED_FILE_FORMATS = new HashSet<Object>(Arrays.asList(VALID_DOCUMENT_FILE_EXTENSION.PDF.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.DOC.getValue(), VALID_DOCUMENT_FILE_EXTENSION.DOCX.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.PPT.getValue(), VALID_DOCUMENT_FILE_EXTENSION.PPTX.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.PPS.getValue(), VALID_DOCUMENT_FILE_EXTENSION.PPSX.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.ODT.getValue(), VALID_DOCUMENT_FILE_EXTENSION.XLS.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.XLSX.getValue(), VALID_DOCUMENT_FILE_EXTENSION.ZIP.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.TEXT.getValue(), VALID_DOCUMENT_FILE_EXTENSION.MP3.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.M4A.getValue(), VALID_DOCUMENT_FILE_EXTENSION.OGG.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.WAV.getValue(), VALID_DOCUMENT_FILE_EXTENSION.MP4.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.M4V.getValue(), VALID_DOCUMENT_FILE_EXTENSION.MOV.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.WMV.getValue(), VALID_DOCUMENT_FILE_EXTENSION.AVI.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.MPG.getValue(), VALID_DOCUMENT_FILE_EXTENSION.OGV.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.GPP3.getValue(), VALID_DOCUMENT_FILE_EXTENSION.GPP2.getValue(),
                IMAGE_FILE_EXTENSION.GIF.getValueWithoutDot(), IMAGE_FILE_EXTENSION.JPG.getValueWithoutDot(),
                IMAGE_FILE_EXTENSION.JPEG.getValueWithoutDot(), IMAGE_FILE_EXTENSION.PNG.getValueWithoutDot()));*/
    	final Set<Object> SUPPORTED_FILE_FORMATS = new HashSet<Object>(Arrays.asList(VALID_DOCUMENT_FILE_EXTENSION.PDF.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.DOC.getValue(), VALID_DOCUMENT_FILE_EXTENSION.DOCX.getValue(),
                VALID_DOCUMENT_FILE_EXTENSION.TEXT.getValue(),
                IMAGE_FILE_EXTENSION.GIF.getValueWithoutDot(), IMAGE_FILE_EXTENSION.JPG.getValueWithoutDot(),
                IMAGE_FILE_EXTENSION.JPEG.getValueWithoutDot(), IMAGE_FILE_EXTENSION.PNG.getValueWithoutDot()));
        final String fileFormat = Files.getFileExtension(fileLocation).toLowerCase();

        if (!SUPPORTED_FILE_FORMATS.contains(fileFormat)) {
            throw new InvalidFileFormatException(fileFormat);
        }
    }

	public static enum FILE_CONTENT_TYPE {
		GIF("image/gif"), JPEG("image/jpeg"), PNG("image/png"), 
		PDF("application/pdf"),
		DOC("application/x-tika-msoffice"),	
		DOCX("application/x-tika-ooxml"),
		TXT("text/plain")
				;

		private final String value;

		private FILE_CONTENT_TYPE(final String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static boolean contains(String type) {

			for (FILE_CONTENT_TYPE c : FILE_CONTENT_TYPE.values()) {
				if (c.getValue().equals(type)) {
					return true;
				}
			}

			return false;
		}
	}

	public static void validateFileContentType(final String fileName,
			final InputStream uploadedInputStream) {
		/** Instance of Tika facade class with default configuration. */
//		final Tika tika = new Tika();
//		try {
//			final String fileType = tika.detect(uploadedInputStream);
//			if (!FILE_CONTENT_TYPE.contains(fileType)) {
//				throw new InvalidFileFormatException(fileName, fileType);
//			}
//
//		} catch (IOException ioe) {
//			// TODO Auto-generated catch block
//			ioe.printStackTrace();
//			throw new ContentManagementException(fileName, ioe.getMessage());
//		}
	}

	public static void validateFileContentType(final byte[] byteArray) {
		/** Instance of Tika facade class with default configuration. */
//		final Tika tika = new Tika();
//
//		String fileType = tika.detect(byteArray);
//		if (!FILE_CONTENT_TYPE.contains(fileType)) {
//			throw new InvalidFileFormatException();
//		}
	}
    public static void validateClientImageNotEmpty(final String imageFileName) {
        final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
        if (imageFileName == null) {
            final StringBuilder validationErrorCode = new StringBuilder("validation.msg.clientImage.cannot.be.blank");
            final StringBuilder defaultEnglishMessage = new StringBuilder("The parameter image cannot be blank.");
            final ApiParameterError error = ApiParameterError.parameterError(validationErrorCode.toString(),
                    defaultEnglishMessage.toString(), "image");
            dataValidationErrors.add(error);
            throw new PlatformApiDataValidationException("validation.msg.validation.errors.exist", "Validation errors exist.",
                    dataValidationErrors);
        }
    }

    /**
     * Generate a random String
     * 
     * @return
     */
    public static String generateRandomString() {
        final String characters = "abcdefghijklmnopqrstuvwxyz123456789";
        final int length = generateRandomNumber();
        final char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(random.nextInt(characters.length()));
        }
        return new String(text);
    }

    /**
     * Generate a random number between 5 to 16
     * 
     * @return
     */
    public static int generateRandomNumber() {
        final Random randomGenerator = new Random();
        return randomGenerator.nextInt(11) + 5;
    }
}
