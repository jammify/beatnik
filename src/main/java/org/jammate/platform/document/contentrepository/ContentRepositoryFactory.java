package org.jammate.platform.document.contentrepository;

import org.jammate.platform.configuration.domain.ConfigurationDomainService;
import org.jammate.platform.configuration.service.ExternalServicesReadPlatformService;
import org.jammate.platform.document.data.S3CredentialsData;
import org.jammate.platform.document.domain.StorageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ContentRepositoryFactory {

    private final ApplicationContext applicationContext;
    private final ExternalServicesReadPlatformService externalServicesReadPlatformService;

    @Autowired
    public ContentRepositoryFactory(final ApplicationContext applicationContext,
            final ExternalServicesReadPlatformService externalServicesReadPlatformService) {
        this.applicationContext = applicationContext;
        this.externalServicesReadPlatformService = externalServicesReadPlatformService;
    }

	public ContentRepository getRepository() {
		final ConfigurationDomainService configurationDomainServiceJpa = this.applicationContext
				.getBean("configurationDomainServiceJpa",
						ConfigurationDomainService.class);
		if (configurationDomainServiceJpa.isAmazonS3Enabled()) {
			return createS3DocumentStore();
		}
		return this.applicationContext.getBean("fileSystemContentRepository",
				FileSystemContentRepository.class);
	}

	public ContentRepository getRepository(final StorageType documentStoreType) {
		if (documentStoreType == StorageType.FILE_SYSTEM) {
			return this.applicationContext.getBean(
					"fileSystemContentRepository",
					FileSystemContentRepository.class);
		}
		return createS3DocumentStore();
	}

    private ContentRepository createS3DocumentStore() {
        final S3CredentialsData s3CredentialsData = this.externalServicesReadPlatformService.getS3Credentials();
        return new S3ContentRepository(s3CredentialsData.getBucketName(), s3CredentialsData.getSecretKey(),
                s3CredentialsData.getAccessKey());
    }
}
