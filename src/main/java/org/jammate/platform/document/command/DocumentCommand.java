package org.jammate.platform.document.command;

import java.io.InputStream;
import java.util.Set;


/**
 * Immutable command for creating or updating details of a client identifier.
 */
public class DocumentCommand {

    private final Long id;
    private final String parentEntityType;
    private Long parentEntityId;
    private final String name;
    private final String description;
    private String fileName;
    private Long size;
    private String type;
    private String location;
    private Integer storageType;
    private String subEntityType;
    private Long subEntityId;
    private String documentStageIdentifier;
    private final Set<String> modifiedParameters;
    private final InputStream inputStream;

    public DocumentCommand(final Set<String> modifiedParameters, final Long id, final String parentEntityType, final Long parentEntityId,
            final String name, final String fileName, final Long size, final String type, final String description, final String location,
            final String subEntityType, final Long subEntityId, final String documentStageIdentifier,
            final InputStream inputStream) {
        this.modifiedParameters = modifiedParameters;
        this.id = id;
        this.parentEntityType = parentEntityType;
        this.parentEntityId = parentEntityId;
        this.name = name;
        this.fileName = fileName;
        this.size = size;
        this.type = type;
        this.description = description;
        this.location = location;
        this.subEntityId = subEntityId;
        this.subEntityType = subEntityType;
        this.documentStageIdentifier =  documentStageIdentifier;
        this.inputStream = inputStream;
    }
    
    public InputStream getInputStream() {
    		return this.inputStream;
    }

    public Long getId() {
        return this.id;
    }

    public String getParentEntityType() {
        return this.parentEntityType;
    }

    public Long getParentEntityId() {
        return this.parentEntityId;
    }

    public String getName() {
        return this.name;
    }

    public String getFileName() {
        return this.fileName;
    }

    public Long getSize() {
        return this.size;
    }

    public String getType() {
        return this.type;
    }

    public String getDescription() {
        return this.description;
    }


    public String getLocation() {
        return this.location;
    }

    public Set<String> getModifiedParameters() {
        return this.modifiedParameters;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public void setSize(final Long size) {
        this.size = size;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public Integer getStorageType() {
        return this.storageType;
    }

    public void setStorageType(final Integer storageType) {
        this.storageType = storageType;
    }

    public boolean isNameChanged() {
        return this.modifiedParameters.contains("name");
    }

    public boolean isFileNameChanged() {
        return this.modifiedParameters.contains("fileName");
    }

    public boolean isSizeChanged() {
        return this.modifiedParameters.contains("size");
    }

    public boolean isFileTypeChanged() {
        return this.modifiedParameters.contains("type");
    }

    public boolean isDescriptionChanged() {
        return this.modifiedParameters.contains("description");
    }

    public boolean isTagChanged() {
        return this.modifiedParameters.contains("tag");
    }

    public boolean isLocationChanged() {
        return this.modifiedParameters.contains("location");
    }

	public String getSubEntityType() {
		return this.subEntityType;
	}

	public Long getSubEntityId() {
		return this.subEntityId;
	}

	public String getDocumentStageIdentifier() {
		return this.documentStageIdentifier;
	}
	
	public void setParentEntityId(final Long parentEntityId) {
		this.parentEntityId = parentEntityId;
	}
}