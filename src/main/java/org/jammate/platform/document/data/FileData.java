package org.jammate.platform.document.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileData {

    private static final Logger logger = LoggerFactory.getLogger(FileData.class);

    private final File file;
    private final String fileName;
    private final String documentName;
    private final String contentType;
    private final InputStream inputStream;

    public FileData(final File file, final String fileName, final String documentName, final String contentType) {
        this.file = file;
        this.fileName = fileName;
        this.contentType = contentType;
        this.inputStream = null;
        this.documentName = documentName;
    }

    public FileData(final InputStream inputStream, final String fileName, final String documentName, final String contentType) {
        this.file = null;
        this.inputStream = inputStream;
        this.fileName = fileName;
        this.contentType = contentType;
        this.documentName = documentName;
    }

    
    public String getDocumentName() {
        return this.documentName;
    }

    public String contentType() {
        return this.contentType;
    }

    public String name() {
        return this.fileName;
    }

    public InputStream file() {
        try {
            if (this.inputStream == null) { return new FileInputStream(this.file); }
            return this.inputStream;
        } catch (final FileNotFoundException e) {
            logger.error(e.toString());
            return null;
        }
    }
}
