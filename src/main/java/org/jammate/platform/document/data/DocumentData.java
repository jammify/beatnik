package org.jammate.platform.document.data;

import org.jammate.platform.code.data.CodeValueData;
import org.jammate.platform.document.domain.StorageType;

public class DocumentData {

    private final Long id;
    private final String parentEntityType;
    private final Long parentEntityId;
    private final String name;
    private final String fileName;
    private final Long size;
    private final String type;
    private final String description;
    private final String location;
    private final Integer storageType;
    private final String imageData;
    private final String subEntityType;
    private final Long subEntityId;
    private final String stageIdentifier;
    
    public DocumentData(final Long id, final String parentEntityType, final Long parentEntityId, final String name, final String fileName,
            final Long size, final String type, final String description, final String location, final Integer storageType,
            final String imageData, 
            final String subEntityType, final Long subEntityId, final String stageIdentifier) {

        this.id = id;
        this.parentEntityType = parentEntityType;
        this.parentEntityId = parentEntityId;
        this.name = name;
        this.fileName = fileName;
        this.size = size;
        this.type = type;
        this.description = description;
        this.location = location;
        this.storageType = storageType;
        this.imageData = imageData;
        this.subEntityId = subEntityId;
        this.subEntityType = subEntityType;
        this.stageIdentifier = stageIdentifier;
    }

    public static DocumentData associateImageData(final DocumentData documentData, final String imageData) {
        final String location = null;
        final Integer storageType = null;
        return new DocumentData(documentData.id, documentData.parentEntityType, documentData.parentEntityId, documentData.name,
                documentData.fileName, documentData.size, documentData.type, documentData.description, location, storageType,
                imageData, documentData.subEntityType, documentData.subEntityId,
                documentData.stageIdentifier);
    }


    public static DocumentData createWith(final Long id, final String parentEntityType, final Long parentEntityId, final String name) {
        final String fileName = null;
        final Long size = null;
        final String type = null;
        final String description = null;
        final String location = null;
        final Integer storageType = null;
        final String imageData = null;
        final String subEntityType =null;
        final Long subEntityId =null;
        final String stageIdentifier =null;
        return new DocumentData(id, parentEntityType, parentEntityId, name, fileName, size, type, description, location, storageType,
                imageData, subEntityType, subEntityId, 
                stageIdentifier);
    }

    public static DocumentData createWith(final Long id, final String parentEntityType, final Long parentEntityId, final String name,
            final CodeValueData documentTag, final Long documentVaultId, final String documentVaultStatus) {
        final String fileName = null;
        final Long size = null;
        final String type = null;
        final String description = null;
        final String location = null;
        final Integer storageType = null;
        final String imageData = null;
        final String subEntityType =null;
        final Long subEntityId =null;
        final String stageIdentifier =null;
        return new DocumentData(id, parentEntityType, parentEntityId, name, fileName, size, type, description, location, storageType,
                imageData, subEntityType, subEntityId, stageIdentifier);
    }
    
    public static DocumentData createWith(final Long id, final String parentEntityType, final Long parentEntityId, final String name,
            final CodeValueData documentTag, final String parentEntityReferenceNo) {
        final String fileName = null;
        final Long size = null;
        final String type = null;
        final String description = null;
        final String location = null;
        final Integer storageType = null;
        final String imageData = null;
        final String subEntityType =null;
        final Long subEntityId =null;
        final String stageIdentifier =null;
        return new DocumentData(id, parentEntityType, parentEntityId, name, fileName, size, type, description, location, storageType,
                imageData, subEntityType, subEntityId, stageIdentifier);
    }
    

    public static DocumentData lookup(final Long id, final String name) {
        final String parentEntityType = null;
        final Long parentEntityId = null;
        final String fileName = null;
        final Long size = null;
        final String type = null;
        final String description = null;
        final String location = null;
        final Integer storageType = null;
        final String imageData = null;
        final String subEntityType =null;
        final Long subEntityId =null;
        final String stageIdentifier =null;
        return new DocumentData(id, parentEntityType, parentEntityId, name, fileName, size, type, description, location, storageType,
                imageData, subEntityType, subEntityId, 
                stageIdentifier);
    }

    public String contentType() {
        return this.type;
    }

    public String fileName() {
        return this.fileName;
    }

    public String fileLocation() {
        return this.location;
    }

    public StorageType storageType() {
        return StorageType.fromInt(this.storageType);
    }

    public String getParentEntityType() {
        return this.parentEntityType;
    }

    public Long getParentEntityId() {
        return this.parentEntityId;
    }

    public Long getId() {
        return this.id;
    }

    public String imageData() {
        return this.imageData;
    }

    public String getName() {
        return this.name;
    }
    
    public Long getSize() {
        return this.size;
    }
    
    public String getDescription() {
        return this.description;
    }

    
    
}