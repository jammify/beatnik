package org.jammate.platform.profile.exception;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

public class UserNotFoundException extends AbstractResourceNotFoundException {

    public UserNotFoundException(final Long id) {
        super("error.msg.user.id.invalid", "User with identifier " + id + " does not exist", id);
    }
}
