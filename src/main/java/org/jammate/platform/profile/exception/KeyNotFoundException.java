package org.jammate.platform.profile.exception;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

public class KeyNotFoundException extends AbstractResourceNotFoundException {
	
    public KeyNotFoundException(final String keyType) {
        super("error.msg.key.not.exist", ""+keyType+" does not exist", keyType);
    }
}
