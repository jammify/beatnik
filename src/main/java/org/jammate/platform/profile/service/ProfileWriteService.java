package org.jammate.platform.profile.service;

import static org.jammate.platform.profile.ProfileConstants.genresParamName;
import static org.jammate.platform.profile.ProfileConstants.idParamName;
import static org.jammate.platform.profile.ProfileConstants.selfRatingParamName;
import static org.jammate.platform.profile.ProfileConstants.skillsParamName;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.domain.AppUserRepository;
import org.jammate.platform.auth.domain.Role;
import org.jammate.platform.auth.domain.RoleRepository;
import org.jammate.platform.auth.service.SessionService;
import org.jammate.platform.code.domain.CodeValue;
import org.jammate.platform.code.domain.CodeValueRepository;
import org.jammate.platform.configuration.service.ExternalServicesReadPlatformService;
import org.jammate.platform.core.exception.PlatformException;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.CommandProcessingResultBuilder;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.jamhosts.api.HostApiConstants;
import org.jammate.platform.jamhosts.domain.JamHost;
import org.jammate.platform.jamhosts.domain.JamHostRepository;
import org.jammate.platform.profile.data.CryptographyData;
import org.jammate.platform.profile.domain.EncryptedKeys;
import org.jammate.platform.profile.domain.EncryptedKeysRepository;
import org.jammate.platform.profile.domain.Genre;
import org.jammate.platform.profile.domain.Message;
import org.jammate.platform.profile.domain.MessageRepository;
import org.jammate.platform.profile.domain.Skill;
import org.jammate.platform.profile.exception.KeyNotFoundException;
import org.jammate.platform.profile.exception.UserNotFoundException;
import org.jammate.platform.profile.serialization.ProfileApiJsonDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


@Service
public class ProfileWriteService {
	
	private final AppUserRepository appuserRepository;
	private final ProfileApiJsonDeserializer profileDeserializer;
	private final FromJsonHelper fromJsonHelper;
	private final CodeValueRepository codeValueRepository;
	private final JdbcTemplate jdbcTemplate;
	private final MessageRepository messageRepository;
	private final ExternalServicesReadPlatformService externalService;
	private final JamHostRepository hostRepository;
	private final CryptographyMapper cryptographyMapper;
	private final EncryptedKeysRepository keysRepository;
	private final GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
	private final SessionService sessionService;
	private final RoleRepository roleRepository;
	
	
	@Autowired
	public ProfileWriteService(final AppUserRepository appuserRepository,
			final ProfileApiJsonDeserializer profileDeserializer,
			final FromJsonHelper fromJsonHelper,
			final CodeValueRepository codeValueRepository,
			final DataSource source,
			final MessageRepository messageRepository,
			final ExternalServicesReadPlatformService externalService,
			final JamHostRepository hostRepository,
			final EncryptedKeysRepository keysRepository,
			final SessionService sessionService,
			final RoleRepository roleRepository) {
		this.jdbcTemplate = new JdbcTemplate();
		this.jdbcTemplate.setDataSource(source);
		this.appuserRepository = appuserRepository;
		this.profileDeserializer = profileDeserializer;
		this.fromJsonHelper = fromJsonHelper;
		this.codeValueRepository = codeValueRepository;
		this.messageRepository = messageRepository;
		this.externalService = externalService;
		this.hostRepository = hostRepository;
		this.keysRepository = keysRepository;
		this.cryptographyMapper = new CryptographyMapper();
		this.sessionService = sessionService;
		this.roleRepository = roleRepository;
	}
	
	@PostConstruct
	public void postConstruct() {
		try {
//			final String homeDir = System.getProperty("user.home");
			final String path = System.getenv("firebase");
			FileInputStream serviceAccount =
			  new FileInputStream(path);
			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://jammate-consumer-app.firebaseio.com")
			  .build();
	
			FirebaseApp.initializeApp(options);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public CommandProcessingResult createMessage(final Long userId,
			final JsonCommand command) {
		final AppUser user = this.appuserRepository.findOne(userId);
		final String message = this.fromJsonHelper
				.extractStringNamed(HostApiConstants.messageParamName, command.parsedJson());
		final Message msg = Message.instance(user, message);
		this.messageRepository.save(msg);
		return CommandProcessingResult.fromDetails(msg.getId(), null);
	}
	
    public CryptographyData getPublicKey(final String types) {
        return getKey(types, "publickey");
    }
	
	public CryptographyData getKey(final String types, final String keyType) {
		try {
            final String sql = "SELECT " + this.cryptographyMapper.schema() + " where e.types = ? and e.key_type = ? ";
            return this.jdbcTemplate.queryForObject(sql, this.cryptographyMapper, new Object[] { types, keyType });
        } catch (final EmptyResultDataAccessException e) {
            throw new KeyNotFoundException(keyType);
        } catch (DataAccessException exception){
        		exception.printStackTrace();
            throw new PlatformException();
        }
	}
	
	@Transactional
	public CommandProcessingResult createUser(final JsonCommand user, Boolean isPartner) {
		AppUser appuser = AppUser.fromJson(user.json());
		
		try {
			FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdTokenAsync(appuser.getFirebaseUserId()).get();
			Role role = null;
//			final String mobileNo = this.appuserRepository.findOneByMobileNo(appuser.getMobileNo());
			if(!isPartner)
				role = this.roleRepository.findOne(1l);
			else
				role = this.roleRepository.findOne(2l);
			appuser.setAuthDetails(decodedToken.getName(),
					decodedToken.getUid(), decodedToken.getPicture(), decodedToken.getEmail(), 
					decodedToken.getIssuer(),  role);
		} catch(Exception ex) {
			ex.printStackTrace();
			return CommandProcessingResult.empty();
			// return authexception
		}
		
		final AppUser existingUser = this.appuserRepository.findOneByEmail(appuser.getEmail());
		Long appuserId = null;
		final Map<String, Object> changes = new HashMap<> ();
		if(existingUser == null) {
			this.appuserRepository.save(appuser);
			appuserId = appuser.getId();
		} else {
			appuserId = existingUser.getId();
			if(existingUser.isPartner()) {
				final JamHost host = this.hostRepository.findByUserId(appuserId);
				if(host != null)
					changes.put("hostId", host.getId());
			}
		}
		
		
		final AppUser finalUser = (existingUser != null) ? existingUser : appuser;
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, finalUser.getUid(),
                authoritiesMapper.mapAuthorities(finalUser.getAuthorities()));
        SecurityContextHolder.getContext().setAuthentication(auth);
        final String sessionKey = createSessionKey(finalUser);
        final String mobileNo = appuser.getMobileNo();
        changes.put("mobileNo", mobileNo);
		changes.put("sessionToken", sessionKey);
        return CommandProcessingResult.fromDetails(appuserId, changes);
	}
	
	private String createSessionKey(final AppUser finalUser) {
		CryptographyData encryptData  = null;
        try {
            encryptData = getPublicKey("login");
        } catch (KeyNotFoundException e) {
            changeKeys();
            encryptData = getPublicKey("login");
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String nowDate = LocalDateTime.now().format(formatter);;
        String text = finalUser.getUsername() + nowDate + encryptData.getKeyValue();
        text = org.apache.commons.codec.digest.DigestUtils.sha256Hex(text);
        final String base64EncodedAuthKey = Base64.getEncoder().encodeToString(text.getBytes());
        sessionService.createSession(finalUser, base64EncodedAuthKey);
        return base64EncodedAuthKey;
	}
	
	@Transactional
	public CommandProcessingResult updateProfile(final Long appuserId, final JsonCommand profile) {
		
		this.profileDeserializer.validateForCreate(profile.json());
		final AppUser appuser = this.appuserRepository.findOne(appuserId);
		if(appuser == null)
			throw new UserNotFoundException(appuserId);
		final Map<String, Object> changes = appuser.update(profile);
        if (!changes.isEmpty()) {
        		if (changes.containsKey(skillsParamName)) {
				final Set<Skill> skills = assembleSkills(profile
						.arrayOfParameterNamed(skillsParamName));
				final boolean updated = appuser.updateSkills(skills);
				if (!updated) {
					changes.remove(skillsParamName);
				}
			}

			if (changes.containsKey(genresParamName)) {
				final Set<Genre> genres = assembleGenres(
						profile.arrayOfParameterNamed(genresParamName));
				final boolean updated = appuser.updateGenres(genres);
				if (!updated) {
					changes.remove(genresParamName);
				}
			}
        	
            this.appuserRepository.saveAndFlush(appuser);
        }
        return new CommandProcessingResultBuilder()
        		.withResourceIdAsString(appuser.getId()).build();
	}
	
	
	
	private Set<Skill> assembleSkills(final JsonArray skillsArray) {

		final Set<Skill> allSkills = new HashSet<>();

		for (int i = 0; i < skillsArray.size(); i++) {

			final JsonObject skillElement = skillsArray.get(i)
					.getAsJsonObject();

			final Long skillId = this.fromJsonHelper
					.extractLongNamed(idParamName, skillElement);
			final CodeValue skillCv = this.codeValueRepository.findOne(skillId);
			final Integer selfRating = this.fromJsonHelper
					.extractIntegerNamed(selfRatingParamName, skillElement);
			final Skill skill = Skill.createNewWithoutUser(
					skillCv, selfRating);
			allSkills.add(skill);
		}

		return allSkills;
	}
	
	private Set<Genre> assembleGenres(final JsonArray genresArray) {

		final Set<Genre> allGenres = new HashSet<>();

		for (int i = 0; i < genresArray.size(); i++) {

			final JsonObject skillElement = genresArray.get(i)
					.getAsJsonObject();

			final Long genreId = this.fromJsonHelper
					.extractLongNamed(idParamName, skillElement);
			final CodeValue genreCv = this.codeValueRepository.findOne(genreId);
			final Integer selfRating = this.fromJsonHelper
					.extractIntegerNamed(selfRatingParamName, skillElement);
			final Genre genre = Genre.createNewWithoutUser(
					genreCv, selfRating);
			allGenres.add(genre);
		}

		return allGenres;
	}
	
	private static final class CryptographyMapper implements RowMapper<CryptographyData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        public CryptographyMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(200);

            sqlBuilder.append("e.key_type AS keyType, AES_DECRYPT(e.key_value,'c4dc0f90845b021e6afd20564abba732') AS keyValue ");

            // sqlBuilder.append("e.key_type AS keyType, e.key_value AS keyValue ");

            sqlBuilder.append("FROM encrypted_keys e ");
            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public CryptographyData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
            final String keyType = rs.getString("keyType");
            final Blob keyValue = rs.getBlob("keyValue");
            return CryptographyData.instance(keyType, keyValue);
        }

    }

    public Boolean isPublicKeyExist(final String types) {
        return isKeyExist(types, "publickey");
    }

    public Boolean isPrivateExist(final String types) {
        return isKeyExist(types, "privatekey");
    }

    private Boolean isKeyExist(final String types, final String keyType) {
        try {
            final String sql = "SELECT " + this.cryptographyMapper.schema() + " where e.types = ? and e.key_type = ? ";
            Collection<CryptographyData> cryptographyData = this.jdbcTemplate.query(sql, this.cryptographyMapper, new Object[] { types,
                    keyType });
            if (cryptographyData != null && cryptographyData.size() > 0) { return true; }
            return false;
        } catch (final EmptyResultDataAccessException e) {
            throw new KeyNotFoundException(keyType);
        } catch (DataAccessException exception){
        		exception.printStackTrace();
            throw new PlatformException();
        }
    }
    
    public void changeKeys() {
        List<EncryptedKeys> existingKeys = this.keysRepository.findByTypes("login");
        KeyPairGenerator keyPairGenerator;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
            byte[] publicKeyAsBytes = publicKey.getEncoded();
            byte[] privateKeyAsBytes = privateKey.getEncoded();
            if (existingKeys == null || existingKeys.isEmpty()) {
                insertEncryptedKeysIntoDB("login", "publickey", publicKeyAsBytes);
                insertEncryptedKeysIntoDB("login", "privatekey", privateKeyAsBytes);
            } else {
                updateEncryptedKeysIntoDB("login", "publickey", publicKeyAsBytes);
                updateEncryptedKeysIntoDB("login", "privatekey", privateKeyAsBytes);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    
  private int insertEncryptedKeysIntoDB(final String types, final String keyType, final byte[] blobKey) {
        
        final String dbSecreteKey = "c4dc0f90845b021e6afd20564abba732";
        final String insertSql = "INSERT INTO `encrypted_keys` (`types`,`key_type`, `key_value`) VALUES (?, ? ,AES_ENCRYPT(?,?)) ";
        return this.jdbcTemplate.update(insertSql, new Object[] { types, keyType, blobKey, dbSecreteKey });
        
    }
    
    private int updateEncryptedKeysIntoDB(final String types, final String keyType, final byte[] blobKey) {
        
        final String dbSecreteKey = "c4dc0f90845b021e6afd20564abba732";
        final String insertSql = "UPDATE `encrypted_keys` set `key_value` = AES_ENCRYPT(?,?) where (`types` = ? and `key_type` = ?) ";
        return this.jdbcTemplate.update(insertSql, new Object[] {  blobKey, dbSecreteKey, types, keyType });
        
    }

}
