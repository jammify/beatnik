package org.jammate.platform.profile;

public class ProfileConstants {
	
	// request parameters
    public static final String firebaseUserIdParamName = "firebaseUserId";
    public static final String skillsParamName = "skills";
    public static final String genresParamName = "genres";
    public static final String idParamName = "id";
    public static final String selfRatingParamName = "selfRating";

}
