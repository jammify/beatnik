package org.jammate.platform.profile.api;

import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.profile.service.ProfileWriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/profiles") 
public class ProfileApiResource {
	
	private final ProfileWriteService profileService;
	private final FromJsonHelper fromApiJsonHelper;
	
	@Autowired
	public ProfileApiResource(final ProfileWriteService profileService,
			final FromJsonHelper fromApiJsonHelper) {
		this.profileService = profileService;
		this.fromApiJsonHelper = fromApiJsonHelper;
	}
	
	@RequestMapping(value="/{userId}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8")  
	@Transactional
    public CommandProcessingResult createProfile(@PathVariable Long userId,
    			@RequestBody final String profile) {
		return this.profileService.updateProfile(userId, JsonCommand.from(profile,
				new JsonParser().parse(profile), fromApiJsonHelper));
	}

}
