package org.jammate.platform.profile.api;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.orders.service.OrderService;
import org.jammate.platform.profile.data.OrderData;
import org.jammate.platform.profile.service.ProfileWriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/users") 
public class UserApiResource {
	
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final ProfileWriteService profileWriteService;
	private final OrderService orderService;
	
	@Autowired
    public UserApiResource(final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper,
    		final ProfileWriteService profileWriteService,
    		final OrderService orderService) {
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.profileWriteService = profileWriteService;
		this.orderService = orderService;
	}
	
	@RequestMapping(value="/{userId}/bookings", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveBookings(@PathVariable Long userId) {
		final OrderData order = this.orderService.retrieveOrders(userId);
		return this.jsonSerializer.serialize(order);
    }
	
	@RequestMapping(value="/{userId}/message", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
    public String sendMessage(@PathVariable Long userId,
    		@RequestBody final String message) {
		final CommandProcessingResult result = this.profileWriteService
				.createMessage(userId, JsonCommand.from(message,
						new JsonParser().parse(message), fromApiJsonHelper));
		return this.jsonSerializer.serialize(result);
    }

}
