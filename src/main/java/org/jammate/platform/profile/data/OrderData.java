package org.jammate.platform.profile.data;

import java.util.List;

import org.jammate.platform.jamhosts.data.QuoteData;

public class OrderData {
	
	final List<BookingData> pastBookings;
	final List<BookingData> upcomingBookings;
	final List<QuoteData> quotations;
	
	public static OrderData instance(final List<BookingData> pastBookings,
			final List<BookingData> upcomingBookings,
			final List<QuoteData> quotations) {
		return new OrderData(pastBookings, upcomingBookings, quotations);
	}
	
	private OrderData(final List<BookingData> pastBookings,
			final List<BookingData> upcomingBookings,
			final List<QuoteData> quotations) {
		this.pastBookings = pastBookings;
		this.upcomingBookings = upcomingBookings;
		this.quotations = quotations;
	}

}
