package org.jammate.platform.profile.data;

import java.sql.Blob;
import java.sql.SQLException;

import org.apache.commons.codec.binary.Base64;

public class CryptographyData {
	
	private final String keyType;
    private final String keyValue;

    CryptographyData(final String keyType, final String keyValue) {
        this.keyType = keyType;
        this.keyValue = keyValue;
    }

    public static CryptographyData instance(final String keyType, final Blob keyValue) throws SQLException {
        return new CryptographyData(keyType, Base64.encodeBase64String(keyValue.getBytes(1, (int) keyValue.length())));
    }

    public String getKeyType() {
        return this.keyType;
    }

    public String getKeyValue() {
        return this.keyValue;
    }

}
