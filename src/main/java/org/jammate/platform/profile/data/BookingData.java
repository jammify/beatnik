package org.jammate.platform.profile.data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BookingData {
	
	private final Long orderId;
	private final Long roomId;
	private final String area;
	private final String studioName;
	private final Long primaryImageId;
	private final String dateFrom; 
	private final String dateTo;
	private final String timeFrom;
	private final String timeTo;
	private final Integer duration; 
	private final Integer amountPaid;
	private final String lat;
	private final String lng;
	private Integer cancellationRefundAmount;
	private  String status;
	
	//Partner fields
	private final Long roomNo;
	private final Integer amount;
	private final String bookedOn;
	private final String cancelledOn;
	
	public static BookingData instance(final Long orderId, final Long roomId,
			final String area, final String studioName,
			final Long primaryImageId, final String dateFrom,
			final String dateTo, final String timeFrom, final String timeTo,
			final Integer duration, final Integer amountPaid, 
			final String lat, final String lng, 
			final Integer cancellationRefundAmount, final String status,
			final Long roomNo, final Integer amount, final String bookedOn,
			final String cancelledOn) {
		return new BookingData(orderId, roomId, area, studioName,
				primaryImageId, dateFrom, dateTo, timeFrom, timeTo,
				duration, amountPaid, lat, lng, cancellationRefundAmount, status,
				roomNo, amount, bookedOn, cancelledOn);
	}
	
	private BookingData(final Long orderId, final Long roomId,
			final String area, final String studioName,
			final Long primaryImageId, final String dateFrom,
			final String dateTo, final String timeFrom, final String timeTo,
			final Integer duration, final Integer amountPaid, 
			final String lat, final String lng, 
			final Integer cancellationRefundAmount, final String status,
			final Long roomNo, final Integer amount, final String bookedOn,
			final String cancelledOn) {
		this.orderId = orderId;
		this.roomId = roomId;
		this.area = area;
		this.studioName = studioName;
		this.primaryImageId = primaryImageId;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.duration = duration;
		this.amountPaid = amountPaid;
		this.lat = lat;
		this.lng = lng;
		this.cancellationRefundAmount = cancellationRefundAmount;
		this.status = status;
		this.roomNo = roomNo;
		this.amount = amount;
		this.bookedOn = bookedOn;
		this.cancelledOn = cancelledOn;
	}
	
	public void setRefundAmount(final Integer refundAmount) {
		this.cancellationRefundAmount = refundAmount;
	}
	
	public Integer getCancellationRefundAmount() {
		return this.cancellationRefundAmount;
	}
	
	public void setStatus (final String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
	 
	
	public Long getRoomId() {
		return this.roomId;
	}
	
	public LocalDateTime getTimeFrom() {
		String combinedFrom = this.dateFrom + " " + this.timeFrom;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(combinedFrom, formatter);
		return dateTime;
	}
	
	public Integer getAmountPaid() {
		return this.amountPaid;
	}

}
