package org.jammate.platform.profile.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "message")
public class Message extends AbstractPersistable<Long> {
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "userId", referencedColumnName = "id", nullable = false)
	private AppUser appuser;
	
	@Column(name="message")
	private String message;
	
	protected Message() {
        //
    }
	
	public static Message instance(final AppUser user, final String message) {
		return new Message(user, message);
	}

    private Message(final AppUser user, final String message) {
        this.appuser = user;
        this.message = message;
    }

}
