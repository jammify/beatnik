package org.jammate.platform.profile.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface EncryptedKeysRepository extends JpaRepository<EncryptedKeys, Long>, JpaSpecificationExecutor<EncryptedKeys> {
    
    EncryptedKeys findByTypesAndKeyType(String types, String keyType);
    
    List<EncryptedKeys> findByTypes(String types);
}
