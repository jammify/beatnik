package org.jammate.platform.profile.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.code.domain.CodeValue;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "genres")
public class Genre extends AbstractPersistable<Long> {
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuserId", referencedColumnName = "id", nullable = false)
	private AppUser appuser;
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "genresCvId", nullable = false)
	private CodeValue genre;
	
	@Column(name="selfRating")
	private Integer selfRating;
	
	protected Genre() {
        //
    }
	
	public static Genre createNewWithoutUser(final CodeValue genre, final Integer selfRating) {
		return new Genre(null, genre, selfRating);
	}

    private Genre(final AppUser user, final CodeValue genre, final Integer selfRating) {
        this.appuser = user;
        this.genre = genre;
        this.selfRating = selfRating;
    }
    
    public void update(final AppUser user) {
		this.appuser = user;
	}
	

}