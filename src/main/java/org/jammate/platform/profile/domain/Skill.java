package org.jammate.platform.profile.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.code.domain.CodeValue;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "skills")
public class Skill extends AbstractPersistable<Long> {
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuserId", referencedColumnName = "id", nullable = false)
	private AppUser appuser;
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "skillsCvId", nullable = false)
	private CodeValue skill;
	
	@Column(name="selfRating")
	private Integer selfRating;
	
	protected Skill() {
        //
    }
	
	public static Skill createNewWithoutUser(final CodeValue skill, final Integer selfRating) {
		return new Skill(null, skill, selfRating);
	}

    private Skill(final AppUser user, final CodeValue skill, final Integer selfRating) {
        this.appuser = user;
        this.skill = skill;
        this.selfRating = selfRating;
    }
    
    public void update(final AppUser user) {
		this.appuser = user;
	}
	

}
