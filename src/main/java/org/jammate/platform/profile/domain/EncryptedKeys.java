package org.jammate.platform.profile.domain;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.jpa.domain.AbstractPersistable;


@Entity
@Table(name = "encrypted_keys", uniqueConstraints = { @UniqueConstraint(columnNames = { "types", "key_type" }, name = "UQ_m_encrypted_keys_types") })
public class EncryptedKeys extends AbstractPersistable<Long> {

    @Column(name = "types", length = 100, nullable = false)
    private String types;

    @Column(name = "key_type", length = 100, nullable = false)
    private String keyType;

    @Column(name = "key_value", nullable = false)
    private Blob keyValue;

    EncryptedKeys() {}

    EncryptedKeys(final String types, final String keyType, final Blob keyValue) {
        this.types = types;
        this.keyType = keyType;
        this.keyValue = keyValue;
    }

    public static EncryptedKeys create(final String types, final String keyType, final Blob keyValue) {
        return new EncryptedKeys(types, keyType, keyValue);
    }

    public Blob getKeyValue() {
        return this.keyValue;
        
    }

}

