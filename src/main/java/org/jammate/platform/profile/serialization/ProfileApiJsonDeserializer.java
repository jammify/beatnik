package org.jammate.platform.profile.serialization;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.core.exception.ApiParameterError;
import org.jammate.platform.core.exception.DataValidatorBuilder;
import org.jammate.platform.core.exception.InvalidJsonException;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

@Component
public class ProfileApiJsonDeserializer {
	
	private final Set<String> supportedParameters = new HashSet<>(Arrays.asList("skills", "genres"));

    private final FromJsonHelper fromApiJsonHelper;

    @Autowired
    public ProfileApiJsonDeserializer(final FromJsonHelper fromApiJsonHelper) {
        this.fromApiJsonHelper = fromApiJsonHelper;
    }

    public void validateForCreate(final String json) {
        if (StringUtils.isBlank(json)) { throw new InvalidJsonException(); }

        final Type typeOfMap = new TypeToken<Map<String, Object>>() {}.getType();
        this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, json, this.supportedParameters);

        final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
        final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource("fund");

        final JsonElement element = this.fromApiJsonHelper.parse(json);

        
        this.fromApiJsonHelper.throwExceptionIfValidationWarningsExist(dataValidationErrors);
    }
    

}
