package org.jammate.platform.content.api;

public interface ContentApiConstants {
	
	public static final String tagParamName = "tag";
	public static final String typeParamName = "type";
	public static final String nameParamName = "name";
	public static final String articleTypeParamName = "articleType";
	public static final String authorParamName = "author";
	
	public static final String dateRangeParamName = "dateRange";
	public static final String imagesParamName = "images";
	public static final String imageParamName = "image";
	public static final String pagesParamName = "pages";
	
	public static final String timeRangeParamName = "timeRange";
	public static final String contactParamName = "contact";
	public static final String primaryContentParamName = "primaryContent";
	public static final String contentParamName = "content";

}
	
