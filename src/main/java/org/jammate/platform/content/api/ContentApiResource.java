package org.jammate.platform.content.api;

import java.util.List;

import org.jammate.platform.content.data.ContentData;
import org.jammate.platform.content.service.ContentService;
import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/content") 
public class ContentApiResource {
	
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final ContentService contentService;
	private final static Logger logger = LoggerFactory
			.getLogger(ContentApiResource.class);
	
	@Autowired
    public ContentApiResource(final ContentService contentService,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper) {
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.contentService = contentService;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveAllContent(@RequestParam(value="type", required=true) String type) {
		final List<ContentData> content = this.contentService.retrieveContent(type);
		return this.jsonSerializer.serialize(content);
    }
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String createContent(@RequestBody final String content)	 {
		final CommandProcessingResult response = this.contentService
				.submitContent(JsonCommand.from(content,
						new JsonParser().parse(content), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{contentId}", produces = "application/json; charset=utf-8")  
    public String retrieveContent(@PathVariable Long contentId,
    		@RequestParam(value="pageId", required=false) Long pageId) {
		final ContentData content = this.contentService.retrieveContent(contentId, pageId);
		return this.jsonSerializer.serialize(content);
    }
	


	@RequestMapping(method = RequestMethod.POST, value="/{contentId}/like",
			produces = "application/json; charset=utf-8")   
		public String likeContent(@PathVariable final long contentId) {
		final CommandProcessingResult response = this.contentService
		.likeContent(contentId);
		return this.jsonSerializer.serialize(response);
		
	
	}
	

	@RequestMapping(method = RequestMethod.POST, value="/{contentId}/unlike",
			produces = "application/json; charset=utf-8")   
		public String unlikeContent(@PathVariable final long contentId) {
		final CommandProcessingResult response = this.contentService
		.unLikeContent(contentId);
		return this.jsonSerializer.serialize(response);
		
	}


	@RequestMapping(method = RequestMethod.POST, value="/{contentId}/view",
			produces = "application/json; charset=utf-8")   
		public String viewContent(@PathVariable final long contentId) {
		final CommandProcessingResult response = this.contentService
		.viewContent(contentId);
		return this.jsonSerializer.serialize(response);
		
    }
}
	
	
