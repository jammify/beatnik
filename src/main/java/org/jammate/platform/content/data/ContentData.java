package org.jammate.platform.content.data;

import java.util.List;

public class ContentData {
	
	private final Long id;
	private final String dateRange;
	private final String timeRange;
	private final String author;
	private final String type;
	private final String articleType;
	private final String tags;
	private final Long primaryImageId;
	private final Long likes;
	private final Long views;
	private final String contact;
	private final String content; 
	
	private List<PageData> pages;
	
	
		
	
	
	public static ContentData instance(final Long id, final String dateRange,
			final String timeRange, final String author,
			final String type, final String articleType,
			final String tags, final long primaryImageId, final Long likes,
			final Long views, final String contact,
			final String content, final List<PageData> pages) {
		return new ContentData(id, dateRange, timeRange, author, type, tags, content, primaryImageId, likes, views,
				contact, content, pages);
	}
	
	private ContentData(final Long id, final String dateRange,
			final String timeRange, final String author,
			final String type, final String articleType,
			final String tags, final Long primaryImageId, final Long likes,
			final Long views, final String contact,
			final String content, final List<PageData> pages) {
		this.id = id;
		this.dateRange = dateRange;
		this.timeRange = timeRange;
		this.author = author;
		this.type = type;
		this.articleType = articleType;
		this.tags = tags;
		this.primaryImageId = primaryImageId;
		this.likes = likes;
		this.views = views;
		this.contact = contact;
		this.content = content;
		this.pages = pages;
		
	}
	
	public void setPages(final List<PageData> pages) {
		this.pages = pages;
		
	}

}
