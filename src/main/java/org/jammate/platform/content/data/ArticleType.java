package org.jammate.platform.content.data;

import java.util.HashMap;
import java.util.Map;

public enum ArticleType {
	SINGLE(100, "Single"), MULTIPLE(200, "Multiple");
	
    private final Integer value;
    private final String code;

    private ArticleType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, ArticleType> intToEnumMap = new HashMap<>();
    private static final Map<String, ArticleType> stringToEnumMap = new HashMap<>();
    static {
        for (final ArticleType type : ArticleType.values()) {
            intToEnumMap.put(type.value, type);
            stringToEnumMap.put(type.code.toLowerCase(), type);
        }
    }

    public static ArticleType fromInt(final int i) {
        final ArticleType type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }
    
    public static ArticleType fromString(final String status) {
        final ArticleType type = stringToEnumMap.get(status);
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }


}



