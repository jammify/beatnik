package org.jammate.platform.content.data;

import java.util.HashMap;
import java.util.Map;

public enum ContentType {
	ARTICLE(100, "Article"), EVENTS(200, "Event");
	
    private final Integer value;
    private final String code;

    private ContentType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, ContentType> intToEnumMap = new HashMap<>();
    private static final Map<String, ContentType> stringToEnumMap = new HashMap<>();
    static {
        for (final ContentType type : ContentType.values()) {
            intToEnumMap.put(type.value, type);
            stringToEnumMap.put(type.code.toLowerCase(), type);
        }
    }

    public static ContentType fromInt(final int i) {
        final ContentType type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }
    
    public static ContentType fromString(final String status) {
        final ContentType type = stringToEnumMap.get(status);
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }


}
