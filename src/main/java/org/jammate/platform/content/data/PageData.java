package org.jammate.platform.content.data;

public class PageData {
	
	
	private final String content;
	private final Long imageId;
	
	
	public static PageData instance(final String content,
			final Long imageId) {
		return new PageData(content, imageId);
	}
	
	private PageData(final String content,
			final Long imageId) {
		this.content = content;
		this.imageId = imageId;
	
	}

	
	

}
