package org.jammate.platform.content.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.content.data.ContentData;
import org.jammate.platform.content.data.ContentType;
import org.jammate.platform.content.data.PageData;
import org.jammate.platform.content.domain.Content;
import org.jammate.platform.content.domain.ContentPieceRepository;
import org.jammate.platform.content.domain.Page;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.GlobalEntityType;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.service.DocumentWritePlatformService;
import org.jammate.platform.orders.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class ContentService {
	
	private final static Logger logger = LoggerFactory
			.getLogger(OrderService.class);
	private final JdbcTemplate jdbcTemplate;
	private final ContentPieceRepository contentRepository;
	private final ContentAssembler contentAssembler;
	private final PlatformSecurityContext context;
	private final DocumentWritePlatformService documentService;
	
	@Autowired
	public ContentService(
			final ContentAssembler contentAssembler,
			final DataSource dataSource,
			final ContentPieceRepository contentRepository,
			final DocumentWritePlatformService documentService,
			final PlatformSecurityContext context) {
		this.jdbcTemplate = new JdbcTemplate();
		this.jdbcTemplate.setDataSource(dataSource);
		this.contentRepository = contentRepository;
		this.contentAssembler = contentAssembler;
		this.context = context;
		this.documentService = documentService;
	}
	
	public CommandProcessingResult submitContent(final JsonCommand content) {
		final Content actualContent = this.contentAssembler.assembleFrom(content.parsedJson());
		this.contentRepository.save(actualContent);
		final List<String> images = actualContent.getimages();
		int imageCount = 0;
		for(String image : images) {
					byte[] imageByteArray = null;
					if(image.startsWith("data"))
						imageByteArray = Base64.decodeBase64(image.split(",")[1]);
					else
						imageByteArray = Base64.decodeBase64(image);
					InputStream inputStream = new ByteArrayInputStream(imageByteArray);
					final ByteArrayOutputStream baos = new ByteArrayOutputStream();
					final String fileType = "image/jpeg";
					final String fileName = RandomStringUtils.randomAlphabetic(10) + ".jpg";
					final DocumentCommand documentCommand = new DocumentCommand(null, null,
							GlobalEntityType.CONTENT.getCode(), actualContent.getId(), fileName,
							fileName, Long.valueOf(imageByteArray.length), fileType, null, null, null, null, null, null);

					
			        final Long documentId = this.documentService.createDocument(documentCommand, inputStream);
			        if(imageCount == 0) {
			        		actualContent.setPrimaryImageId(documentId);
			        		this.contentRepository.save(actualContent);
			        }
			        imageCount++;
		}
		
		//For saving images inside page objects
		final Set<Page> pages = actualContent.getPages();
		for(Page page : pages ) {
			final String image = page.getImage();
			byte[] imageByteArray = null;
			if(image.startsWith("data"))
				imageByteArray = Base64.decodeBase64(image.split(",")[1]);
			else
				imageByteArray = Base64.decodeBase64(image);
			InputStream inputStream = new ByteArrayInputStream(imageByteArray);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final String fileType = "image/jpeg";
			final String fileName = RandomStringUtils.randomAlphabetic(10) + ".jpg";
			final DocumentCommand documentCommand = new DocumentCommand(null, null,
					GlobalEntityType.CONTENT.getCode(), actualContent.getId(), fileName,
					fileName, Long.valueOf(imageByteArray.length), fileType, null, null,
					GlobalEntityType.PAGE.getCode(), page.getId(), null, null);
			
	        final Long documentId = this.documentService.createDocument(documentCommand, inputStream);
		}
		return CommandProcessingResult.fromDetails(actualContent.getId(), null);
	}
	
	public List<ContentData> retrieveContent(final String type) {
		final Integer contentType = ContentType.fromString(type).getValue();
		final ContentDataMapper cm = new ContentDataMapper(this.jdbcTemplate);
		String contentSql = "select " + cm.contentSchema() 
		+  " where `type` = ?" ;
		final List<ContentData> contents = this.jdbcTemplate.query(contentSql, cm,
				new Object[] { contentType });
		return contents;
	}
	
	public ContentData retrieveContent(final Long contentId, final Long pageId) {
		final ContentDataMapper cm = new ContentDataMapper(this.jdbcTemplate);
		String contentSql = "select " + cm.contentSchema() 
		+  " where `id` = ?" ;
		final ContentData content = this.jdbcTemplate.queryForObject(contentSql, cm,
				new Object[] { contentId });
		return content;
	}
	
	public CommandProcessingResult likeContent(final Long contentId) {
		this.jdbcTemplate.update("UPDATE `content` SET likes = IFNULL(likes, 1) +1 WHERE id = ?",
				new Object[] {contentId});
		return CommandProcessingResult.fromDetails(contentId, null);
	}
	
	public CommandProcessingResult unLikeContent(final Long contentId) {
		this.jdbcTemplate.update("UPDATE `content` SET likes = IFNULL(likes, 1) -1 WHERE id = ?",
				new Object[] {contentId});
		return CommandProcessingResult.fromDetails(contentId, null);
	}
	
	
	public CommandProcessingResult viewContent(final Long contentId) {
		this.jdbcTemplate.update("UPDATE `content` SET views = IFNULL(views, 1) +1 WHERE id = ?",
				new Object[] {contentId});
		return CommandProcessingResult.fromDetails(contentId, null);
	}
	
	private static final class ContentDataMapper implements RowMapper<ContentData> {
    	
		final JdbcTemplate jdbcTemplate;
		
    	public ContentDataMapper(final JdbcTemplate jdbcTemplate) {
    		this.jdbcTemplate = jdbcTemplate;
		}
	
	    public String contentSchema() {
		    	final StringBuilder sqlBuilder = new StringBuilder();
	    		sqlBuilder
	    		.append(" id,  primaryContent, primaryImageId, dateRange, timeRange, author, `type`, articleType, tags, likes, views, contact ")
	    		.append(" from content ");
	    		return sqlBuilder.toString();
	    		
	    }
	
	    @Override
	    public ContentData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	    		final Long id = rs.getLong("id");
	        final String dateRange = rs.getString("dateRange");
	        final String timeRange = rs.getString("timeRange");
	        final String author = rs.getString("author");
	        final String type = rs.getString("type");
	        final String articleType = rs.getString("articleType");
	        final String tags = rs.getString("tags");
	        final Long likes = rs.getLong("likes");
	        final String primaryContent = rs.getString("primaryContent");
	        final Long primaryImageId = rs.getLong("primaryImageId");
	        final Long views = rs.getLong("views");
	        final String contact = rs.getString("contact");
	        final List<PageData> pages = getPages(id);
	        
	        return ContentData.instance(id, dateRange, timeRange, author, type, articleType,
	        		tags, primaryImageId, likes, views, contact, primaryContent, pages);
	    }
	    
	    

        private List<PageData> getPages(final Long contentId) {

            final PageDataMapper rm = new PageDataMapper();
            final String sql = "select " + rm.schema()
                    + " where p.contentId= ?";

            final List<PageData> pages = this.jdbcTemplate.query(sql, rm,
                    new Object[]{contentId});

            return pages;
        }
	}
	
	 private static final class PageDataMapper implements RowMapper<PageData> {

	        public String schema() {
	            return " p.content as content, d.id as imageId  from page p inner join document d "
	            		+ " on d.subEntityId = p.id and d.subEntityType = 'page'";
	        }

	        @Override
	        public PageData mapRow(final ResultSet rs,
	                @SuppressWarnings("unused") final int rowNum)
	                throws SQLException {
	            final String content = rs.getString("content");
	            final Long imageId = rs.getLong("imageId");
	            return PageData.instance(content, imageId);
	        }
	    }
	
	
	
	
	
	
	
}
