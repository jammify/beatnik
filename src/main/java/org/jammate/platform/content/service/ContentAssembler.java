package org.jammate.platform.content.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.content.api.ContentApiConstants;
import org.jammate.platform.content.data.ArticleType;
import org.jammate.platform.content.data.ContentType;
import org.jammate.platform.content.domain.Content;
import org.jammate.platform.content.domain.Page;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
public class ContentAssembler {
	
	private final static Logger logger = LoggerFactory
			.getLogger(ContentAssembler.class);
	private final FromJsonHelper fromApiJsonHelper;
	private final PlatformSecurityContext context;
	 private transient List<String> images;

	
	
	@Autowired
	public ContentAssembler(
			final FromJsonHelper fromApiJsonHelper,
			final PlatformSecurityContext context) {
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.context = context;
	}
	
	public Content assembleFrom(final JsonElement element) {
		final JsonObject contentElement = element.getAsJsonObject();
		 //Assembling host
		 final String tags = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.tagParamName, contentElement);
		 final String type = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.typeParamName, contentElement);
		 final Integer contentType = ContentType.fromString(type.toLowerCase()).getValue();
		 final String articleType = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.articleTypeParamName, contentElement);
		 final Integer articleTypes = ArticleType.fromString(articleType.toLowerCase()).getValue();
		 final String author = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.authorParamName, contentElement);
		 final String dateRange = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.dateRangeParamName, contentElement);
		 final String timeRange = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.timeRangeParamName, contentElement);
		 final String name = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.nameParamName, contentElement);
		 final String contact = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.contactParamName, contentElement);
		 final String content = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.primaryContentParamName, contentElement);
		 final List<String> images = new ArrayList<> ();
         if(contentElement.has(ContentApiConstants.imagesParamName) && contentElement.get(ContentApiConstants.imagesParamName).getAsJsonArray() != null) {
        	 final JsonArray imagesArray = contentElement.get(ContentApiConstants.imagesParamName).getAsJsonArray();
     	 	for (int j = 0; j < imagesArray.size(); j++) {
     	 		final String base64Image = imagesArray.get(j).getAsString();
     	 		images.add(base64Image);
     	 	}
     	
         }
         
         final Set<Page> pages = new HashSet<>();
         if(contentElement.has(ContentApiConstants.pagesParamName) && contentElement.get(ContentApiConstants.pagesParamName).getAsJsonArray() != null) {
        	 	final JsonArray pagesArray = contentElement.get(ContentApiConstants.pagesParamName).getAsJsonArray();
     	 	for (int j = 0; j < pagesArray.size(); j++) {
     	 		final JsonObject pageObj = pagesArray.get(j).getAsJsonObject();
     	 		final String pageContent = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.contentParamName, pageObj);
     	 		final String image = this.fromApiJsonHelper.extractStringNamed(ContentApiConstants.imageParamName, pageObj);
     	 		pages.add(Page.instance(pageContent, image));
     	 	}
     	
         }
         
		 //		 final Array ImageIds = this.fromApiJsonHelper.extractIntegerNamed(ContentApiConstants.serviceTypeParamName, element);
//		 final Integer serviceType = this.fromApiJsonHelper.extractIntegerNamed(ContentApiConstants.serviceTypeParamName, element);
		 return Content.instance(dateRange, timeRange, name, author, contentType, images, articleTypes,
				 tags, content, contact, pages);
	}


		
}
