package org.jammate.platform.content.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "content")
public class Content extends AbstractPersistable<Long> {
	
	
	@LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentContent", orphanRemoval = true)
    private Set<Page> pages = null;
	
	@Column(name = "dateRange")
	private String dateRange;
	
	@Column(name = "timeRange")
	private String timeRange;
	
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "author")
	private String author;
	
	@Column(name = "type")
	private Integer type;
	
	@Column(name = "articleType")
	private Integer articleType;
	
	@Column(name = "tags")
	private String tags;
	
	@Column(name = "primaryImageId")
	private Long primaryImageId;
	
	@Column(name = "primaryContent")
	private String primaryContent;
	
	@Column(name = "likes")
	private Long likes;
	
	@Column(name = "views")
	private Long views;
	
	@Column(name = "contact")
	private String contact;
	
	
	private transient List<String> images;
	
	protected Content() {
		// default
	}
	
	public static Content instance(final String dateRange, final String timeRange, final String name,
			final String author, final Integer type,final List<String> images,
			final Integer articleType, final String tags, final String primaryContent,
			final String contact, final Set<Page> pages) {
		return new Content(dateRange, timeRange, name, author, type, images, articleType, tags, primaryContent, null, null,
				contact, pages);
	}
	
	private Content(final String dateRange, final String timeRange, final String name,
			final String author, final Integer type, final List<String> images,
			final Integer articleType, final String tags, final String primaryContent,
			final Long likes, final Long views,
			final String contact, final Set<Page> pages) {
		this.dateRange = dateRange;
		this.timeRange = timeRange;
		this.name = name;
		this.author = author;
		this.type = type;
		this.articleType = articleType;
		this.tags = tags;
		this.primaryContent = primaryContent;
		this.likes = likes;
		this.views = views;
		this.contact = contact;
		this.images = images;
		this.pages = associatePages(pages);
	}
	
	private Set<Page> associatePages(final Set<Page> pages) {
		for (final Page page : pages) {
			page.update(this);
	    }
	    return pages;
	}
	
	public Set<Page> getPages() {
		return pages;
	}
	
	
	  public List<String> getimages() {
	      return images;
	  }
	  
	  
	  public void setPrimaryImageId(final Long documentId) {
		  this.primaryImageId = documentId;
	  }
}

