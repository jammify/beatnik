package org.jammate.platform.content.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "page")
public class Page extends AbstractPersistable<Long> {
	
	@ManyToOne
    @JoinColumn(name = "contentId", referencedColumnName = "id", nullable = false)
	private Content parentContent;
	
	@Column(name = "content")
	private String content;
	
	private transient String image;
	
	protected Page() {
		
	}
	
	public static Page instance(final String content, final String image) {
		return new Page(content, image);
	}
	
	private Page(final String content, final String image) {
		this.content = content;
		this.image = image;
	}
	
	public void update(final Content parentContent) {
		this.parentContent = parentContent;
	}
	
	public String getImage() {
		return this.image;
	}
}
