package org.jammate.platform.content.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentPieceRepository extends JpaRepository<Content, String>,
	JpaSpecificationExecutor<Content> {


}
