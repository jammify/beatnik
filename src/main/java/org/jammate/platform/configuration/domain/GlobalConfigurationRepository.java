package org.jammate.platform.configuration.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GlobalConfigurationRepository extends JpaRepository<GlobalConfigurationProperty, Long>,
        JpaSpecificationExecutor<GlobalConfigurationProperty> {

    GlobalConfigurationProperty findOneByName(String name);
    
    List<GlobalConfigurationProperty> findByNameStartingWithIgnoreCase(String prefix);
}