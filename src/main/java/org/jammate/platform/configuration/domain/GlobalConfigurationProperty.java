package org.jammate.platform.configuration.domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.jammate.platform.configuration.domain.GlobalConfigurationPropertyConstants.CONFIGURATION_VALUE_TYPE;
import org.jammate.platform.core.exception.ApiParameterError;
import org.jammate.platform.core.exception.DataValidatorBuilder;
import org.jammate.platform.core.exception.PlatformApiDataValidationException;
import org.jammate.platform.core.infra.JsonCommand;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "configuration")
public class GlobalConfigurationProperty extends AbstractPersistable<Long> {

    @Column(name = "name", nullable = false)
    private final String name;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "value", nullable = true)
    private String value;

    @Column(name = "valueType", nullable = false)
    private int valueType;

    @Column(name = "description", nullable = true)
    private final String description;

    protected GlobalConfigurationProperty() {
        this.name = null;
        this.enabled = false;
        this.value = null;
        this.description = null;
    }

    public GlobalConfigurationProperty(final String name, final boolean enabled, final String value, final String description) {
        this.name = name;
        this.enabled = enabled;
        this.value = value;
        this.description = description;
        this.valueType = CONFIGURATION_VALUE_TYPE.INVALID.getValue();
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public String getValue() {
        return this.value;
    }

    public boolean updateTo(final boolean value) {
        final boolean updated = this.enabled != value;
        this.enabled = value;
        return updated;
    }

    public Map<String, Object> update(final JsonCommand command) {

        final Map<String, Object> actualChanges = new LinkedHashMap<>(7);

        final String enabledParamName = "enabled";
        if (command.isChangeInBooleanParameterNamed(enabledParamName, this.enabled)) {
            final Boolean newValue = command.booleanPrimitiveValueOfParameterNamed(enabledParamName);
            actualChanges.put(enabledParamName, newValue);
            this.enabled = newValue;
        }

        final String valueParamName = "value";
        if (command.isChangeInStringParameterNamed(valueParamName, this.value)) {
            final String newValue = command.stringValueOfParameterNamed(valueParamName);
            actualChanges.put(valueParamName, newValue);
            this.value = newValue;
            validateConfigurationValueType(newValue);
        }

        return actualChanges;

    }

    public static GlobalConfigurationProperty newSurveyConfiguration(final String name) {
        return new GlobalConfigurationProperty(name, false, null, null);
    }

    private void validateConfigurationValueType(final String newValue) {
        final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
        final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource(GlobalConfigurationPropertyConstants.CONFIGURATION_RESOURCE_NAME);
        CONFIGURATION_VALUE_TYPE type = CONFIGURATION_VALUE_TYPE.fromInt(valueType);
        switch (type) {
            case FLOAT:
                baseDataValidator.reset().parameter("value").value(newValue).zeroOrPositiveAmount();
            break;
            case INTEGER:
                baseDataValidator.reset().parameter("value").value(newValue).integerZeroOrGreater();
            break;
            case LONG:
                baseDataValidator.reset().parameter("value").value(newValue).longGreaterThanZero();
            break;
            default:
            break;
        }

        if (!dataValidationErrors.isEmpty()) { throw new PlatformApiDataValidationException(dataValidationErrors); }
    }

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getName(){
		return this.name;
	}
}