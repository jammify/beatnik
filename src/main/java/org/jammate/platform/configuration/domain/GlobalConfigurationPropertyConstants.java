package org.jammate.platform.configuration.domain;

import java.util.HashMap;
import java.util.Map;

public class GlobalConfigurationPropertyConstants {
	
	public static final String CONFIGURATION_RESOURCE_NAME = "globalConfiguration";

    public static enum CONFIGURATION_VALUE_TYPE {
        INVALID(0, "invalid"), INTEGER(1, "integer"), LONG(2, "long"), FLOAT(3, "float"), STRING(4, "string");

        private final Integer value;
        private final String code;

        private CONFIGURATION_VALUE_TYPE(final Integer value, final String code) {
            this.value = value;
            this.code = code;
        }

        public Integer getValue() {
            return this.value;
        }

        public String getCode() {
            return this.code;
        }

        private static final Map<Integer, CONFIGURATION_VALUE_TYPE> intToEnumMap = new HashMap<>();
        static {
            for (final CONFIGURATION_VALUE_TYPE type : CONFIGURATION_VALUE_TYPE.values()) {
                intToEnumMap.put(type.value, type);
            }
        }

        public static CONFIGURATION_VALUE_TYPE fromInt(final int configurationType) {
            final CONFIGURATION_VALUE_TYPE type = intToEnumMap.get(Integer.valueOf(configurationType));
            return type;
        }

    }

}
