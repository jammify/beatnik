package org.jammate.platform.configuration.domain;

import org.jammate.platform.configuration.exception.GlobalConfigurationPropertyNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationDomainServiceJpa implements ConfigurationDomainService {

    private final GlobalConfigurationRepositoryWrapper globalConfigurationRepository;

    @Autowired
    public ConfigurationDomainServiceJpa(
            final GlobalConfigurationRepositoryWrapper globalConfigurationRepository) {
        this.globalConfigurationRepository = globalConfigurationRepository;
    }


    @Override
    public boolean isAmazonS3Enabled() {
        return this.globalConfigurationRepository.findOneByNameWithNotFoundDetection("amazon-S3").isEnabled();
    }



    @Override
    public Integer getAuthTimeOut() {
        Integer timeOut = 180;
        final String propertyName = "authentication-token-timeout-in-seconds";
        final GlobalConfigurationProperty property;
        try {
            property = this.globalConfigurationRepository.findOneByNameWithNotFoundDetection(propertyName);
            if (property.isEnabled()) {
                timeOut = Integer.parseInt(property.getValue());
            }
        } catch (final GlobalConfigurationPropertyNotFoundException e) {
            // do nothing
        }
        return timeOut;
    }


	@Override
	public Double getRazorpayGatewayCharge() {
	    Double razorpayGatewayCharge = null;
		final String propertyName = "razorpay-gateway-fee";
        final GlobalConfigurationProperty property = this.globalConfigurationRepository
        		.findOneByNameWithNotFoundDetection(propertyName);
        if (property.isEnabled()) {
        		razorpayGatewayCharge = Double.parseDouble(property.getValue());
        }
        return razorpayGatewayCharge;
	}


	@Override
	public Double getRazorpayTransferCharge() {
		Double razorpayTransferFee = null;
		final String propertyName = "razorpay-transfer-fee";
        final GlobalConfigurationProperty property = this.globalConfigurationRepository
        		.findOneByNameWithNotFoundDetection(propertyName);
        if (property.isEnabled()) {
        		razorpayTransferFee = Double.parseDouble(property.getValue());
        }
        return razorpayTransferFee;
	}


	@Override
	public Double getDefaultCommission() {
		Double defaultCommission = null;
		final String propertyName = "default-commission";
        final GlobalConfigurationProperty property = this.globalConfigurationRepository
        		.findOneByNameWithNotFoundDetection(propertyName);
        if (property.isEnabled()) {
        		defaultCommission = Double.parseDouble(property.getValue());
        }
        return defaultCommission;
	}



}
