package org.jammate.platform.configuration.domain;

public interface ConfigurationDomainService {

    boolean isAmazonS3Enabled();

    	Double getRazorpayGatewayCharge();
    	
    	Double getRazorpayTransferCharge();
    	
    	Double getDefaultCommission();
    
    Integer getAuthTimeOut();

}