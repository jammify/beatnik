package org.jammate.platform.configuration.exception;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

/**
 * A {@link RuntimeException} thrown when global configuration properties are
 * not found.
 */
public class GlobalConfigurationPropertyNotFoundException extends AbstractResourceNotFoundException {

    public GlobalConfigurationPropertyNotFoundException(final String propertyName) {
        super("error.msg.configuration.property.invalid", "Configuration property `" + propertyName + "` does not exist", propertyName);
    }

    public GlobalConfigurationPropertyNotFoundException(final Long configId) {
        super("error.msg.configuration.id.invalid", "Configuration identifier `" + configId + "` does not exist", configId);
    }
}