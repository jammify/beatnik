package org.jammate.platform.configuration.service;

import org.jammate.platform.document.data.S3CredentialsData;

public interface ExternalServicesReadPlatformService {

    S3CredentialsData getS3Credentials();
    
    String getRazorpaySecret();
    
    String getRazorpayApiKey();
    
    String getFirebaseAdminSDKKey();
    
    String getFCMEndPoint();
    
    String getFCMServerKey();
    

}