package org.jammate.platform.configuration.service;

public class ExternalServicesConstants {

    public static final String S3_BUCKET_NAME = "s3_bucket_name";
    public static final String S3_ACCESS_KEY = "s3_access_key";
    public static final String S3_SECRET_KEY = "s3_secret_key";
    public static final String RAZORPAY_SECRET = "razorpaySecret";
    public static final String FIREBASE_KEY = "firebase_key";
    public static final String FCM_END_POINT = "fcm_end_point";

}
