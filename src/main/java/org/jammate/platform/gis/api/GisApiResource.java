package org.jammate.platform.gis.api;

import java.util.List;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.gis.domain.Location;
import org.jammate.platform.gis.domain.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@RequestMapping("/gis") 
public class GisApiResource {
	
	private final LocationRepository locationRepository;
	private final ApiSerializer<Location> jsonSerializer;
	private final static Logger logger = LoggerFactory
			.getLogger(GisApiResource.class);
	
	@Autowired
    public GisApiResource(final LocationRepository locationRepository,
    		final ApiSerializer<Location> jsonSerializer) {
		this.locationRepository = locationRepository;
		this.jsonSerializer = jsonSerializer;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveLocations() {
		List<Location> fetchedLocations = this.locationRepository.findAll();
		return this.jsonSerializer.serialize(fetchedLocations);
    }
	

}

