package org.jammate.platform.gis.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;


@Entity
@Table(name = "gis")
public class Location extends AbstractPersistable<Long> {

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "latitude", scale = 6, precision = 19, nullable = true)
    private BigDecimal latitude;

    @Column(name = "longitude", scale = 6, precision = 19, nullable = true)
    private BigDecimal longitude;
	
	@Column(name = "locationType")
    private Integer locationType;
	
	@Column(name="live")
	private Boolean live;

    public Location() {
        // TODO Auto-generated constructor stub
    }

    private Location(final String name, final BigDecimal latitude,
    		final BigDecimal longitude, final Integer locationType,
    		final Boolean live) {
    		this.name = name;
    		this.latitude = latitude;
    		this.longitude = longitude;
    		this.live = live;
    		this.locationType = locationType;
    }
}