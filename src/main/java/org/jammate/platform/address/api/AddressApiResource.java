package org.jammate.platform.address.api;

import java.util.List;

import org.jammate.platform.address.domain.Region;
import org.jammate.platform.address.domain.RegionRepository;
import org.jammate.platform.core.infra.ApiSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@RequestMapping("/regions") 
public class AddressApiResource {
	
	private final RegionRepository regionRepository;
	private final ApiSerializer<Region> jsonSerializer;
	private final static Logger logger = LoggerFactory
			.getLogger(AddressApiResource.class);
	
	@Autowired
    public AddressApiResource(final RegionRepository regionRepository,
    		final ApiSerializer<Region> jsonSerializer) {
		this.regionRepository = regionRepository;
		this.jsonSerializer = jsonSerializer;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveRegions(@RequestParam(value="levelId", required=false) String levelId,
    		@RequestParam(value="parentId", required=false) String parentId) {
		List<Region> fetchedRegions = null;
		if(levelId != null)
			 fetchedRegions = this.regionRepository.findByLevel(Long.parseLong(levelId));
		else if(parentId != null)
			fetchedRegions = this.regionRepository.findCitiesByState(Long.parseLong(parentId));
		if(fetchedRegions != null)
			return this.jsonSerializer.serialize(fetchedRegions);
		else
			return null;
    }
	

}
