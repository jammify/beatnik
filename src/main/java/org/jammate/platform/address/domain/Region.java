package org.jammate.platform.address.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.core.exception.PlatformApiDataValidationException;
import org.springframework.data.jpa.domain.AbstractPersistable;


@Entity
@Table(name = "region", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "name", "hierarchy" }, name = "name_hierarchy_UNIQUE"),
        @UniqueConstraint(columnNames = { "code", "hierarchy" }, name = "code_hierarchy_UNIQUE"),
        @UniqueConstraint(columnNames = { "name", "parentId", "levelId" }, name = "name_parent_level_UNIQUE"),
        @UniqueConstraint(columnNames = { "code", "parentId", "levelId" }, name = "code_parent_level_UNIQUE") })
public class Region extends AbstractPersistable<Long> {

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "code", length = 20)
    private String code;
    
    @Column(name = "censusCode", length = 19, nullable = true)
    private String censusCode;

//    @OneToMany(fetch = FetchType.LAZY)
//    @JoinColumn(name = "parentId")
//    private final List<Region> children = new LinkedList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    private Region parent;

    @Column(name = "levelId", nullable = false)
    private Integer levelId;

    @Column(name = "hierarchy", nullable = true, length = 200)
    private String hierarchy;

    public Region() {
        // TODO Auto-generated constructor stub
    }

    private Region(final String name, final String code, final Region parent, final Integer levelId, final String censusCode) {

        if (StringUtils.isNotBlank(name)) {
            this.name = name.trim();
        } else {
            this.name = null;
        }
        if (StringUtils.isNotBlank(code)) {
            this.code = code.trim();
        } else {
            this.code = null;
        }
        if (StringUtils.isNotBlank(censusCode)) {
            this.censusCode = censusCode.trim();
        } else {
            this.censusCode = null;
        }
        this.parent = parent;
        this.levelId = levelId;
    }




    public boolean isChildEntityOf(final Region parent) {
        if (this.parent != null && parent != null) { return this.parent.getId().equals(parent.getId()); }
        return false;
    }

    public boolean isNotChildEntityOf(final Region parent) {
        return !isChildEntityOf(parent);
    }

    private Long parentId() {
        Long parentId = null;
        if (this.parent != null) {
            parentId = this.parent.getId();
        }
        return parentId;
    }

    public void updateparent(final Region newParent) {

        if (identifiedBy(newParent.getId())) { throw new PlatformApiDataValidationException("error.msg.parent.cant.be.self",
        		"Parent cant be self", null); }
        this.parent = newParent;
        generateHierarchy();
    }

    public void generateHierarchy() {

        if (this.parent != null) {
            this.hierarchy = this.parent.hierarchyOf(getId());
        } else {
            this.hierarchy = ".";
        }
    }

    private String hierarchyOf(final Long id) {
        return this.hierarchy + id.toString() + ".";
    }

    public boolean identifiedBy(final Long id) {
        return getId().equals(id);
    }

    public String getName() {
        return this.name;
    }

    
    public Region getParent() {
        return this.parent;
    }

}
