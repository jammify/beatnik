package org.jammate.platform.address.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface RegionRepository extends JpaRepository<Region, Long>,
JpaSpecificationExecutor<Region> {

	@Query(value="select * from region r where r.levelId = :levelId", nativeQuery=true)
	public List<Region> findByLevel(@Param("levelId") final Long levelId);
	
	@Query(value="select * from region r where r.parentId = :stateId", nativeQuery=true)
	public List<Region> findCitiesByState(@Param("stateId") final Long stateId);
	


}
