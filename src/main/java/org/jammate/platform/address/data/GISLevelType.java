package org.jammate.platform.address.data;

import java.util.HashMap;
import java.util.Map;

public enum GISLevelType {
    INVALID(0, "gislevel.invalid"), COUNTRY(1, "gislevel.country"), STATE(2, "gislevel.state"), DISTRICT(3, "gislevel.district");

    private final Integer value;
    private final String code;

    private GISLevelType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, GISLevelType> intToEnumMap = new HashMap<>();
    static {
        for (final GISLevelType type : GISLevelType.values()) {
            intToEnumMap.put(type.value, type);
        }
    }

    public static GISLevelType fromInt(final int i) {
        final GISLevelType type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }
}
