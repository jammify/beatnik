package org.jammate.platform.core.email.data;


public class EmailData {

    private String fromUserName;
    private String[] toEmailIds;
    private String subject;
    private String message;

    public static EmailData instance(final String fromUserName, final String[] to, 
            final String subject, final String message) {
        return new EmailData(fromUserName, to, subject, message);
    }

    private EmailData(final String fromUserName, final String[] to, final String subject,
            final String message) {
        this.fromUserName = fromUserName;
        this.toEmailIds = to;
        this.subject = subject;
        this.message = message;
    }


    public String getFromUserName() {
        return this.fromUserName;
    }

    public String[] getTo() {
        return this.toEmailIds;
    }


    public String getSubject() {
        return this.subject;
    }

    public String getMessage() {
        return this.message;
    }


    public String setFromUserName(final String userName) {
        this.fromUserName = userName;
        return this.fromUserName;
    }

    public String setSubject(final String subject) {
        this.subject = subject;
        return this.subject;
    }

    public String setContent(final String content) {
        this.message = content;
        return this.message;
    }

    public String[] setTo(final String to) {
        this.toEmailIds = to.split(";");
        return this.toEmailIds;
    }
}