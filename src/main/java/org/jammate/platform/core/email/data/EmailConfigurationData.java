package org.jammate.platform.core.email.data;


public class EmailConfigurationData {

    private final String authUser;
    private final String authPass;
    private final String hostName;
    private final Boolean mailSmtpStartTlsEnabled;
    private final Integer portNumber;

    public static EmailConfigurationData instance(final String authUser, final String authPass, final String hostName,
            final Boolean mailSmtpStartTlsEnable, final Integer portNumber) {
        return new EmailConfigurationData(authUser, authPass, hostName, mailSmtpStartTlsEnable, portNumber);
    }

    private EmailConfigurationData(final String authUser, final String authPass, final String hostName,
            final Boolean mailSmtpStartTlsEnable, final Integer portNumber) {
        this.authPass = authPass;
        this.authUser = authUser;
        this.hostName = hostName;
        this.mailSmtpStartTlsEnabled = mailSmtpStartTlsEnable;
        this.portNumber = portNumber;
    }

    public String getAuthUser() {
        return this.authUser;
    }

    public String getAuthPass() {
        return this.authPass;
    }

    public String getHostName() {
        return this.hostName;
    }

    public Boolean isMailSmtpStartTlsEnabled() {
        return this.mailSmtpStartTlsEnabled;
    }

    public Integer getPortNumber() {
        return this.portNumber;
    }

}