package org.jammate.platform.core.email.exception;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class InvalidEmailAddressException extends AbstractRuntimeException {

    public InvalidEmailAddressException() {
        super("error.msg.invalid.email.address", "Given Email-Id is invalid");
    }

}
