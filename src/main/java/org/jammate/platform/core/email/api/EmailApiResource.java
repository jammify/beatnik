package org.jammate.platform.core.email.api;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.notifications.service.DeviceRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@RequestMapping("/email")
public class EmailApiResource {

	 private final FromJsonHelper fromApiJsonHelper;
     private final ApiSerializer jsonSerializer;

    @Autowired
    public EmailApiResource(
            final FromJsonHelper fromApiJsonHelper,
            final ApiSerializer jsonSerializer) {
        this.fromApiJsonHelper = fromApiJsonHelper;
        this.jsonSerializer = jsonSerializer;
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8") 
    public String sendEmail(@RequestBody final String device) {
    	
    		return null;
    }
}
