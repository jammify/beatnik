package org.jammate.platform.core.email.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.jammate.platform.core.infra.JdbcSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;


@Service
public class EmailConfigurationService {

    private final JdbcTemplate jdbcTemplate;
    private final EmailConfigMapper emailConfigMapper;

    @Autowired
    public EmailConfigurationService(final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.emailConfigMapper = new EmailConfigMapper();
    }

    public EmailConfigurationData getEmailConfig() {
        try {
            final String sql = "SELECT " + this.emailConfigMapper.schema();

            return this.jdbcTemplate.queryForObject(sql, this.emailConfigMapper);
        } catch (final EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final class EmailConfigMapper implements RowMapper<EmailConfigurationData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        public EmailConfigMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("ec.authUser as authUser, ec.authPass as authPass, ec.hostName as hostName, ");
            sqlBuilder.append("ec.mailSmtpStarttlsEnabled as mailSmtpStartTlsEnabled, ec.port as portNumber ");
            sqlBuilder.append("FROM email_config ec ");

            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public EmailConfigurationData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
            final String authUser = rs.getString("authUser");
            final String authPass = rs.getString("authPass");
            final String hostName = rs.getString("hostName");
            final Boolean mailSmtpStartTlsEnable = rs.getBoolean("mailSmtpStartTlsEnabled");
            final Integer portNumber = JdbcSupport.getInteger(rs, "portNumber");

            return EmailConfigurationData.instance(authUser, authPass, hostName, mailSmtpStartTlsEnable, portNumber);
        }

    }

}