package org.jammate.platform.core.email.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.jammate.platform.core.email.data.EmailData;
import org.jammate.platform.core.email.exception.EmailTemplateNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;


@Service
public class EmailTemplateService {

    private final JdbcTemplate jdbcTemplate;
    private final EmailTemplateMapper emailTemplateMapper;

    @Autowired
    public EmailTemplateService(final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.emailTemplateMapper = new EmailTemplateMapper();
    }

    public EmailData getEmailTemplateByName(final String templateName) {
        try {
            final String sql = "SELECT " + this.emailTemplateMapper.schema() + " WHERE cetc.`templateName` = '" + templateName + "'";
            return this.jdbcTemplate.queryForObject(sql, this.emailTemplateMapper);
        } catch (final EmptyResultDataAccessException e) {
            throw new EmailTemplateNotFoundException(templateName);
        } catch (DataAccessException exception){
        		exception.printStackTrace();
        }
        return null;
    }

    private static final class EmailTemplateMapper implements RowMapper<EmailData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        public EmailTemplateMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append(" cetc.`templateName` AS templateName, cetc.`fromName` AS fromUserName, cetc.`to` AS toEmailIds,");
            sqlBuilder.append(" cetc.`subject` AS subject, cetc.`body` AS message");
            sqlBuilder.append(" FROM email_template_config cetc");

            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public EmailData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
            final String fromUserName = rs.getString("fromUserName");
            final String toEmailIds = rs.getString("toEmailIds");
            String[] toIdsList = toEmailIds.split(";");
            final String emailSubject = rs.getString("subject");
            final String emailContent = rs.getString("message");

            return EmailData.instance(fromUserName, toIdsList, emailSubject, emailContent);
        }

    }

	public String findOne(String templateName) {
		// TODO Auto-generated method stub
		return null;
	}
}

