package org.jammate.platform.core.email.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.jammate.platform.core.email.data.EmailConfigurationData;
import org.jammate.platform.core.email.data.EmailConfigurationService;
import org.jammate.platform.core.email.data.EmailData;
import org.jammate.platform.core.email.exception.InvalidEmailAddressException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class MailService {
	
	private final EmailConfigurationService configurationReadService;
	private final EmailTemplateService emailTemplateService;
	
	@Autowired
    private MailService(final EmailConfigurationService configurationReadService,
    		final EmailTemplateService emailTemplateService) {
        this.configurationReadService = configurationReadService;
        this.emailTemplateService = emailTemplateService;
    }

	public void sendEmail(final String emailAddress, final String templateName, final Map<String, String> allowedParams) {
		
		final EmailData emailData = this.emailTemplateService.getEmailTemplateByName(templateName);
        final MustacheFactory mf = new DefaultMustacheFactory();
        final Mustache mustache = mf.compile(new StringReader(new Gson().toJson(emailData)), templateName);
        final StringWriter stringWriter = new StringWriter();
        mustache.execute(stringWriter, allowedParams);
		
		EmailConfigurationData emailConfig = this.configurationReadService.getEmailConfig();
		final MultiPartEmail email = new MultiPartEmail();

        // Very Important, Don't use email.setAuthentication()
        email.setAuthenticator(new DefaultAuthenticator(emailConfig.getAuthUser(), emailConfig.getAuthPass()));
        email.setDebug(true); // true if you want to debug
        email.setHostName(emailConfig.getHostName());

        try {
        	final JsonObject request = new JsonParser().parse(stringWriter.toString()).getAsJsonObject();
        	final String htmlRequest = request.get("message").getAsString();
        	
            email.getMailSession().getProperties()
                    .put("mail.smtp.starttls.enable", emailConfig.isMailSmtpStartTlsEnabled());
            email.getMailSession().getProperties().put("mail.smtp.port", emailConfig.getPortNumber());
            email.setFrom(emailConfig.getAuthUser(), emailData.getFromUserName());
            email.addTo(emailAddress);
            email.setSubject(emailData.getSubject());
            email.setContent(htmlRequest, "text/html");
            email.send();
        } catch (final EmailException e) {
        	e.printStackTrace();
            throw new InvalidEmailAddressException();
        } catch (Exception e) {
            e.printStackTrace();
        }
       
     }
}
