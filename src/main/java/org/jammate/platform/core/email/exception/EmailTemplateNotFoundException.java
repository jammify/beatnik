package org.jammate.platform.core.email.exception;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;

public class EmailTemplateNotFoundException extends  AbstractResourceNotFoundException {

    public EmailTemplateNotFoundException(final String templateName) {
        super("error.msg.generic.email.template.not.found", "Email Template Configuration with name '" + templateName + "' does not exist",
                templateName);
    }

}
