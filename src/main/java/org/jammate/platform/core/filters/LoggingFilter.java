package org.jammate.platform.core.filters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.jammate.platform.core.infra.ApiSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.GenericFilterBean;

@Service
public class LoggingFilter   extends GenericFilterBean {
	
	private final static Logger logger = LoggerFactory
			.getLogger(LoggingFilter.class);
	private final ApiSerializer<PlatformRequestLog> apiSerializer;
	
	@Autowired
	public LoggingFilter(final ApiSerializer<PlatformRequestLog> apiSerializer) {
		this.apiSerializer = apiSerializer;
	}
	
	 private class BufferedServletInputStream extends ServletInputStream {

	        ByteArrayInputStream bais;

	        public BufferedServletInputStream(ByteArrayInputStream bais) {
	            this.bais = bais;
	        }

	        @Override
	        public int available() {
	            return bais.available();
	        }

	        @Override
	        public int read() {
	            return bais.read();
	        }

	        @Override
	        public int read(byte[] buf, int off, int len) {
	            return bais.read(buf, off, len);
	        }

			@Override
			public boolean isFinished() {
				return bais.available() != 0;
			}

			@Override
			public boolean isReady() {
				return bais.available() != 0;
			}

			@Override
			public void setReadListener(ReadListener listener) {
				// TODO Auto-generated method stub
				
			}

	    }
	 
	 private static class ByteArrayServletStream extends ServletOutputStream {

	        ByteArrayOutputStream baos;

	        ByteArrayServletStream(ByteArrayOutputStream baos) {
	            this.baos = baos;
	        }

	        @Override
	        public void write(int param) throws IOException {
	            baos.write(param);
	        }

			@Override
			public boolean isReady() {
				return false;
			}

			@Override
			public void setWriteListener(WriteListener listener) {
				
			}
	    }
	 
	 
	 private static class ByteArrayPrintWriter {

	        private ByteArrayOutputStream baos = new ByteArrayOutputStream();

	        private PrintWriter pw = new PrintWriter(baos);

	        private ServletOutputStream sos = new ByteArrayServletStream(baos);

	        public PrintWriter getWriter() {
	            return pw;
	        }

	        public ServletOutputStream getStream() {
	            return sos;
	        }

	        byte[] toByteArray() {
	            return baos.toByteArray();
	        }
	    }
	
	
	private class BufferedRequestWrapper extends HttpServletRequestWrapper {

        ByteArrayInputStream bais;

        ByteArrayOutputStream baos;

        BufferedServletInputStream bsis;

        byte[] buffer;

        public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
            super(req);
//            if ("POST".equalsIgnoreCase(req.getMethod()) && req.getRequestURI().contains("order")) {
//           		logger.info(CharStreams.toString(req.getReader()));
//              return;
//           }
            InputStream is = req.getInputStream();
            baos = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int letti;
            while ((letti = is.read(buf)) > 0) {
                baos.write(buf, 0, letti);
            }

            buffer = baos.toByteArray();
        }

        @Override
        public ServletInputStream getInputStream() {
            try {
                bais = new ByteArrayInputStream(buffer);
                bsis = new BufferedServletInputStream(bais);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return bsis;
        }

        public byte[] getBuffer() {
            return buffer;
        }
        
        public void setBuffer(byte[] newbuffer)
        {
            buffer = newbuffer;
        }

    }
	
	@Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;
        BufferedRequestWrapper bufferedRequest = new BufferedRequestWrapper(request);
        final String requestBody = new String(bufferedRequest.getBuffer());
        final ByteArrayPrintWriter pw = new ByteArrayPrintWriter();
        HttpServletResponse wrappedResp = new HttpServletResponseWrapper(response) {

            @Override
            public PrintWriter getWriter() {
                return pw.getWriter();
            }

            @Override
            public ServletOutputStream getOutputStream() {
                return pw.getStream();
            }

        };
        
        response.setHeader("Cache-Control", "no-store,must-revalidate,no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        final String reqHead = request.getHeader("Access-Control-Request-Headers");

        if (null != reqHead && !reqHead.equals(null)) {
        		response.setHeader("Access-Control-Allow-Headers", reqHead);
        }
        logger.info(bufferedRequest.getMethod() + " - " + request.getRequestURI()
        		.substring(request.getContextPath().length() + 1));
        chain.doFilter(bufferedRequest, res);
	}

}
