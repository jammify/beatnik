package org.jammate.platform.core.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.StopWatch;

public class PlatformRequestLog {

    private final long startTime;
    private final long totalTime;
    private String key;
    private final String method;
    private String url;
    private final Map<String, String[]> parameters;
    private String requestBody;
    private String responseBody;

    public static PlatformRequestLog from(final StopWatch task, final HttpServletRequest request) throws IOException {
        final String requestUrl = request.getRequestURL().toString();

        final Map<String, String[]> parameters = new HashMap<>(request.getParameterMap());
        parameters.remove("password");
        parameters.remove("_");

        return new PlatformRequestLog(task.getStartTime(), task.getTime(), request.getMethod(), requestUrl, parameters);
    }

    public static PlatformRequestLog fromReqResBody(final StopWatch task, final HttpServletRequest request, final String requestBody,
            final String responseBody, final String key) throws IOException {

        final String requestUrl = request.getPathInfo().toString();

        final Map<String, String[]> parameters = new HashMap<>(request.getParameterMap());
        parameters.remove("password");
        parameters.remove("_");

        return new PlatformRequestLog(task.getStartTime(), task.getTime(), request.getMethod(), requestUrl, parameters, requestBody,
                responseBody, key);
    }

    private PlatformRequestLog(final long startTime, final long time, final String method, final String requestUrl,
            final Map<String, String[]> parameters) {
        this.startTime = startTime;
        this.totalTime = time;
        this.method = method;
        this.url = requestUrl;
        this.parameters = parameters;
    }

    private PlatformRequestLog(final long startTime, final long time, final String method, final String requestUrl,
            final Map<String, String[]> parameters, final String requestBody, final String responseBody, final String key) {
        this.startTime = startTime;
        this.totalTime = time;
        this.method = method;
        this.url = requestUrl;
        this.parameters = parameters;
        this.requestBody = requestBody;
        this.responseBody = responseBody;
        this.key = key;
    }

    public String getMethod() {
        return this.method;
    }

    public String getUrl() {
        return this.url;
    }

    
    public String getRequestBody() {
        return this.requestBody;
    }

    
    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    
    public String getResponseBody() {
        return this.responseBody;
    }

    
    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    
    public void setUrl(String url) {
        this.url = url;
    }

    
    public void setKey(String key) {
        this.key = key;
    }

    
    public long getStartTime() {
        return this.startTime;
    }

    
    public Map<String, String[]> getParameters() {
        return this.parameters;
    }

    
    public long getTotalTime() {
        return this.totalTime;
    }

    
    public String getKey() {
        return this.key;
    }
    
}
