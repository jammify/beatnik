package org.jammate.platform.core.filters;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;


@Component
public class RestAuthenticationEntryPoint 
	implements AuthenticationEntryPoint
{

    Logger logger = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);
    
    @Value("${org.jammate.running.env:devenv}")
    private String runningEnv;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {
        logger.info("Unauthorized access. Request URI: '" + request.getRequestURI() + "'");
      //for security fixes
        	response.setHeader("Access-Control-Allow-Origin", "*");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());

    }

}