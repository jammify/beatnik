package org.jammate.platform.core.infra;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StringUtil {
	
	public static String formatDate(final String date, final String destinationFormat) {
		DateTimeFormatter sourceFormat = DateTimeFormatter.ofPattern("ddMMMyyyy");
		DateTimeFormatter destFormat = DateTimeFormatter.ofPattern(destinationFormat);
		String destDate = null;
		
		if(destinationFormat.length() > 12) {
			LocalDateTime parsedDate = LocalDate.parse(date, sourceFormat).atStartOfDay();
		    destDate = parsedDate.format(destFormat);
		} else {
			LocalDate parsedDate = LocalDate.parse(date, sourceFormat);
			destDate = parsedDate.format(destFormat);
		}
	    return destDate;
	}
	
	public static String formatTime(final Integer hours, final Integer mins, final Boolean open) {
		String time = null;
		if(open) {
				String timeHourFrom = null;
				if(String.valueOf(hours).length() > 1)
					timeHourFrom = String.valueOf(hours);
				else
					timeHourFrom = "0" + String.valueOf(hours);
				String timeMinsFrom = null;
				if(String.valueOf(mins).length() > 1)
					timeMinsFrom = String.valueOf(mins);
				else
					timeMinsFrom = "0" + String.valueOf(mins);
				time = timeHourFrom + ":" + timeMinsFrom;
		} else {
				String timeHourTo = null;
				if(String.valueOf(hours).length() > 1)
					timeHourTo = String.valueOf(hours);
				else
					timeHourTo = "0" + String.valueOf(hours);
				String timeMinsTo = null;
				if(String.valueOf(mins).length() > 1)
					timeMinsTo = String.valueOf(mins);
				else
					timeMinsTo = "0" + String.valueOf(mins);
				time = timeHourTo + ":" + timeMinsTo;
		} 
		return time;
	}

}
