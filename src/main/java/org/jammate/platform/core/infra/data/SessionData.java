package org.jammate.platform.core.infra.data;

import java.io.Serializable;

import org.jammate.platform.auth.domain.AppUser;


public class SessionData implements Serializable{

	private final AppUser user;
	
	private final String token;
	

	public SessionData(AppUser user, String token) {
		super();
		this.user = user;
		this.token = token;
	}

	public AppUser getUser() {
		return this.user;
	}

	public String getToken() {
		return this.token;
	}

	
	public boolean equals(SessionData obj) {
		boolean result = false;
		if (this.getToken() != null && obj.getToken() != null) 
			result =  this.getToken().equals(obj.getToken());
		else if (this.getUser() != null && obj.getUser() != null)
			result = this.getUser().equals(obj.getUser());
		return result;
    }
	
	@Override
	public int hashCode() {
		return token.hashCode();
	}

}
