package org.jammate.platform.core.infra;


import java.util.HashMap;
import java.util.Map;



public enum GlobalEntityType {

        INVALID(0, "invalid"),
        STUDIO(1, "studio"),
        CONSUMER(2, "consumer"),
        BAND(3, "band"),
        EVENT(4, "event"),
        USER(5, "user"),
        JOBS(6, "jobs"),
        SMS(7, "sms"),
        DOCUMENTS(8, "documents"),
        NOTES(9, "templates"),
        CALENDAR(10, "calendar"),
        MEETINGS(11, "meetings"),
        HOLIDAYS(12, "holidays"),
        CODE_VALUE(13, "codevalue"),
        CODE(14, "code"),
        ROOMS(15, "rooms"),
        CONTENT(16, "content"),
        PAGE(17, "page");

    private final Integer value;
    private final String code;

    private static final Map<Integer, GlobalEntityType> intToEnumMap = new HashMap<>();
    private static final Map<String, GlobalEntityType> stringToEnumMap = new HashMap<>();
    private static int minValue;
    private static int maxValue;
    static {
        int i = 0;
        for (final GlobalEntityType entityType : GlobalEntityType.values()) {
            if (i == 0) {
                minValue = entityType.value;
            }
            intToEnumMap.put(entityType.value, entityType);
            stringToEnumMap.put(entityType.code, entityType);
            if (minValue >= entityType.value) {
                minValue = entityType.value;
            }
            if (maxValue < entityType.value) {
                maxValue = entityType.value;
            }
            i = i + 1;
        }
        
        i = 1;
    }

    private GlobalEntityType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }
    
    public static GlobalEntityType fromInt(final int i) {
        final GlobalEntityType entityType = intToEnumMap.get(Integer.valueOf(i));
        return entityType;
    }
    
    
    public static GlobalEntityType fromCode(final String key) {
        final GlobalEntityType entityType = stringToEnumMap.get(key);
        return entityType;
    }
    
    public static int getMinValue() {
        return minValue;
    }

    public static int getMaxValue() {
        return maxValue;
    }

    @Override
    public String toString() {
        return name().toString();
    }

    public boolean isCodeValueEntity() {
        return this.value.equals(GlobalEntityType.CODE_VALUE.getValue());
    }
}