package org.jammate.platform.core.reporting.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "parameter_master")
public class ParameterMaster extends AbstractPersistable<Long>{

	@Column(name = "name")
	private String name;
	
	@Column(name = "label")
	private String label;
			
	@Column(name = "parameterSql")
	private String parameterSql;
	
 
    
    public static ParameterMaster instance(final String name, final String label, final String parameterSql) {
		return new ParameterMaster(name,label,parameterSql);
		
	}

    protected ParameterMaster() {
    	
    }

    private ParameterMaster(final String name, final String label,
			final String parameterSql) {
		this.name = name;
		this.label= label;
		this.parameterSql = parameterSql;
		
    }
		
		public String getName() {
			return this.name;
		}
		
		public String getLabel() {
			return this.label;
		}
		
		public String getParametersql() {
			return this.parameterSql;
		}
		
		
		
	}



