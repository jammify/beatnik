package org.jammate.platform.core.reporting.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "report")
public class Report extends AbstractPersistable<Long>{

	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	private String type;
			
	@Column(name = "category")
	private String category;
	
    @Column(name = "reportSql")
    private String reportSql;
    
    @Column(name = "description")
    private String description;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "report", orphanRemoval = true, fetch=FetchType.EAGER)
    private Set<ReportParameter> reportParameter = new HashSet<>();
    
    
    public static Report instance(final String name, final String type, final String category,
			final String reportSql, final String description) {
		return new Report(name, type, category, reportSql, description);
		
	}

    protected Report() {
    	
    }

    private Report(final String name, final String type,
			final String category, final String reportSql, final String description) {
		this.name = name;
		this.type = type;
		this.category = category;
		this.reportSql = reportSql;
		this.description = description;
    }
    
	    public Set<ReportParameter> getReportParameter() {
	    	return this.reportParameter;
	    }
		
		public String getName() {
			return this.name;
		}
		
		public String getType() {
			return this.type;
		}
		
		public String getCategory() {
			return this.category;
		}
		
		public String getReportSql() {
			return this.reportSql;
		}
		
		public String getDescription() {
			return this.description;
		}
		
	}



