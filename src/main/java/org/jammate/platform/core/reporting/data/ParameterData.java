package org.jammate.platform.core.reporting.data;

public class ParameterData {
	
	private final String paramName;
	
	private final String paramValue;
	
	public static ParameterData instance(final String paramName, final String paramValue) {
		return new ParameterData(paramName, paramValue);
	}
	
	private ParameterData(final String paramName, final String paramValue) {
		this.paramName = paramName;
		this.paramValue = paramValue;
	}
	
	public String getParamName() {
		return this.paramName;
	}

	public String getParamValue() {
		return paramValue;
	}
	

}
