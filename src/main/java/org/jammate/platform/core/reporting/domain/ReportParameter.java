package org.jammate.platform.core.reporting.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "parameter")
public class ReportParameter extends AbstractPersistable<Long>{

	@ManyToOne(optional = false)
    @JoinColumn(name = "reportId", nullable = false)
	private Report report;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "parameterId", nullable = false)
	private ParameterMaster parameter;
			

     public static ReportParameter instance(final Report report, final ParameterMaster parameter) {
		return new ReportParameter(report, parameter);
		
	}

    protected ReportParameter() {
    	
    }

    private ReportParameter(final Report report, final ParameterMaster parameter) {
		this.report = report;
		this.parameter= parameter;
    }
	
    public Report getReport() {
    	return this.report;
    }
    
    public ParameterMaster getParameter() {
    	return this.parameter;
    }
		
		
		
		
}



