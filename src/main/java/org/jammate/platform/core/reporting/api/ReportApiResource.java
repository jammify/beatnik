package org.jammate.platform.core.reporting.api;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.core.reporting.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/reports") 
public class ReportApiResource {
	
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final ReportService reportService;
	private final static Logger logger = LoggerFactory
			.getLogger(ReportApiResource.class);
	
	@Autowired
    public ReportApiResource(final ReportService reportService,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper) {
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.reportService = reportService;
	}
	
	
	

	@RequestMapping(method = RequestMethod.POST, value="/{reportName}/execute",
			produces = "application/json; charset=utf-8")   
		public String reportName(@PathVariable final String reportName, 
				@RequestBody final String requestBody) {
		final JsonArray response = this.reportService.executeReport(reportName,
				JsonCommand.from(requestBody,
						new JsonParser().parse(requestBody), fromApiJsonHelper));
		return response.toString();
}
	
}