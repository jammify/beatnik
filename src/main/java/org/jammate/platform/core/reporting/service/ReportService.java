package org.jammate.platform.core.reporting.service;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.core.reporting.api.ReportApiConstants;
import org.jammate.platform.core.reporting.data.ParameterData;
import org.jammate.platform.core.reporting.domain.Report;
import org.jammate.platform.core.reporting.domain.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Service
public class ReportService {
	
	private final ReportRepository reportRepository;
	private final PlatformSecurityContext context;
	private final JdbcTemplate jdbcTemplate;
	private final FromJsonHelper jsonHelper;
	
	@Autowired
	public ReportService(final DataSource dataSource,
			final ReportRepository reportRepository,
			final PlatformSecurityContext context,
			final FromJsonHelper jsonHelper){
		this.context = context;
		this.jdbcTemplate = new JdbcTemplate();
	    this.jdbcTemplate.setDataSource(dataSource);
	    this.reportRepository = reportRepository;
	    this.jsonHelper = jsonHelper;
	}

	
    
    public JsonArray executeReport(final String reportName, final JsonCommand command) {
    	final JsonArray parameterArray = command.parsedJson().getAsJsonArray();
    	final List<ParameterData> params = new ArrayList<>();
    	for(int i=0; i < parameterArray.size(); i++) {
    		 final JsonObject parameter = parameterArray.get(i).getAsJsonObject();
	   		 final String  paramName = this.jsonHelper.extractStringNamed(ReportApiConstants.paramNameParamName, parameter);
	   		 final String paramValue = this.jsonHelper.extractStringNamed(ReportApiConstants.paramValueParamName, parameter);
	   		 params.add(ParameterData.instance(paramName, paramValue));
    	}
    	
    	final  Report report = reportRepository.findOneByName(reportName);
    	String reportSql = report.getReportSql();
    	
    	for(ParameterData parameter : params) {
    		String replacePattern = "\\$\\{" + parameter.getParamName() + "\\}";
    		reportSql = reportSql.replaceAll(replacePattern, parameter.getParamValue());
    	}
    		
    	final SqlRowSet rs = this.jdbcTemplate.queryForRowSet(reportSql);
        final SqlRowSetMetaData rsmd = rs.getMetaData();
        final JsonArray arr = new JsonArray();

        while (rs.next()) {
            final JsonObject obj = new JsonObject();
            for(int i=0; i < rsmd.getColumnCount(); i++) {
                final String columnName = rsmd.getColumnName(i + 1);
                final String columnValue = rs.getString(columnName);
                obj.addProperty(columnName, columnValue);
           }
            arr.add(obj);
        }
    	return arr;
    }
    	
}