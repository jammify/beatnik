package org.jammate.platform.core.outbound;

import java.util.Map;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.QueryMap;


public interface OutboundService {
	
	@GET("/json")
	Response sendRequestWithBounds(@QueryMap Map<String, String> map);

}
