package org.jammate.platform.core.exception;

public interface AbstractExceptionMethods {
	

	public String getGlobalisationMessageCode();

    public String getDefaultUserMessage();

    public Object[] getDefaultUserMessageArgs();

}
