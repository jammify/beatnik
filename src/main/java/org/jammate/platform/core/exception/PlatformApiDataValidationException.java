package org.jammate.platform.core.exception;

import java.util.List;

public class PlatformApiDataValidationException extends AbstractRuntimeException {

    private final List<ApiParameterError> errors;
    public static final String DEFAULT_MESSAGE_CODE = "validation.msg.validation.errors.exist";
    public static final String DEFAULT_USER_MESSAGE = "Validation errors exist.";

    public PlatformApiDataValidationException(final List<ApiParameterError> errors) {
    	super(DEFAULT_MESSAGE_CODE, DEFAULT_USER_MESSAGE, "");
        this.errors = errors;
    }

    public PlatformApiDataValidationException(final String globalisationMessageCode, final String defaultUserMessage,
            final List<ApiParameterError> errors) {
    	super(globalisationMessageCode, defaultUserMessage, "");
        this.errors = errors;
    }

    public List<ApiParameterError> getErrors() {
        return this.errors;
    }
}