package org.jammate.platform.core.exception;


/**
 * Common User defined Abstract Exception Class extend by RuntimeException,
 * Other user defined exception has to implement AbstractRuntimeException
 * 
 */
public class AbstractRuntimeException  extends PlatformException implements AbstractExceptionMethods {

	private final String globalisationMessageCode;
    private final String defaultUserMessage;
    private final Object[] defaultUserMessageArgs;

    /**
     * @param globalisationMessageCode	
     * @param defaultUserMessage		
     * @param defaultUserMessageArgs	
     */
    public AbstractRuntimeException(final String globalisationMessageCode, final String defaultUserMessage,
            final Object... defaultUserMessageArgs) {
    	super(globalisationMessageCode, defaultUserMessage, defaultUserMessageArgs);
        this.globalisationMessageCode = globalisationMessageCode;
        this.defaultUserMessage = defaultUserMessage;
        this.defaultUserMessageArgs = defaultUserMessageArgs;
    }
    
    public AbstractRuntimeException(Throwable cause) {
    	super(cause.getLocalizedMessage(), cause.getMessage());
		this.globalisationMessageCode = cause.getLocalizedMessage();
		this.defaultUserMessage = cause.getMessage();
		this.defaultUserMessageArgs = null;
	}
    
    public AbstractRuntimeException(){
    	super("", "");
    	this.globalisationMessageCode = "";
		this.defaultUserMessage = "";
		this.defaultUserMessageArgs = null;
    }
    
	public AbstractRuntimeException(String message) {
		super(message, message);
		this.globalisationMessageCode = message;
		this.defaultUserMessage = message;
		this.defaultUserMessageArgs = null;
	}

	@Override
	public String getGlobalisationMessageCode() {
        return this.globalisationMessageCode;
    }

    @Override
	public String getDefaultUserMessage() {
        return this.defaultUserMessage;
    }

    @Override
	public Object[] getDefaultUserMessageArgs() {
        return this.defaultUserMessageArgs;
    }
}