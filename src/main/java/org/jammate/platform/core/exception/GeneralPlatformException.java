package org.jammate.platform.core.exception;

public class GeneralPlatformException extends AbstractRuntimeException {
	
	public GeneralPlatformException(final String globalisationMessageCode, final String defaultUserMessage,
            final Object... defaultUserMessageArgs) {
    	super(globalisationMessageCode, defaultUserMessage, defaultUserMessageArgs);
    }

}
