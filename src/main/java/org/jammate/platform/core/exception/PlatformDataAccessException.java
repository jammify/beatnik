package org.jammate.platform.core.exception;

/**
 * A {@link RuntimeException} thrown when data integrity problems happen due to
 * state modifying actions.
 */
public class PlatformDataAccessException extends AbstractRuntimeException {

    private final String parameterName;

    public PlatformDataAccessException() {
    	super("data.access.exception", "Unknown data access exception. Please contact system administrator.", "");
        this.parameterName = null;
    }

    public PlatformDataAccessException(final String globalisationMessageCode, final String defaultUserMessage,
            final String parameterName, final Object... defaultUserMessageArgs) {
    	super(globalisationMessageCode, defaultUserMessage, defaultUserMessageArgs);
        this.parameterName = parameterName;
    }

    public String getParameterName() {
        return this.parameterName;
    }
}