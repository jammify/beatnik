package org.jammate.platform.core.exceptionmappers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jammate.platform.core.exception.AbstractResourceNotFoundException;
import org.jammate.platform.core.exception.AbstractRuntimeException;
import org.jammate.platform.core.exception.PlatformException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

@ControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Throwable.class)
    @ResponseBody
    ResponseEntity<Object> handleControllerException(HttpServletRequest req, Throwable ex) {
		final JsonObject errorResponse = new JsonObject();
        if(ex instanceof AbstractResourceNotFoundException) {
        		populateErrorResponseMessage(errorResponse,(PlatformException) ex);
            return new ResponseEntity<Object>(errorResponse.toString(), HttpStatus.NOT_FOUND);
        } else if(ex instanceof AbstractRuntimeException) {
                populateErrorResponseMessage(errorResponse,(PlatformException) ex);
                return new ResponseEntity<Object>(errorResponse.toString(), HttpStatus.BAD_REQUEST);
        } else if(ex instanceof DataAccessException) {
	        	ex.printStackTrace();
	        	return new ResponseEntity<Object>(errorResponse.toString(), HttpStatus.SERVICE_UNAVAILABLE);
        } else if(ex instanceof JsonSyntaxException) {
        		ex.printStackTrace();
        		return new ResponseEntity<Object>(errorResponse.toString(), HttpStatus.BAD_REQUEST);
        } else {
        		ex.printStackTrace();
            return new ResponseEntity<Object>(errorResponse.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String,String> responseBody = new HashMap<>();
        responseBody.put("path",request.getContextPath());
        responseBody.put("message","The URL you have reached is not in service at this time (404).");
        return new ResponseEntity<Object>(responseBody,HttpStatus.NOT_FOUND);
    }
    
    private void populateErrorResponseMessage(final JsonObject errorResponse,
    		final PlatformException exception) {
    	errorResponse.addProperty("globalisationMessageCode", exception.getGlobalisationMessageCode());
    	errorResponse.addProperty("defaultUserMessage", exception.getDefaultUserMessage());
    	final JsonArray defaultUserMessageArgs = new JsonArray();
    	for(int i=0; i < exception.getDefaultUserMessageArgs().length; i++)
    		defaultUserMessageArgs.add(exception.getDefaultUserMessageArgs()[i].toString());
    	errorResponse.add("defaultUserMessageArgs", defaultUserMessageArgs);
    }

}
