package org.jammate.platform.notifications.api;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.notifications.service.DeviceRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/devices")
public class DeviceRegistrationApiResource {

    private final DeviceRegistrationService deviceRegistrationWritePlatformService;
    private final FromJsonHelper fromApiJsonHelper;
    private final ApiSerializer jsonSerializer;

    @Autowired
    public DeviceRegistrationApiResource(
            final FromJsonHelper fromApiJsonHelper,
            final DeviceRegistrationService deviceRegistrationWritePlatformService,
            final ApiSerializer jsonSerializer) {
        this.fromApiJsonHelper = fromApiJsonHelper;
        this.deviceRegistrationWritePlatformService = deviceRegistrationWritePlatformService;
        this.jsonSerializer = jsonSerializer;
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8") 
    public String registerDevice(@RequestBody final String device) {
        CommandProcessingResult result = this.deviceRegistrationWritePlatformService
        		.registerDevice(JsonCommand.from(device,
        				new JsonParser().parse(device), fromApiJsonHelper));
        return this.jsonSerializer.serialize(result);
    }

//    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8") 
//    public String retrieveAllDeviceRegistrations() {
//
//
//        Collection<DeviceRegistrationData> deviceRegistrationDataList = null;
//        this.deviceRegistrationReadService
//                .retrieveAllDeviceRegiistrations();
//
//        return this.toApiJsonSerializer.serialize(deviceRegistrationDataList);
//    }

//    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
//    public String retrieveDeviceRegistrationByClientId(@PathParam("userId") final Long clientId, @Context final UriInfo uriInfo) {
//
//        this.context.authenticatedUser();
//
//        DeviceRegistrationData deviceRegistrationData = this.deviceRegistrationReadPlatformService
//                .retrieveDeviceRegiistrationByClientId(clientId);
//
//        return this.toApiJsonSerializer.serialize(deviceRegistrationData);
//    }
//
//    @GET
//    @Path("{id}")
//    @Consumes({ MediaType.APPLICATION_JSON })
//    @Produces({ MediaType.APPLICATION_JSON })
//    public String retrieveDeviceRegiistration(@PathParam("id") final Long id, @Context final UriInfo uriInfo) {
//
//        this.context.authenticatedUser();
//
//        DeviceRegistrationData deviceRegistrationData = this.deviceRegistrationReadPlatformService.retrieveDeviceRegiistration(id);
//
//        return this.toApiJsonSerializer.serialize(deviceRegistrationData);
//    }
//
//    @PUT
//    @Path("{id}")
//    @Consumes({ MediaType.APPLICATION_JSON })
//    @Produces({ MediaType.APPLICATION_JSON })
//    public String updateDeviceRegistration(@PathParam("id") final Long id, final String apiRequestBodyAsJson) {
//
//        this.context.authenticatedUser();
//
//        Gson gson = new Gson();
//        JsonObject json = new Gson().fromJson(apiRequestBodyAsJson, JsonObject.class);
//        Long clientId = json.get(DeviceRegistrationApiConstants.clientIdParamName).getAsLong();
//        String registrationId = json.get(DeviceRegistrationApiConstants.registrationIdParamName).getAsString();
//        DeviceRegistration deviceRegistration = this.deviceRegistrationWritePlatformService.updateDeviceRegistration(id, clientId,
//                registrationId);
//        String response = gson.toJson(deviceRegistration.getId());
//        return response;
//    }
//
//    @DELETE
//    @Path("{id}")
//    @Consumes({ MediaType.APPLICATION_JSON })
//    @Produces({ MediaType.APPLICATION_JSON })
//    public String delete(@PathParam("id") final Long id) {
//        
//        this.context.authenticatedUser();
//        this.deviceRegistrationWritePlatformService.deleteDeviceRegistration(id);
//        return responseMap(id);
//        
//    }
//    
//    public String responseMap(Long id){
//        HashMap<String, Object> responseMap = new HashMap<>();
//        responseMap.put("resource", id);
//        return new Gson().toJson(responseMap);
//    }

}
