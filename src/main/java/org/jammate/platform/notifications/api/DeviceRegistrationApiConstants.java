package org.jammate.platform.notifications.api;

public class DeviceRegistrationApiConstants {
	public static final String userIdParamName = "userId";
	public static final String registrationIdParamName = "token";

}
