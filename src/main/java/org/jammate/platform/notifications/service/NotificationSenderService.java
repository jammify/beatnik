package org.jammate.platform.notifications.service;

import java.util.ArrayList;
import java.util.List;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.domain.AppUserRepository;
import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.configuration.service.ExternalServicesReadPlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;

@Service
public class NotificationSenderService {
	
	private final static Logger logger = LoggerFactory.getLogger(NotificationSenderService.class);

	private ExternalServicesReadPlatformService propertiesReadPlatformService;
	private AppUserRepository appUserRepository;
	private final PlatformSecurityContext context;

	@Autowired
	public NotificationSenderService(
			final AppUserRepository appUserRepository,
			final ExternalServicesReadPlatformService propertiesReadPlatformService,
			final PlatformSecurityContext context) {
		this.propertiesReadPlatformService = propertiesReadPlatformService;
		this.appUserRepository = appUserRepository;
		this.context = context;
	}

//	public void sendNotification(List<SmsMessage> smsMessages) {
//		Map<Long, List<SmsMessage>> notificationByEachClient = getNotificationListByClient(smsMessages);
//		for (Map.Entry<Long, List<SmsMessage>> entry : notificationByEachClient
//				.entrySet()) {
//			this.sendNotifiaction(entry.getKey(), entry.getValue());
//		}
//	}

//	public Map<Long, List<SmsMessage>> getNotificationListByClient(
//			List<SmsMessage> smsMessages) {
//		Map<Long, List<SmsMessage>> notificationByEachClient = new HashMap<>();
//		for (SmsMessage smsMessage : smsMessages) {
//			if (smsMessage.getClient() != null) {
//				Long clientId = smsMessage.getClient().getId();
//				if (notificationByEachClient.containsKey(clientId)) {
//					notificationByEachClient.get(clientId).add(smsMessage);
//				} else {
//					List<SmsMessage> msgList = new ArrayList<>(
//							Arrays.asList(smsMessage));
//					notificationByEachClient.put(clientId, msgList);
//				}
//
//			}
//		}
//		return notificationByEachClient;
//	}

	public void sendNotification(Long appuserId, String message, final String title) {
		final AppUser user = this.appUserRepository.findOne(appuserId);
		String fcmEndPoint = this.propertiesReadPlatformService.getFCMEndPoint();
		String serverKey = this.propertiesReadPlatformService.getFCMServerKey();
		String registrationId = null;
		if (user != null) {
			registrationId = user.getInstanceId();
		}
		
		try {
			
			String topic = this.context.authenticatedUser().getName().split(" ")[0];
			AndroidConfig config = AndroidConfig.builder()
					.setTtl(3600 * 1000)
			        .setPriority(AndroidConfig.Priority.HIGH)
			        .setNotification(AndroidNotification.builder().setTitle(title).setBody(message).build())
			        .build();
			Message msg = Message.builder()
				    .setAndroidConfig(config) 
				    .setToken(registrationId)
				    .build();
			Message msg2 = Message.builder()
				    .setAndroidConfig(config) 
				    .setTopic(topic)
				    .build();

				// Send a message to the device corresponding to the provided
				// registration token.
			final List<String> tokens = new ArrayList<>();
			tokens.add(registrationId);
			FirebaseMessaging.getInstance().subscribeToTopic(tokens, topic);
			String response = FirebaseMessaging.getInstance().send(msg);
			FirebaseMessaging.getInstance().send(msg2);
			
			// Response is a message ID string.
			logger.info("Successfully sent message: " + response);
//			Notification notification = new Notification.Builder(
//					GcmConstants.defaultIcon).title(title)
//					.body(message).build();
//			Builder b = new Builder();
//			b.notification(notification);
//			b.dryRun(false);
//			b.contentAvailable(true);
//			b.timeToLive(GcmConstants.TIME_TO_LIVE);
//			b.priority(Priority.HIGH);
//			b.delayWhileIdle(true);
//			b.delivery_receipt_requested(true);
//			Message msg = b.build();
//			Sender s = new Sender(serverKey,
//					fcmEndPoint);
//			Result res;
//
//			res = s.send(msg, registrationId, 3);
			
			
//			if (res.getSuccess() != null && res.getSuccess()>0) {
//				smsMessage.setStatusType(SmsMessageStatusType.SENT
//						.getValue());
//				smsMessage.setDeliveredOnDate(DateUtils.getLocalDateOfTenant().toDate());
//			} else if (res.getFailure() != null && res.getFailure()>0) {
//				smsMessage.setStatusType(SmsMessageStatusType.FAILED
//						.getValue());
//			}
		} catch(FirebaseMessagingException fme) {
			fme.printStackTrace();
		}

	}

}
