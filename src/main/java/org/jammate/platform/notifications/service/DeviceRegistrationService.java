package org.jammate.platform.notifications.service;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.domain.AppUserRepository;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.notifications.api.DeviceRegistrationApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeviceRegistrationService {

	private final AppUserRepository userRepository;
	private final FromJsonHelper fromApiJsonHelper;

	@Autowired
	public DeviceRegistrationService(
			final AppUserRepository userRepository,
			final FromJsonHelper fromApiJsonHelper) {
		this.userRepository = userRepository;
		this.fromApiJsonHelper = fromApiJsonHelper;
	}

	@Transactional
	public CommandProcessingResult registerDevice(final JsonCommand command) {
		final Long userId = this.fromApiJsonHelper
				.extractLongNamed(DeviceRegistrationApiConstants.userIdParamName,
						command.parsedJson());
		final String token = this.fromApiJsonHelper
				.extractStringNamed(DeviceRegistrationApiConstants.registrationIdParamName,
						command.parsedJson());
		AppUser user = this.userRepository.findOne(userId);
		user.setInstanceId(token);
		this.userRepository.save(user);
		return CommandProcessingResult.fromDetails(user.getId(), null);

	}

	

}
