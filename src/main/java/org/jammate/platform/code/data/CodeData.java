package org.jammate.platform.code.data;

import java.io.Serializable;


public class CodeData implements Serializable {

    private final Long id;
    private final String name;
    @SuppressWarnings("unused")
    private final boolean systemDefined;
    @SuppressWarnings("unused")
    private final String label;
    @SuppressWarnings("unused")
    private final String context;

    public static CodeData instance(final Long id, final String name, final boolean systemDefined, final String label) {
        final String context = null;
        return new CodeData(id, name, systemDefined, label, context);
    }

    public static CodeData instanceWithContext(final Long id, final String name, final boolean systemDefined, final String label,
            final String context) {
        return new CodeData(id, name, systemDefined, label, context);
    }

    public static CodeData instance(final Long id, final String name, final boolean systemDefined, final String label, final String context) {
        return new CodeData(id, name, systemDefined, label, context);
    }
    
    public CodeValueData build() {
    	return CodeValueData.instance(this.id, this.name, this.id.intValue(), this.systemDefined, this.label);
    }

    private CodeData(final Long id, final String name, final boolean systemDefined, final String label, final String context) {
        this.id = id;
        this.name = name;
        this.systemDefined = systemDefined;
        this.label = label;
        this.context = context;
    }

    public Long getCodeId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

	public Long getId() {
		return this.id;
	}
}
