package org.jammate.platform.code.data;

import java.io.Serializable;
import java.util.List;

public class CodeValueData implements Serializable {

    private final Long id;

    private final String name;

    @SuppressWarnings("unused")
    private final Integer position;

    private final boolean systemDefined;

    private final String label;

    private final String context;
    
    private final List<CodeValueData> codeValues;

    public static CodeValueData instance(final Long id, final String name, final Integer position, final boolean systemDefined,
            final String label) {
        final String context = null;

        return new CodeValueData(id, name, position, systemDefined, label, context, null);
    }
    
    public static CodeValueData instanceForNesting(final Long id, final String name, final Integer position, final boolean systemDefined,
            final String label, final List<CodeValueData> codeValues) {
        final String context = null;

        return new CodeValueData(id, name, position, systemDefined, label, context, codeValues);
    }

    public static CodeValueData instanceWithContext(final Long id, final String name, final Integer position, final boolean systemDefined,
            final String label, final String context) {
        return new CodeValueData(id, name, position, systemDefined, label, context, null);
    }

    public static CodeValueData instance(final Long id, final String name, final boolean systemDefined, final String label) {
        final String context = null;
        return new CodeValueData(id, name, null, systemDefined, label, context, null);
    }
    
    public static CodeValueData instance(final Long id, final String codeLabel, final String codeValue) {
        return new CodeValueData(id, codeValue, null, false, codeLabel, null, null);
    }

    private CodeValueData(final Long id, final String name, final Integer position, final boolean systemDefined, final String label,
            final String context, final List<CodeValueData> codeValues) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.systemDefined = systemDefined;
        this.label = label;
        this.context = context;
        this.codeValues = codeValues;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getLabel() {
        return this.label;
    }

    public String getContext() {
        return this.context;
    }

    public boolean isSystemDefined() {
        return this.systemDefined;
    }
    
    public List<CodeValueData> getCodeValues() {
    	return this.codeValues;
    }

}
