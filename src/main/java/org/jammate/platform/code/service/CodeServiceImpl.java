package org.jammate.platform.code.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.jammate.platform.code.data.CodeValueData;
import org.jammate.platform.core.exception.PlatformDataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class CodeServiceImpl {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CodeServiceImpl(DataSource dataSource) {
	    this.jdbcTemplate = new JdbcTemplate();
	    this.jdbcTemplate.setDataSource(dataSource);
	}
	
	public List<CodeValueData> retrieve(final String codes) {
			List<String> requestedCodes = Arrays.asList(codes.split("\\s*,\\s*"));
			final StringBuilder commaSeparatedCodes = new StringBuilder();
			for(String requestedCode : requestedCodes) {
				commaSeparatedCodes.append("\"" + requestedCode + "\",");
			}
			commaSeparatedCodes.deleteCharAt(commaSeparatedCodes.length() - 1);
            final CodeDataMapper rm = new CodeDataMapper(jdbcTemplate);
            final String sql = "select " + rm.codeSchema() + "where c.name in (" +
            		commaSeparatedCodes.toString() + ")";

            return this.jdbcTemplate.query(sql, rm, new Object[] { });
	}
	
	
	 private static final class CodeDataMapper implements RowMapper<CodeValueData> {
	    	
	    	private final JdbcTemplate jdbcTemplate;
	    	public CodeDataMapper(final JdbcTemplate jdbcTemplate) {
				this.jdbcTemplate = jdbcTemplate;
		}
	
	    public String codeSchema() {
	        return " c.id as id, c.name as value, c.parentId as codeId, c.id as position, c.isSystemDefined as systemDefined, c.label as label"
	                + " from `code` c ";
	    }

	    @Override
	    public CodeValueData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	        final Long id = rs.getLong("id");
	        final String value = rs.getString("value");
	        final Integer position = rs.getInt("position");
	        final String label = rs.getString("label");
	        final boolean systemDefined = rs.getBoolean("systemDefined");
	        final List<CodeValueData> codeValues = retrieveCodeValues(id);
	        return CodeValueData.instanceForNesting(id, value, position, systemDefined, label, codeValues);
	    }
	    
	    private List<CodeValueData> retrieveCodeValues(final Long codeId) {
		    try{
			final CodeValueDataMapper rm = new CodeValueDataMapper();
			final String sql = "select " + rm.schema() + " where c.id= ?";
	
			return this.jdbcTemplate.query(sql, rm, new Object[] { codeId });
		    } catch (DataAccessException exception){
	        	exception.printStackTrace();
	            throw new PlatformDataAccessException();
	        }
		}

	}
	
	private static final class CodeValueDataMapper implements RowMapper<CodeValueData> {
	
	    public String schema() {
	        return " cv.id as id, cv.codeValue as value, cv.codeId as codeId, cv.orderPosition as position, cv.isSystemDefined as systemDefined, cv.label as label, cv.context AS context "
	                + " from code_value as cv join code c on cv.codeId = c.id ";
	    }
	
	    @Override
	    public CodeValueData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	
	        final Long id = rs.getLong("id");
	        final String value = rs.getString("value");
	        final Integer position = rs.getInt("position");
	        final String label = rs.getString("label");
	        final boolean systemDefined = rs.getBoolean("systemDefined");
	        final String context = rs.getString("context");
	
	        return CodeValueData.instanceWithContext(id, value, position, systemDefined, label, context);
	    }
	}


}
