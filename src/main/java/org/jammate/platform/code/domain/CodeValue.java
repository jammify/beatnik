package org.jammate.platform.code.domain;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.code.CodeConstants.CODEVALUE_JSON_INPUT_PARAMS;
import org.jammate.platform.code.data.CodeValueData;
import org.jammate.platform.code.exception.CodeValueCannotBeChangedException;
import org.jammate.platform.core.infra.JsonCommand;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "code_value", uniqueConstraints = { @UniqueConstraint(columnNames = { "codeId", "codeValue" }, name = "code_value_duplicate") })
public class CodeValue extends AbstractPersistable<Long> {

    @Column(name = "codeValue", length = 100)
    private String codeValue;

    @Column(name = "orderPosition")
    private int position;

    @ManyToOne
    @JoinColumn(name = "codeId", nullable = false)
    private Code code;

    @Column(name = "isSystemDefined")
    private final boolean systemDefined;

    @Column(name = "label", length = 100)
    private String label;

    @Column(name = "context", length = 100)
    private String context;

    public static CodeValue createNew(final Code code, final String codeValue, final int position, final boolean systemDefined,
            final String label) {
        final String context = null;
        return new CodeValue(code, codeValue, position, systemDefined, label, context);
    }

    public static CodeValue createNewWithContext(final Code code, final String codeValue, final int position, final boolean systemDefined,
            final String label, final String context) {
        return new CodeValue(code, codeValue, position, systemDefined, label, context);
    }

    public static CodeValue createNew(final Code code, final String codeValue, final int position, final boolean systemDefined,
            final String label, final String context) {
        return new CodeValue(code, codeValue, position, systemDefined, label, context);
    }

    protected CodeValue() {
        this.systemDefined = false;
    }

    private CodeValue(final Code code, final String codeValue, final int position, final boolean systemDefined, final String label,
            final String context) {
        this.code = code;
        this.codeValue = StringUtils.defaultIfEmpty(codeValue, null);
        this.position = position;
        this.systemDefined = systemDefined;
        this.label = label;
        this.context = context;
    }

    public String codeValue() {
        return this.codeValue;
    }

    public int position() {
        return this.position;
    }

    public boolean isSystemDefined() {
        return this.systemDefined;
    }

    public String getLabel() {
        return this.label;
    }

    public static CodeValue fromJson(final Code code, final JsonCommand command) {

        final String codeValue = command.stringValueOfParameterNamed(CODEVALUE_JSON_INPUT_PARAMS.NAME.getValue());
        Integer position = command.integerValueOfParameterNamed(CODEVALUE_JSON_INPUT_PARAMS.POSITION.getValue());
        if (position == null) { 
            position = new Integer(0);
        }
        final String label = command.stringValueOfParameterNamed(CODEVALUE_JSON_INPUT_PARAMS.LABEL.getValue());
        final boolean systemDefined = command.booleanPrimitiveValueOfParameterNamed("systemDefined");
        final String context = command.stringValueOfParameterNamed(CODEVALUE_JSON_INPUT_PARAMS.CONTEXT.getValue());
        return new CodeValue(code, codeValue, position.intValue(), systemDefined, label, context);
    }

    public Map<String, Object> update(final JsonCommand command) {

        final Map<String, Object> actualChanges = new LinkedHashMap<>(2);

        final String codeValueParamName = CODEVALUE_JSON_INPUT_PARAMS.NAME.getValue();

        if (command.isChangeInStringParameterNamed(codeValueParamName, this.codeValue)) {
            if (!this.systemDefined) {
                final String newValue = command.stringValueOfParameterNamed(codeValueParamName);
                actualChanges.put(codeValueParamName, newValue);
                this.codeValue = StringUtils.defaultIfEmpty(newValue, null);
            } else {
                throw new CodeValueCannotBeChangedException();
            }
        }

        final String positionParamName = CODEVALUE_JSON_INPUT_PARAMS.POSITION.getValue();
        if (command.isChangeInIntegerSansLocaleParameterNamed(positionParamName, this.position)) {
            final Integer newValue = command.integerValueSansLocaleOfParameterNamed(positionParamName);
            actualChanges.put(positionParamName, newValue);
            this.position = newValue.intValue();
        }

        final String labelParamName = CODEVALUE_JSON_INPUT_PARAMS.LABEL.getValue();
        if (command.isChangeInStringParameterNamed(labelParamName, this.label)) {
            final String newValue = command.stringValueOfParameterNamed(labelParamName);
            actualChanges.put(labelParamName, newValue);
            this.label = StringUtils.defaultIfEmpty(newValue, null);
        }

        final String contextParamName = CODEVALUE_JSON_INPUT_PARAMS.CONTEXT.getValue();
        if (command.isChangeInStringParameterNamed(contextParamName, this.context)) {
            final String newValue = command.stringValueOfParameterNamed(contextParamName);
            actualChanges.put(contextParamName, newValue);
            this.context = StringUtils.defaultIfEmpty(newValue, null);
        }

        return actualChanges;
    }

    public CodeValueData toData() {
        return CodeValueData.instanceWithContext(getId(), this.codeValue, this.position, this.systemDefined, this.label, this.context);
    }

    public String codeName() {
        String codeName = null;
        if (this.code != null) {
            codeName = this.code.name();
        }
        return codeName;
    }

    public Long codeId() {
        Long codeId = null;
        if (this.code != null) {
            codeId = this.code.getId();
        }
        return codeId;
    }

    public Code getCode() {
        return this.code;
    }

	public String getCodeValue() {
		return this.codeValue;
	}

	public String getContext() {
		return this.context;
	}
	
}
