package org.jammate.platform.code.domain;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.code.exception.SystemDefinedCodeCannotBeChangedException;
import org.jammate.platform.core.infra.JsonCommand;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "code", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }, name = "name") })
public class Code extends AbstractPersistable<Long> {

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "isSystemDefined")
    private final boolean systemDefined;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "code", orphanRemoval = true)
    private Set<CodeValue> values;

    @Column(name = "label", length = 100)
    private String label;

    public static Code fromJson(final JsonCommand command) {
        final String name = command.stringValueOfParameterNamed("name");
        final String label = command.stringValueOfParameterNamed("label");
        return new Code(name, label);
    }

    protected Code() {
        this.systemDefined = false;
    }

    private Code(final String name, final String label) {
        this.name = name;
        this.systemDefined = false;
        this.label = label;
    }

    public String name() {
        return this.name;
    }

    public boolean isSystemDefined() {
        return this.systemDefined;
    }

    public String label() {
        return this.label;
    }


    public Map<String, Object> update(final JsonCommand command) {

        // if (this.systemDefined) { throw new
        // SystemDefinedCodeCannotBeChangedException(); }

        final Map<String, Object> actualChanges = new LinkedHashMap<>(1);

        final String nameParamName = "name";
        final String label = "label";
        final String context = "context";

        if (command.isChangeInStringParameterNamed(nameParamName, this.name)) {
            if (!this.systemDefined) {
                final String newValue = command.stringValueOfParameterNamed(nameParamName);
                actualChanges.put(nameParamName, newValue);
                this.name = StringUtils.defaultIfEmpty(newValue, null);
            } else {
                throw new SystemDefinedCodeCannotBeChangedException();
            }
        }

        if (command.isChangeInStringParameterNamed(label, this.label)) {
            final String newValue = command.stringValueOfParameterNamed(label);
            actualChanges.put(label, newValue);
            this.label = StringUtils.defaultIfEmpty(newValue, "");
        }

        return actualChanges;
    }

    public boolean remove(final CodeValue codeValueToDelete) {
        return this.values.remove(codeValueToDelete);
    }
}
