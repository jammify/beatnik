package org.jammate.platform.code.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CodeValueRepository extends JpaRepository<CodeValue, Long>, JpaSpecificationExecutor<CodeValue> {

    CodeValue findByCodeNameAndId(String codeName, Long id);

    CodeValue findByCodeNameAndCodeValue(String codeName, String label);
    
    CodeValue findByCodeNameAndLabel(String codeName, String label);
    
    List<CodeValue> findByCodeNameOrderByPositionAsc(String codeName);
    
    CodeValue findOneByCodeValue(String codeValue);
    
    
}
