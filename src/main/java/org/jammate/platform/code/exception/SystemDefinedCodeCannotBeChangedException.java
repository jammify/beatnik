package org.jammate.platform.code.exception;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class SystemDefinedCodeCannotBeChangedException extends AbstractRuntimeException {

    public SystemDefinedCodeCannotBeChangedException() {
        super("error.msg.code.systemdefined", "This code is system defined and 'code name' cannot be modified or deleted.");
    }
}
