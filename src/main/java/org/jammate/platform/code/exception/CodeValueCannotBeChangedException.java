package org.jammate.platform.code.exception;

import org.jammate.platform.core.exception.AbstractRuntimeException;

public class CodeValueCannotBeChangedException extends AbstractRuntimeException {

    public CodeValueCannotBeChangedException() {
        super("error.msg.code.value.systemdefined", "This code value is system defined and 'code value' cannot be modified or deleted.");
    }

}