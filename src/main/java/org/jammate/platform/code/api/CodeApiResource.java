package org.jammate.platform.code.api;

import java.util.List;

import org.jammate.platform.code.data.CodeValueData;
import org.jammate.platform.code.service.CodeServiceImpl;
import org.jammate.platform.core.infra.ApiSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@RequestMapping("/codes") 
public class CodeApiResource {
	
	private final CodeServiceImpl codeServiceImpl;
	private final ApiSerializer<CodeValueData> jsonSerializer;
	private final static Logger logger = LoggerFactory
			.getLogger(CodeApiResource.class);
	
	@Autowired
    public CodeApiResource(final CodeServiceImpl codeServiceImpl,
    		final ApiSerializer<CodeValueData> jsonSerializer) {
		this.codeServiceImpl = codeServiceImpl;
		this.jsonSerializer = jsonSerializer;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveCodes(@RequestParam("name") String codes) {
		final List<CodeValueData> fetchedCodes = this.codeServiceImpl.retrieve(codes);
		return this.jsonSerializer.serialize(fetchedCodes);
    }

}
