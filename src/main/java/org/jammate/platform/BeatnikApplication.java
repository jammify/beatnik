package org.jammate.platform;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BeatnikApplication extends SpringBootServletInitializer {

    public static void main(String[] args) throws IOException {
    		ConfigurableApplicationContext ctx = SpringApplication.run(Configuration.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Configuration.class);
    }
}
