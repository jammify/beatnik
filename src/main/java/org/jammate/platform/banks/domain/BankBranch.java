package org.jammate.platform.banks.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "branch_master")
public class BankBranch extends AbstractPersistable<Long> {
	
	@Column(name = "bank_name")
    private String bankName;
	
	@Column(name = "bank_code")
    private String bankCode;
	
	@Column(name = "branch_name")
    private String branchName;
	
	@Column(name = "branch_code")
    private String branchCode;
	
	@Column(name = "ifsc_code")
    private String ifscCode;
	
	protected BankBranch() {
		
	}
	
	public String getBankName() {
		return this.bankName;
	}
	
	public String getBranchName() {
		return this.branchName;
	}
	
	public String getBankCode() {
		return this.bankCode;
	}
	
	public String getBranchCode() {
		return this.branchCode;
	}
	
	public String getIfscCode() {
		return this.ifscCode;
	}

}
