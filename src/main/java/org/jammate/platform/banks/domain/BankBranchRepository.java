package org.jammate.platform.banks.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BankBranchRepository extends JpaRepository<BankBranch, Long>, JpaSpecificationExecutor<BankBranch> {
	
	@Query(value = "select distinct(b.bank_name) from branch_master b order by b.bank_name", nativeQuery = true)
	public List<String> findDistinctBanks();
	
	@Query("from BankBranch b where b.bankName = :bankName order by b.branchName")
	public List<BankBranch> findBranchesByBankName(@Param("bankName") final String bankName);
	
	@Query("from BankBranch b where b.ifscCode = :ifscCode")
	public BankBranch findBranchByIfscCode(@Param("ifscCode") final String ifscCode);

}
