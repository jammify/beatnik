package org.jammate.platform.banks.api;

import java.util.List;

import org.jammate.platform.banks.domain.BankBranch;
import org.jammate.platform.banks.domain.BankBranchRepository;
import org.jammate.platform.core.infra.ApiSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController  
@RequestMapping("/banks") 
public class BanksApiResource {
	
	private final BankBranchRepository branchRepository;
	private final ApiSerializer<BankBranch> jsonSerializer;
	private final static Logger logger = LoggerFactory
			.getLogger(BanksApiResource.class);
	
	@Autowired
    public BanksApiResource(final BankBranchRepository branchRepository,
    		final ApiSerializer<BankBranch> jsonSerializer) {
		this.branchRepository = branchRepository;
		this.jsonSerializer = jsonSerializer;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveBanks() {
		final List<String> fetchedBanks = this.branchRepository.findDistinctBanks();
		return new Gson().toJson(fetchedBanks);
    }
	
	@RequestMapping(value="/{bankName}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveBranches(@PathVariable String bankName) {
		final List<BankBranch> fetchedBranches = this.branchRepository.findBranchesByBankName(bankName);
		return this.jsonSerializer.serialize(fetchedBranches);
    }
	
	@RequestMapping(value="/ifsc/{ifscCode}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	@Transactional(readOnly = true)
    public String retrieveBranch(@PathVariable String ifscCode) {
		final BankBranch fetchedBranch = this.branchRepository.findBranchByIfscCode(ifscCode);
		return this.jsonSerializer.serialize(fetchedBranch);
    }

}
