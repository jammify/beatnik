package org.jammate.platform.calendar.data;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;

public enum Days {


	MONDAY(1, "Monday"),
	TUESDAY(2, "Tuesday"),
	WEDNESDAY(3, "Wednesday"),
	THURSDAY(4, "Thursday"),
	FRIDAY(5, "Friday"),
	SATURDAY(6, "Saturday"),
	SUNDAY(7, "Sunday");

    private final Integer value;
    private final String code;

    private Days(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, Days> intToEnumMap = new HashMap<>();
    private static final Map<String, Days> stringToEnumMap = new HashMap<>();
    static {
        for (final Days type : Days.values()) {
            intToEnumMap.put(type.value, type);
            stringToEnumMap.put(type.code, type);
        }
    }
    
    public static int diffBetweenDays(String day1, String day2) {
    		Days fromDay = fromString(day1);
    		Days toDay = fromString(day2);
    		return toDay.value - fromDay.value;
    }
    
    public static Minutes isBetween(final String day1, final String day2,
    		final Integer startRange, final Integer endRange,
    		final LocalTime fromTime, final LocalTime toTime, final String checkDay) {
    		Boolean isBetweenDay = isBetweenDay(day1, day2, checkDay);
    		if(isBetweenDay) {
    			final LocalTime start =  LocalTime.parse(startRange.toString(), DateTimeFormat.forPattern("HH")) ;
    			final LocalTime end =  LocalTime.parse(endRange.toString(), DateTimeFormat.forPattern("HH")) ;
    			//(StartA <= EndB) and (EndA >= StartB)
    			//6-9 and 8:30-9:30
    			//6<=9:30 and 9>=8:30
    			//max(6, 8:30) - min(9, 9:30)
    			if(start.isBefore(toTime) && end.isAfter(fromTime)) {
    				LocalTime max = null;
    				LocalTime min = null;
    				if(start.isAfter(fromTime)) 
    					max = start; 
    				else
    					max = fromTime;
    				if(end.isBefore(toTime))
    					min = end;
    				else 
    					min = toTime;
    				return Minutes.minutesBetween(max, min);
    			}
    		}
    		return null;
    }
    
    public static Boolean isBetweenDay(final String day1, final String day2, final String checkDay) {
    		Days fromDay = fromString(day1);
		Days toDay = fromString(day2);
		if(checkDay.equalsIgnoreCase(day1) || checkDay.equalsIgnoreCase(day2)) {
			return true;
		} else if(fromDay.equals(toDay) && !checkDay.equalsIgnoreCase(fromDay.toString())) {
			return false;
		}
		Days nextDay = nextDay(fromDay);
		while(!nextDay.getCode().equalsIgnoreCase(checkDay) && !nextDay.code.equalsIgnoreCase(day2)) {
			nextDay = nextDay(nextDay);
		}
		if(nextDay.getCode().equalsIgnoreCase(checkDay)) {
			return true;
		}
		return false;
    }
    
    public static Days nextDay(final Days day) {
    		Days nextDay = null;
    		switch(day.code) {
    			case "Monday":
    				nextDay = Days.TUESDAY;
    				break;
    			case "Tuesday":
    				nextDay = Days.WEDNESDAY;
    				break;
    			case "Wednesday":
    				nextDay = Days.THURSDAY;
    				break;
    			case "Thursday":
    				nextDay = Days.FRIDAY;
    				break;
    			case "Friday":
    				nextDay = Days.SATURDAY;
    				break;
    			case "Saturday":
    				nextDay = Days.SUNDAY;
    				break;
    			case "Sunday":
    				nextDay = Days.MONDAY;
    				break;
    		}
    		return nextDay;
    }
    
    public static Days fromString(final String day) {
    		final Days type = stringToEnumMap.get(day);
    		return type;
    }

    public static Days fromInt(final int i) {
        final Days type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }

}
