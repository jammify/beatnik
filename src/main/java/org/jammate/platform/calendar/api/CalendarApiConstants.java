package org.jammate.platform.calendar.api;

public class CalendarApiConstants {
	
	public static final String roomIdParamName = "roomId";
	public static final String dateParamName = "date";
	public static final String startParamName = "start";
	public static final String endParamName = "end";

}
