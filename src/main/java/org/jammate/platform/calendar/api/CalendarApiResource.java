package org.jammate.platform.calendar.api;

import org.jammate.platform.calendar.service.CalendarService;
import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/calendar") 
public class CalendarApiResource {
	
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final CalendarService calendarService;
	private final static Logger logger = LoggerFactory
			.getLogger(CalendarApiResource.class);
	
	@Autowired
    public CalendarApiResource(final CalendarService calendarService,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper) {
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.calendarService = calendarService;
	}
	
	@RequestMapping(value="/block",
			method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String createBlock(@RequestBody final String block) {
		final CommandProcessingResult response = this.calendarService
				.blockTime(JsonCommand.from(block,
						new JsonParser().parse(block), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(value="/block/{blockId}",
			method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")  
	public String deleteBlock(@PathVariable final Long blockId) {
		final CommandProcessingResult response = this.calendarService
				.deleteBlockedTime(blockId);
		return this.jsonSerializer.serialize(response);
	}
	
			
}
	

