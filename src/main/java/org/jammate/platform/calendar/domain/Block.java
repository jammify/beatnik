package org.jammate.platform.calendar.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.jamhosts.domain.JamRoom;

@Entity
@Table(name = "block")
public class Block extends AbstractAuditableCustom<AppUser, Long> {
	
	@ManyToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "start")
	private String start;
	
	@Column(name = "end")
	private String end;
	
	@Column(name = "isDeleted")
	private Boolean isDeleted;
	
	protected Block() {
		//
	}
	
	public static Block instance(final JamRoom room, final Date date, final String start, final String end) {
		return new Block(room, date, start, end);
	}
	
	private Block(final JamRoom room, final Date date, final String start, final String end) {
		this.room = room;
		this.date = date;
		this.start = start;
		this.end = end;
		this.isDeleted = false;
	}
	
	public String getStart() {
		return this.start;
	}
	
	public String getEnd() {
		return this.end;
	}
	
	public void setIsDeleted(final Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
