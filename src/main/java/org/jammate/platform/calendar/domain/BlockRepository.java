package org.jammate.platform.calendar.domain;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BlockRepository extends JpaRepository<Block, Long>,
JpaSpecificationExecutor<Block> {

	@Query(value = " select * from block b inner join room r on b.roomId = r.id where b.roomId = :room and b.date = :date and b.isDeleted = 0", nativeQuery = true)
	List<Block> findByRoomAndDate(@Param("room") final Long roomId, @Param("date") final String date);
	
	@Query(value="select if(count(b.id) > 0, true, false) from `block`  b " + 
			" where CAST(CONCAT(b.start, ':00') as TIME) < :endB  and " + 
			" CAST(CONCAT(b.end, ':00') as TIME) > :startB and " + 
			" b.roomId = :roomId and b.date = :orderDate and b.isDeleted <> 1", nativeQuery=true)
	BigInteger isOverlappingOrder(@Param("startB") final String startB, @Param("endB") final String endB,
			@Param("roomId") final Long roomId, @Param("orderDate") final String orderDate);
}
