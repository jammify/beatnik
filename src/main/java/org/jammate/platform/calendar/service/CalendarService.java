package org.jammate.platform.calendar.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.jammate.platform.calendar.api.CalendarApiConstants;
import org.jammate.platform.calendar.domain.Block;
import org.jammate.platform.calendar.domain.BlockRepository;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.jamhosts.domain.JamRoomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonElement;

@Service
public class CalendarService {
	
	private final static Logger logger = LoggerFactory
			.getLogger(CalendarService.class);
	private final BlockRepository blockRepository;
	private final FromJsonHelper fromApiJsonHelper;
	private final JamRoomRepository roomRepository;
	
	@Autowired
	public CalendarService(final BlockRepository blockRepository,
		 final FromJsonHelper fromApiJsonHelper,
		 final JamRoomRepository roomRepository) {
		this.blockRepository = blockRepository;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.roomRepository = roomRepository;
	}
	
	@Transactional
	public CommandProcessingResult blockTime(final JsonCommand command) {
		final Block block = assembleFrom(command.parsedJson());
		this.blockRepository.save(block);
		return CommandProcessingResult.fromDetails(block.getId(), null);
	}
	
	@Transactional
	public CommandProcessingResult deleteBlockedTime(final Long blockId) {
		final Block block = this.blockRepository.findOne(blockId);
		block.setIsDeleted(true);
		this.blockRepository.save(block);
		return CommandProcessingResult.fromDetails(blockId, null);
	}
	
	private Block assembleFrom(final JsonElement element) {
		 final Long roomId = this.fromApiJsonHelper.extractLongNamed(CalendarApiConstants.roomIdParamName, element);
		 final JamRoom room = this.roomRepository.findOne(roomId);
		 final String date = this.fromApiJsonHelper.extractStringNamed(CalendarApiConstants.dateParamName, element);
		 final String start = this.fromApiJsonHelper.extractStringNamed(CalendarApiConstants.startParamName, element);
		 final String end = this.fromApiJsonHelper.extractStringNamed(CalendarApiConstants.endParamName, element);
		 DateTimeFormatter dateTimeformatter = DateTimeFormatter.ofPattern("ddMMMyyyy");
		 final LocalDate blockedDate = LocalDate.parse(date, dateTimeformatter);
		 final Date dt = Date.from(blockedDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		 return Block.instance(room, dt, start, end);
	}

}
