package org.jammate.platform.auth.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "permission")
public class Permission extends AbstractPersistable<Long> {

    @Column(name = "grouping", nullable = false, length = 45)
    private final String grouping;

    @Column(name = "code", nullable = false, length = 100)
    private final String code;

    @Column(name = "entityName", nullable = true, length = 100)
    private final String entityName;

    @Column(name = "actionName", nullable = true, length = 100)
    private final String actionName;

    public Permission(final String grouping, final String entityName, final String actionName) {
        this.grouping = grouping;
        this.entityName = entityName;
        this.actionName = actionName;
        this.code = actionName + "_" + entityName;
    }

    protected Permission() {
        this.grouping = null;
        this.entityName = null;
        this.actionName = null;
        this.code = null;
    }

    public boolean hasCode(final String checkCode) {
        return this.code.equalsIgnoreCase(checkCode);
    }

    public String getCode() {
        return this.code;
    }

    public String getGrouping() {
        return this.grouping;
    }

    public String getEntityName() {
        return this.entityName;
    }

    public String getActionName() {
        return this.actionName;
    }

}
