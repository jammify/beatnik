package org.jammate.platform.auth.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.profile.ProfileConstants;
import org.jammate.platform.profile.domain.Genre;
import org.jammate.platform.profile.domain.Skill;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Entity
@Table(name = "appuser")
public class AppUser extends AbstractPersistable<Long> implements UserDetails{
	
	@Column(name = "firebaseUserId")
	private String firebaseUserId;
	
	@Column(name = "uid")
	private String uid;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "picture")
	private String picture;
	
	@Column(name = "signInProvider")
	private String signInProvider;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "mobileNo")
	private String mobileNo;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "appuser")
	private Set<Skill> skills = new HashSet<>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "appuser")
	private Set<Genre> genres = new HashSet<>();
	
	@Column(name = "isPartner")
	private Boolean isPartner;
	
	
	//Device registration instanceId
	@Column(name = "instanceId")
	private String instanceId;
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "appuser_role", joinColumns = @JoinColumn(name = "appuserId"), inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Set<Role> roles;
	
	@Column(name = "session")
	private String session;


	
	
	public static AppUser fromJson(final String object) {
		final JsonObject appuser = new JsonParser().parse(object).getAsJsonObject();
        final String firebaseUserId = appuser.
        		get("firebaseUserId").getAsString();

        return new AppUser(firebaseUserId, null, null);
    }
	
	public AppUser() {
		
	}
	
	private AppUser(final String firebaseUserId, final Set<Skill> skills,
			final Set<Genre> genres) {
		this.firebaseUserId = firebaseUserId;
		this.skills = skills;
		this.genres = genres;
	}
	
	public void setAuthDetails(final String name, final String uid, final String picture,
			final String email, final String signInProvider, final Role role) {
		this.name = name;
		this.uid = uid;
		this.picture = picture;
		this.email = email;
		this.signInProvider = signInProvider;
		this.isPartner = isPartner != null ? isPartner : false;
		this.roles = new HashSet<>();
		this.roles.add(role);
	}
	
	
	public String getFirebaseUserId() {
		return this.firebaseUserId;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getUid() {
		return this.uid;
	}
	
	public String getPicture() {
		return this.picture;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	public String getSignInProvider() {
		return this.signInProvider;
	}
	
	 public Map<String, Object> update(final JsonCommand command) {

	        final Map<String, Object> actualChanges = new LinkedHashMap<>(7);

	        final String tokenParamName = ProfileConstants.firebaseUserIdParamName;
	        if (command.isChangeInStringParameterNamed(tokenParamName, this.firebaseUserId)) {
	            final String newValue = command.stringValueOfParameterNamed(tokenParamName);
	            actualChanges.put(tokenParamName, newValue);
	            this.firebaseUserId = StringUtils.defaultIfEmpty(newValue, null);
	        }
	        
	        final String skillsParamName = ProfileConstants.skillsParamName;
	        if (command.hasParameter(skillsParamName)) {
				final JsonArray jsonArray = command
						.arrayOfParameterNamed(skillsParamName);
				if (jsonArray != null) {
					actualChanges.put(skillsParamName, jsonArray);
				}
			}
	        
	        final String genresParamName = ProfileConstants.genresParamName;
	        if (command.hasParameter(genresParamName)) {
				final JsonArray jsonArray = command
						.arrayOfParameterNamed(genresParamName);
				if (jsonArray != null) {
					actualChanges.put(genresParamName, jsonArray);
				}
			}

	        return actualChanges;
      }
	 
	 public boolean updateSkills(final Set<Skill> newSkills) {
			if (newSkills == null) {
				return false;
			}

			if (this.skills == null) {
				this.skills = new HashSet<>();
			}
			this.skills.clear();
			this.skills.addAll(associateSkills(newSkills));
			return true;
		}
	 
	 public boolean updateGenres(final Set<Genre> newGenres) {
			if (newGenres == null) {
				return false;
			}

			if (this.genres == null) {
				this.genres = new HashSet<>();
			}
			this.genres.clear();
			this.genres.addAll(associateGenres(newGenres));
			return true;
		}
	 
	 private Set<Skill> associateSkills(
				final Set<Skill> skills) {
			for (final Skill skill : skills) {
				skill.update(this);
			}
			return skills;
		}
	 
	 private Set<Genre> associateGenres(
				final Set<Genre> genres) {
			for (final Genre genre : genres) {
				genre.update(this);
			}
			return genres;
		}
	 
	 public String getFirstName() {
		 String[] splitted = this.name.split(" ");
		 return splitted[0];
	 }
	 
	 public Boolean isPartner() {
		 return this.isPartner;
	 }
	 
	 public void setInstanceId(final String instanceId) {
		 this.instanceId = instanceId;
	 }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		final List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (final Role role : this.roles) {
            final Collection<Permission> permissions = role.getPermissions();
            for (final Permission permission : permissions) {
                grantedAuthorities.add(new SimpleGrantedAuthority(permission.getCode()));
            }
        }
        return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return this.uid;
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	public void setSession(final String session) {
		this.session = session;
	}
	
	public String getSession() {
		return session;
	}
	
	public String getInstanceId() {
		return this.instanceId;
	}
	
	
	
}
