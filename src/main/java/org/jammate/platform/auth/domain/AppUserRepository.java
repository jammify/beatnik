package org.jammate.platform.auth.domain;

import org.apache.commons.mail.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> { 
	
	AppUser findOneByUid(String uid);
	
	AppUser findOneByEmail(String email);
	
	AppUser findOneByMobileNo(String mobileNo);
	
	AppUser findOneBySession(String session);

	Email findOneByEmail(Email email);

}
