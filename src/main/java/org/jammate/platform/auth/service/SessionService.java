package org.jammate.platform.auth.service;

import java.util.HashMap;
import java.util.Map;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.domain.AppUserRepository;
import org.jammate.platform.code.service.CodeServiceImpl;
import org.jammate.platform.core.infra.data.SessionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SessionService {
	
	private final CodeServiceImpl codeService;
	private final AppUserRepository userRepository;
	
	@Autowired
    public SessionService(final CodeServiceImpl codeService,
    		final AppUserRepository userRepository) {
		this.codeService = codeService;
		this.userRepository = userRepository;
	}

    private Map<AppUser, SessionData> appUserToSessionDataMap = new HashMap<>();
    private Map<String, SessionData> tokentoSessionDataMap = new HashMap<>();
    
    public void createSession(final AppUser user, final String token) {
        final SessionData existingSession = this.getFromAppUserToSessionDataMap(user);
        if (existingSession != null) {
            final String existingToken = existingSession.getToken();
        }
        final SessionData newSession = new SessionData(user, token);
        this.putToAppUserToSessionDataMap(user, newSession);
        this.putToTokentoSessionDataMap(token, newSession);
        user.setSession(token);
        this.userRepository.save(user);
    }
    
    private void putToAppUserToSessionDataMap(AppUser user,
            SessionData newSession) {
        this.appUserToSessionDataMap.put(user, newSession);
    }
    
    private void putToTokentoSessionDataMap(String token, SessionData newSession) {
        this.tokentoSessionDataMap.put(token, newSession);
    }

    public AppUser retrieveUserFromToken(final String token) {
        AppUser result = null;
        final SessionData existingSession = getFromTokentoSessionDataMap(token);
        if (existingSession != null) {
            result = existingSession.getUser();
        } else {
        		result = this.userRepository.findOneBySession(token);
        		final SessionData newSession = new SessionData(result, token);
            this.putToAppUserToSessionDataMap(result, newSession);
            this.putToTokentoSessionDataMap(token, newSession);
        }
        return result;
    }
    
    public Map<AppUser, SessionData> getAppUserToSessionDataMap() {
        return this.appUserToSessionDataMap;
    }
    
    public SessionData getFromAppUserToSessionDataMap(AppUser user) {
        return this.getAppUserToSessionDataMap().get(user);
    }
    
    public Map<String, SessionData> getTokentoSessionDataMap() {
		return this.tokentoSessionDataMap;
    }
    
    public SessionData getFromTokentoSessionDataMap(String token) {
        return this.getTokentoSessionDataMap().get(token);
    }

}
