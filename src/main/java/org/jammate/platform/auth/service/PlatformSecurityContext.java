package org.jammate.platform.auth.service;

import org.jammate.platform.auth.domain.AppUser;

public interface PlatformSecurityContext {

    AppUser authenticatedUser();

    AppUser getAuthenticatedUserIfPresent();


}