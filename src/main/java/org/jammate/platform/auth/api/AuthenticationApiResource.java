package org.jammate.platform.auth.api;

import java.util.HashMap;
import java.util.Map;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.profile.data.CryptographyData;
import org.jammate.platform.profile.exception.KeyNotFoundException;
import org.jammate.platform.profile.service.ProfileWriteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/login") 
public class AuthenticationApiResource {
	
	private final static Logger logger = LoggerFactory
			.getLogger(AuthenticationApiResource.class);
	private final FromJsonHelper fromApiJsonHelper;
	private final ApiSerializer jsonSerializer;
	private final ProfileWriteService profileWritePlatformService; 
	
	@Autowired
    public AuthenticationApiResource(final FromJsonHelper fromApiJsonHelper,
    		final ProfileWriteService profileWritePlatformService,
    		final ApiSerializer jsonSerializer) {
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.profileWritePlatformService = profileWritePlatformService;
		this.jsonSerializer = jsonSerializer;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public String getPublicRSAKey() {
        CryptographyData encryptData  = null;
        try {
            encryptData = this.profileWritePlatformService.getPublicKey("LOGIN");
        } catch (KeyNotFoundException e) {
            Map<String, String> jobParameters = new HashMap<>();
            this.profileWritePlatformService.changeKeys();
            encryptData = this.profileWritePlatformService.getPublicKey("LOGIN");
        } 
         String s = this.jsonSerializer.serialize(encryptData);
         return s;
    } 
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	@ResponseBody
    public String createUser(@RequestBody final String user) {
		final CommandProcessingResult result = this.profileWritePlatformService
				.createUser(JsonCommand.from(user,
				new JsonParser().parse(user), fromApiJsonHelper), false);
		return this.jsonSerializer.serialize(result);
	}
	
	@RequestMapping(value="/partner", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	@ResponseBody
    public String createPartnerUser(@RequestBody final String user) {
		final CommandProcessingResult result = this.profileWritePlatformService
				.createUser(JsonCommand.from(user,
				new JsonParser().parse(user), fromApiJsonHelper), true);
		return this.jsonSerializer.serialize(result);
	}
	

}
