package org.jammate.platform.jamhosts.data;

public class MicData {
	
	private final Boolean hasCondenser;
	private final Boolean isWireless;
	private final Boolean forInstrument;
	
	public static MicData instance(final Boolean condenser,
			final Boolean wireless, final Boolean instrument) {
		return new MicData(condenser, wireless, instrument);
	}
	
	private MicData(final Boolean condenser,
			final Boolean wireless, final Boolean instrument) {
		this.hasCondenser = condenser;
		this.isWireless = wireless;
		this.forInstrument = instrument;
	}
	
	public Boolean hasCondenser() {
		return this.hasCondenser;
	}
	
	public Boolean isWireless() {
		return this.isWireless;
	}
	
	public Boolean forInstrument() {
		return this.forInstrument;
	}

}
