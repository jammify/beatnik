package org.jammate.platform.jamhosts.data;

public class CancellationPolicyData {
	
	private final Integer hoursFromBookingTo;
	private final Integer hoursFromBookingFrom;
	private final Integer percentageRefund;
	
	public static CancellationPolicyData instance(final Integer hoursFromBookingFrom,
			final Integer hoursFromBookingTo, final Integer percentageRefund) {
		return new CancellationPolicyData(hoursFromBookingTo,
				hoursFromBookingFrom, percentageRefund);
	}

	private CancellationPolicyData(final Integer hoursFromBookingFrom,
			final Integer hoursFromBookingTo, final Integer percentageRefund) {
		this.hoursFromBookingTo = hoursFromBookingTo;
		this.hoursFromBookingFrom = hoursFromBookingFrom;
		this.percentageRefund = percentageRefund;
	}
	
	public Integer getStart() {
		return hoursFromBookingTo;
	}
	
	public Integer getEnd() {
		return hoursFromBookingFrom;
	}
	
	public Integer getPercentageRefund() {
		return this.percentageRefund;
	}
}
