package org.jammate.platform.jamhosts.data;

import java.util.List;

public class HostData {
	
	private final String name;
	private final String latitude;
	private final String longitude;
	private final String houseNo;
	private final String street;
	private final String landmark;
	private final String area;
	private final String city;
	private final String pinCode;
	private final String ownerName;
	private final String orgType;
	private final String email;
	private final String contact;
	private final String website;
	private final String fbPage;
	private final Integer openHours;
	private final Integer openMins;
	private final Integer closeHours;
	private final Integer closeMins;
	private final Integer startTimeMultiples;
	private final Integer slotDurationMultiples;
	private final String accountHolderName;
	private final String accountNo;
	private final String ifscCode;
	private final String accountType;
	private List<Integer> roomIds;
	
	public static HostData instance(final String name, final String latitude, final String longitude,
			final String houseNo, final String street, final String landmark,
			final String area, final String city, final String pinCode,
			final String ownerName, final String orgType, final String email,
			final String contact, final String website, final String fbPage,
			final Integer openHours, final Integer openMins, final Integer closeHours,
			final Integer closeMins, final Integer startTimeMultiples,
			final Integer slotDurationMultiples, final String accountHolderName,
			final String accountNo, final String ifscCode, final String accountType) {
		return new HostData(name,  latitude,  longitude,
				 houseNo,  street,  landmark,
				 area,  city,  pinCode,
				 ownerName,  orgType,  email,
				 contact,  website,  fbPage,
				 openHours,  openMins,  closeHours,
				 closeMins,  startTimeMultiples,
				 slotDurationMultiples,  accountHolderName,
				 accountNo,  ifscCode,  accountType);
	}
	
	private HostData(final String name, final String latitude, final String longitude,
			final String houseNo, final String street, final String landmark,
			final String area, final String city, final String pinCode,
			final String ownerName, final String orgType, final String email,
			final String contact, final String website, final String fbPage,
			final Integer openHours, final Integer openMins, final Integer closeHours,
			final Integer closeMins, final Integer startTimeMultiples,
			final Integer slotDurationMultiples, final String accountHolderName,
			final String accountNo, final String ifscCode, final String accountType) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.houseNo = houseNo;
		this.street = street;
		this.landmark = landmark;
		this.area = area;
		this.city = city;
		this.pinCode = pinCode;
		this.ownerName = ownerName;
		this.orgType = orgType;
		this.email = email;
		this.contact = contact;
		this.website = website;
		this.fbPage = fbPage;
		this.openHours = openHours;
		this.openMins = openMins;
		this.closeHours = closeHours;
		this.closeMins = closeMins;
		this.startTimeMultiples = startTimeMultiples;
		this.slotDurationMultiples = slotDurationMultiples;
		this.accountHolderName = accountHolderName;
		this.accountNo = accountNo;
		this.ifscCode = ifscCode;
		this.accountType = accountType;
		
	}
	
	public void setRoomIds(final List<Integer> roomIds) {
		this.roomIds = roomIds;
	}

}
