package org.jammate.platform.jamhosts.data;

import java.util.HashMap;
import java.util.Map;

public enum HostService {
	
	JUST_JAM(100, "Just Jam"),
	JAMMING_WITH_RECORDING(200, "Jamming with recording of the session"),
	RECORDING_MIXING_MASTERING(300, "Recording with Mixing and Mastering"),
	VOICEOVER_RECORDING(400, "Voice-Over");

    private final Integer value;
    private final String code;

    private HostService(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, HostService> intToEnumMap = new HashMap<>();
    static {
        for (final HostService type : HostService.values()) {
            intToEnumMap.put(type.value, type);
        }
    }

    public static HostService fromInt(final int i) {
        final HostService type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }

}
