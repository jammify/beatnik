package org.jammate.platform.jamhosts.data;

public class LastBookingData {
	
	final Long orderId;
	final String studioName;
	final String lastBookedDate;
	final Long profilePicId;
	
	public static LastBookingData instance(final Long orderId,
			final String studioName, final String lastBookedDate,
			final Long profilePicId) {
		return new LastBookingData(orderId, studioName, lastBookedDate,
				profilePicId);
	}
	
	private LastBookingData(final Long orderId,
			final String studioName, final String lastBookedDate,
			final Long profilePicId) {
		this.orderId = orderId;
		this.studioName = studioName;
		this.lastBookedDate = lastBookedDate;
		this.profilePicId = profilePicId;
	}
	

}
