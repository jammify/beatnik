package org.jammate.platform.jamhosts.data;

public enum LocationType {
	
	GENERAL(0, "general"), //
    STUDIO(100, "studio"), //
    RESIDENCE(200, "residence");
	
	
	private final Integer value;
    private final String code;

    public static LocationType fromInt(final Integer statusValue) {

    	LocationType enumeration = null;
        switch (statusValue) {
            case 0:
                enumeration = LocationType.GENERAL;
            break;
            case 100:
                enumeration = LocationType.STUDIO;
                break;
            case 200:
                enumeration = LocationType.RESIDENCE;
                break;
        }
        return enumeration;
    }

    private LocationType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public boolean hasStateOf(final LocationType state) {
        return this.value.equals(state.getValue());
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    public boolean isGeneral() {
        return this.value.equals(LocationType.GENERAL.getValue());
    }

    public boolean isStudio() {
        return this.value.equals(LocationType.STUDIO.getValue());
    }

    public boolean isResidence() {
        return this.value.equals(LocationType.RESIDENCE.getValue());
    }

}
