package org.jammate.platform.jamhosts.data;

import java.util.List;

public class LandingResponseData {
	
	final List<JamHostData> hosts;
	final LastBookingData firstLoginAfterLastBooking;
	
	public static LandingResponseData instance(final List<JamHostData> hosts,
			final LastBookingData firstLoginAfterLastBooking) {
		return new LandingResponseData(hosts, firstLoginAfterLastBooking);
	}
	
	private LandingResponseData(final List<JamHostData> hosts,
			final LastBookingData firstLoginAfterLastBooking) {
		this.hosts = hosts;
		this.firstLoginAfterLastBooking = firstLoginAfterLastBooking;
	}
	

}
