package org.jammate.platform.jamhosts.data;

public class QuoteData {

	private final Long roomId;
	private final String studioName;
	private final String	area;
	private final String status;
	private final String message;
	
	public static QuoteData instance(final Long roomId, 
			final String studioName, final String area,
			final String status, final String message) {
		return new QuoteData(roomId, studioName, area,
				status, message);
	}
	
	private QuoteData(final Long roomId, 
			final String studioName, final String area,
			final String status, final String message) {
		this.roomId = roomId;
		this.studioName = studioName;
		this.area = area;
		this.status = status;
		this.message = message;
	}
}
