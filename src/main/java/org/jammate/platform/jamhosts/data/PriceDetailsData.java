package org.jammate.platform.jamhosts.data;

public class PriceDetailsData {

	private final String currency;
	private final Integer jammingHourlyWeekdayMorning ;
	private final Integer jammingHourlyWeekdayAfternoon ;
	private final Integer jammingHourlyWeekdayEvening ;
	private final Integer jammingHourlyWeekdayNight ;
	private final Integer jammingHourlyWeekendMorning ;
	private final Integer jammingHourlyWeekendAfternoon ;
	private final Integer jammingHourlyWeekendEvening ;
	private final Integer jammingHourlyWeekendNight ;
	private final Integer jammingFullWeekday ;
	private final Integer jammingFullWeekend ;
	private final Integer jammingXHourWeekday ;
	private final Integer jammingXHourWeekend ;
	private final Integer recordingHourlyWeekdayMorning ;
	private final Integer recordingHourlyWeekdayAfternoon ;
	private final Integer recordingHourlyWeekdayEvening ;
	private final Integer recordingHourlyWeekdayNight ;
	private final Integer recordingHourlyWeekendMorning ;
	private final Integer recordingHourlyWeekendAfternoon ;
	private final Integer recordingHourlyWeekendEvening ;
	private final Integer recordingHourlyWeekendNight ;
	private final Integer recordingFullWeekday ;
	private final Integer recordingFullWeekend ;
	private final Integer recordingXHourWeekday ;
	private final Integer recordingXHourWeekend ;
	private final Integer masteringHourlyWeekdayMorning ;
	private final Integer masteringHourlyWeekdayAfternoon ;
	private final Integer masteringHourlyWeekdayEvening ;
	private final Integer masteringHourlyWeekdayNight ;
	private final Integer masteringHourlyWeekendMorning ;
	private final Integer masteringHourlyWeekendAfternoon ;
	private final Integer masteringHourlyWeekendEvening ;
	private final Integer masteringHourlyWeekendNight ;
	private final Integer masteringFullWeekday ;
	private final Integer masteringFullWeekend ;
	private final Integer masteringXHourWeekday ;
	private final Integer masteringXHourWeekend ;
	private final Integer voiceoverHourlyWeekdayMorning ;
	private final Integer voiceoverHourlyWeekdayAfternoon ;
	private final Integer voiceoverHourlyWeekdayEvening ;
	private final Integer voiceoverHourlyWeekdayNight ;
	private final Integer voiceoverHourlyWeekendMorning ;
	private final Integer voiceoverHourlyWeekendAfternoon ;
	private final Integer voiceoverHourlyWeekendEvening ;
	private final Integer voiceoverHourlyWeekendNight ;
	private final Integer voiceoverFullWeekday ;
	private final Integer voiceoverFullWeekend ;
	private final Integer voiceoverXHourWeekday ;
	private final Integer voiceoverXHourWeekend ;
	
	public static PriceDetailsData instance(final String currency,
			final Integer jammingHourlyWeekdayMorning, final Integer jammingHourlyWeekdayAfternoon,
			final Integer jammingHourlyWeekdayEvening, final Integer jammingHourlyWeekdayNight,
			final Integer jammingHourlyWeekendMorning, final Integer jammingHourlyWeekendAfternoon,
			final Integer jammingHourlyWeekendEvening, final Integer jammingHourlyWeekendNight,
			final Integer jammingFullWeekday, final Integer jammingFullWeekend,
			final Integer jammingXHourWeekday, final Integer jammingXHourWeekend,
			final Integer recordingHourlyWeekdayMorning, final Integer recordingHourlyWeekdayAfternoon,
			final Integer recordingHourlyWeekdayEvening, final Integer recordingHourlyWeekdayNight,
			final Integer recordingHourlyWeekendMorning, final Integer recordingHourlyWeekendAfternoon,
			final Integer recordingHourlyWeekendEvening, final Integer recordingHourlyWeekendNight,
			final Integer recordingFullWeekday, final Integer recordingFullWeekend,
			final Integer recordingXHourWeekday, final Integer recordingXHourWeekend,
			final Integer masteringHourlyWeekdayMorning, final Integer masteringHourlyWeekdayAfternoon,
			final Integer masteringHourlyWeekdayEvening, final Integer masteringHourlyWeekdayNight,
			final Integer masteringHourlyWeekendMorning, final Integer masteringHourlyWeekendAfternoon,
			final Integer masteringHourlyWeekendEvening, final Integer masteringHourlyWeekendNight,
			final Integer masteringFullWeekday, final Integer masteringFullWeekend,
			final Integer masteringXHourWeekday, final Integer masteringXHourWeekend,
			final Integer voiceoverHourlyWeekdayMorning, final Integer voiceoverHourlyWeekdayAfternoon,
			final Integer voiceoverHourlyWeekdayEvening, final Integer voiceoverHourlyWeekdayNight,
			final Integer voiceoverHourlyWeekendMorning, final Integer voiceoverHourlyWeekendAfternoon,
			final Integer voiceoverHourlyWeekendEvening, final Integer voiceoverHourlyWeekendNight,
			final Integer voiceoverFullWeekday, final Integer voiceoverFullWeekend,
			final Integer voiceoverXHourWeekday, final Integer voiceoverXHourWeekend) {
		return new PriceDetailsData(currency, jammingHourlyWeekdayMorning, jammingHourlyWeekdayAfternoon, jammingHourlyWeekdayEvening, jammingHourlyWeekdayNight,
				jammingHourlyWeekendMorning, jammingHourlyWeekendAfternoon, jammingHourlyWeekendEvening, jammingHourlyWeekendNight,
				jammingFullWeekday, jammingFullWeekend, jammingXHourWeekday, jammingXHourWeekend,
				recordingHourlyWeekdayMorning, recordingHourlyWeekdayAfternoon, recordingHourlyWeekdayEvening, recordingHourlyWeekdayNight,
				recordingHourlyWeekendMorning, recordingHourlyWeekendAfternoon, recordingHourlyWeekendEvening, recordingHourlyWeekendNight,
				recordingFullWeekday, recordingFullWeekend, recordingXHourWeekday, recordingXHourWeekend,
				masteringHourlyWeekdayMorning, masteringHourlyWeekdayAfternoon, masteringHourlyWeekdayEvening, masteringHourlyWeekdayNight,
				masteringHourlyWeekendMorning, masteringHourlyWeekendAfternoon, masteringHourlyWeekendEvening, masteringHourlyWeekendNight,
				masteringFullWeekday, masteringFullWeekend, masteringXHourWeekday, masteringXHourWeekend,
				voiceoverHourlyWeekdayMorning, voiceoverHourlyWeekdayAfternoon, voiceoverHourlyWeekdayEvening, voiceoverHourlyWeekdayNight,
				voiceoverHourlyWeekendMorning, voiceoverHourlyWeekendAfternoon, voiceoverHourlyWeekendEvening, voiceoverHourlyWeekendNight,
				voiceoverFullWeekday, voiceoverFullWeekend, voiceoverXHourWeekday, voiceoverXHourWeekend);
	}
	
	private PriceDetailsData(final String currency,
			final Integer jammingHourlyWeekdayMorning, final Integer jammingHourlyWeekdayAfternoon,
			final Integer jammingHourlyWeekdayEvening, final Integer jammingHourlyWeekdayNight,
			final Integer jammingHourlyWeekendMorning, final Integer jammingHourlyWeekendAfternoon,
			final Integer jammingHourlyWeekendEvening, final Integer jammingHourlyWeekendNight,
			final Integer jammingFullWeekday, final Integer jammingFullWeekend,
			final Integer jammingXHourWeekday, final Integer jammingXHourWeekend,
			final Integer recordingHourlyWeekdayMorning, final Integer recordingHourlyWeekdayAfternoon,
			final Integer recordingHourlyWeekdayEvening, final Integer recordingHourlyWeekdayNight,
			final Integer recordingHourlyWeekendMorning, final Integer recordingHourlyWeekendAfternoon,
			final Integer recordingHourlyWeekendEvening, final Integer recordingHourlyWeekendNight,
			final Integer recordingFullWeekday, final Integer recordingFullWeekend,
			final Integer recordingXHourWeekday, final Integer recordingXHourWeekend,
			final Integer masteringHourlyWeekdayMorning, final Integer masteringHourlyWeekdayAfternoon,
			final Integer masteringHourlyWeekdayEvening, final Integer masteringHourlyWeekdayNight,
			final Integer masteringHourlyWeekendMorning, final Integer masteringHourlyWeekendAfternoon,
			final Integer masteringHourlyWeekendEvening, final Integer masteringHourlyWeekendNight,
			final Integer masteringFullWeekday, final Integer masteringFullWeekend,
			final Integer masteringXHourWeekday, final Integer masteringXHourWeekend,
			final Integer voiceoverHourlyWeekdayMorning, final Integer voiceoverHourlyWeekdayAfternoon,
			final Integer voiceoverHourlyWeekdayEvening, final Integer voiceoverHourlyWeekdayNight,
			final Integer voiceoverHourlyWeekendMorning, final Integer voiceoverHourlyWeekendAfternoon,
			final Integer voiceoverHourlyWeekendEvening, final Integer voiceoverHourlyWeekendNight,
			final Integer voiceoverFullWeekday, final Integer voiceoverFullWeekend,
			final Integer voiceoverXHourWeekday, final Integer voiceoverXHourWeekend) {
		this.currency = currency;
		this.jammingHourlyWeekdayMorning = jammingHourlyWeekdayMorning;
		this.jammingHourlyWeekdayAfternoon = jammingHourlyWeekdayAfternoon;
		this.jammingHourlyWeekdayEvening = jammingHourlyWeekdayEvening;
		this.jammingHourlyWeekdayNight = jammingHourlyWeekdayNight;
		this.jammingHourlyWeekendMorning = jammingHourlyWeekendMorning;
		this.jammingHourlyWeekendAfternoon = jammingHourlyWeekendAfternoon;
		this.jammingHourlyWeekendEvening = jammingHourlyWeekendEvening;
		this.jammingHourlyWeekendNight = jammingHourlyWeekendNight;
		this.jammingFullWeekday = jammingFullWeekday;
		this.jammingFullWeekend = jammingFullWeekend;
		this.jammingXHourWeekday = jammingXHourWeekday;
		this.jammingXHourWeekend = jammingXHourWeekend;
		this.recordingHourlyWeekdayMorning = recordingHourlyWeekdayMorning;
		this.recordingHourlyWeekdayAfternoon = recordingHourlyWeekdayAfternoon;
		this.recordingHourlyWeekdayEvening = recordingHourlyWeekdayEvening;
		this.recordingHourlyWeekdayNight = recordingHourlyWeekdayNight;
		this.recordingHourlyWeekendMorning = recordingHourlyWeekendMorning;
		this.recordingHourlyWeekendAfternoon = recordingHourlyWeekendAfternoon;
		this.recordingHourlyWeekendEvening = recordingHourlyWeekendEvening;
		this.recordingHourlyWeekendNight = recordingHourlyWeekendNight;
		this.recordingFullWeekday = recordingFullWeekday;
		this.recordingFullWeekend = recordingFullWeekend;
		this.recordingXHourWeekday = recordingXHourWeekday;
		this.recordingXHourWeekend = recordingXHourWeekend;
		this.masteringHourlyWeekdayMorning = masteringHourlyWeekdayMorning;
		this.masteringHourlyWeekdayAfternoon = masteringHourlyWeekdayAfternoon;
		this.masteringHourlyWeekdayEvening = masteringHourlyWeekdayEvening;
		this.masteringHourlyWeekdayNight = masteringHourlyWeekdayNight;
		this.masteringHourlyWeekendMorning = masteringHourlyWeekendMorning;
		this.masteringHourlyWeekendAfternoon = masteringHourlyWeekendAfternoon;
		this.masteringHourlyWeekendEvening = masteringHourlyWeekendEvening;
		this.masteringHourlyWeekendNight = masteringHourlyWeekendNight;
		this.masteringFullWeekday = masteringFullWeekday;
		this.masteringFullWeekend = masteringFullWeekend;
		this.masteringXHourWeekday = masteringXHourWeekday;
		this.masteringXHourWeekend = masteringXHourWeekend;
		this.voiceoverHourlyWeekdayMorning = voiceoverHourlyWeekdayMorning;
		this.voiceoverHourlyWeekdayAfternoon = voiceoverHourlyWeekdayAfternoon;
		this.voiceoverHourlyWeekdayEvening = voiceoverHourlyWeekdayEvening;
		this.voiceoverHourlyWeekdayNight = voiceoverHourlyWeekdayNight;
		this.voiceoverHourlyWeekendMorning = voiceoverHourlyWeekendMorning;
		this.voiceoverHourlyWeekendAfternoon = voiceoverHourlyWeekendAfternoon;
		this.voiceoverHourlyWeekendEvening = voiceoverHourlyWeekendEvening;
		this.voiceoverHourlyWeekendNight = voiceoverHourlyWeekendNight;
		this.voiceoverFullWeekday = voiceoverFullWeekday;
		this.voiceoverFullWeekend = voiceoverFullWeekend;
		this.voiceoverXHourWeekday = voiceoverXHourWeekday;
		this.voiceoverXHourWeekend = voiceoverXHourWeekend;
	}
}
