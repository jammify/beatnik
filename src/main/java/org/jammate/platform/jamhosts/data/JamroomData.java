package org.jammate.platform.jamhosts.data;

import java.util.List;

public class JamroomData {
	
	private final Long roomId;
	private List<Long> imgIds;
	private final String studioName;
	private final Double avgRating;
	private final Long profilePic;
	private final String houseNo;
	private final String street;
	private final String area;
	private final String landmark;
	private final String openingHours;
	private final String closingHours;
	private final Boolean hasJamming;
	private final Boolean hasRecording;
	private final Boolean hasMixingAndMastering;
	private final Integer minCapacity;
	private final Integer maxCapacity;
	private final Boolean hasSoundproofing;
	private final String drumkitBrand;
	private final Integer drumkitPieces;
	private final List<String> amplifierBrands;
	private final Integer noOfAmplifiers;
	private final Integer noOfStools;
	private final String speakerBrand;
	private Integer totalMics;
	private Integer totalMicsWithCondenser;
	private Integer totalWirelessMics;
	private Integer totalInstrumentalMics;
	private final Integer mixerBoardInputs;
	private final List<String> otherItems;
	private final String facebookPage;
	private final String website;
	private final String latitude;
	private final String longitude;
	private final PriceData priceData;
	private final PriceDetailsData price;
	private final Integer xHourPackage;
	private final List<Integer> availableServices;
	private List<MicData> mics;
	
	public static JamroomData instance(final Long roomId, final List<Long> imgIds, final String studioName,
			final Double avgRating, final Long profilePic, final String houseNo,
			final String street, final String area, final String landmark,
			final String openingHours, final String closingHours,
			final Boolean hasJamming, final Boolean hasRecording,
			final Boolean hasMixingAndMastering, final Integer minCapacity,
			final Integer maxCapacity, final Boolean hasSoundproofing,
			final String drumkitBrand, final Integer drumkitPieces,
			final List<String> amplifierBrands, final Integer noOfAmplifiers,
			final Integer totalMics, final Integer totalMicsWithCondenser,
			final Integer totalWirelessMics, final Integer totalInstrumentalMics,
			final Integer mixerBoardInputs, final List<String> otherItems,
			final String facebookPage, final String website, final String latitude,
			final String longitude, final PriceData priceData, final PriceDetailsData price,
			final Integer xHourPackage, final List<Integer> availableServices,
			final Integer noOfStools, final String speakerBrand) {
		return new JamroomData( roomId, imgIds,  studioName, avgRating,  profilePic,  houseNo,
				 street,  area,  landmark, openingHours,  closingHours,
				 hasJamming,  hasRecording, hasMixingAndMastering,  minCapacity,
				 maxCapacity,  hasSoundproofing, drumkitBrand,  drumkitPieces,
				 amplifierBrands,  noOfAmplifiers, totalMics,  totalMicsWithCondenser,
				 totalWirelessMics,  totalInstrumentalMics, mixerBoardInputs,  otherItems,
				 facebookPage,  website,  latitude, longitude, priceData, price,
				 xHourPackage, availableServices, noOfStools, speakerBrand);
	}

	private JamroomData(final Long roomId, final List<Long> imgIds, final String studioName,
			final Double avgRating, final Long profilePic, final String houseNo,
			final String street, final String area, final String landmark,
			final String openingHours, final String closingHours,
			final Boolean hasJamming, final Boolean hasRecording,
			final Boolean hasMixingAndMastering, final Integer minCapacity,
			final Integer maxCapacity, final Boolean hasSoundproofing,
			final String drumkitBrand, final Integer drumkitPieces,
			final List<String> amplifierBrands, final Integer noOfAmplifiers,
			final Integer totalMics, final Integer totalMicsWithCondenser,
			final Integer totalWirelessMics, final Integer totalInstrumentalMics,
			final Integer mixerBoardInputs, final List<String> otherItems,
			final String facebookPage, final String website, final String latitude,
			final String longitude, final PriceData priceData, final PriceDetailsData price,
			final Integer xHourPackage, final List<Integer> availableServices,
			final Integer noOfStools, final String speakerBrand) {
		this.roomId = roomId;
		this.imgIds = imgIds;
		this.studioName = studioName;
		this.avgRating = avgRating;
		this.profilePic = profilePic;
		this.houseNo = houseNo;
		this.street = street;
		this.area = area;
		this.landmark = landmark;
		this.noOfStools = noOfStools;
		this.speakerBrand = speakerBrand;
		this.openingHours = openingHours;
		this.closingHours = closingHours;
		this.hasJamming = hasJamming;
		this.hasRecording = hasRecording;
		this.hasMixingAndMastering = hasMixingAndMastering;
		this.minCapacity = minCapacity;
		this.maxCapacity = maxCapacity;
		this.hasSoundproofing = hasSoundproofing;
		this.drumkitBrand = drumkitBrand;
		this.drumkitPieces = drumkitPieces;
		this.amplifierBrands = amplifierBrands;
		this.noOfAmplifiers = noOfAmplifiers;
		this.totalMics = totalMics;
		this.totalMicsWithCondenser = totalMicsWithCondenser;
		this.totalWirelessMics = totalWirelessMics;
		this.totalInstrumentalMics = totalInstrumentalMics;
		this.mixerBoardInputs = mixerBoardInputs;
		this.otherItems = otherItems;
		this.facebookPage = facebookPage;
		this.website = website;
		this.latitude = latitude;
		this.longitude = longitude;
		this.priceData = priceData;
		this.price = price;
		this.xHourPackage = xHourPackage;
		this.availableServices = availableServices;
	}
	
	public void setImageIds(final List<Long> imageIds) {
		this.imgIds = imageIds;
	}
	
	public void setMics(final List<MicData> mics) {
		this.mics = mics;
		setTotalMics(mics.size());
		Integer totalCondenser = 0;
		Integer totalWireless = 0;
		Integer totalInstrumental = 0;
		for(MicData mic : mics) {
			if(mic.hasCondenser())
				totalCondenser++;
			if(mic.forInstrument())
				totalInstrumental++;
			if(mic.isWireless())
				totalWireless++;
		}
		setTotalMicsWithCondenser(totalCondenser);
		setTotalWirelessMics(totalWireless);
		setTotalInstrumentalMics(totalInstrumental);
	}

	private void setTotalMics(final Integer totalMics) {
		this.totalMics = totalMics;
	}
	
	private void setTotalMicsWithCondenser(final Integer totalMics) {
		this.totalMicsWithCondenser = totalMics;
	}
	
	private void setTotalWirelessMics(final Integer totalMics) {
		this.totalWirelessMics = totalMics;
	}
	
	private void setTotalInstrumentalMics(final Integer totalMics) {
		this.totalInstrumentalMics = totalMics;
	}

}
