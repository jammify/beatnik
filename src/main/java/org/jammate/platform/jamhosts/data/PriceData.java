package org.jammate.platform.jamhosts.data;

public class PriceData {
	
	private final String currency;
	private final Integer minHourly;
	private final Integer maxHourly;
	private final Integer avgHourly;
	private final Integer avgXHourPackage;
	private final Integer fullDay;
	private final Integer recordingMinHourly;
	private final Integer recordingMaxHourly;
	private final Integer avgRecordingHourly;
	private final Integer recordingAvgXHourPackage ;
	private final Integer recordingFullDay ;
	private final Integer voiceoverMinHourly;
	private final Integer voiceoverMaxHourly;
	private final Integer avgVoiceoverHourly;
	private final Integer voiceoverAvgXHourPackage ;
	private final Integer voiceoverFullDay ;
	private final Integer mixingMasteringMinHourly;
	private final Integer mixingMasteringMaxHourly;
	private final Integer avgMixingMasteringHourly;
	private final Integer mixingMasteringAvgXHourPackage ;
	private final Integer mixingMasteringFullDay ;
	private final Integer overallAvg;
	
	public static PriceData instance(final String currency, final Integer minHourly, final Integer maxHourly,
			final Integer avgHourly, final Integer avgXHourPackage, final Integer fullDay,
			final Integer recordingMinHourly, final Integer recordingMaxHourly,
			final Integer avgRecordingHourly, final Integer recordingAvgXHourPackage ,
			final Integer recordingFullDay , final Integer voiceoverMinHourly,
			final Integer voiceoverMaxHourly, final Integer avgVoiceoverHourly,
			final Integer voiceoverAvgXHourPackage , final Integer voiceoverFullDay ,
			final Integer mixingMasteringMinHourly, final Integer mixingMasteringMaxHourly,
			final Integer avgMixingMasteringHourly, final Integer mixingMasteringAvgXHourPackage ,
			final Integer mixingMasteringFullDay, final Integer overallAvg) {
		return new PriceData(currency, minHourly, maxHourly, avgHourly, avgXHourPackage, fullDay,
				recordingMinHourly, recordingMaxHourly, avgRecordingHourly, recordingAvgXHourPackage ,
				recordingFullDay , voiceoverMinHourly, voiceoverMaxHourly, avgVoiceoverHourly,
				voiceoverAvgXHourPackage , voiceoverFullDay , mixingMasteringMinHourly, mixingMasteringMaxHourly,
				avgMixingMasteringHourly, mixingMasteringAvgXHourPackage , mixingMasteringFullDay, overallAvg);
		
	}
	
	public PriceData(final String currency, final Integer minHourly, final Integer maxHourly,
			final Integer avgHourly, final Integer avgXHourPackage, final Integer fullDay,
			final Integer recordingMinHourly, final Integer recordingMaxHourly,
			final Integer avgRecordingHourly, final Integer recordingAvgXHourPackage ,
			final Integer recordingFullDay , final Integer voiceoverMinHourly,
			final Integer voiceoverMaxHourly, final Integer avgVoiceoverHourly,
			final Integer voiceoverAvgXHourPackage , final Integer voiceoverFullDay ,
			final Integer mixingMasteringMinHourly, final Integer mixingMasteringMaxHourly,
			final Integer avgMixingMasteringHourly, final Integer mixingMasteringAvgXHourPackage ,
			final Integer mixingMasteringFullDay, final Integer overallAvg) {
		this.currency = currency ;
		this.minHourly = minHourly ;
		this.maxHourly = maxHourly ;
		this.avgHourly = avgHourly;
		this.avgXHourPackage = avgXHourPackage;
		this.fullDay = fullDay;
		this.recordingMinHourly = recordingMinHourly ;
		this.recordingMaxHourly = recordingMaxHourly ;
		this.avgRecordingHourly = avgRecordingHourly ;
		this.recordingAvgXHourPackage = recordingAvgXHourPackage ;
		this.recordingFullDay = recordingFullDay ;
		this.voiceoverMinHourly = voiceoverMinHourly ;
		this.voiceoverMaxHourly = voiceoverMaxHourly ;
		this.avgVoiceoverHourly = avgVoiceoverHourly ;
		this.voiceoverAvgXHourPackage = voiceoverAvgXHourPackage ;
		this.voiceoverFullDay = voiceoverFullDay ;
		this.mixingMasteringMinHourly =  mixingMasteringMinHourly;
		this.mixingMasteringMaxHourly =  mixingMasteringMaxHourly;
		this.avgMixingMasteringHourly =  avgMixingMasteringHourly;
		this.mixingMasteringAvgXHourPackage = mixingMasteringAvgXHourPackage ;
		this.mixingMasteringFullDay = mixingMasteringFullDay ;
		this.overallAvg = overallAvg;
	}

}
