package org.jammate.platform.jamhosts.data;

import java.util.HashMap;
import java.util.Map;

public enum TimeRange {
	
	MORNING(100, "Morning    (Till 12 noon)"),
	AFTERNOON(200, "Afternoon  (12 noon - 4 pm)"),
	EVENING(300, "Evening    (4 pm - 7 pm)"),
	NIGHT(400, "Night      (8 pm - Closing Hours)");

    private final Integer value;
    private final String code;

    private TimeRange(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, TimeRange> intToEnumMap = new HashMap<>();
    static {
        for (final TimeRange type : TimeRange.values()) {
            intToEnumMap.put(type.value, type);
        }
    }

    public static TimeRange fromInt(final int i) {
        final TimeRange type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }


}
