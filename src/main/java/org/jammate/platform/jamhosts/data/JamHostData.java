package org.jammate.platform.jamhosts.data;

import java.io.Serializable;

public class JamHostData implements Serializable {

	private final Long roomId;
	private final String studioName;
	private final String roomName;
	private final String area; 
	private final Integer overallAverage;
	private final Integer avgHourly; 
	private final Integer avgRecordingHourly; 
	private final Integer avgMixingMasteringHourly;
	private final Boolean hasJamming; 
	private final Boolean hasRecording; 
	private final Boolean hasMixingAndMastering; 
	private final Double avgRating;
	private final Long primaryImageId;

    public static JamHostData instance(final Long roomId, final String studioName,
    		final String roomName, final String area, final Integer overallAverage,
    		final Integer avgHourly, final Integer avgRecordingHourly,
    		final Integer avgMixingMasteringHourly, final Boolean hasJamming,
    		final Boolean hasRecording, final Boolean hasMixingAndMastering,
    		final Double avgRating, final Long primaryImageId) {
        return new JamHostData(roomId, studioName, roomName, area, overallAverage,
        		avgHourly, avgRecordingHourly, avgMixingMasteringHourly,
        		hasJamming, hasRecording, hasMixingAndMastering, avgRating, primaryImageId);
    }

    private JamHostData(final Long roomId, final String studioName,
    		final String roomName, final String area, final Integer overallAverage,
    		final Integer avgHourly, final Integer avgRecordingHourly,
    		final Integer avgMixingMasteringHourly, final Boolean hasJamming,
    		final Boolean hasRecording, final Boolean hasMixingAndMastering,
    		final Double avgRating, final Long primaryImageId) {
        this.roomId = roomId;
        this.studioName = studioName;
        this.roomName = roomName;
        this.area = area;
        this.overallAverage = overallAverage;
        this.avgHourly = avgHourly;
        this.avgRecordingHourly = avgRecordingHourly;
        this.avgMixingMasteringHourly = avgMixingMasteringHourly;
        this.hasJamming = hasJamming;
        this.hasRecording = hasRecording;
        this.hasMixingAndMastering = hasMixingAndMastering;
        this.avgRating = avgRating;
        this.primaryImageId = primaryImageId;
    }
}
