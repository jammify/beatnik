package org.jammate.platform.jamhosts.data;

import java.util.HashMap;
import java.util.Map;

public enum QuoteStatus {
	
	REQUESTED(100, "Requested"), RESPONDED(200, "Responded");
	
	private final Integer value;
    private final String code;

    private QuoteStatus(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getCode() {
        return this.code;
    }

    private static final Map<Integer, QuoteStatus> intToEnumMap = new HashMap<>();
    static {
        for (final QuoteStatus type : QuoteStatus.values()) {
            intToEnumMap.put(type.value, type);
        }
    }

    public static QuoteStatus fromInt(final int i) {
        final QuoteStatus type = intToEnumMap.get(Integer.valueOf(i));
        return type;
    }

    @Override
    public String toString() {
        return name().toString();
    }

}
