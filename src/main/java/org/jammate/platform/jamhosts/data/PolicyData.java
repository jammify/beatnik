package org.jammate.platform.jamhosts.data;

import java.io.Serializable;
import java.util.List;

public class PolicyData implements Serializable {
	
	private final List<CancellationPolicyData> cancellationPolicy;
	private final String studioName;
	private final Long profilePicId;
	private final String address;
	
	public static PolicyData instance(final List<CancellationPolicyData> cancellationPolicy,
			final String studioName, final Long profilePicId, final String address) {
		return new PolicyData(cancellationPolicy, studioName, profilePicId, address);
	}
	
	private PolicyData(final List<CancellationPolicyData> cancellationPolicy,
			final String studioName, final Long profilePicId, final String address) {
		this.cancellationPolicy = cancellationPolicy;
		this.studioName = studioName;
		this.profilePicId = profilePicId;
		this.address = address;
	}
	
	public List<CancellationPolicyData> getCancellationPolicy() {
		return this.cancellationPolicy;
	}
	
	public Integer getRefundPercentage(final long hours) {
		Integer refundPercentage = null;
		for(CancellationPolicyData data : cancellationPolicy) {
			Integer endHours = null;
			if(data.getEnd() == null)
				endHours = Integer.MAX_VALUE;
			else 
				endHours = data.getEnd();
			if(hours > data.getStart() && hours < endHours)
				refundPercentage = data.getPercentageRefund();
		}
		return refundPercentage;
	}
}