package org.jammate.platform.jamhosts.data;

public class BlockedData {
	
	private String timeFrom;
	private String timeTo;
	private String name;
	private String status;
	private String contact;
	private Long orderId;
	private Long blockId;
	
	public static BlockedData instance(final String timeFrom, final String timeTo, final Long blockId) {
		return new BlockedData(timeFrom, timeTo, null, null, null, null, blockId);
	}
	
	public static BlockedData instance(final String timeFrom, final String timeTo,
			final String name, final String status, final String contact,
			final Long orderId) {
		return new BlockedData(timeFrom, timeTo, name, status, contact, orderId, null);
	}
	
	
	private BlockedData(final String timeFrom, final String timeTo,
			final String name, final String status, final String contact,
			final Long orderId, final Long blockId) {
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.name = name;
		this.status = status;
		this.contact = contact;
		this.orderId = orderId;
		this.blockId = blockId;
	}
	
	public String getTimeTo() {
		return this.timeTo;
	}
	
	public String getTimeFrom() {
		return this.timeFrom;
	}
	
	public void setTimeTo(final String timeTo) {
		this.timeTo = timeTo;
	}

}
