package org.jammate.platform.jamhosts.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.jammate.platform.jamhosts.data.HostService;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "room_price")
public class JamRoomPrice extends AbstractPersistable<Long> {
	
	@OneToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;

	@Column(name = "jammingFullWeekday")
	private  Integer jammingFullWeekday ;

	@Column(name = "jammingFullWeekend")
	private  Integer jammingFullWeekend ;

	@Column(name = "jammingXHourWeekday")
	private  Integer jammingXHourWeekday ;

	@Column(name = "jammingXHourWeekend")
	private  Integer jammingXHourWeekend ;

	@Column(name = "recordingFullWeekday")
	private  Integer recordingFullWeekday ;

	@Column(name = "recordingFullWeekend")
	private  Integer recordingFullWeekend ;

	@Column(name = "recordingXHourWeekday")
	private  Integer recordingXHourWeekday ;

	@Column(name = "recordingXHourWeekend")
	private  Integer recordingXHourWeekend ;

	@Column(name = "masteringFullWeekday")
	private  Integer masteringFullWeekday ;

	@Column(name = "masteringFullWeekend")
	private  Integer masteringFullWeekend ;

	@Column(name = "masteringXHourWeekday")
	private  Integer masteringXHourWeekday ;

	@Column(name = "masteringXHourWeekend")
	private  Integer masteringXHourWeekend ;

	@Column(name = "voiceoverFullWeekday")
	private  Integer voiceoverFullWeekday ;

	@Column(name = "voiceoverFullWeekend")
	private  Integer voiceoverFullWeekend ;

	@Column(name = "voiceoverXHourWeekday")
	private  Integer voiceoverXHourWeekday ;

	@Column(name = "voiceoverXHourWeekend")
	private  Integer voiceoverXHourWeekend ;
	
	@Column(name = "minHourly")
	private Integer minHourly;
	
	@Column(name = "maxHourly")
	private Integer maxHourly;
	
	@Column(name = "avgXHourPackage")
	private Integer avgXHourPackage;
	
	@Column(name = "fullDay")
	private Integer fullDay;
	
	@Column(name = "recordingMinHourly")
	private Integer  recordingMinHourly;
	
	@Column(name = "recordingMaxHourly")
	private Integer recordingMaxHourly;
	
	@Column(name = "recordingAvgXHourPackage")
	private Integer recordingAvgXHourPackage;
	
	@Column(name = "recordingFullDay")
	private Integer recordingFullDay;
	
	@Column(name = "voiceoverMinHourly")
	private Integer voiceoverMinHourly;
	
	@Column(name = "voiceoverMaxHourly")
	private Integer voiceoverMaxHourly;
	
	@Column(name = "voiceoverAvgXHourPackage")
	private Integer voiceoverAvgXHourPackage;
	
	@Column(name = "voiceoverFullDay")
	private Integer voiceoverFullDay;
	
	@Column(name = "mixingMasteringMinHourly")
	private Integer mixingMasteringMinHourly;
	
	@Column(name = "mixingMasteringMaxHourly")
	private Integer mixingMasteringMaxHourly;
	
	@Column(name = "mixingMasteringAvgXHourPackage")
	private Integer mixingMasteringAvgXHourPackage;
	
	@Column(name = "mixingMasteringFullDay")
	private Integer mixingMasteringFullDay;
	
	private transient Set<JamRoomPriceFlexi> priceWindows;
	
	public static JamRoomPrice instance(final Set<JamRoomPriceFlexi> priceWindows,
			final Integer jammingFullWeekday ,
			final Integer jammingFullWeekend ,final Integer jammingXHourWeekday ,final Integer jammingXHourWeekend ,
			final Integer recordingFullWeekday ,
			final Integer recordingFullWeekend ,final Integer recordingXHourWeekday ,final Integer recordingXHourWeekend,
			final Integer masteringFullWeekday ,final Integer masteringFullWeekend ,
			final Integer masteringXHourWeekday ,final Integer masteringXHourWeekend, 
			final Integer voiceoverFullWeekday ,final Integer voiceoverFullWeekend ,
			final Integer voiceoverXHourWeekday ,final Integer voiceoverXHourWeekend, 
			final Integer minHourly, final Integer maxHourly,
			final Integer avgXHourPackage, final Integer fullDay,
			final Integer recordingMinHourly, final Integer recordingMaxHourly,
			final Integer recordingAvgXHourPackage, final Integer recordingFullDay,
			final Integer voiceoverMinHourly, final Integer voiceoverMaxHourly,
			final Integer voiceoverAvgXHourPackage, final Integer voiceoverFullDay,
			final Integer mixingMasteringMinHourly, final Integer mixingMasteringMaxHourly,
			final Integer mixingMasteringAvgXHourPackage, final Integer mixingMasteringFullDay) {
		
		return new JamRoomPrice(priceWindows, jammingFullWeekday ,
				jammingFullWeekend ,jammingXHourWeekday ,jammingXHourWeekend ,
				recordingFullWeekday ,
				recordingFullWeekend ,recordingXHourWeekday ,recordingXHourWeekend, 
				masteringFullWeekday ,masteringFullWeekend ,
				masteringXHourWeekday ,masteringXHourWeekend, 
				voiceoverFullWeekday, voiceoverFullWeekend,
				voiceoverXHourWeekday ,voiceoverXHourWeekend, minHourly, maxHourly, avgXHourPackage, fullDay,
				recordingMinHourly, recordingMaxHourly,
				recordingAvgXHourPackage, recordingFullDay,
				voiceoverMinHourly, voiceoverMaxHourly,
				voiceoverAvgXHourPackage, voiceoverFullDay,
				mixingMasteringMinHourly, mixingMasteringMaxHourly,
				mixingMasteringAvgXHourPackage, mixingMasteringFullDay);
	}
	
	protected JamRoomPrice() {
		//
	}
	
	private JamRoomPrice(final Set<JamRoomPriceFlexi> priceWindows,
			final Integer jammingFullWeekday,
			final Integer jammingFullWeekend, final Integer jammingXHourWeekday, final Integer jammingXHourWeekend ,
			final Integer recordingFullWeekday ,
			final Integer recordingFullWeekend ,final Integer recordingXHourWeekday ,final Integer recordingXHourWeekend,
			final Integer masteringFullWeekday ,final Integer masteringFullWeekend,
			final Integer masteringXHourWeekday ,final Integer masteringXHourWeekend, 
			final Integer voiceoverFullWeekday ,final Integer voiceoverFullWeekend ,
			final Integer voiceoverXHourWeekday ,final Integer voiceoverXHourWeekend,
			final Integer minHourly, final Integer maxHourly,
			final Integer avgXHourPackage, final Integer fullDay,
			final Integer recordingMinHourly, final Integer recordingMaxHourly,
			final Integer recordingAvgXHourPackage, final Integer recordingFullDay,
			final Integer voiceoverMinHourly, final Integer voiceoverMaxHourly,
			final Integer voiceoverAvgXHourPackage, final Integer voiceoverFullDay,
			final Integer mixingMasteringMinHourly, final Integer mixingMasteringMaxHourly,
			final Integer mixingMasteringAvgXHourPackage, final Integer mixingMasteringFullDay) {
		this.priceWindows = priceWindows;
		this.minHourly = minHourly;
		this.maxHourly = maxHourly;
		this.avgXHourPackage = avgXHourPackage;
		this.fullDay = fullDay;
		this.recordingMinHourly = recordingMinHourly;
		this.recordingMaxHourly = recordingMaxHourly;
		this.recordingAvgXHourPackage = recordingAvgXHourPackage;
		this.recordingFullDay = recordingFullDay;
		this.voiceoverMinHourly = voiceoverMinHourly;
		this.voiceoverMaxHourly = voiceoverMaxHourly;
		this.voiceoverAvgXHourPackage = voiceoverAvgXHourPackage;
		this.voiceoverFullDay = voiceoverFullDay;
		this.mixingMasteringMinHourly = mixingMasteringMinHourly;
		this.mixingMasteringMaxHourly = mixingMasteringMaxHourly;
		this.mixingMasteringAvgXHourPackage = mixingMasteringAvgXHourPackage;
		this.mixingMasteringFullDay = mixingMasteringFullDay;
		this.jammingFullWeekday = jammingFullWeekday ;
		this.jammingFullWeekend = jammingFullWeekend ;
		this.jammingXHourWeekday = jammingXHourWeekday ;
		this.jammingXHourWeekend = jammingXHourWeekend ;
		this.recordingFullWeekday = recordingFullWeekday ;
		this.recordingFullWeekend = recordingFullWeekend ;
		this.recordingXHourWeekday = recordingXHourWeekday ;
		this.recordingXHourWeekend = recordingXHourWeekend ;
		this.masteringFullWeekday = masteringFullWeekday ;
		this.masteringFullWeekend = masteringFullWeekend ;
		this.masteringXHourWeekday = masteringXHourWeekday ;
		this.masteringXHourWeekend = masteringXHourWeekend ;
		this.voiceoverFullWeekday = voiceoverFullWeekday ;
		this.voiceoverFullWeekend = voiceoverFullWeekend ;
		this.voiceoverXHourWeekday = voiceoverXHourWeekday ;
		this.voiceoverXHourWeekend = voiceoverXHourWeekend;
	}
	
	public void update(JamRoom room) {
		this.room = room;
	}
	
	public Integer getAvgHourly(HostService service) {
		 // 100 * 10 HRS WD, 400 * 4 HRS WD, 200 * 10 HRS WE, 800 * 4 HRS WE
		 // (100*10+400*4+200*10+800*4)/28 
		 Integer totalSum = 0;
		 Integer totalHours = 0;
		 for(JamRoomPriceFlexi flex : priceWindows) {
			 if(flex.getServiceType().equals(service.getValue())) {
				 totalSum += flex.getPrice() * flex.getTotalHours();
				 totalHours += flex.getTotalHours();
			 }
		 }
		 if(totalSum == 0 || totalHours == 0)
			 return null;
		 return (int) Math.round((totalSum/totalHours)/10.0) * 10;
	}
	
	
	public void setFlexiPrice(final Set<JamRoomPriceFlexi> prices) {
		this.priceWindows = prices;
	}
	
	public Set<JamRoomPriceFlexi> getFlexiPrice() {
		return this.priceWindows;
	}
	
	private static int zeroIfNull(Integer amount) {
	    return (amount == null) ? 0 : amount;
	}
	
	/** round n down to nearest multiple of m */
	private Integer roundDown(Integer n, Integer m) {
	    return n >= 0 ? (n / m) * m : ((n - m + 1) / m) * m;
	}
	
	public Integer getJammingFullWeekday() { return this.jammingFullWeekday; }
	public Integer getJammingFullWeekend() { return this.jammingFullWeekend; }
	public Integer getJammingXHourWeekday() { return this.jammingXHourWeekday; }
	public Integer getJammingXHourWeekend() { return this.jammingXHourWeekend; }
	public Integer getRecordingFullWeekday() { return this.recordingFullWeekday; }
	public Integer getRecordingFullWeekend() { return this.recordingFullWeekend; }
	public Integer getRecordingXHourWeekday() { return this.recordingXHourWeekday; }
	public Integer getRecordingXHourWeekend() { return this.recordingXHourWeekend; }
	public Integer getMasteringFullWeekday() { return this.masteringFullWeekday; }
	public Integer getMasteringFullWeekend() { return this.masteringFullWeekend; }
	public Integer getMasteringXHourWeekday() { return this.masteringXHourWeekday; }
	public Integer getMasteringXHourWeekend() { return this.masteringXHourWeekend; }
	public Integer getVoiceoverFullWeekday() { return this.voiceoverFullWeekday; }
	public Integer getVoiceoverFullWeekend() { return this.voiceoverFullWeekend; }
	public Integer getVoiceoverXHourWeekday() { return this.voiceoverXHourWeekday; }
	public Integer getVoiceoverXHourWeekend() { return this.voiceoverXHourWeekend; }

}
