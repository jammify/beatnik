package org.jammate.platform.jamhosts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.jammate.platform.calendar.data.Days;
import org.jammate.platform.core.infra.domain.AbstractPersistableCustom;

@Entity
@Table(name = "room_price_flexi")
public class JamRoomPriceFlexi extends AbstractPersistableCustom<Long> {
	
	@OneToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;

	@Column(name = "serviceType")
    private Integer serviceType;
	
	@Column(name = "dayOfWeekStart")
    private String dayOfWeekStart;
   
	@Column(name = "dayOfWeekEnd")
    private String dayOfWeekEnd;
  
	@Column(name = "timeStart")
    private String timeStart;
   
	@Column(name = "timeEnd")
    private String timeEnd;
	
	@Column(name = "price")
    private Integer price;
	
	public static JamRoomPriceFlexi instance(final Integer serviceType,
			final String dayOfWeekStart, final String dayOfWeekEnd, final String timeStart,
			final String timeEnd, final Integer price) {
		return new JamRoomPriceFlexi(serviceType, dayOfWeekStart, dayOfWeekEnd, timeStart, timeEnd,
				price);
	}
	
	protected JamRoomPriceFlexi() {
		//
	}
	
	private JamRoomPriceFlexi(final Integer serviceType,
			final String dayOfWeekStart, final String dayOfWeekEnd, final String timeStart,
			final String timeEnd, final Integer price) {
		this.serviceType = serviceType;
		this.dayOfWeekEnd = dayOfWeekEnd;
		this.dayOfWeekStart = dayOfWeekStart;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.price = price;
	}
	
	public void update(JamRoom room) {
		this.room = room;
	}
	
	public String getDayOfWeekStart() {
		return this.dayOfWeekStart;
	}
	
	public String getDayOfWeekEnd() {
		return this.dayOfWeekEnd;
	}
	
	public Integer getTimeStart() {
		return Integer.parseInt(this.timeStart);
	}
	
	public Integer getTimeEnd() {
		return Integer.parseInt(this.timeEnd);
	}
	
	public Integer getTotalHours() {
		return getDayGap() * (getTimeEnd() - getTimeStart());
	}
	
	public Integer getDayGap() {
		return Days.diffBetweenDays(dayOfWeekStart, dayOfWeekEnd)+1;
	}
	
	public Integer getHours() {
		return getTimeEnd() - getTimeStart();
	}
	
	public Integer getServiceType() {
		return this.serviceType;
	}
	
	public JamRoom getRoom() {
		return this.room;
	}
	
	public Integer getPrice() {
		return this.price;
	}

}
