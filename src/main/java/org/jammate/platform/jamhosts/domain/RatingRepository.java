package org.jammate.platform.jamhosts.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RatingRepository extends JpaRepository<Rating, Long>,
	JpaSpecificationExecutor<Rating>{
	
}
