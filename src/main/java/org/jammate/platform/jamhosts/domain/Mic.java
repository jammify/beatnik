package org.jammate.platform.jamhosts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "mic")
public class Mic extends AbstractPersistable<Long> {
	
	@ManyToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;
	
	@Column(name = "condenser")
	private Boolean condenser;
	
	@Column(name = "wireless")
	private Boolean wireless;
	
	@Column(name = "instrument")
	private Boolean instrument;
	
	protected Mic() {
		
	}
	
	public static Mic getInstance(final Boolean condenser, final Boolean wireless, final Boolean instrument) {
		return new Mic(condenser, wireless, instrument);
	}
	
	private Mic(final Boolean condenser, final Boolean wireless, final Boolean instrument) {
		this.condenser = condenser;
		this.wireless = wireless;
		this.instrument = instrument;
	}
	
	public void update(JamRoom room) {
		this.room = room;
	}
	
	public Boolean hasCondenser() {
		return this.condenser;
	}
	
	public Boolean isWireless() {
		return this.wireless;
	}
	
	public Boolean forInstrument() {
		return this.instrument;
	}
	
	public JamRoom getRoom() {
		return this.room;
	}

}
