package org.jammate.platform.jamhosts.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.document.command.DocumentCommand;

@Entity
@Table(name = "quote")
public class Quote extends AbstractAuditableCustom<AppUser, Long> {
	
	@ManyToOne
    @JoinColumn(name = "roomId", referencedColumnName = "id", nullable = false)
	private JamRoom room;
	
	@Column(name = "noOfBandMembers")
	private Integer noOfBandMembers;
	
	@Column(name = "noOfSongs")
	private Integer noOfSongs;
	
	@Column(name = "avgSongLength")
	private Integer avgSongLength;
	
	@Column(name = "avgNoOfTracksPerSong")
	private Integer avgNoOfTracksPerSong;
	
	@Column(name = "message")
	private String message;
	
	private transient List<DocumentCommand> songSamples;
	
	protected Quote() {
		//
	}
	
	public static Quote instance(final JamRoom room, final Integer noOfBandMembers, final Integer noOfSongs,
			final Integer avgSongLength, final Integer avgNoOfTracksPerSong,
			final String message, final List<DocumentCommand> songSamples) {
		return new Quote(room, noOfBandMembers, noOfSongs, avgSongLength,
				avgNoOfTracksPerSong, message, songSamples);
	}
	
	private Quote(final JamRoom room, final Integer noOfBandMembers, final Integer noOfSongs,
			final Integer avgSongLength, final Integer avgNoOfTracksPerSong,
			final String message, final List<DocumentCommand> songSamples) {
		this.room = room;
		this.noOfBandMembers = noOfBandMembers;
		this.noOfSongs = noOfSongs;
		this.avgSongLength = avgSongLength;
		this.avgNoOfTracksPerSong = avgNoOfTracksPerSong;
		this.message = message;
		this.songSamples = songSamples;
	}
	
	public List<DocumentCommand> getSongSamples() {
		return this.songSamples;
	}
	
	

}
