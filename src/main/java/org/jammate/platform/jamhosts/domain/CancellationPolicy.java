package org.jammate.platform.jamhosts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;

@Entity
@Table(name = "cancellation_policy")
public class CancellationPolicy extends AbstractAuditableCustom<AppUser, Long>{
	
	@ManyToOne
    @JoinColumn(name = "hostId", referencedColumnName = "id", nullable = false)
	private JamHost host;
	
	@Column(name = "start")
	private Integer start;
	
	@Column(name = "end")
	private Integer end;
	
	@Column(name = "refund_percent")
	private Integer percent;
	
	protected CancellationPolicy() {
		
	}
	
	public static CancellationPolicy instance(final Integer start, final Integer end, final Integer percent) {
		return new CancellationPolicy(start, end, percent);
	}
	
	private CancellationPolicy(final Integer start, final Integer end, final Integer percent) {
		this.start = start;
		this.end = end;
		this.percent = percent;
	}
	
	public void update(JamHost host) {
		this.host = host;
	}

}
