package org.jammate.platform.jamhosts.domain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.jamhosts.api.HostApiConstants;
import org.springframework.util.CollectionUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Entity
@Table(name = "room")
public class JamRoom extends AbstractAuditableCustom<AppUser, Long> {

	@ManyToOne
    @JoinColumn(name = "hostId", referencedColumnName = "id", nullable = false)
	private JamHost host;
	
	@Column(name = "minCapacity")
	private Integer minCapacity;
	
	@Column(name = "maxCapacity")
	private Integer maxCapacity;
	
	@Column(name = "soundProofed")
	private Boolean soundProofed;
	
	@Column(name = "noOfStools")
	private Integer noOfStools;
	
	@Column(name = "mixerBoardInputs")
	private Integer mixerBoardInputs;
	
	@Column(name = "noOfMics")
	private Integer noOfMics;
	
	@Column(name = "drumkitBrandCvId")
	private Integer drumkitBrandCvId;
	
	@Column(name = "drumkitBrand")
	private String drumkitBrand;
	
	@Column(name = "speakerBrandCvId")
	private Integer speakerBrandCvId;
	
	@Column(name = "speakerBrand")
	private String speakerBrand;
	
	@Column(name = "otherItems")
	private String otherItems;
	
	@Column(name = "amplifierBrands")
	private String amplifierBrands;
	
	@Column(name = "noOfAmplifiers")
	private Integer noOfAmplifiers;
	
	@Column(name = "drumkitPieces")
	private Integer drumkitPieces;
	
	@Column(name = "hasJamming")
	private Boolean hasJamming;
	
	@Column(name = "hasMasterRecording")
	private Boolean hasMasterRecording;
	
	@Column(name = "hasRecording")
	private Boolean hasRecording;
	
	@Column(name = "hasVoiceover")
	private Boolean hasVoiceover;
	
	@Column(name = "avgHourly")
	private Integer avgHourly;
	
	@Column(name = "avgMixingMasteringHourly")
	private Integer avgMixingMasteringHourly;
	
	@Column(name = "avgRecordingHourly")
	private Integer avgRecordingHourly;
	
	@Column(name = "avgVoiceoverHourly")
	private Integer avgVoiceoverHourly;
	
	@Column(name= "xHourPackage")
	private Integer xHourPackage;
	
	@Column(name = "overallAverage")
	private Integer overallAverage;
	
	@Column(name = "avgRating")
	private Double avgRating;
	
	@Column(name = "totalReviews")
	private Integer totalReviews;
	
	@Column(name = "primaryImageId")
	private Long primaryImageId;
	
	private transient List<String> images;
	
	@LazyCollection(LazyCollectionOption.FALSE)
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "room", orphanRemoval = true)
    private JamRoomPrice price = null;
	
	@LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,  fetch = FetchType.EAGER, mappedBy = "room" )
	@Fetch(FetchMode.JOIN)
    private Set<Mic> mics;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "room", orphanRemoval = true)
    private Set<JamRoomPriceFlexi> flexiPrice = null;
	
	protected JamRoom() {
		
	}
	
	public static JamRoom getInstance(final Integer minCapacity, final Integer maxCapacity,
			final Integer avgHourly, final Boolean soundProofed,
			final Integer noOfStools, final Integer mixerBoardInputs, final Integer noOfMics,
			final Integer drumkitBrandCvId, final String drumkitBrand, final Integer speakerBrandCvId,
			final String speakerBrand, final Integer drumkitPieces, final Boolean hasRecording, final Boolean hasMasterRecording,
			final Integer avgRecordingHourly, final Integer avgMixingMasteringHourly, final Integer noOfAmplifiers,
			final String amplifierBrands, final String otherItems, final Set<Mic> mics, final List<String> images,
			final Boolean hasVoiceover, final Integer avgVoiceoverHourly, final Integer xHourPackage,
			final JamRoomPrice price, final Boolean hasJamming, final Set<JamRoomPriceFlexi> flexi) {
		return new JamRoom(minCapacity, maxCapacity, avgHourly, soundProofed,
				noOfStools, mixerBoardInputs, noOfMics, drumkitBrandCvId, drumkitBrand,
				speakerBrandCvId, speakerBrand, drumkitPieces, hasRecording, hasMasterRecording,
				avgRecordingHourly, avgMixingMasteringHourly, noOfAmplifiers, amplifierBrands,
				otherItems, mics, images, hasVoiceover, avgVoiceoverHourly, xHourPackage, price, hasJamming, flexi);
	}
	
	private JamRoom(final Integer minCapacity, final Integer maxCapacity,
			final Integer avgHourly, final Boolean soundProofed,
			final Integer noOfStools, final Integer mixerBoardInputs, final Integer noOfMics,
			final Integer drumkitBrandCvId, final String drumkitBrand,
			final Integer speakerBrandCvId, final String speakerBrand,
			final Integer drumkitPieces, final Boolean hasRecording, final Boolean hasMasterRecording,
			final Integer avgRecordingHourly, final Integer avgMixingMasteringHourly, final Integer noOfAmplifiers,
			final String amplifierBrands, final String otherItems, final Set<Mic> mics,
			final List<String> images, final Boolean hasVoiceover,
			final Integer avgVoiceoverHourly, final Integer xHourPackage,
			final JamRoomPrice price, final Boolean hasJamming, final Set<JamRoomPriceFlexi> flexi) {
		this.minCapacity = minCapacity;
		this.maxCapacity = maxCapacity;
		this.avgHourly = avgHourly;
		this.soundProofed = soundProofed;
		this.noOfMics = noOfMics;
		this.noOfStools = noOfStools;
		this.mixerBoardInputs = mixerBoardInputs;
		this.drumkitBrandCvId = drumkitBrandCvId;
		this.drumkitBrand = drumkitBrand;
		this.speakerBrand = speakerBrand;
		this.speakerBrandCvId = speakerBrandCvId;
		this.drumkitPieces = drumkitPieces;
		this.hasRecording = hasRecording;
		this.hasMasterRecording = hasMasterRecording;
		this.avgMixingMasteringHourly = avgMixingMasteringHourly;
		this.avgRecordingHourly = avgRecordingHourly;
		this.noOfAmplifiers = noOfAmplifiers;
		this.amplifierBrands = amplifierBrands;
		this.otherItems = otherItems;
		this.mics = associateMicsWithThisRoom(mics);
		this.images = images;
		this.hasVoiceover = hasVoiceover;
		this.xHourPackage = xHourPackage;
		this.avgVoiceoverHourly = avgVoiceoverHourly;
		this.price = associatePriceWithThisRoom(price);
		this.hasJamming = hasJamming;
		this.flexiPrice = associateFlexiPriceWithThisRoom(flexi);
	}
	
	public Map<String, String> update(final JsonElement element, final FromJsonHelper fromApiJsonHelper) {
		final Map<String, String> changes = new HashMap<>();
		 final JsonObject room = element.getAsJsonObject();
         //Updating room
         final Integer minCapacity = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.minCapacityParamName, element);
         if(minCapacity != null && !this.minCapacity.equals(minCapacity)) {
        	 changes.put(HostApiConstants.minCapacityParamName, minCapacity.toString());
        	 this.minCapacity = minCapacity;
         }
         final Integer maxCapacity = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.maxCapacityParamName, element);
         if (maxCapacity != null && !this.maxCapacity.equals(maxCapacity)) {
        	  changes.put(HostApiConstants.maxCapacityParamName, maxCapacity.toString());
        	  this.maxCapacity = maxCapacity;
         }
         
         final Boolean soundProofed = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.soundProofedParamName, element);
         if (soundProofed != null && !this.soundProofed.equals(soundProofed)) {
        	  changes.put(HostApiConstants.soundProofedParamName, soundProofed.toString());
        	  this.soundProofed = soundProofed;
         }
         
         final Integer noOfMics = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfMicsParamName, element);
         if (noOfMics != null && !this.noOfMics.equals(noOfMics)) {
        	  changes.put(HostApiConstants.noOfMicsParamName, noOfMics.toString());
        	  this.noOfMics = noOfMics;
         }
         final Integer noOfStools = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfStoolsParamName, element);
         if (noOfStools != null && !this.noOfStools.equals(noOfStools)) {
        	  changes.put(HostApiConstants.noOfStoolsParamName, noOfStools.toString());
        	  this.noOfStools = noOfStools;
         }
         final Integer mixerBoardInputs = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfMixerBoardInputsParamName, element);
         if (mixerBoardInputs != null && !this.mixerBoardInputs.equals(mixerBoardInputs)) {
        	  changes.put(HostApiConstants.noOfMixerBoardInputsParamName, mixerBoardInputs.toString());
        	  this.mixerBoardInputs = mixerBoardInputs;
         }
         final String drumkitBrand = fromApiJsonHelper.extractStringNamed(HostApiConstants.drumkitBrandParamName, element);
         if (drumkitBrand != null && !this.drumkitBrand.equals(drumkitBrand)) {
        	  changes.put(HostApiConstants.drumkitBrandParamName, drumkitBrand);
        	  this.drumkitBrand = drumkitBrand;
         }
         final String speakerBrand = fromApiJsonHelper.extractStringNamed(HostApiConstants.speakerBrandParamName, element);
         if (speakerBrand != null && !this.speakerBrand.equals(speakerBrand)) {
        	  changes.put(HostApiConstants.speakerBrandParamName, speakerBrand.toString());
        	  this.speakerBrand = speakerBrand;
         }
         final Integer drumkitPieces = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.drumkitPiecesParamName, element);
         if (drumkitPieces != null && !this.drumkitPieces.equals(drumkitPieces)) {
        	  changes.put(HostApiConstants.drumkitPiecesParamName, drumkitPieces.toString());
        	  this.drumkitPieces = drumkitPieces;
         }
         final Boolean hasRecording = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasRecordingParamName, element);
         if (hasRecording != null && !this.hasRecording.equals(hasRecording)) {
        	  changes.put(HostApiConstants.hasRecordingParamName, hasRecording.toString());
        	  this.hasRecording = hasRecording;
         }
         final Boolean hasMasterRecording = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasMasterRecordingParamName, element);
         if (hasMasterRecording != null && !this.hasMasterRecording.equals(hasMasterRecording)) {
        	  changes.put(HostApiConstants.hasMasterRecordingParamName, hasMasterRecording.toString());
        	  this.hasMasterRecording = hasMasterRecording;
         }
         final Integer avgMixingMasteringHourly = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.avgMixingMasteringHourly, element);
         if (avgMixingMasteringHourly != null && !this.avgMixingMasteringHourly.equals(avgMixingMasteringHourly)) {
        	  changes.put(HostApiConstants.avgMixingMasteringHourly, avgMixingMasteringHourly.toString());
        	  this.avgMixingMasteringHourly = avgMixingMasteringHourly;
         }
         
         final Integer noOfAmplifiers = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfAmplifiersParamName, element);
         if (noOfAmplifiers != null && !this.noOfAmplifiers.equals(noOfAmplifiers)) {
        	  changes.put(HostApiConstants.noOfAmplifiersParamName, noOfAmplifiers.toString());
        	  this.noOfAmplifiers = noOfAmplifiers;
         }
         final String amplifierBrands = fromApiJsonHelper.extractStringNamed(HostApiConstants.amplifierParamName, element);
         if (amplifierBrands != null && !this.amplifierBrands.equals(amplifierBrands)) {
        	  changes.put(HostApiConstants.amplifierParamName, amplifierBrands);
        	  this.amplifierBrands = amplifierBrands;
         }
         final String otherItems = fromApiJsonHelper.extractStringNamed(HostApiConstants.otherItemsParamName, element);
         if ( otherItems!= null && !this.otherItems.equals(otherItems)) {
        	  changes.put(HostApiConstants.otherItemsParamName, otherItems);
        	  this.otherItems = otherItems;
         }
         final Boolean hasVoiceover = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasVoiceoverParamName, element);
         if (hasVoiceover != null && !this.hasVoiceover.equals(hasVoiceover)) {
        	  changes.put(HostApiConstants.hasVoiceoverParamName, hasVoiceover.toString());
        	  this.hasVoiceover = hasVoiceover;
         }
         final Integer xHourPackage = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourPackageParamName, element);
         if (xHourPackage != null && !this.xHourPackage.equals(xHourPackage)) {
        	  changes.put(HostApiConstants.xHourPackageParamName, xHourPackage.toString());
        	  this.xHourPackage = xHourPackage;
         }
         
         final Boolean hasJamming = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasJammingParamName, element);
         if (hasJamming != null && !this.hasJamming.equals(hasJamming)) {
        	  changes.put(HostApiConstants.hasJammingParamName, hasJamming.toString());
        	  this.hasJamming = hasJamming;
         }
         
         
         final Set<Mic> mics =  new HashSet<>();
         if(room.has(HostApiConstants.micsParamName) && room.get(HostApiConstants.micsParamName).getAsJsonArray()!=  null) {
            final JsonArray micArray =  room.get(HostApiConstants.micsParamName).getAsJsonArray();
            for (int j =  0; j < micArray.size(); j++) {
                final JsonObject micElement =  micArray.get(j).getAsJsonObject();
                final Boolean condenser = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasCondenserParamName, micElement);
                final Boolean wireless = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.isWirelessParamName, micElement);
                final Boolean instrument = fromApiJsonHelper.extractBooleanNamed(HostApiConstants.forInstrumentParamName, micElement);
                mics.add(Mic.getInstance(condenser, wireless, instrument));
            }
         }
         
         if(!CollectionUtils.isEmpty(mics))
        	 this.mics = associateMicsWithThisRoom(mics);
    
         
         
         final Set<JamRoomPriceFlexi> flexi =  new HashSet<>();
         if(room.has(HostApiConstants.priceParamName) && room.get(HostApiConstants.priceParamName).getAsJsonArray() !=  null) {
            final JsonArray slabsArray =  room.get(HostApiConstants.priceParamName).getAsJsonArray();
            for (int j =  0; j < slabsArray.size(); j++) {
                final JsonObject slabElement =  slabsArray.get(j).getAsJsonObject();
                final Integer serviceType = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.serviceTypeParamName, slabElement);
                final String dayOfWeekStart = fromApiJsonHelper.extractStringNamed(HostApiConstants.dayOfWeekStartParamName, slabElement);
                final String dayOfWeekEnd = fromApiJsonHelper.extractStringNamed(HostApiConstants.dayOfWeekStartParamName, slabElement);
                final String timeStart = fromApiJsonHelper.extractStringNamed(HostApiConstants.timeStartParamName, slabElement);
                final String timeEnd = fromApiJsonHelper.extractStringNamed(HostApiConstants.timeEndParamName, slabElement);
                final Integer price = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.priceParamName, slabElement);
                flexi.add(JamRoomPriceFlexi.instance(serviceType, dayOfWeekStart, dayOfWeekEnd, timeStart, timeEnd, price));
            }
         }
         
         if(!CollectionUtils.isEmpty(flexi))
         	 flexi.removeAll(flexi);
        	 
        	 this.flexiPrice = associateFlexiPriceWithThisRoom(flexiPrice);
         
         return changes;				
	} 
	
	private Set<Mic> associateMicsWithThisRoom(final Set<Mic> mics) {
		this.mics = null;
        for (final Mic mic : mics) {
        		mic.update(this);
        		
        }
        return mics;
    }
	
	private JamRoomPrice associatePriceWithThisRoom(final JamRoomPrice price) {
		price.update(this);
		return price;
	}
	
	private Set<JamRoomPriceFlexi> associateFlexiPriceWithThisRoom(final Set<JamRoomPriceFlexi> price) {
		for(JamRoomPriceFlexi flexi : price)
			flexi.update(this);
		return price;
	}
	
	public void update(JamHost host) {
		this.host = host;
	}
	
	public JamHost getHost() {
		return this.host;
	}
	
	public Integer getMinCapacity() {
		return this.minCapacity;
	}
	
	public Integer getMaxCapacity() {
		return this.maxCapacity;
	}
	
	public Boolean soundProofed() {
		return this.soundProofed;
	}
	
	public Integer getNoOfStools() {
		return this.noOfStools;
	}
	
	public Integer getNoOfMics() {
		return this.noOfMics;
	}
	
	public Integer getMixerBoardInputs() {
		return this.mixerBoardInputs;
	}
	
	public Integer getDrumkitBrandCvId() {
		return this.drumkitBrandCvId;
	}
	
	public String getDrumkitBrand() {
		return this.drumkitBrand;
	}
	
	public Integer getSpeakerBrandCvId() {
		return this.speakerBrandCvId;
	}
	
	public Integer getDrumkitPieces() {
		return this.drumkitPieces;
	}
	
	public Boolean hasRecording() {
		return this.hasRecording;
	}
	
	public Boolean hasMasterRecording() {
		return this.hasMasterRecording;
	}
	
	public Integer getAvgHourly() {
		return this.avgHourly;
	}
	
	public Integer getAvgMixingMasteringHourly() {
		return this.avgMixingMasteringHourly;
	}

	public Integer getNoOfAmplifiers() {
		return this.noOfAmplifiers;
	}
	
	public String getAmplifierBrands() {
		return this.amplifierBrands;
	}
	
	public String getOtherItems() {
		return this.otherItems;
	}
	
	public List<String> getImages() {
		return this.images;
	}
	
	public JamRoomPrice getPrice() {
		return this.price;
	}
	
	public Integer getXHourPackage() {
		return this.xHourPackage;
	}
	
	public void setPrimaryImage(final Long primaryImageId) {
		this.primaryImageId = primaryImageId;
	}
	
	public Long getPrimaryImageId() {
		return this.primaryImageId;
	}
	
	public void setTotalReviews(final Integer totalReviews) {
		this.totalReviews = totalReviews;
	}
	
	public Integer getTotalReviews() {
		return this.totalReviews;
	}
	
	public void setAvgRating(final Double avgRating) {
		this.avgRating = avgRating;
	}
	
	public Double getAvgRating() {
		return this.avgRating;
	}
	
	public Set<JamRoomPriceFlexi> getFlexiPrice() {
		return this.flexiPrice;
	}
	
}
