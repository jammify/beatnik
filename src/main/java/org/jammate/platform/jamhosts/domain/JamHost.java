package org.jammate.platform.jamhosts.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.jamhosts.api.HostApiConstants;
import org.jammate.platform.jamhosts.data.LocationType;
import org.springframework.util.CollectionUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Entity
@Table(name = "jamhost")
public class JamHost extends AbstractAuditableCustom<AppUser, Long> {
	
	@Column(name="userId")
	private Long userId;
	
	@Column(name = "name")
    private String name;
	
	@LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "host", orphanRemoval = false)
    private Set<JamRoom> jamRooms = null;
	
	@Column(name = "latitude", scale = 6, precision = 19, nullable = true)
    private BigDecimal latitude;

    @Column(name = "longitude", scale = 6, precision = 19, nullable = true)
    private BigDecimal longitude;
	
	@Column(name = "locationType")
    private Integer locationType;
	
	@Column(name = "verified")
    private Boolean verified;
	
	@Column(name = "houseNo")
    private String houseNo;
	
	@Column(name = "street")
    private String street;
	
	@Column(name = "landmark")
    private String landmark;
	
	@Column(name = "area")
    private String area;
	
	@Column(name = "cityId")
    private Long cityId;
	
	@Column(name = "pinCode")
    private Integer pinCode;
	
	@Column(name = "ownerName")
    private String ownerName;
	
	@Column(name = "orgTypeId")
    private Integer orgTypeId;
	
	@Column(name = "email")
    private String email;
	
	@Column(name = "contact")
    private String contact;
	
	@Column(name = "fbPage")
    private String fbPage;
	
	@Column(name = "website")
    private String website;
	
	@Column(name = "openHours")
    private Integer openHours;
	
	@Column(name = "openMins")
    private Integer openMins;
	
	@Column(name = "closeHours")
    private Integer closeHours;
	
	@Column(name = "closeMins")
    private Integer closeMins;
	
	@Column(name = "startTimeMultiples")
    private Integer startTimeMultiples;
	
	@Column(name = "slotDurationMultiples")
    private Integer slotDurationMultiples;
	
	@Column(name = "imageId")
    private Long imageId;
	
	@Column(name = "accountHolderName")
    private String accountHolderName;
	
	@Column(name = "accountNo")
    private String accountNo;
	
	@Column(name = "ifscCode")
    private String ifscCode;
	
	@Column(name = "accountTypeId")
    private Integer accountTypeId;
	
	@Column(name = "razorpayAccountId")
    private String razorpayAccountId;
	
	@Column(name = "live")
    private Boolean live;
	
	@Column(name = "commission")
	private Double commission;
	
	@LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "host")
	private Set<CancellationPolicy> policies;
	
	
	protected JamHost() {
		
	}
	
	public static JamHost instance(final Long userId, final String name, final BigDecimal latitude, final BigDecimal longitude,
			final Integer locationType, final Boolean verified,
			final String houseNo, final String street, final String landmark,
			final String area, final Long cityId, final Integer pinCode, 
			final String ownerName, final String email, final String contact, final String  fbPage, final String website,
			final Integer openHours, final Integer openMins, 
			final Integer closeHours, final Integer closeMins,  final Integer startTimeMultiples, 
			final Integer slotDurationMultiples, final Long imageId, final String accountHolderName, 
			final String  accountNo,  final String ifscCode,
			final Integer orgTypeId, final Integer accountTypeId, final Boolean live,
			final Set<JamRoom> jamRooms, final Set<CancellationPolicy> policies) {
		return new JamHost(userId, name, latitude, longitude, locationType, verified, houseNo,
				street, landmark, area, cityId,
				pinCode, ownerName, email, contact, fbPage, website, openHours, openMins,
				closeHours, closeMins, startTimeMultiples, slotDurationMultiples, imageId,
				accountHolderName, accountNo, ifscCode,
				orgTypeId, accountTypeId, live, jamRooms, policies);
	}

	private JamHost(final Long userId, final String name, final BigDecimal latitude, final BigDecimal longitude,
			final Integer locationType, final Boolean verified,
			final String houseNo, final String street, final String landmark, final String area,
			final Long cityId, final Integer pinCode, 
			final String ownerName, final String email, final String contact, final String  fbPage, final String website,
			final Integer openHours, final Integer openMins, 
			final Integer closeHours, final Integer closeMins,  final Integer startTimeMultiples, 
			final Integer slotDurationMultiples, final Long imageId, final String accountHolderName, 
			final String  accountNo,  final String ifscCode,
			final Integer orgTypeId, final Integer accountTypeId, final Boolean live,
			final Set<JamRoom> jamRooms, final Set<CancellationPolicy> policies) {
		this.userId = userId;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.locationType = 1;
		this.verified = verified;
		this.houseNo = houseNo;
		this.street = street;
		this.landmark = landmark;
		this.area = area;
		this.cityId = cityId;
		this.pinCode = pinCode;
		this.ownerName = ownerName;
		this.email = email;
		this.contact = contact;
		this.fbPage = fbPage;
		this.openHours = openHours;
		this.openMins = openMins;
		this.closeHours = closeHours;
		this.closeMins = closeMins;
		this.startTimeMultiples = startTimeMultiples;
		this.slotDurationMultiples = slotDurationMultiples;
		this.imageId = imageId;
		this.accountHolderName = accountHolderName;
		this.accountNo = accountNo;
		this.ifscCode = ifscCode;
		this.website = website;
		this.orgTypeId = orgTypeId;
		this.accountTypeId = accountTypeId;
		this.live = live;
		if(jamRooms != null)
			this.jamRooms = associateRoomsWithThisHost(jamRooms);
		this.policies = associatePoliciesWithThisHost(policies);
		this.commission = 10.0;
	}
	
	public Map<String, String> update(final JsonElement element, final FromJsonHelper fromApiJsonHelper) {
		final Map<String, String> changes = new HashMap<>();
		 final JsonObject host = element.getAsJsonObject();
         //Updating host
         final String website = fromApiJsonHelper.extractStringNamed(HostApiConstants.websiteParamName, element);
         if(website != null && !this.website.equalsIgnoreCase(website)) {
        	 changes.put(HostApiConstants.websiteParamName, website);
        	 this.website = website;
         }
         final String fbPage = fromApiJsonHelper.extractStringNamed(HostApiConstants.fbPageParamName, element);
         if (fbPage != null && !this.fbPage.equalsIgnoreCase(fbPage)) {
        	  changes.put(HostApiConstants.fbPageParamName, fbPage);
        	  this.fbPage = fbPage;
         }
         final String contact = fromApiJsonHelper.extractStringNamed(HostApiConstants.contactParamName, element);
         if (contact != null && !this.contact.equalsIgnoreCase(contact)) {
        	  changes.put(HostApiConstants.contactParamName, contact);
        	  this.contact = contact;
         }
         final String email = fromApiJsonHelper.extractStringNamed(HostApiConstants.emailParamName, element);
         if (email != null && !this.email.equalsIgnoreCase(email)) {
        	  changes.put(HostApiConstants.emailParamName, email);
        	  this.email = email;
         }
         
         final String houseNo = fromApiJsonHelper.extractStringNamed(HostApiConstants.houseNoParamName, element);
         if (houseNo != null && !this.houseNo.equalsIgnoreCase(houseNo)) {
        	  changes.put(HostApiConstants.houseNoParamName, houseNo);
        	  this.houseNo = houseNo;
         }
         final String street = fromApiJsonHelper.extractStringNamed(HostApiConstants.streetParamName, element);
         if (street != null && !this.street.equalsIgnoreCase(street)) {
        	  changes.put(HostApiConstants.streetParamName, street);
        	  this.street = street;
         }
         final String landmark = fromApiJsonHelper.extractStringNamed(HostApiConstants.landmarkParamName, element);
         if (landmark != null && !this.landmark.equalsIgnoreCase(landmark)) {
        	  changes.put(HostApiConstants.landmarkParamName, landmark);
        	  this.landmark = landmark;
         }
         final String area = fromApiJsonHelper.extractStringNamed(HostApiConstants.areaParamName, element);
         if (area != null && !this.area.equalsIgnoreCase(area)) {
        	  changes.put(HostApiConstants.areaParamName, area);
        	  this.area = area;
         }
         final Long cityId = fromApiJsonHelper.extractLongNamed(HostApiConstants.cityIdParamName, element);
         if (cityId != null && !this.cityId.equals(cityId)) {
        	  changes.put(HostApiConstants.cityIdParamName, cityId.toString());
        	  this.cityId = cityId;
         }
         final Integer pinCode = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.pinCodeParamName, element);
         if (pinCode != null && !this.pinCode.equals(pinCode)) {
        	  changes.put(HostApiConstants.pinCodeParamName, pinCode.toString());
        	  this.pinCode = pinCode;
         }
         final Integer openHours = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.openHoursParamName, element);
         if (openHours != null && !this.openHours.equals(openHours)) {
        	  changes.put(HostApiConstants.openHoursParamName, openHours.toString());
        	  this.openHours = openHours;
         }
         final Integer openMins = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.openMinsParamName, element);
         if (openMins != null && !this.openMins.equals(openMins)) {
        	  changes.put(HostApiConstants.openMinsParamName, openMins.toString());
        	  this.openMins = openMins;
         }
         final Integer closeHours = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.closeHoursParamName, element);
         if (closeHours != null && !this.closeHours.equals(closeHours)) {
        	  changes.put(HostApiConstants.closeHoursParamName, closeHours.toString());
        	  this.closeHours = closeHours;
         }
         
         final Integer closeMins = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.closeMinsParamName, element);
         if (closeMins != null && !this.closeMins.equals(closeMins)) {
        	  changes.put(HostApiConstants.closeMinsParamName, closeMins.toString());
        	  this.closeMins = closeMins;
         }
         final BigDecimal latitude = fromApiJsonHelper.extractBigDecimalNamed(HostApiConstants.latitudeParamName, element);
         if (latitude != null && !this.latitude.equals(latitude)) {
        	  changes.put(HostApiConstants.latitudeParamName, latitude.toEngineeringString());
        	  this.latitude = latitude;
         }
         final BigDecimal longitude = fromApiJsonHelper.extractBigDecimalNamed(HostApiConstants.longitudeParamName, element);
         if (longitude != null && !this.longitude.equals(longitude)) {
        	  changes.put(HostApiConstants.longitudeParamName, longitude.toEngineeringString());
        	  this.longitude = longitude;
         }
         final Integer startTimeMultiples = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.startTimeMultiplesParamName, element);
         if (startTimeMultiples != null && !this.startTimeMultiples.equals(startTimeMultiples)) {
        	  changes.put(HostApiConstants.startTimeMultiplesParamName, startTimeMultiples.toString());
        	  this.startTimeMultiples = startTimeMultiples;
         }
         final Integer slotDurationMultiples = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.slotDurationMultiplesParamName, element);
         if (slotDurationMultiples != null && !this.slotDurationMultiples.equals(slotDurationMultiples)) {
        	  changes.put(HostApiConstants.slotDurationMultiplesParamName, slotDurationMultiples.toString());
        	  this.slotDurationMultiples = slotDurationMultiples;
         }
         
         final Integer orgTypeId = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.orgTypeParamName, element);
         if (orgTypeId != null && !this.orgTypeId.equals(orgTypeId)) {
        	  changes.put(HostApiConstants.orgTypeParamName, orgTypeId.toString());
        	  this.orgTypeId = orgTypeId;
         }
         
         
         final Set<CancellationPolicy> policies =  new HashSet<>();
         if(host.has(HostApiConstants.slabsParamName) && host.get(HostApiConstants.slabsParamName).getAsJsonArray() !=  null) {
            final JsonArray slabsArray =  host.get(HostApiConstants.slabsParamName).getAsJsonArray();
            for (int j =  0; j < slabsArray.size(); j++) {
                final JsonObject slabElement =  slabsArray.get(j).getAsJsonObject();
                final Integer start = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.startParamName, slabElement);
                final Integer end = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.endParamName, slabElement);
                final Integer percent = fromApiJsonHelper.extractIntegerNamed(HostApiConstants.percentParamName, slabElement);
                policies.add(CancellationPolicy.instance(start, end, percent));
            }
         }
         
         if(!CollectionUtils.isEmpty(policies))
        	 this.policies = associatePoliciesWithThisHost(policies);
         return changes;				
	}
	
	
	
	private Set<CancellationPolicy> associatePoliciesWithThisHost(final Set<CancellationPolicy> policies) {
		this.policies = null;
        for (final CancellationPolicy policy : policies) {
        		policy.update(this);
        }
        return policies;
        
    }

	
	
	public void setRoom(final JamRoom room) {
		if(!CollectionUtils.isEmpty(this.jamRooms)) {
			this.jamRooms.add(room);
		} else {
			this.jamRooms = new HashSet<>();
			this.jamRooms.add(room);
		}
		room.update(this);
	}
	
	 private Set<JamRoom> associateRoomsWithThisHost(final Set<JamRoom> rooms) {
	        for (final JamRoom room : rooms) {
	        		room.update(this);
	        }
	        return rooms;
	 }
	 
	 public Set<JamRoom> getRooms() {
		 return this.jamRooms;
	 }
	
	public String getName() {
		return name;
	}
	
	public LocationType getLocationType() {
		if(locationType != null)
			return LocationType.fromInt(locationType);
		else
			return null;
	}
	
	public Boolean isVerified() {
		return verified;
	}
	
	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}
	
	public Integer getSlotDurationMultiples() {
		return this.slotDurationMultiples;
	}
	
	public Long getImageId() {
		return this.imageId;
	}
	
	public String getAccountHolderName() {
		return this.accountHolderName;
	}
	
	public String getAccountNo() {
		return this.accountNo;
	}
	
	public String getIfscCode() {
		return this.ifscCode;
	}
	
	public Integer getOpenHours() {
		return this.openHours;
	}
	
	public Integer getOpenMins() {
		return this.openMins;
	}
	
	public Integer getCloseHours() {
		return this.closeHours;
	}
	
	public Integer getCloseMins() {
		return this.closeMins;
	}
	
	public Integer getStartTimeMultiples() {
		return this.startTimeMultiples;
	}
	
	public Long getCityId() {
		return this.cityId;
	}
	
	public Double getCommission() {
		return this.commission;
	}
	
	public Integer getPinCode() {
		return this.pinCode;
	}
	
	public String getOwnerName() {
		return this.ownerName;
	}
	
	public String getMail() {
		return this.email;
	}
	
	public String getContact() {
		return this.contact;
	}
	
	public String getFbPage() {
		return this.fbPage;
	}
	
	public String getRazorpayAccountId() {
		return this.razorpayAccountId;
	}
	
	public Long getUserId() {
		return this.userId;
	}
}
