package org.jammate.platform.jamhosts.domain;

import org.apache.commons.mail.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JamHostRepository extends JpaRepository<JamHost, Long>,
JpaSpecificationExecutor<JamHost> {
	
	@Query("from JamHost h where h.userId = :userId")
	public JamHost findByUserId(@Param("userId") final Long userId);

	public Email findOneByEmail(Email hostEmail);


}
