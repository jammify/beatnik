package org.jammate.platform.jamhosts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.core.infra.domain.AbstractAuditableCustom;
import org.jammate.platform.orders.domain.Order;

@Entity
@Table(name = "rating")
public class Rating extends AbstractAuditableCustom<AppUser, Long> {
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "orderId", referencedColumnName = "id", nullable = false)
	private Order order;
	
	@Column(name="message")
	private String message;
	
	@Column(name="rating")
	private String rating;
	
	
	protected Rating() {
        //
    }
	
	public static Rating instance(final Order order, final String message, 
			final String rating) {
		return new Rating(order, message, rating);
	}
	
	private Rating(final Order order, final String message,
			final String rating) {
		this.order = order;
		this.message = message;
		this.rating = rating;
	}
	
	public Double getRating() {
		return Double.parseDouble(rating);
	}	
}
