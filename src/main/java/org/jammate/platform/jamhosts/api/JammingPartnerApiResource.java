package org.jammate.platform.jamhosts.api;

import javax.transaction.Transactional;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.jamhosts.data.HostData;
import org.jammate.platform.jamhosts.data.JamroomData;
import org.jammate.platform.jamhosts.service.JamHostsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/hosts") 
public class JammingPartnerApiResource {
	
	private final JamHostsService hostsServiceImpl;
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final static Logger logger = LoggerFactory
			.getLogger(JamroomsApiResource.class);
	
	@Autowired
    public JammingPartnerApiResource(final JamHostsService hostsServiceImpl,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper) {
		this.hostsServiceImpl = hostsServiceImpl;
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
    public String onboardPartner(@RequestBody final String partner) {
		logger.info(partner);
		final CommandProcessingResult result = this.hostsServiceImpl
				.createPartner(JsonCommand.from(partner,
				new JsonParser().parse(partner), fromApiJsonHelper));
		return jsonSerializer.serialize(result);
    }
	
	@RequestMapping(value="/{hostId}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8") 
	 public String updateJamhost(@PathVariable Long hostId, @RequestBody final String updateJamhost) {
		 logger.info(updateJamhost);
		 final CommandProcessingResult result = this.hostsServiceImpl
					.updateJamHost(hostId, JsonCommand.from(updateJamhost,
					new JsonParser().parse(updateJamhost), fromApiJsonHelper));
             return jsonSerializer.serialize(result);
           
	 }
	@RequestMapping(value="/{hostId}/rooms/{roomId}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8") 
	 public String updateJamroom(@PathVariable Long roomId, @RequestBody final String updateJamRoom) {
		 logger.info(updateJamRoom);
		 final CommandProcessingResult result = this.hostsServiceImpl.updateJamRoom(roomId, JsonCommand.from(updateJamRoom,
				  new JsonParser().parse(updateJamRoom), fromApiJsonHelper));
		 
           return jsonSerializer.serialize(result);
	

}
	
	@RequestMapping(value="/{hostId}/rooms", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
    public String createRoom(@PathVariable Long hostId, @RequestBody final String room) {
		logger.info(room);
		final CommandProcessingResult result = this.hostsServiceImpl
				.createRoom(hostId, JsonCommand.from(room,
				new JsonParser().parse(room), fromApiJsonHelper));
		return jsonSerializer.serialize(result);
    }
	
	@RequestMapping(value="/{hostId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String getHost(@PathVariable Long hostId) {
		final HostData host = this.hostsServiceImpl.retrieveHost(hostId);
		return this.jsonSerializer.serialize(host);
    }
	
	@RequestMapping(value="/{hostId}/rooms/{roomId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String getRoom(@PathVariable Long hostId, @PathVariable Long roomId) {
		final JamroomData room = this.hostsServiceImpl.retrieveJamroomDetails(roomId);
		return jsonSerializer.serialize(room);
    }
	
}
