package org.jammate.platform.jamhosts.api;

import java.util.List;

import org.jammate.platform.core.infra.ApiSerializer;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.jamhosts.data.BlockedData;
import org.jammate.platform.jamhosts.data.JamroomData;
import org.jammate.platform.jamhosts.data.LandingResponseData;
import org.jammate.platform.jamhosts.data.PolicyData;
import org.jammate.platform.jamhosts.service.JamHostPolicyService;
import org.jammate.platform.jamhosts.service.JamHostsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@RestController  
@RequestMapping("/rooms") 
public class JamroomsApiResource {
	
	private final JamHostsService hostsServiceImpl;
	private final ApiSerializer jsonSerializer;
	private final FromJsonHelper fromApiJsonHelper;
	private final JamHostPolicyService hostPolicyService;
	private final static Logger logger = LoggerFactory
			.getLogger(JamroomsApiResource.class);
	
	@Autowired
    public JamroomsApiResource(final JamHostsService hostsServiceImpl,
    		final ApiSerializer jsonSerializer,
    		final FromJsonHelper fromApiJsonHelper,
    		final JamHostPolicyService hostPolicyService) {
		this.hostsServiceImpl = hostsServiceImpl;
		this.jsonSerializer = jsonSerializer;
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.hostPolicyService = hostPolicyService;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveJamLocations(@RequestParam(value="lat", required=true) String latitude,
    		@RequestParam(value="lng", required=true) String longitude,
    		@RequestParam(value="offset", required=false) Integer offset,
    		@RequestParam(value="limit", required=false) Integer limit,
    		@RequestParam(value="search", required=false) String search,
    		@RequestParam(value="date", required=false) String date,
    		@RequestParam(value="time", required=false) Long time,
    		@RequestParam(value="service", required=false) Long service) {
		final LandingResponseData hosts = this.hostsServiceImpl
				.retrieveJamLocations(latitude, longitude,
				offset, limit, search, date, time, service);
		return this.jsonSerializer.serialize(hosts);
    }
	
	@RequestMapping(value="/{roomId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveJamLocation(@PathVariable Long roomId, @RequestParam(value="date", required=false) String date,
    		@RequestParam(value="time", required=false) Long time,
    		@RequestParam(value="service", required=false) Long service) {
		final JamroomData host = this.hostsServiceImpl.retrieveJamLocation(roomId);
		return this.jsonSerializer.serialize(host);
    }
	
	@RequestMapping(value="/{roomId}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8") 
	 public String updateJamhost(@PathVariable Long roomId, @RequestBody final String updateJamRoom) {
		 logger.info(updateJamRoom);
		 final CommandProcessingResult result = this.hostsServiceImpl
					.updateJamHost(roomId, JsonCommand.from(updateJamRoom,
					new JsonParser().parse(updateJamRoom), fromApiJsonHelper));
            return jsonSerializer.serialize(result);
          
	 }
	
	@RequestMapping(value="/{roomId}/price", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveJamLocation(@PathVariable Long roomId,
    		@RequestParam(value="dateTo", required=false) String dateTo,
    		@RequestParam(value="dateFrom", required=true) String dateFrom,
    		@RequestParam(value="timeTo", required=false) String timeTo,
    		@RequestParam(value="timeFrom", required=false) String timeFrom,
    		@RequestParam(value="service", required=true) Integer service) {
		final Integer priceQuote = this.hostsServiceImpl.retrievePrice(roomId,
				service, dateFrom, dateTo, timeFrom, timeTo);
		final JsonObject price = new JsonObject();
		price.addProperty("price", priceQuote);
		return price.toString();
    }
	
	@RequestMapping(value="/{roomId}/availability", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrieveJamAvailability(@PathVariable Long roomId,
    		@RequestParam(value="date", required=true) String date) {
		final List<BlockedData> blocks = this.hostsServiceImpl.retrieveAvailability(roomId,
				date, false);
		return this.jsonSerializer.serialize(blocks);
	}
	
	@RequestMapping(value="/{roomId}/partnerAvailability", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
    public String retrievePartnerAvailability(@PathVariable Long roomId,
    		@RequestParam(value="date", required=true) String date) {
		final List<BlockedData> blocks = this.hostsServiceImpl.retrieveAvailability(roomId,
				date, true);
		return this.jsonSerializer.serialize(blocks);
	}
	
	@RequestMapping(value="/{roomId}/quotation", method = RequestMethod.POST, produces = "application/json; charset=utf-8")  
	public String askForQuotation(@PathVariable Long roomId, @RequestBody final String quotationRequest) {
		final CommandProcessingResult response = this.hostsServiceImpl
				.submitQuotationRequest(roomId, JsonCommand.from(quotationRequest,
						new JsonParser().parse(quotationRequest), fromApiJsonHelper));
		return this.jsonSerializer.serialize(response);
	}
	
	@RequestMapping(value="/{roomId}/policies", method = RequestMethod.GET, produces = "application/json; charset=utf-8")  
	public String getPolicies(@PathVariable Long roomId) {
		final PolicyData policies = this.hostPolicyService.retrievePolicies(roomId);
		return this.jsonSerializer.serialize(policies);
	}
	
	
	

}
