/rooms/{roomId}:
    get:
      tags:
        - booking
      summary: Fetches a particular jamroom and shows specific prices if filters are applied.
      description: Fetches a particular jamroom and shows specific prices if filters are applied.
      parameters:
        - in: path
          required: true
          name: roomId
          type: integer
          description: Numeric ID of the room to fetch
        - in: query
          name: date
          type: string
          description: >-
            Date string in ddMMMyyyy format
        - in: query
          name: time
          type: integer
          description: >-
            ID from timeRange code options
        - in: query
          name: service
          type: integer
          description: >-
            ID from roomService code options
            
      responses:
        '200':
          description: Room details and pricing details are returned.
          schema:
            type: object
            required:
              - roomId
              - imgIds
              - studioName
              - avgRating
              - profilePic
              - houseNo
              - street
              - area
              - landmark
              - openingHours
              - closingHours
              - hasJamming
              - hasRecording
              - hasMixingAndMastering
              - minCapacity
              - maxCapacity
              - hasSoundproofing
              - drumkitBrand
              - drumkitPieces
              - amplifierBrands
              - noOfAmplifiers
              - noOfStools
              - speakerBrand
              - totalMics
              - totalMicsWithCondenser
              - totalWirelessMics
              - totalInstrumentalMics
              - mixerBoardInputs
              - otherItems
              - facebookPage
              - website
              - latitude
              - longitude
              - priceData
              - xHourPackage
              - availableServices
            properties:
              roomId:
                type: integer
                format: int32
                example: 234
              studioName:
                type: string
                example: Music Beatz
              avgRating:
                type: number
                example: 4.1
              profilePic:
                type: integer
                format: int32
                example: 234
              houseNo:
                type: string
                example: 32
              street:
                type: string
                example: 3rd Main
              landmark:
                type: string
                example: Ganesh Temple
              area:
                type: string
                example: Koramangala
              openingHours:
                type: string
                example: 3:00
              closingHours:
                type: string
                example: 21:00
              hasJamming:
                type: boolean
              hasRecording:
                type: boolean
              hasMixingAndMastering:
                type: boolean
              xHourPackage:
                type: integer
                format: int32
                example: 4
              minCapacity:
                type: integer
                format: int32
                example: 4
              maxCapacity: 
                type: integer
                format: int32
                example: 8
              noOfStools: 
                type: integer
                format: int32
                example: 6
              speakerBrand:
                type: string
                example: Marshall
              hasSoundproofing:
                type: boolean
              drumkitBrand:
                type: string
                example: Stratocaster
              drumkitPieces:
                type: integer
                format: int32
                example: 6
              noOfAmplifiers:
                type: integer
                format: int32
                example: 4
              totalMics:
                type: integer
                format: int32
                example: 3
              totalMicsWithCondenser:
                type: integer
                format: int32
                example: 2
              totalWirelessMics:
                type: integer
                format: int32
              totalInstrumentalMics:
                type: integer
                format: int32
              mixerBoardInputs:
                type: integer
                format: int32
              facebookPage:
                type: string
              website:
                type: string
              latitude:
                type: string
              longitude:
                type: string
              imgIds:
                type: array
                items:
                  type: integer
              amplifierBrands:
                type: array
                items:
                  type: string
              otherItems:
                type: array
                items:
                  type: string
              availableServices:
                type: array
                items:
                  type: integer
                  example: 100/200/300 Check HostService enum
              priceData:
                type: object
                required:
                  - currency
                  - minHourly
                  - maxHourly
                  - avgHourly
                  - avgXHourPackage
                  - fullDay
                  - recordingMinHourly
                  - recordingMaxHourly
                  - avgRecordingHourly
                  - recordingAvgXHourPackage 
                  - recordingFullDay 
                  - voiceoverMinHourly
                  - voiceoverMaxHourly
                  - avgVoiceoverHourly
                  - voiceoverAvgXHourPackage 
                  - voiceoverFullDay 
                  - mixingMasteringMinHourly
                  - mixingMasteringMaxHourly
                  - avgMixingMasteringHourly
                  - mixingMasteringAvgXHourPackage
                  - mixingMasteringFullDay 
                  - overallAvg
                properties:
                  currency:
                    type: string
                    example: INR
                  minHourly:
                    type: integer
                    format: int32
                    example: 300
                  maxHourly:
                    type: integer
                    format: int32
                    example: 300
                  avgHourly:
                    type: integer
                    format: int32
                    example: 300
                  avgXHourPackage:
                    type: integer
                    format: int32
                    example: 300
                  fullDay:
                    type: integer
                    format: int32
                    example: 300
                  recordingMinHourly:
                    type: integer
                    format: int32
                    example: 300
                  recordingMaxHourly:
                    type: integer
                    format: int32
                    example: 300
                  avgRecordingHourly:
                    type: integer
                    format: int32
                    example: 300
                  recordingAvgXHourPackage:
                    type: integer
                    format: int32
                    example: 300
                  recordingFullDay:
                    type: integer
                    format: int32
                    example: 300
                  voiceoverMinHourly:
                    type: integer
                    format: int32
                    example: 300
                  voiceoverMaxHourly:
                    type: integer
                    format: int32
                    example: 300
                  avgVoiceoverHourly:
                    type: integer
                    format: int32
                    example: 300
                  voiceoverAvgXHourPackage:
                    type: integer
                    format: int32
                    example: 300
                  voiceoverFullDay:
                    type: integer
                    format: int32
                    example: 300
                  mixingMasteringMinHourly:
                    type: integer
                    format: int32
                    example: 300
                  mixingMasteringMaxHourly:
                    type: integer
                    format: int32
                    example: 300
                  avgMixingMasteringHourly:
                    type: integer
                    format: int32
                    example: 300
                  mixingMasteringAvgXHourPackage:
                    type: integer
                    format: int32
                    example: 300
                  mixingMasteringFullDay:
                    type: integer
                    format: int32
                    example: 300
                  overallAvg:
                    type: integer
                    format: int32
                    example: 300
        '400':
          description: 'invalid input, object invalid'

    
  
