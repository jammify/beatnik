package org.jammate.platform.jamhosts.api;

public interface HostApiConstants {
	
	//Host constants
	public static final String userIdParamName = "userId";
	public static final String orgNameParamName = "orgName";
	public static final String ownerNameParamName = "ownerName";
	public static final String websiteParamName = "website";
	public static final String fbPageParamName = "fbPage";
	public static final String contactParamName = "contact";
	public static final String emailParamName = "email";
	public static final String orgTypeParamName = "orgTypeId";
	public static final String accountTypeParamName = "accountTypeId";
	
	public static final String houseNoParamName = "houseNo";
	public static final String streetParamName = "street";
	public static final String landmarkParamName = "landmark";
	public static final String areaParamName = "area";
	public static final String cityIdParamName = "cityId";
	public static final String openHoursParamName = "openHours";
	public static final String openMinsParamName = "openMins";
	public static final String closeHoursParamName = "closeHours";
	public static final String closeMinsParamName = "closeMins";
	
	public static final String latitudeParamName = "latitude";
	public static final String longitudeParamName = "longitude";
	public static final String startTimeMultiplesParamName = "startTimeMultiples";
	public static final String slotDurationMultiplesParamName = "slotDurationMultiples";
	public static final String pinCodeParamName = "pinCode";
	
	public static final String accountHolderNameParamName = "accountHolderName";
	public static final String accountNoParamName = "accountNo";
	public static final String ifscCodeParamName = "ifscCode";
	
	//Slab constants
	public static final String startParamName = "start";
	public static final String endParamName = "end";
	public static final String percentParamName = "percent";
	
	//Room constants
	public static final String roomsParamName = "rooms";
	public static final String amplifierParamName = "amplifierBrands";
	public static final String drumkitBrandParamName = "drumkitBrand";
	public static final String drumkitPiecesParamName = "drumkitPieces";
	public static final String hasMasterRecordingParamName = "hasMasterRecording";
	public static final String hasJammingParamName = "hasJamming";
	public static final String hasRecordingParamName = "hasRecording";
	public static final String hasVoiceoverParamName = "hasVoiceover";
	public static final String xHourPackageParamName = "xHourPackage";
	
	public static final String maxCapacityParamName = "maxCapacity";
	public static final String micsParamName = "mics";
	public static final String hasCondenserParamName = "hasCondenser";
	
	public static final String isWirelessParamName = "isWireless";
	public static final String forInstrumentParamName = "forInstrument";
	public static final String minCapacityParamName = "minCapacity";
	public static final String noOfAmplifiersParamName = "noOfAmplifiers";
	public static final String avgMixingMasteringHourly = "avgMixingMasteringHourly";
	public static final String noOfMicsParamName = "noOfMics";
	public static final String noOfMixerBoardInputsParamName = "noOfMixerBoardInputs";
	
	public static final String noOfStoolsParamName = "noOfStools";
	public static final String otherItemsParamName = "otherItems";
	public static final String soundProofedParamName = "soundProofed";
	public static final String speakerBrandParamName = "speakerBrand";
	
	public static final String imagesParamName = "images";
	public static final String slabsParamName = "slabs";
	
	//Quote
	public static final String songSamplesParamName = "songSamples";
	public static final String noOfSongsParamName = "noOfSongs";
	public static final String noOfBandMembersParamName = "noOfBandMembers";
	public static final String avgSongLengthParamName = "avgSongLength";
	public static final String avgNoOfTracksPerSongParamName = "avgNoOfTracksPerSong";
	public static final String messageParamName = "message";
	
	//Prices
	public static final String voiceoverParamName = "voiceover";
	public static final String masterParamName = "master";
	public static final String recordingParamName = "recording";
	public static final String jammingParamName = "jamming";
	
	public static final String flexiParamName = "flexi";
	public static final String serviceTypeParamName = "serviceType";
	public static final String dayOfWeekStartParamName = "dayOfWeekStart";
	public static final String dayOfWeekEndParamName = "dayOfWeekEnd";
	public static final String timeStartParamName = "timeStart";
	public static final String timeEndParamName = "timeEnd";
	public static final String priceParamName = "price";
	
	public static final String fullWeekdayParamName = "fullweekday"; 
	public static final String fullWeekendParamName = "fullweekend"; 
	public static final String xHourWeekdayParamName = "xweekday"; 
	public static final String xHourWeekendParamName = "xweekend"; 


}
