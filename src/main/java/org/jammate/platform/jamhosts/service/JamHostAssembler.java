package org.jammate.platform.jamhosts.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.service.DocumentWritePlatformService.DOCUMENT_MANAGEMENT_ENTITY;
import org.jammate.platform.jamhosts.api.HostApiConstants;
import org.jammate.platform.jamhosts.data.HostService;
import org.jammate.platform.jamhosts.domain.CancellationPolicy;
import org.jammate.platform.jamhosts.domain.JamHost;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.jamhosts.domain.JamRoomPrice;
import org.jammate.platform.jamhosts.domain.JamRoomPriceFlexi;
import org.jammate.platform.jamhosts.domain.Mic;
import org.jammate.platform.jamhosts.domain.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
public class JamHostAssembler {
	
	private final FromJsonHelper fromApiJsonHelper;
	private final PlatformSecurityContext context;
	
	@Autowired
	public JamHostAssembler(final FromJsonHelper fromApiJsonHelper,
			final PlatformSecurityContext context) {
		this.fromApiJsonHelper = fromApiJsonHelper;
		this.context = context;
	}
	
	
	 public JamHost assembleHost(final JsonElement element) {
		 
		 final JsonObject host = element.getAsJsonObject();
		 //Assembling host
		 final Long userId = this.context.authenticatedUser().getId();
		 final String orgName = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.orgNameParamName, element);
		 final String ownerName = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.ownerNameParamName, element);
		 final String website = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.websiteParamName, element);
		 final String fbPage = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.fbPageParamName, element);
		 final String contact = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.contactParamName, element);
		 final String email = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.emailParamName, element);
		 
		 final String houseNo = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.houseNoParamName, element);
		 final String street = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.streetParamName, element);
		 final String landmark = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.landmarkParamName, element);
		 final String area = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.areaParamName, element);
		 final Long cityId = this.fromApiJsonHelper.extractLongNamed(HostApiConstants.cityIdParamName, element);
		 final Integer pinCode = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.pinCodeParamName, element);
		 final Integer openHours = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.openHoursParamName, element);
		 final Integer openMins = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.openMinsParamName, element);
		 final Integer closeHours = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.closeHoursParamName, element);
		 
		 final Integer closeMins = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.closeMinsParamName, element);
		 final BigDecimal latitude = this.fromApiJsonHelper.extractBigDecimalNamed(HostApiConstants.latitudeParamName, element);
		 final BigDecimal longitude = this.fromApiJsonHelper.extractBigDecimalNamed(HostApiConstants.longitudeParamName, element);
		 final Integer startTimeMultiples = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.startTimeMultiplesParamName, element);
		 final Integer slotDurationMultiples = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.slotDurationMultiplesParamName, element);
		 
		 final String accountHolderName = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.accountHolderNameParamName, element);
		 final String accountNo = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.accountNoParamName, element);
		 final String ifscCode = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.ifscCodeParamName, element);
		 final Integer orgTypeId = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.orgTypeParamName, element);
		 final Integer accountTypeId = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.accountTypeParamName, element);
		 
		 final Boolean live = false;
		 
		 final Set<CancellationPolicy> policies = new HashSet<>();
		 if(host.has(HostApiConstants.slabsParamName) && host.get(HostApiConstants.slabsParamName).getAsJsonArray() != null) {
     	 	final JsonArray slabsArray = host.get(HostApiConstants.slabsParamName).getAsJsonArray();
     	 	for (int j = 0; j < slabsArray.size(); j++) {
     	 		final JsonObject slabElement = slabsArray.get(j).getAsJsonObject();
     	 		final Integer start = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.startParamName, slabElement);
     	 		final Integer end = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.endParamName, slabElement);
     	 		final Integer percent = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.percentParamName, slabElement);
     	 		policies.add(CancellationPolicy.instance(start, end, percent));
     	 	}
		 }
		 
		 return JamHost.instance(userId, orgName, latitude, longitude, null, false, houseNo, street, landmark, area, cityId,
				 pinCode, ownerName, email, contact, fbPage, website, openHours, openMins, closeHours,
				 closeMins, startTimeMultiples, slotDurationMultiples, null, accountHolderName,
				 accountNo, ifscCode, orgTypeId, accountTypeId, live, null, policies);
	 }
	 
	 
	 
	 public JamRoom assembleRoom(final JsonElement element) {
		 final JsonObject roomElement = element.getAsJsonObject();
         final String drumkitBrand = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.drumkitBrandParamName, roomElement);
         final Integer drumkitPieces = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.drumkitPiecesParamName, roomElement);
         final Boolean hasMasterRecording = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasMasterRecordingParamName, roomElement);
         final Boolean hasJamming = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasJammingParamName, roomElement);
         final Boolean hasRecording = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasRecordingParamName, roomElement);
         final Boolean hasVoiceover = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasVoiceoverParamName, roomElement); 
         final Integer xHourPackage = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourPackageParamName, roomElement); 
         
         final Integer maxCapacity = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.maxCapacityParamName, roomElement);
         final Integer minCapacity = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.minCapacityParamName, roomElement);
         final Integer noOfAmplifiers = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfAmplifiersParamName, roomElement);
         final Integer noOfMics = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfMicsParamName, roomElement);
         
         final Integer mixerBoardInputs = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfMixerBoardInputsParamName, roomElement);
         final Integer noOfStools = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfStoolsParamName, roomElement);
         String[] otherItems = null;
         String others = "";
         if(roomElement.has(HostApiConstants.otherItemsParamName)) {
        	 	otherItems = this.fromApiJsonHelper.extractArrayNamed(HostApiConstants.otherItemsParamName, roomElement);
        	 	for(String other: otherItems) {
        	 		others += other + "|";
        	 	}
         }
         String[] amplifierBrands = null;
         String amplifiers = "";
         if(roomElement.has(HostApiConstants.amplifierParamName)) {
        	 	amplifierBrands = this.fromApiJsonHelper.extractArrayNamed(HostApiConstants.amplifierParamName, roomElement);
        	 	for(String amplifier: amplifierBrands) {
        	 		amplifiers += amplifier + "|";
        	 	}
         }
         
         final Boolean soundProofed = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.soundProofedParamName, roomElement);
         final String speakerBrand = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.speakerBrandParamName, roomElement);
         
         final Set<Mic> mics = new HashSet<> ();
         if(roomElement.has(HostApiConstants.micsParamName) && roomElement.get(HostApiConstants.micsParamName).getAsJsonArray() != null) {
        	 	final JsonArray micsArray = roomElement.get(HostApiConstants.micsParamName).getAsJsonArray();
        	 	for (int j = 0; j < micsArray.size(); j++) {
        	 		final JsonObject micElement = micsArray.get(j).getAsJsonObject();
    	 		    final Boolean hasCondenser = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.hasCondenserParamName, micElement);
                final Boolean isWireless = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.isWirelessParamName, micElement);
                final Boolean forInstrument = this.fromApiJsonHelper.extractBooleanNamed(HostApiConstants.forInstrumentParamName, micElement);
                mics.add(Mic.getInstance(hasCondenser, isWireless, forInstrument));
        	 	}
         }
         
         final List<String> images = new ArrayList<> ();
         if(roomElement.has(HostApiConstants.imagesParamName) && roomElement.get(HostApiConstants.imagesParamName).getAsJsonArray() != null) {
        	 final JsonArray imagesArray = roomElement.get(HostApiConstants.imagesParamName).getAsJsonArray();
     	 	for (int j = 0; j < imagesArray.size(); j++) {
     	 		final String base64Image = imagesArray.get(j).getAsString();
     	 		images.add(base64Image);
     	 	}
         }
         
         
         JamRoomPrice price = assemblePrices(roomElement, hasJamming, hasRecording,
        		 hasMasterRecording, hasVoiceover);
         
         Integer avgHourlyCost = price.getAvgHourly(HostService.JUST_JAM);
         Integer avgRecordingHourlyRate = price.getAvgHourly(HostService.JAMMING_WITH_RECORDING);
         Integer avgMasterRecordingHourlyRate = price.getAvgHourly(HostService.RECORDING_MIXING_MASTERING);
         Integer avgVoiceoverHourlyRate = price.getAvgHourly(HostService.VOICEOVER_RECORDING);
        	 
         return JamRoom.getInstance(minCapacity, maxCapacity, avgHourlyCost, soundProofed, noOfStools,
        		 mixerBoardInputs, noOfMics, null, drumkitBrand, null, speakerBrand, drumkitPieces,
        		 hasRecording, hasMasterRecording, avgRecordingHourlyRate, avgMasterRecordingHourlyRate, noOfAmplifiers,
        		 amplifiers, others, mics, images, hasVoiceover,
        		 avgVoiceoverHourlyRate, xHourPackage, price, hasJamming, price.getFlexiPrice());
	 }
	 
	 private Set<JamRoomPriceFlexi> addFlexi(final JsonArray slabs, final Set<JamRoomPriceFlexi> flexi,
			 final HostService service) {
		 for(int i = 0; i < slabs.size(); i++) {
			 final JsonObject slab = slabs.get(i).getAsJsonObject();
    			 final String dayOfWeekStart = this.fromApiJsonHelper
    					 .extractStringNamed(HostApiConstants.dayOfWeekStartParamName, slab);
    			 final String dayOfWeekEnd = this.fromApiJsonHelper
    					 .extractStringNamed(HostApiConstants.dayOfWeekEndParamName, slab);
    			 final String timeStart = this.fromApiJsonHelper
    					 .extractStringNamed(HostApiConstants.timeStartParamName, slab);
    			 final String timeEnd = this.fromApiJsonHelper
    					 .extractStringNamed(HostApiConstants.timeEndParamName, slab);
    			 final Integer price = this.fromApiJsonHelper
    					 .extractIntegerNamed(HostApiConstants.priceParamName, slab);
    			 flexi.add(JamRoomPriceFlexi.instance(service.getValue(),
	        			 dayOfWeekStart, dayOfWeekEnd, timeStart, timeEnd, price));
		 }
		 return flexi;
	 }
	 
	 
	 private JamRoomPrice assemblePrices(final JsonObject roomElement, final Boolean hasJamming,
			 final Boolean hasRecording, final Boolean hasMasterRecording,
			 final Boolean hasVoiceover) {
		 
		 Integer jammingFullWeekday = null;
		 Integer jammingFullWeekend = null;
		 Integer jammingXHourWeekday = null;
		 Integer jammingXHourWeekend = null;
		 Integer recordingFullWeekday = null;
		 Integer recordingFullWeekend = null;
		 Integer recordingXHourWeekday = null;
		 Integer recordingXHourWeekend = null;
		 Integer masteringFullWeekday = null;
		 Integer masteringFullWeekend = null;
		 Integer masteringXHourWeekday = null;
		 Integer masteringXHourWeekend = null;
		 Integer voiceoverFullWeekday = null;
		 Integer voiceoverFullWeekend = null;
		 Integer voiceoverXHourWeekday = null;
		 Integer voiceoverXHourWeekend = null;
		 
		 //Min, Max and Avgs
		 Integer minHourly = null;
		 Integer maxHourly = null;
		 Integer avgXHourPackage = null;
		 Integer fullDay = null;
		 Integer recordingMinHourly = null;
		 Integer recordingMaxHourly = null;
		 Integer recordingAvgXHourPackage = null;
		 Integer recordingFullDay = null;
		 Integer voiceoverMinHourly = null;
		 Integer voiceoverMaxHourly = null;
		 Integer voiceoverAvgXHourPackage = null;
		 Integer voiceoverFullDay = null;
		 Integer mixingMasteringMinHourly = null;
		 Integer mixingMasteringMaxHourly = null;
		 Integer mixingMasteringAvgXHourPackage = null;
		 Integer mixingMasteringFullDay = null;
		 
		 Set<JamRoomPriceFlexi> flexi = new HashSet<>();
         if(hasJamming && roomElement.has(HostApiConstants.jammingParamName)) {
	        	 final JsonObject jamming = roomElement.get(HostApiConstants.jammingParamName)
	        			 .getAsJsonObject();
	        	 jammingFullWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekdayParamName, jamming  );
	        	 jammingFullWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekendParamName, jamming  );
	        	 jammingXHourWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekdayParamName, jamming  );
	        	 jammingXHourWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekendParamName, jamming  );
	        	 final JsonArray slabs = jamming.get("slabs").getAsJsonArray();
	        	 flexi = addFlexi(slabs, flexi, HostService.JUST_JAM);
	        	 
	        	 minHourly = min(flexi, HostService.JUST_JAM);
	        	 maxHourly = max(flexi, HostService.JUST_JAM);
	        	 avgXHourPackage = avg(jammingXHourWeekday, jammingXHourWeekend);
	        	 fullDay = avg(jammingFullWeekday, jammingFullWeekend);
         }
         
         if(hasRecording && roomElement.has(HostApiConstants.recordingParamName)) {
        	 	final JsonObject recording = roomElement.get(HostApiConstants.recordingParamName)
        			 .getAsJsonObject();
	        	 recordingFullWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekdayParamName, recording  );
	        	 recordingFullWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekendParamName, recording  );
	        	 recordingXHourWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekdayParamName, recording  );
	        	 recordingXHourWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekendParamName, recording  );
	        	 final JsonArray slabs = recording.get("slabs").getAsJsonArray();
	        	 flexi = addFlexi(slabs, flexi, HostService.JAMMING_WITH_RECORDING);
	        	 
	        	 recordingMinHourly = min(flexi, HostService.JAMMING_WITH_RECORDING);
	        	 recordingMaxHourly = max(flexi, HostService.JAMMING_WITH_RECORDING);
	        	 recordingAvgXHourPackage = avg(recordingXHourWeekday, recordingXHourWeekend);
	        	 recordingFullDay = avg(recordingFullWeekday, recordingFullWeekend);
	        	 
         }
         
         if(hasMasterRecording && roomElement.has(HostApiConstants.masterParamName)) {
        	 	final JsonObject mastering = roomElement.get(HostApiConstants.masterParamName)
        			 .getAsJsonObject();
	        	 	masteringFullWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekdayParamName, mastering  );
	        	 	masteringFullWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekendParamName, mastering  );
	        	 	masteringXHourWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekdayParamName, mastering  );
	        	 	masteringXHourWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekendParamName, mastering  );
	        	 	final JsonArray slabs = mastering.get("slabs").getAsJsonArray();
		        	flexi = addFlexi(slabs, flexi, HostService.RECORDING_MIXING_MASTERING);
		        	mixingMasteringMinHourly = min(flexi, HostService.RECORDING_MIXING_MASTERING);
	        	 	mixingMasteringMaxHourly = max(flexi, HostService.RECORDING_MIXING_MASTERING);
	        	 	mixingMasteringAvgXHourPackage = avg(masteringXHourWeekday, masteringXHourWeekend);
	        	 	mixingMasteringFullDay = avg(masteringFullWeekday, masteringFullWeekend);
         }
         
         if(hasVoiceover && roomElement.has(HostApiConstants.voiceoverParamName)) {
        	 	final JsonObject voiceover = roomElement.get(HostApiConstants.voiceoverParamName)
        			 .getAsJsonObject();
	        	 	voiceoverFullWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekdayParamName, voiceover  );
	        	 	voiceoverFullWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.fullWeekendParamName, voiceover  );
	        	 	voiceoverXHourWeekday = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekdayParamName, voiceover  );
	        	 	voiceoverXHourWeekend = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.xHourWeekendParamName, voiceover  );
	        	 	final JsonArray slabs = voiceover.get("slabs").getAsJsonArray();
		        	flexi = addFlexi(slabs, flexi, HostService.VOICEOVER_RECORDING);
		        	voiceoverMinHourly = min(flexi, HostService.VOICEOVER_RECORDING);
	        	 	voiceoverMaxHourly = max(flexi, HostService.VOICEOVER_RECORDING);
	        	 	voiceoverAvgXHourPackage = avg(voiceoverXHourWeekday, voiceoverXHourWeekend);
	        	 	voiceoverFullDay = avg(voiceoverFullWeekday, voiceoverFullWeekend);
         }
		 
         // TODO: Add validations for flexi
         // Check no overlap between slots and whether time window spans across all working hours
         
		 return JamRoomPrice.instance(flexi, jammingFullWeekday,
    			 jammingFullWeekend, jammingXHourWeekday, jammingXHourWeekend,
    			 recordingFullWeekday, recordingFullWeekend, recordingXHourWeekday,
    			 recordingXHourWeekend, masteringFullWeekday, masteringFullWeekend,
    			 masteringXHourWeekday, masteringXHourWeekend, voiceoverFullWeekday, voiceoverFullWeekend,
    			 voiceoverXHourWeekday, voiceoverXHourWeekend,
    			 minHourly, maxHourly, avgXHourPackage, fullDay,
    			 recordingMinHourly, recordingMaxHourly,
    			 recordingAvgXHourPackage, recordingFullDay,
    			 voiceoverMinHourly, voiceoverMaxHourly,
    			 voiceoverAvgXHourPackage, voiceoverFullDay,
    			 mixingMasteringMinHourly, mixingMasteringMaxHourly,
    			 mixingMasteringAvgXHourPackage, mixingMasteringFullDay);
		 
	 }
	 
	 public Quote assembleQuote(final JamRoom room, final JsonElement element) {
		 
		 final Integer noOfBandMembers = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfBandMembersParamName, element);
		 final Integer noOfSongs = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.noOfSongsParamName, element);
		 final Integer avgSongLength = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.avgSongLengthParamName, element);
		 final Integer avgNoOfTracksPerSong = this.fromApiJsonHelper.extractIntegerNamed(HostApiConstants.avgNoOfTracksPerSongParamName, element);
		 final String message = this.fromApiJsonHelper.extractStringNamed(HostApiConstants.messageParamName, element);
		 
		 final List<DocumentCommand> songSamples = new ArrayList<> ();
		 final JsonObject quote = element.getAsJsonObject();
         if(quote.has(HostApiConstants.songSamplesParamName) && quote.get(HostApiConstants.songSamplesParamName).getAsJsonArray() != null) {
        	 final JsonArray songSamplesArray = quote.get(HostApiConstants.songSamplesParamName).getAsJsonArray();
     	 	for (int j = 0; j < songSamplesArray.size(); j++) {
     	 		final String base64Song = songSamplesArray.get(j).getAsString();
     	 		byte[] imageByteArray =  null;
     	 		if(base64Song.startsWith("data"))
     	 			imageByteArray = Base64.decodeBase64(base64Song.split(",")[1]);
     	 		else 
     	 			imageByteArray = Base64.decodeBase64(base64Song);
				InputStream inputStream = new ByteArrayInputStream(imageByteArray);
     	 		final String fileType = "audio/mp3";
     	 		final String fileName = RandomStringUtils.randomAlphabetic(10) + ".mp3";
     	 		songSamples.add(new DocumentCommand(null, null, DOCUMENT_MANAGEMENT_ENTITY.QUOTES.name(), null,
     	 				fileName, fileName, new Long(imageByteArray.length), fileType, null, null,
     	 				DOCUMENT_MANAGEMENT_ENTITY.CONSUMERS.name(), this.context.authenticatedUser().getId(),
     	 				null, inputStream));
     	 	}
         }
		 
		 return Quote.instance(room, noOfBandMembers, noOfSongs, avgSongLength, avgNoOfTracksPerSong, message, songSamples);
	 }
	 
	 private Integer min(final Set<JamRoomPriceFlexi> flexi, HostService service) {
		 int count = 0;
		 Integer min = 0;
		 for(JamRoomPriceFlexi flex : flexi) {
			 if(flex.getServiceType().equals(service.getValue())) {
				 count++;
				 if(count == 1)
					 min = flex.getPrice();
				 if(flex.getPrice() < min)
					 min = flex.getPrice();
			 }
		 }
		 return min;
	 }
	 
	 private Integer max(final Set<JamRoomPriceFlexi> flexi, HostService service) {
		 int count = 0;
		 Integer max = 0;
		 for(JamRoomPriceFlexi flex : flexi) {
			 if(flex.getServiceType().equals(service.getValue())) {
				 count++;
				 if(count == 1)
					 max = flex.getPrice();
				 if(flex.getPrice() > max)
					 max = flex.getPrice();
			 }
		 }
		 return max;
	 }
	 
	 private Integer avg(Integer... nos) {
		 Integer sum = 0;
		 for(Integer no : nos) {
			 sum += no;
		 }
		 return (int) Math.round((sum/nos.length)/10.0) * 10;
	 }
	 
	 

}
