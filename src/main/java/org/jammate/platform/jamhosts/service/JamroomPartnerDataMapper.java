package org.jammate.platform.jamhosts.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.jamhosts.data.HostService;
import org.jammate.platform.jamhosts.data.JamroomData;
import org.jammate.platform.jamhosts.data.PriceData;
import org.jammate.platform.jamhosts.data.PriceDetailsData;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public final class JamroomPartnerDataMapper implements RowMapper<JamroomData> {
	
	private final JdbcTemplate jdbcTemplate;
	public JamroomPartnerDataMapper(final JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public String roomSchema() {
		final StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("r.id as roomId, h.name as studioName, null as avgRating,")
		.append(" h.imageId as profilePic, r.xHourPackage as xHourPackage, h.houseNo as houseNo, h.street as street,")
		.append(" h.landmark as landmark, h.area as area, CONCAT(h.openHours,':', h.openMins) as openingHours,")
		.append(" CONCAT(h.closeHours,':', h.closeMins) as closingHours, r.`hasJamming` as hasJamming,")
		.append(" r.hasRecording as hasRecording, r.hasMasterRecording as hasMixingAndMastering,")
		.append(" r.minCapacity as minCapacity, r.maxCapacity as maxCapacity, r.soundProofed as")
		.append(" hasSoundProofing, r.drumkitBrand as drumkitBrand, r.drumkitPieces as drumkitPieces,")
		.append(" r.noOfAmplifiers as noOfAmplifiers, r.amplifierBrands as amplifierBrands, r.noOfStools as noOfStools, r.speakerBrand as speakerBrand,")
		.append(" r.noOfMics as totalMics, r.otherItems as otherItems, 1 as totalMicsWithCondenser, r.`mixerBoardInputs`")
		.append(" as mixerBoardInputs, h.fbPage as facebookPage, h.website as website, h.latitude as latitude,")
		.append(" h.longitude as longitude, 1 as totalWirelessMics, 1 as totalInstrumentalMics, 2 as totalInstrumentalMics,")
		.append(" 'INR' as currency, r.avgHourly as avgHourly, r.avgRecordingHourly as avgRecordingHourly,")
		.append("  r.avgMixingMasteringHourly as avgMixingMasteringHourly,")
		.append("rp.minHourly as minHourly, rp.maxHourly as maxHourly, r.`avgHourly` as avgHourly,")
		.append("rp.avgXHourPackage as avgXHourPackage, rp.fullDay as fullDay,")
		.append("rp.recordingMinHourly as recordingMinHourly, rp.recordingMaxHourly as recordingMaxHourly,")
		.append("r.`avgRecordingHourly` as avgRecordingHourly, rp.recordingAvgXHourPackage as recordingAvgXHourPackage,")
		.append("rp.recordingFullDay as recordingFullDay, rp.voiceoverMinHourly as voiceoverMinHourly,")
		.append("rp.voiceoverMaxHourly as voiceoverMaxHourly, r.`avgVoiceoverHourly` as avgVoiceoverHourly,")
		.append("rp.voiceoverAvgXHourPackage as voiceoverAvgXHourPackage, rp.voiceoverFullDay as voiceoverFullDay, ")
		.append("rp.mixingMasteringMinHourly as mixingMasteringMinHourly, rp.mixingMasteringMaxHourly as mixingMasteringMaxHourly, ")
		.append("r.`avgMixingMasteringHourly` as avgMixingMasteringHourly, rp.mixingMasteringAvgXHourPackage as mixingMasteringAvgXHourPackage, ")
		.append("rp.mixingMasteringFullDay as mixingMasteringFullDay, r.overallAverage as overallAverage, ")
		.append("rp.jammingFullWeekday  as  jammingFullWeekday, rp.jammingFullWeekend  as  jammingFullWeekend,")
		.append("rp.jammingXHourWeekday  as  jammingXHourWeekday, rp.jammingXHourWeekend  as  jammingXHourWeekend,")
		.append("rp.recordingFullWeekday  as  recordingFullWeekday, rp.recordingFullWeekend  as  recordingFullWeekend,")
		.append("rp.recordingXHourWeekday  as recordingXHourWeekday, rp.recordingXHourWeekend  as recordingXHourWeekend,")
		.append("rp.masteringFullWeekday  as  masteringFullWeekday, rp.masteringFullWeekend  as masteringFullWeekend ,")
		.append("rp.masteringXHourWeekday  as masteringXHourWeekday , rp.masteringXHourWeekend  as  masteringXHourWeekend,")
		.append("rp.voiceoverFullWeekday  as  voiceoverFullWeekday, rp.voiceoverFullWeekend  as  voiceoverFullWeekend,")
		.append("rp.voiceoverXHourWeekday  as  voiceoverXHourWeekday, rp.voiceoverXHourWeekend  as  voiceoverXHourWeekend")
		.append(" from room r inner join room_price rp on r.id = rp.roomId inner join jamhost h on r.hostId = h.id");
		
		return sqlBuilder.toString();
	}
	
	@Override
	public JamroomData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
		
	    	final Long roomId = rs.getLong("roomId");
	    	final String studioName = rs.getString("studioName");
	    	final Double avgRating = rs.getDouble("avgRating");
	    	final Long profilePic = rs.getLong("profilePic");
	    	final String houseNo = rs.getString("houseNo");
	    	final String street = rs.getString("street");
	    	final String area = rs.getString("area");
	    	final String landmark = rs.getString("landmark");
	    	final String openingHours = rs.getString("openingHours");
	    	final String closingHours = rs.getString("closingHours");
	    	final Boolean hasJamming = rs.getBoolean("hasJamming");
	    	final Boolean hasRecording = rs.getBoolean("hasRecording");
	    	final Boolean hasMixingAndMastering = rs.getBoolean("hasMixingAndMastering");
	    	final Integer minCapacity = rs.getInt("minCapacity");
	    	final Integer maxCapacity = rs.getInt("maxCapacity");
	    	final Boolean hasSoundproofing = rs.getBoolean("hasSoundproofing");
	    	final String drumkitBrand = rs.getString("drumkitBrand");
	    	final Integer drumkitPieces = rs.getInt("drumkitPieces");
	    	final Integer noOfAmplifiers = rs.getInt("noOfAmplifiers");
     	final Integer noOfStools = rs.getInt("noOfStools");
     	final String speakerBrand = rs.getString("speakerBrand");
	    	final Integer totalMics = rs.getInt("totalMics");
	    	final Integer totalMicsWithCondenser = rs.getInt("totalMicsWithCondenser");
	    	final Integer totalWirelessMics = rs.getInt("totalWirelessMics");
	    	final Integer totalInstrumentalMics = rs.getInt("totalInstrumentalMics");
	    	final Integer mixerBoardInputs = rs.getInt("mixerBoardInputs");
	    	final String facebookPage = rs.getString("facebookPage");
	    	final String website = rs.getString("website");
	    	final String latitude = rs.getString("latitude");
	    	final String longitude = rs.getString("longitude");
	    	
	    	final String otherItem = rs.getString("otherItems");
	    List<String> otherItems = new ArrayList<>();
	    if(!StringUtils.isEmpty(otherItem)) {
	    		final String split[] = otherItem.split("\\|");
	    		Collections.addAll(otherItems, split);
	    }
	    final String amplifierBrand = rs.getString("amplifierBrands");
	    	List<String> amplifierBrands = new ArrayList<>();
	    	if(!StringUtils.isEmpty(amplifierBrand)) {
	    		final String split[] = amplifierBrand.split("\\|");
	    		Collections.addAll(amplifierBrands, split);
	    }
	
	    	//PriceData
	    	final String currency = rs.getString("currency");
	    	final Integer minHourly = rs.getInt("minHourly");
	    	final Integer maxHourly = rs.getInt("maxHourly");
	    	final Integer avgHourly = rs.getInt("avgHourly");
	    	final Integer avgXHourPackage = rs.getInt("avgXHourPackage");
	    	final Integer fullDay = rs.getInt("fullDay");
	    	final Integer recordingMinHourly = rs.getInt("recordingMinHourly");
	    	final Integer recordingMaxHourly = rs.getInt("recordingMaxHourly");
	    	final Integer avgRecordingHourly = rs.getInt("avgRecordingHourly");
	    	final Integer recordingAvgXHourPackage = rs.getInt("recordingAvgXHourPackage"); 
	    	final Integer recordingFullDay = rs.getInt("recordingFullDay"); 
	    	final Integer voiceoverMinHourly = rs.getInt("voiceoverMinHourly");
	    	final Integer voiceoverMaxHourly = rs.getInt("voiceoverMaxHourly");
	    	final Integer avgVoiceoverHourly = rs.getInt("avgVoiceoverHourly");
	    	final Integer voiceoverAvgXHourPackage = rs.getInt("voiceoverAvgXHourPackage");
	    	final Integer voiceoverFullDay = rs.getInt("voiceoverFullDay");
	    	final Integer mixingMasteringMinHourly = rs.getInt("mixingMasteringMinHourly");
	    	final Integer mixingMasteringMaxHourly = rs.getInt("mixingMasteringMaxHourly");
	    	final Integer avgMixingMasteringHourly = rs.getInt("avgMixingMasteringHourly");
	    	final Integer mixingMasteringAvgXHourPackage = rs.getInt("mixingMasteringAvgXHourPackage");
	    	final Integer mixingMasteringFullDay = rs.getInt("mixingMasteringFullDay");
	    	final Integer overallAvg = rs.getInt("overallAverage");
	    	final Integer xHourPackage = rs.getInt("xHourPackage");
	    	final List<Integer> availableServices = new ArrayList<> ();
	    	if(hasJamming)
	    		availableServices.add(HostService.JUST_JAM.getValue());
	    	if(hasRecording)
	    		availableServices.add(HostService.JAMMING_WITH_RECORDING.getValue());
	    	if(hasMixingAndMastering) {
	    		availableServices.add(HostService.RECORDING_MIXING_MASTERING.getValue());
	    		availableServices.add(HostService.VOICEOVER_RECORDING.getValue());
	    	}
	    	
	    	final PriceData priceData = PriceData.instance(currency, minHourly, maxHourly, avgHourly, avgXHourPackage, fullDay, recordingMinHourly,
	        		recordingMaxHourly, avgRecordingHourly, recordingAvgXHourPackage, recordingFullDay, voiceoverMinHourly, voiceoverMaxHourly,
	        		avgVoiceoverHourly, voiceoverAvgXHourPackage, voiceoverFullDay, mixingMasteringMinHourly, mixingMasteringMaxHourly,
	        		avgMixingMasteringHourly, mixingMasteringAvgXHourPackage, mixingMasteringFullDay, overallAvg);
	        
	    	
	    	//Price Details Data
	    	final Integer jammingHourlyWeekdayMorning = rs.getInt("jammingHourlyWeekdayMorning");
	    	final Integer jammingHourlyWeekdayAfternoon = rs.getInt("jammingHourlyWeekdayAfternoon");
	    	final Integer jammingHourlyWeekdayEvening = rs.getInt("jammingHourlyWeekdayEvening");
	    	final Integer jammingHourlyWeekdayNight = rs.getInt("jammingHourlyWeekdayNight");
	    	final Integer jammingHourlyWeekendMorning = rs.getInt("jammingHourlyWeekendMorning");
	    	final Integer jammingHourlyWeekendAfternoon = rs.getInt("jammingHourlyWeekendAfternoon");
	    	final Integer jammingHourlyWeekendEvening = rs.getInt("jammingHourlyWeekendEvening");
	    	final Integer jammingHourlyWeekendNight = rs.getInt("jammingHourlyWeekendNight");
	    	final Integer jammingFullWeekday = rs.getInt("jammingFullWeekday");
	    	final Integer jammingFullWeekend = rs.getInt("jammingFullWeekend");
	    	final Integer jammingXHourWeekday = rs.getInt("jammingXHourWeekday");
	    	final Integer jammingXHourWeekend = rs.getInt("jammingXHourWeekend");
	    	final Integer recordingHourlyWeekdayMorning = rs.getInt("recordingHourlyWeekdayMorning");
	    	final Integer recordingHourlyWeekdayAfternoon = rs.getInt("recordingHourlyWeekdayAfternoon");
	    	final Integer recordingHourlyWeekdayEvening = rs.getInt("recordingHourlyWeekdayEvening");
	    	final Integer recordingHourlyWeekdayNight = rs.getInt("recordingHourlyWeekdayNight");
	    	final Integer recordingHourlyWeekendMorning = rs.getInt("recordingHourlyWeekendMorning");
	    	final Integer recordingHourlyWeekendAfternoon = rs.getInt("recordingHourlyWeekendAfternoon");
	    	final Integer recordingHourlyWeekendEvening = rs.getInt("recordingHourlyWeekendEvening");
	    	final Integer recordingHourlyWeekendNight = rs.getInt("recordingHourlyWeekendNight");
	    	final Integer recordingFullWeekday = rs.getInt("recordingFullWeekday");
	    	final Integer recordingFullWeekend = rs.getInt("recordingFullWeekend");
	    	final Integer recordingXHourWeekday = rs.getInt("recordingXHourWeekday");
	    	final Integer recordingXHourWeekend = rs.getInt("recordingXHourWeekend");
	    	final Integer masteringHourlyWeekdayMorning = rs.getInt("masteringHourlyWeekdayMorning");
	    	final Integer masteringHourlyWeekdayAfternoon = rs.getInt("masteringHourlyWeekdayAfternoon");
	    	final Integer masteringHourlyWeekdayEvening = rs.getInt("masteringHourlyWeekdayEvening");
	    	final Integer masteringHourlyWeekdayNight = rs.getInt("masteringHourlyWeekdayNight");
	    	final Integer masteringHourlyWeekendMorning = rs.getInt("masteringHourlyWeekendMorning");
	    	final Integer masteringHourlyWeekendAfternoon = rs.getInt("masteringHourlyWeekendAfternoon");
	    	final Integer masteringHourlyWeekendEvening = rs.getInt("masteringHourlyWeekendEvening");
	    	final Integer masteringHourlyWeekendNight = rs.getInt("masteringHourlyWeekendNight");
	    	final Integer masteringFullWeekday = rs.getInt("masteringFullWeekday");
	    	final Integer masteringFullWeekend = rs.getInt("masteringFullWeekend");
	    	final Integer masteringXHourWeekday = rs.getInt("masteringXHourWeekday");
	    	final Integer masteringXHourWeekend = rs.getInt("masteringXHourWeekend");
	    	final Integer voiceoverHourlyWeekdayMorning = rs.getInt("voiceoverHourlyWeekdayMorning");
	    	final Integer voiceoverHourlyWeekdayAfternoon = rs.getInt("voiceoverHourlyWeekdayAfternoon");
	    	final Integer voiceoverHourlyWeekdayEvening = rs.getInt("voiceoverHourlyWeekdayEvening");
	    	final Integer voiceoverHourlyWeekdayNight = rs.getInt("voiceoverHourlyWeekdayNight");
	    	final Integer voiceoverHourlyWeekendMorning = rs.getInt("voiceoverHourlyWeekendMorning");
	    	final Integer voiceoverHourlyWeekendAfternoon = rs.getInt("voiceoverHourlyWeekendAfternoon");
	    	final Integer voiceoverHourlyWeekendEvening = rs.getInt("voiceoverHourlyWeekendEvening");
	    	final Integer voiceoverHourlyWeekendNight = rs.getInt("voiceoverHourlyWeekendNight");
	    	final Integer voiceoverFullWeekday = rs.getInt("voiceoverFullWeekday");
	    	final Integer voiceoverFullWeekend = rs.getInt("voiceoverFullWeekend");
	    	final Integer voiceoverXHourWeekday = rs.getInt("voiceoverXHourWeekday");
	    	final Integer voiceoverXHourWeekend = rs.getInt("voiceoverXHourWeekend");
	    	
	    final PriceDetailsData priceDetailsData = PriceDetailsData.instance(currency,
	    		jammingHourlyWeekdayMorning, jammingHourlyWeekdayAfternoon, jammingHourlyWeekdayEvening,
	    		jammingHourlyWeekdayNight, jammingHourlyWeekendMorning, jammingHourlyWeekendAfternoon,
	    		jammingHourlyWeekendEvening, jammingHourlyWeekendNight, jammingFullWeekday,
	    		jammingFullWeekend, jammingXHourWeekday, jammingXHourWeekend, recordingHourlyWeekdayMorning,
	    		recordingHourlyWeekdayAfternoon, recordingHourlyWeekdayEvening, recordingHourlyWeekdayNight,
	    		recordingHourlyWeekendMorning, recordingHourlyWeekendAfternoon, recordingHourlyWeekendEvening,
	    		recordingHourlyWeekendNight, recordingFullWeekday, recordingFullWeekend, recordingXHourWeekday,
	    		recordingXHourWeekend, masteringHourlyWeekdayMorning, masteringHourlyWeekdayAfternoon,
	    		masteringHourlyWeekdayEvening, masteringHourlyWeekdayNight, masteringHourlyWeekendMorning,
	    		masteringHourlyWeekendAfternoon, masteringHourlyWeekendEvening, masteringHourlyWeekendNight,
	    		masteringFullWeekday, masteringFullWeekend, masteringXHourWeekday, masteringXHourWeekend,
	    		voiceoverHourlyWeekdayMorning, voiceoverHourlyWeekdayAfternoon, voiceoverHourlyWeekdayEvening,
	    		voiceoverHourlyWeekdayNight, voiceoverHourlyWeekendMorning, voiceoverHourlyWeekendAfternoon,
	    		voiceoverHourlyWeekendEvening, voiceoverHourlyWeekendNight, voiceoverFullWeekday,
	    		voiceoverFullWeekend, voiceoverXHourWeekday, voiceoverXHourWeekend);
	    
	    
	    return JamroomData.instance(roomId, null,  studioName, avgRating,  profilePic,  houseNo,
				 street,  area,  landmark, openingHours,  closingHours,
				 hasJamming,  hasRecording, hasMixingAndMastering,  minCapacity,
				 maxCapacity,  hasSoundproofing, drumkitBrand,  drumkitPieces,
				 amplifierBrands,  noOfAmplifiers, totalMics,  totalMicsWithCondenser,
				 totalWirelessMics,  totalInstrumentalMics, mixerBoardInputs,  otherItems,
				 facebookPage,  website,  latitude, longitude, priceData, priceDetailsData,
				 xHourPackage, availableServices, noOfStools, speakerBrand);
	}
	
}
