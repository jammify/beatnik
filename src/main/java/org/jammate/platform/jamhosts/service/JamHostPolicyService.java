package org.jammate.platform.jamhosts.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.jammate.platform.core.infra.JdbcSupport;
import org.jammate.platform.jamhosts.data.CancellationPolicyData;
import org.jammate.platform.jamhosts.data.PolicyData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class JamHostPolicyService {
	
	private final static Logger logger = LoggerFactory
			.getLogger(JamHostPolicyService.class);
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JamHostPolicyService(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate();
	    this.jdbcTemplate.setDataSource(dataSource);
	}
	
	public PolicyData retrievePolicies(final Long roomId) {
		final HostPolicyDataMapper pm = new HostPolicyDataMapper(this.jdbcTemplate);
		final String sql = "select " + pm.policySchema() + " where r.id = ?";
		final List<CancellationPolicyData> cancellationPolicy = this.jdbcTemplate.query(sql, pm, new Object[] { roomId});
		final String selectSql = "select h.`name` as studioName, h.`imageId` as profilePicId," + 
				" CONCAT(h.`houseNo`, ',', h.`street`,',', h.landmark,',', h.`area`,'-', h.`pinCode`) as address" + 
				" from room r inner join jamhost h on r.hostId=h.id where r.id = ?";
		List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(selectSql,
				new Object[] { roomId});
		Map<String, Object> row = rows.get(0);
		final String studioName = row.get("studioName").toString();
		final String address = row.get("address").toString();
		Long profilePicId = null;
		if(row.get("profilePicId") != null)
			profilePicId = Long.parseLong(row.get("profilePicId").toString());
		final PolicyData policy = PolicyData.instance(cancellationPolicy, studioName, profilePicId,
				address);
		return policy;
	}
	
	private static final class HostPolicyDataMapper implements RowMapper<CancellationPolicyData> {
    	
    	private final JdbcTemplate jdbcTemplate;
    	
    	public HostPolicyDataMapper(final JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
	}

    public String policySchema() {
	    	final StringBuilder sqlBuilder = new StringBuilder();
    		sqlBuilder
    		.append(" p.start as hoursFromBookingFrom, p.end as  hoursFromBookingto, p.refund_percent as percentageRefund ") 
		.append(" from cancellation_policy p ")
		.append(" inner join jamhost h on p.hostid=h.id ") 
		.append(" inner join room r on h.id=r.hostId ");
    		return sqlBuilder.toString();
    		
    }

    @Override
    public CancellationPolicyData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
    		final Integer hoursFromBookingFrom = JdbcSupport.getInteger(rs, "hoursFromBookingFrom");
        final Integer hoursFromBookingTo = JdbcSupport.getInteger(rs, "hoursFromBookingto");
        final Integer percentageRefund = rs.getInt("percentageRefund");
        
        
        return CancellationPolicyData.instance(hoursFromBookingFrom, hoursFromBookingTo,
        		percentageRefund);
    }
}

}
