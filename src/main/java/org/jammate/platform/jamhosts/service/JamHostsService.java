package org.jammate.platform.jamhosts.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.jammate.platform.auth.domain.AppUser;
import org.jammate.platform.auth.service.PlatformSecurityContext;
import org.jammate.platform.calendar.data.Days;
import org.jammate.platform.calendar.domain.Block;
import org.jammate.platform.calendar.domain.BlockRepository;
import org.jammate.platform.core.infra.CommandProcessingResult;
import org.jammate.platform.core.infra.CommandProcessingResultBuilder;
import org.jammate.platform.core.infra.FromJsonHelper;
import org.jammate.platform.core.infra.GlobalEntityType;
import org.jammate.platform.core.infra.JsonCommand;
import org.jammate.platform.core.infra.StringUtil;
import org.jammate.platform.document.command.DocumentCommand;
import org.jammate.platform.document.service.DocumentWritePlatformService;
import org.jammate.platform.jamhosts.data.BlockedData;
import org.jammate.platform.jamhosts.data.HostData;
import org.jammate.platform.jamhosts.data.HostService;
import org.jammate.platform.jamhosts.data.JamHostData;
import org.jammate.platform.jamhosts.data.JamroomData;
import org.jammate.platform.jamhosts.data.LandingResponseData;
import org.jammate.platform.jamhosts.data.LastBookingData;
import org.jammate.platform.jamhosts.data.MicData;
import org.jammate.platform.jamhosts.data.PriceData;
import org.jammate.platform.jamhosts.domain.JamHost;
import org.jammate.platform.jamhosts.domain.JamHostRepository;
import org.jammate.platform.jamhosts.domain.JamRoom;
import org.jammate.platform.jamhosts.domain.JamRoomPrice;
import org.jammate.platform.jamhosts.domain.JamRoomPriceFlexi;
import org.jammate.platform.jamhosts.domain.JamRoomRepository;
import org.jammate.platform.jamhosts.domain.Quote;
import org.jammate.platform.jamhosts.domain.QuoteRepository;
import org.jammate.platform.orders.data.OrderStatus;
import org.jammate.platform.orders.domain.Order;
import org.jammate.platform.orders.domain.OrderRepository;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JamHostsService {

	private final static Logger logger = LoggerFactory
			.getLogger(JamHostsService.class);
	private final JdbcTemplate jdbcTemplate;
	private final JamHostAssembler hostAssembler;
	private final JamHostRepository hostRepository;
	private final JamRoomRepository roomRepository;
	private final QuoteRepository quoteRepository;
	private final DocumentWritePlatformService documentService;
	private final OrderRepository orderRepository;
	private final BlockRepository blockRepository;
	private final PlatformSecurityContext context;
	private final FromJsonHelper fromApiJsonHelper;
	
	@Autowired
	public JamHostsService(final DataSource dataSource,
			final JamHostAssembler hostAssembler,
			final JamHostRepository hostRepository,
			final DocumentWritePlatformService documentService,
			final JamRoomRepository roomRepository,
			final QuoteRepository quoteRepository,
			final OrderRepository orderRepository,
			final BlockRepository blockRepository,
			final PlatformSecurityContext context,
			final FromJsonHelper fromApiJsonHelper) {
	    this.jdbcTemplate = new JdbcTemplate();
	    this.jdbcTemplate.setDataSource(dataSource);
	    this.hostAssembler = hostAssembler;
	    this.hostRepository = hostRepository;
	    this.documentService = documentService;
	    this.roomRepository = roomRepository;
	    this.quoteRepository = quoteRepository;
	    this.orderRepository = orderRepository;
	    this.context = context;
	    this.blockRepository = blockRepository;
	    this.fromApiJsonHelper = fromApiJsonHelper;
	}
	
	@Transactional
	public CommandProcessingResult createPartner(final JsonCommand partner) {
		JamHost host = this.hostAssembler.assembleHost(partner.parsedJson());
		this.hostRepository.save(host);
		
		return new CommandProcessingResultBuilder()
        		.withResourceIdAsString(host.getId()).build();
	} 
	
	@Transactional
	public CommandProcessingResult updateJamHost(Long jamhostId, JsonCommand from) {
        final JamHost host = this.hostRepository.findOne(jamhostId);   
        host.update(from.parsedJson(), fromApiJsonHelper);
        this.hostRepository.save(host);
        
        return new CommandProcessingResultBuilder()
        		.withResourceIdAsString(host.getId()).build();
	}	
	
	@Transactional
	public CommandProcessingResult updateJamRoom(final Long roomId, final JsonCommand from) {
        final JamRoom room = this.roomRepository.findOne(roomId);
        room.update(from.parsedJson(), fromApiJsonHelper);
        
        this.roomRepository.save(room);
        
        return new CommandProcessingResultBuilder()
        		.withResourceIdAsString(room.getId()).build();
	}
	
	@Transactional
	public CommandProcessingResult createRoom(final Long hostId, 
			final JsonCommand command) {
		final JamHost host = this.hostRepository.findOne(hostId);
		JamRoom room = this.hostAssembler.assembleRoom(command.parsedJson());
		host.setRoom(room);
		this.roomRepository.save(room);
		final List<String> images = room.getImages();
		int imageCount = 0;
		for(String image : images) {
//			try {
				byte[] imageByteArray = null;
				if(image.startsWith("data"))
					imageByteArray = Base64.decodeBase64(image.split(",")[1]);
				else
					imageByteArray = Base64.decodeBase64(image);
				InputStream inputStream = new ByteArrayInputStream(imageByteArray);
				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
//				IOUtils.copy(inputStream, baos);
//	            final byte[] bytes = baos.toByteArray();
//	            final Tika tika = new Tika();
//	            final TikaInputStream tikaInputStream = TikaInputStream.get(inputStream);
//				final String fileType = tika.detect(tikaInputStream);
				final String fileType = "image/jpeg";
				final String fileName = RandomStringUtils.randomAlphabetic(10) + ".jpg";
				final DocumentCommand documentCommand = new DocumentCommand(null, null, GlobalEntityType.ROOMS.getCode(), room.getId(), fileName,
						fileName, Long.valueOf(imageByteArray.length), fileType, null, null, null, null, null, null);

				
		        final Long documentId = this.documentService.createDocument(documentCommand, inputStream);
		        if(imageCount == 0) {
		        		room.setPrimaryImage(documentId);
		        		logger.info(room.getPrimaryImageId() + "-");
		        }
		        imageCount++;
//			} catch(IOException io) {
//				io.printStackTrace();
//			}
		}
		logger.info(room.getPrimaryImageId() + "-");
		this.roomRepository.save(room);
		return new CommandProcessingResultBuilder()
        		.withResourceIdAsString(room.getId()).build();
	}
	
	public HostData retrieveHost(final Long hostId) {
		final HostMapper rm = new HostMapper(jdbcTemplate);
		String sql = "select " + rm.schema() + " where h.id = ?";
		final HostData host = this.jdbcTemplate.queryForObject(sql, rm, new Object[] { hostId});
		final String roomSql = "select id from room where hostId = ?";
		final List<Integer> roomIds = this.jdbcTemplate.queryForList(roomSql, Integer.class, new Object[] {hostId});
		host.setRoomIds(roomIds);
		return host;
	}
	
	public LandingResponseData retrieveJamLocations(final String latitude,
			final String longitude, Integer offset, Integer limit,
			final String search, final String date, final Long time, final Long service) {
		BigDecimal currentLatDeg = new BigDecimal(latitude);
		BigDecimal currentLonDeg = new BigDecimal(longitude);
		BigDecimal radiansToDegs = new BigDecimal(57.2957795);

		Integer distance = 10;
		BigDecimal currentLat = currentLatDeg.divide(radiansToDegs, 6, RoundingMode.HALF_UP);
		BigDecimal currentLon = currentLonDeg.divide(radiansToDegs, 6, RoundingMode.HALF_UP);
		Integer earthsRadius = 6371;

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT *, (acos(sin(").append(currentLat) .append(") * sin(j.latitude) + cos(")
		.append(currentLat) .append(") * cos(j.latitude) ")
		.append(" * cos(j.longitude - (").append(currentLon) .append("))) * ").append(earthsRadius) 
		.append(") as distance FROM jamhost j WHERE ")
		.append("acos(sin(").append(currentLat) .append(") * sin(j.latitude) + cos(").append(currentLat) 
		.append(") * cos(j.latitude) * cos(j.longitude ")
		.append("- (").append(currentLon) .append("))) * ").append(earthsRadius) .append(" <= ").append(distance) 
		.append(" ORDER BY acos(sin(").append(currentLat) .append(") * sin(j.latitude) ")
		.append("+ cos(").append(currentLat) .append(") * cos(j.latitude) * cos(j.longitude - (")
		.append(currentLon) .append("))) * ").append(earthsRadius) .append(" DESC");
		
		
		final JamHostDataMapper rm = new JamHostDataMapper(jdbcTemplate);
		String sql = "select " + rm.landingSchema() + " where h.live=1 ";
		if(limit == null) {
			limit = 5;
		}
		sql += " limit " + limit;
		if(offset == null) {
			offset = 0;
		}
		sql += " offset " + offset;
		final List<JamHostData> hosts = this.jdbcTemplate.query(sql, rm, new Object[] { });
		final Order order = this.orderRepository.findNonSeenLapsedOrder(this.context.authenticatedUser().getId());
		LastBookingData lastBooking = null;
		if(order != null) {
			order.setFeedbackFormSeen();
			this.orderRepository.save(order);
			DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
			final LocalDateTime timeFrom = LocalDateTime.parse(order.getTimeFrom("ddMMMyyyy HH:mm"), DATEFORMATTER);
			final String month = timeFrom.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH);
			final Integer dayOfMonth = timeFrom.getDayOfMonth();
			final JamHost host = order.getRoom().getHost();
			lastBooking = LastBookingData.instance(order.getId(), host.getName(),
					dayOfMonth + ", " + month, host.getImageId());
		}
		return LandingResponseData.instance(hosts, lastBooking);
	}
	
	public JamroomData retrieveJamroomDetails(final Long roomId) {
		final JamroomPartnerDataMapper rm = new JamroomPartnerDataMapper(jdbcTemplate);
		final String sql = "select " + rm.roomSchema() + " where r.id = ?";
		final JamroomData room = this.jdbcTemplate.queryForObject(sql, rm, new Object[] { roomId});
		final String imagesSql = "select id from document where parentEntityId = ? and parentEntityType = ?";
		final List<Long> imageIds = this.jdbcTemplate.queryForList(imagesSql, Long.class, new Object[] {roomId, "rooms"});
		room.setImageIds(imageIds);
		
		final MicMapper mm = new MicMapper(jdbcTemplate);
		final String micSql = "select " + mm.schema() + " where m.roomId = ?";
		final List<MicData> mics = this.jdbcTemplate.query(micSql, mm, new Object[] {roomId});
		room.setMics(mics);
		return room;
	}
	
	public JamroomData retrieveJamLocation(final Long roomId) {
		try {
		final JamroomDataMapper rm = new JamroomDataMapper(jdbcTemplate);
		final String sql = "select " + rm.roomSchema() + " where r.id = ?";
		final JamroomData room = this.jdbcTemplate.queryForObject(sql, rm, new Object[] { roomId});
		final String imagesSql = "select id from document where parentEntityId = ? and parentEntityType = ?";
		final List<Long> imageIds = this.jdbcTemplate.queryForList(imagesSql, Long.class, new Object[] {roomId, "rooms"});
		room.setImageIds(imageIds);
		logger.info(imageIds.get(0).toString());
		final MicMapper mm = new MicMapper(jdbcTemplate);
		final String micSql = "select " + mm.schema() + " where m.roomId = ?";
		final List<MicData> mics = this.jdbcTemplate.query(micSql, mm, new Object[] {roomId});
		room.setMics(mics);
		return room;
		} catch(final EmptyResultDataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	

	public Integer retrievePrice(final Long roomId, final Integer service, 
			final String dateFrom, final String dateTo,
			final String timeFrom, final String timeTo) {
		final JamRoom room = this.roomRepository.findOne(roomId);
		final JamRoomPrice price = room.getPrice();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMMyyyy");
		final LocalDate fromDate = LocalDate.parse(dateFrom, formatter);
		LocalDate toDate = null;
		if(dateTo != null)
			toDate = LocalDate.parse(dateTo, formatter);
		DayOfWeek day = fromDate.getDayOfWeek();
		Boolean isWeekend = false;
		if(day.name().equalsIgnoreCase("SATURDAY") 
				|| day.name().equalsIgnoreCase("SUNDAY")) { //Weekend
			isWeekend = true;
		} 
		final Integer xHourPackage = room.getXHourPackage();
		Boolean xHourPackageApplied = false;
		Integer noOfMins = null;
		LocalTime from = null;
		LocalTime to = null;
		if(timeTo != null && timeFrom != null) {
			from = LocalTime.parse(timeFrom, DateTimeFormat.forPattern("HH:mm")) ;
			to = LocalTime.parse(timeTo, DateTimeFormat.forPattern("HH:mm")) ;
			noOfMins = Minutes.minutesBetween(from, to).getMinutes();
			if(noOfMins == (xHourPackage*60))
				xHourPackageApplied = true;
			
//			morningMins = getMins(from, to, morningStart, noon);
//			from = getBeginning(from, to, noon);
//			afternoonMins = getMins(from, to, noon, eveningStart);
//			from = getBeginning(from, to, eveningStart);
//			eveningMins = getMins(from, to, eveningStart, eveningEnd);
//			from = getBeginning(from, to, eveningEnd);
//			nightMins = getMins(from, to, eveningEnd, midnight);
			
		} 
		
		//Fullday package
		if(timeFrom == null || timeTo == null) {
//			final long days = ChronoUnit.DAYS.between(fromDate, toDate);
			if(isWeekend) {
				return dayPrice(service, true, price);
			} else { //Weekday
				return dayPrice(service, false, price);
			}
		} else if(xHourPackageApplied) { //Timediff is same as xHourPackage
			if(isWeekend)
				return xHourPrice(service, true, price);
			else
				return xHourPrice(service, false, price);
		} else { //Hourly Package
			return hourlyPrice(service, room.getFlexiPrice(), from, to, day);
		}
	}
	
	//Returns time in mins before cutoff 
	Integer getMins(LocalTime from, LocalTime to, final LocalTime windowStart,
			LocalTime windowEnd) {
		int mins = 0;
		if(from.isBefore(windowEnd) && (from.isAfter(windowStart) || from.isEqual(windowStart))) {
			if(to.isAfter(windowEnd)) {
				mins = Minutes.minutesBetween(from, windowEnd).getMinutes();
			} else
				mins = Minutes.minutesBetween(from, to).getMinutes();
		}
		return mins;
	}
	
	//Returns new to if range is spanning over to next bucket
	LocalTime getBeginning(LocalTime from, LocalTime to, LocalTime cutoff) {
		if(from.isBefore(cutoff)) {
			if(to.isAfter(cutoff)) 
				from = cutoff;
		}
		return from;
	}
	
	private Integer hourlyPrice(final Integer service, final Set<JamRoomPriceFlexi> flexi,
			final LocalTime timeFrom, final LocalTime timeTo, final DayOfWeek day) {
		Integer priceSum = 0;
		for(JamRoomPriceFlexi flex : flexi) {
			if(flex.getServiceType().equals(service)) {
				double hourMultiplier = 0;
				Minutes mins = Days.isBetween(flex.getDayOfWeekStart(), flex.getDayOfWeekEnd(),
						flex.getTimeStart(), flex.getTimeEnd(),
						timeFrom, timeTo, day.name());
				if(mins != null) {
					hourMultiplier =  mins.getMinutes()/60;
					if(mins.getMinutes() % 60 != 0)
						hourMultiplier = hourMultiplier + 0.5;
				}
				priceSum = (int) (priceSum + (hourMultiplier * flex.getPrice()));
			}
		}
		return priceSum;
	}
	
	private Integer dayPrice(final Integer serviceType, final Boolean isWeekend,
			final JamRoomPrice price) {
		if(isWeekend) {
			if(HostService.JUST_JAM.getValue().equals(serviceType))
				return price.getJammingFullWeekend();
			else if(HostService.JAMMING_WITH_RECORDING.getValue().equals(serviceType))
				return price.getRecordingFullWeekend();
			else if(HostService.RECORDING_MIXING_MASTERING.getValue().equals(serviceType))
				return price.getMasteringFullWeekend();
			else if(HostService.VOICEOVER_RECORDING.getValue().equals(serviceType)) 
				return price.getVoiceoverFullWeekend();
			else
				return 0;
		} else {
			if(HostService.JUST_JAM.getValue().equals(serviceType))
				return price.getJammingFullWeekday();
			else if(HostService.JAMMING_WITH_RECORDING.getValue().equals(serviceType))
				return price.getRecordingFullWeekday();
			else if(HostService.RECORDING_MIXING_MASTERING.getValue().equals(serviceType))
				return price.getMasteringFullWeekday();
			else if(HostService.VOICEOVER_RECORDING.getValue().equals(serviceType)) 
				return price.getVoiceoverFullWeekday();
			else 
				return 0;
		}
		
	}
	
	private Integer xHourPrice(final Integer serviceType, final Boolean isWeekend,
			final JamRoomPrice price) {
		if(isWeekend) {
			if(HostService.JUST_JAM.getValue().equals(serviceType))
				return price.getJammingXHourWeekend();
			else if(HostService.JAMMING_WITH_RECORDING.getValue().equals(serviceType))
				return price.getRecordingXHourWeekend();
			else if(HostService.RECORDING_MIXING_MASTERING.getValue().equals(serviceType))
				return price.getMasteringXHourWeekend();
			else if(HostService.VOICEOVER_RECORDING.getValue().equals(serviceType)) 
				return price.getVoiceoverXHourWeekend();
			else
				return 0;
		} else {
			if(HostService.JUST_JAM.getValue().equals(serviceType))
				return price.getJammingXHourWeekday();
			else if(HostService.JAMMING_WITH_RECORDING.getValue().equals(serviceType))
				return price.getRecordingXHourWeekday();
			else if(HostService.RECORDING_MIXING_MASTERING.getValue().equals(serviceType))
				return price.getMasteringXHourWeekday();
			else if(HostService.VOICEOVER_RECORDING.getValue().equals(serviceType)) 
				return price.getVoiceoverXHourWeekday();
			
			else
				return 0;
		}
		
	}
	
	@Transactional
	public CommandProcessingResult submitQuotationRequest(final Long roomId, final JsonCommand quotationRequest) {
		final JamRoom room = this.roomRepository.findOne(roomId);
		final Quote quote = this.hostAssembler.assembleQuote(room, quotationRequest.parsedJson());
		this.quoteRepository.save(quote);
		final List<DocumentCommand> songs = quote.getSongSamples();
		for(DocumentCommand song : songs) {
			song.setParentEntityId(quote.getId());
			this.documentService.createDocument(song, song.getInputStream());
		}
		return CommandProcessingResult.fromDetails(quote.getId(), null);
	}
	
	public List<BlockedData> retrieveAvailability(final Long roomId,  
			final String date, final Boolean isPartner) {
		LocalDate dateConv = LocalDate
				.parse(date, DateTimeFormatter.ofPattern("ddMMMyyyy"));
		final String convDate = dateConv.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		List<BlockedData> unavailableSlots = null;
		if(!isPartner) {
			final AvailabilityMapper rm = new AvailabilityMapper(jdbcTemplate);
			final String sql = "select " + rm.schema() + " and roomId = ? and date(timeFrom)=?";
			unavailableSlots = this.jdbcTemplate
					.query(sql, rm, new Object[] { roomId, convDate});
		} else {
			final PartnerAvailabilityMapper pm = new PartnerAvailabilityMapper(jdbcTemplate);
			final String sql = "select " + pm.schema() + " and roomId = ? and date(timeFrom)=?";
			unavailableSlots = this.jdbcTemplate
					.query(sql, pm, new Object[] { roomId, convDate});
		}
		
		
		final JamRoom room = this.roomRepository.findOne(roomId);
		final JamHost host = room.getHost();
		final Integer openHours = host.getOpenHours();
		final Integer openMins = host.getOpenMins();
		final Integer closeHours = host.getCloseHours();
		final Integer closeMins = host.getCloseMins();
		
		final String closingTime = StringUtil.formatTime(closeHours, closeMins, false);
		for(BlockedData block : unavailableSlots) {
			if(block.getTimeTo() == null) {
				block.setTimeTo(closingTime);
			}
		}
		
		
		
		blockElapsedTimeToday(unavailableSlots, room, openHours, openMins,
				convDate, closeHours, closeMins);
		
		
		addNonWorkingHoursBlocked(unavailableSlots,
				openHours, openMins, closeHours, closeMins); 
		addBlockedHours(room, unavailableSlots, convDate, dateConv);
		if(!isPartner)
		blockMinimumResponseTime(unavailableSlots, dateConv);
		return unavailableSlots;
	}
	
	public void blockElapsedTimeToday(final List <BlockedData> unavailableSlots,
			 final JamRoom room, final Integer openHours, final Integer openMins,
			 final String orderDate, final Integer closeHours, final Integer closeMins){
	    final String openTimeFrom = StringUtil.formatTime(openHours, openMins, false);
	    LocalDate dateConv = LocalDate
				.parse(orderDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		if(dateConv.equals(LocalDate.now())) {
			final String blockEnd = getTimeElapsedTodayEnd();
			unavailableSlots.add(BlockedData.instance(openTimeFrom, blockEnd, null));
		}
	}
	
		
	
	private String getTimeElapsedTodayEnd() {
		final LocalTime now = LocalTime.now();
		//For bookings now, blocked slots will be 30 minutes in the past. 
		final org.joda.time.format.DateTimeFormatter format = DateTimeFormat.forPattern("HH:mm");
		final org.joda.time.format.DateTimeFormatter format2 = DateTimeFormat.forPattern("HH:");
		final Integer mins = now.getMinuteOfHour();
		String blockEnd = "";
		if(mins == 0 || mins == 30) {
			blockEnd = format.print(now);
		} else if (mins > 30 && mins < 60) {
			blockEnd = format2.print(now) + "30";
		} else if (mins > 0 && mins < 30) {
			blockEnd = format2.print(now) + "00";
		} 
		return blockEnd;
	
	}
	
	private void addBlockedHours(final JamRoom room, final List<BlockedData> unavailableSlots,
			final String convDate, final LocalDate checkDate) {
		
		final List<Block> blocks = this.blockRepository.findByRoomAndDate(room.getId(), convDate);
		for(Block block : blocks) {
			unavailableSlots.add(BlockedData.instance(block.getStart(),block.getEnd(), block.getId()));
		}
		
	}
	
	

	//Minimum Response Time Block
	private void blockMinimumResponseTime(final List<BlockedData> unavailableSlots, final LocalDate checkDate) {
			if(checkDate.equals(LocalDate.now())) {
				LocalTime now = new LocalTime();
				final String blockStart = getBlockStart(now);
				final String blockEnd = getBlockEnd(now);
				unavailableSlots.add(BlockedData.instance(blockStart, blockEnd, null));
			}
	}
	private String getBlockStart(final LocalTime now) {
		//For 2:31, it will be 2:30 (floor). For 3:00, it will be 3:00 (same) For 2:01, it will be 2:00.
		final org.joda.time.format.DateTimeFormatter format = DateTimeFormat.forPattern("HH:mm");
		final org.joda.time.format.DateTimeFormatter format2 = DateTimeFormat.forPattern("HH:");
		final Integer mins = now.getMinuteOfHour();
		String blockStart = "";
		if(mins == 0 || mins == 30) {
			blockStart = format.print(now);
		} else if (mins > 0 && mins < 30) {
			blockStart = format2.print(now) + "00";
		} else if (mins > 30 && mins < 60) {
			blockStart = format2.print(now) + "30";
		}
			
		return blockStart;
	}
	
	private String getBlockEnd(final LocalTime now) {
		//For 2:31, it will be 4:00. For 2:01, it will be 3:30. For 3:00, it will be 4:00. For 2:30, it will be 3:30.
		final org.joda.time.format.DateTimeFormatter format = DateTimeFormat.forPattern("HH:mm");
		final org.joda.time.format.DateTimeFormatter format2 = DateTimeFormat.forPattern("HH:");
		final Integer mins = now.getMinuteOfHour();
		String blockEnd = "";
		if(mins == 0 || mins == 30) {
			blockEnd = format.print(now.plusMinutes(60));
		} else if (mins > 0 && mins < 30) {
			blockEnd = format2.print(now.plusMinutes(60)) + "30";
		} else if (mins > 30 && mins < 60) {
			blockEnd = format2.print(now.plusMinutes(120)) + "00";
		}
			
		return blockEnd;
	}
	
	private void addNonWorkingHoursBlocked(final List<BlockedData> unavailableSlots,
			final Integer openHours, final Integer openMins, 
			final Integer closeHours, final Integer closeMins) {
		final String openTimeFrom = "00:00";
		final String openTimeTo = StringUtil.formatTime(openHours, openMins, true);
		unavailableSlots.add(BlockedData.instance(openTimeFrom, openTimeTo, null));
		
		final String timeTo = "24:00";
		final String timeFrom = StringUtil.formatTime(closeHours, closeMins, false);
		unavailableSlots.add(BlockedData.instance(timeFrom, timeTo, null));
	}
	
	
	private static final class JamHostDataMapper implements RowMapper<JamHostData> {
    	
	    	private final JdbcTemplate jdbcTemplate;
	    	public JamHostDataMapper(final JdbcTemplate jdbcTemplate) {
				this.jdbcTemplate = jdbcTemplate;
		}
	
	    public String landingSchema() {
		    	final StringBuilder sqlBuilder = new StringBuilder();
	    		sqlBuilder.append(" r.id as roomId, h.`name` as studioName, r.name as roomName, h.area as area,") 
	        		.append(" r.`overallAverage` as overallAvgPrice, r.avgHourly as avgHourly, ") 
					.append(" r.avgRecordingHourly as avgRecordingHourly, r.avgMixingMasteringHourly as avgMixingMasteringHourly,")
					.append(" r.`hasJamming` as hasJamming ,r.`hasRecording` as hasRecording, r.`hasMasterRecording` as")
					.append(" hasMixingAndMastering, null as avgRating, r.`primaryImageId` as primaryImageId ") 
				.append(" from jamhost h inner join room r on h.id=r.`hostId` ");
	    		return sqlBuilder.toString();
	    		
	    }
	
	    @Override
	    public JamHostData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	    		final Long roomId = rs.getLong("roomId");
	        final String studioName = rs.getString("studioName");
	        final String roomName = rs.getString("roomName");
	        final String area = rs.getString("area");
	        final Integer overallAverage = rs.getInt("overallAvgPrice");
	        final Integer avgHourly = rs.getInt("avgHourly");
	        final Integer avgRecordingHourly = rs.getInt("avgRecordingHourly");
	        final Integer avgMixingMasteringHourly = rs.getInt("avgMixingMasteringHourly");
	        final Boolean hasJamming = rs.getBoolean("hasJamming");
	        final Boolean hasRecording = rs.getBoolean("hasRecording");
	        final Boolean hasMixingAndMastering = rs.getBoolean("hasMixingAndMastering");
	        final Double avgRating = rs.getDouble("avgRating");
	        final Long primaryImageId = rs.getLong("primaryImageId");
	        
	        
	        return JamHostData.instance(roomId, studioName, roomName, area, overallAverage,
	        		avgHourly, avgRecordingHourly, avgMixingMasteringHourly,
	        		hasJamming, hasRecording, hasMixingAndMastering, avgRating, primaryImageId);
	    }
	}
	
	
	private static final class JamroomDataMapper implements RowMapper<JamroomData> {
    	
    	private final JdbcTemplate jdbcTemplate;
    	public JamroomDataMapper(final JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
	}

    public String roomSchema() {
    	final StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("r.id as roomId, h.name as studioName, null as avgRating,")
		.append(" h.imageId as profilePic, r.xHourPackage as xHourPackage, h.houseNo as houseNo, h.street as street,")
		.append(" h.landmark as landmark, h.area as area, h.openHours as openingHours, h.openMins as openingMins,")
		.append(" h.closeHours as closingHours, h.closeMins as closingMins, r.`hasJamming` as hasJamming,")
		.append(" r.hasRecording as hasRecording, r.hasMasterRecording as hasMixingAndMastering,")
		.append(" r.minCapacity as minCapacity, r.maxCapacity as maxCapacity, r.soundProofed as")
		.append(" hasSoundProofing, r.drumkitBrand as drumkitBrand, r.drumkitPieces as drumkitPieces,")
		.append(" r.noOfStools as noOfStools, r.speakerBrand as speakerBrand, r.noOfAmplifiers as noOfAmplifiers, r.amplifierBrands as amplifierBrands,")
		.append(" r.noOfMics as totalMics, r.otherItems as otherItems, 1 as totalMicsWithCondenser, r.`mixerBoardInputs`")
		.append(" as mixerBoardInputs, h.fbPage as facebookPage, h.website as website, h.latitude as latitude,")
		.append(" h.longitude as longitude, 1 as totalWirelessMics, 1 as totalInstrumentalMics, 2 as totalInstrumentalMics,")
		.append(" 'INR' as currency, r.avgHourly as avgHourly, r.avgRecordingHourly as avgRecordingHourly,")
		.append("  r.avgMixingMasteringHourly as avgMixingMasteringHourly,")
		.append("rp.minHourly as minHourly, rp.maxHourly as maxHourly, r.`avgHourly` as avgHourly,")
		.append("rp.avgXHourPackage as avgXHourPackage, rp.fullDay as fullDay,")
		.append("rp.recordingMinHourly as recordingMinHourly, rp.recordingMaxHourly as recordingMaxHourly,")
		.append("r.`avgRecordingHourly` as avgRecordingHourly, rp.recordingAvgXHourPackage as recordingAvgXHourPackage,")
		.append("rp.recordingFullDay as recordingFullDay, rp.voiceoverMinHourly as voiceoverMinHourly,")
		.append("rp.voiceoverMaxHourly as voiceoverMaxHourly, r.`avgVoiceoverHourly` as avgVoiceoverHourly,")
		.append("rp.voiceoverAvgXHourPackage as voiceoverAvgXHourPackage, rp.voiceoverFullDay as voiceoverFullDay, ")
		.append("rp.mixingMasteringMinHourly as mixingMasteringMinHourly, rp.mixingMasteringMaxHourly as mixingMasteringMaxHourly, ")
		.append("r.`avgMixingMasteringHourly` as avgMixingMasteringHourly, rp.mixingMasteringAvgXHourPackage as mixingMasteringAvgXHourPackage, ")
		.append("rp.mixingMasteringFullDay as mixingMasteringFullDay, r.overallAverage as overallAverage ")
		.append("from room r inner join room_price rp on r.id = rp.roomId inner join jamhost h on r.hostId = h.id");
		
		return sqlBuilder.toString();
    }

    @Override
    public JamroomData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
    	
	    	final Long roomId = rs.getLong("roomId");
	    	final String studioName = rs.getString("studioName");
	    	final Double avgRating = rs.getDouble("avgRating");
	    	final Long profilePic = rs.getLong("profilePic");
	    	final String houseNo = rs.getString("houseNo");
	    	final String street = rs.getString("street");
	    	final String area = rs.getString("area");
	    	final String landmark = rs.getString("landmark");
	    	final Integer openingHours = rs.getInt("openingHours");
	    	final Integer openingMins = rs.getInt("openingMins");
	    	final String openHours = StringUtil.formatTime(openingHours, openingMins, true);
	    	final Integer closingHours = rs.getInt("closingHours");
	    	final Integer closingMins = rs.getInt("closingMins");
	    	final String closeHours = StringUtil.formatTime(closingHours, closingMins, false);
	    	final Boolean hasJamming = rs.getBoolean("hasJamming");
	    	final Boolean hasRecording = rs.getBoolean("hasRecording");
	    	final Boolean hasMixingAndMastering = rs.getBoolean("hasMixingAndMastering");
	    	final Integer minCapacity = rs.getInt("minCapacity");
	    	final Integer maxCapacity = rs.getInt("maxCapacity");
	    	final Boolean hasSoundproofing = rs.getBoolean("hasSoundproofing");
	    	final String drumkitBrand = rs.getString("drumkitBrand");
	    	final Integer drumkitPieces = rs.getInt("drumkitPieces");
	    	final Integer noOfAmplifiers = rs.getInt("noOfAmplifiers");
	    	final Integer totalMics = rs.getInt("totalMics");
	    	final Integer noOfStools = rs.getInt("noOfStools");
	    	final String speakerBrand = rs.getString("speakerBrand");
	    	final Integer totalMicsWithCondenser = rs.getInt("totalMicsWithCondenser");
	    	final Integer totalWirelessMics = rs.getInt("totalWirelessMics");
	    	final Integer totalInstrumentalMics = rs.getInt("totalInstrumentalMics");
	    	final Integer mixerBoardInputs = rs.getInt("mixerBoardInputs");
	    	final String facebookPage = rs.getString("facebookPage");
	    	final String website = rs.getString("website");
	    	final String latitude = rs.getString("latitude");
	    	final String longitude = rs.getString("longitude");
	    	
	    	final String otherItem = rs.getString("otherItems");
	    List<String> otherItems = new ArrayList<>();
	    if(!StringUtils.isEmpty(otherItem)) {
	    		final String split[] = otherItem.split("\\|");
	    		Collections.addAll(otherItems, split);
	    }
	    final String amplifierBrand = rs.getString("amplifierBrands");
	    	List<String> amplifierBrands = new ArrayList<>();
	    	if(!StringUtils.isEmpty(amplifierBrand)) {
	    		final String split[] = amplifierBrand.split("\\|");
	    		Collections.addAll(amplifierBrands, split);
	    }

	    	//PriceData
	    	final String currency = rs.getString("currency");
	    	final Integer minHourly = rs.getInt("minHourly");
	    	final Integer maxHourly = rs.getInt("maxHourly");
	    	final Integer avgHourly = rs.getInt("avgHourly");
	    	final Integer avgXHourPackage = rs.getInt("avgXHourPackage");
	    	final Integer fullDay = rs.getInt("fullDay");
	    	final Integer recordingMinHourly = rs.getInt("recordingMinHourly");
	    	final Integer recordingMaxHourly = rs.getInt("recordingMaxHourly");
	    	final Integer avgRecordingHourly = rs.getInt("avgRecordingHourly");
	    	final Integer recordingAvgXHourPackage = rs.getInt("recordingAvgXHourPackage"); 
	    	final Integer recordingFullDay = rs.getInt("recordingFullDay"); 
	    	final Integer voiceoverMinHourly = rs.getInt("voiceoverMinHourly");
	    	final Integer voiceoverMaxHourly = rs.getInt("voiceoverMaxHourly");
	    	final Integer avgVoiceoverHourly = rs.getInt("avgVoiceoverHourly");
	    	final Integer voiceoverAvgXHourPackage = rs.getInt("voiceoverAvgXHourPackage");
	    	final Integer voiceoverFullDay = rs.getInt("voiceoverFullDay");
	    	final Integer mixingMasteringMinHourly = rs.getInt("mixingMasteringMinHourly");
	    	final Integer mixingMasteringMaxHourly = rs.getInt("mixingMasteringMaxHourly");
	    	final Integer avgMixingMasteringHourly = rs.getInt("avgMixingMasteringHourly");
	    	final Integer mixingMasteringAvgXHourPackage = rs.getInt("mixingMasteringAvgXHourPackage");
	    	final Integer mixingMasteringFullDay = rs.getInt("mixingMasteringFullDay");
	    	final Integer overallAvg = rs.getInt("overallAverage");
	    	final Integer xHourPackage = rs.getInt("xHourPackage");
	    	final List<Integer> availableServices = new ArrayList<> ();
	    	if(hasJamming)
	    		availableServices.add(HostService.JUST_JAM.getValue());
	    	if(hasRecording)
	    		availableServices.add(HostService.JAMMING_WITH_RECORDING.getValue());
	    	if(hasMixingAndMastering) {
	    		availableServices.add(HostService.RECORDING_MIXING_MASTERING.getValue());
	    		availableServices.add(HostService.VOICEOVER_RECORDING.getValue());
	    	}
	    	
        final PriceData priceData = PriceData.instance(currency, minHourly, maxHourly, avgHourly, avgXHourPackage, fullDay, recordingMinHourly,
        		recordingMaxHourly, avgRecordingHourly, recordingAvgXHourPackage, recordingFullDay, voiceoverMinHourly, voiceoverMaxHourly,
        		avgVoiceoverHourly, voiceoverAvgXHourPackage, voiceoverFullDay, mixingMasteringMinHourly, mixingMasteringMaxHourly,
        		avgMixingMasteringHourly, mixingMasteringAvgXHourPackage, mixingMasteringFullDay, overallAvg);
        
        
        return JamroomData.instance(roomId, null,  studioName, avgRating,  profilePic,  houseNo,
				 street,  area,  landmark, openHours,  closeHours,
				 hasJamming,  hasRecording, hasMixingAndMastering,  minCapacity,
				 maxCapacity,  hasSoundproofing, drumkitBrand,  drumkitPieces,
				 amplifierBrands,  noOfAmplifiers, totalMics,  totalMicsWithCondenser,
				 totalWirelessMics,  totalInstrumentalMics, mixerBoardInputs,  otherItems,
				 facebookPage,  website,  latitude, longitude, priceData, null,
				 xHourPackage, availableServices, noOfStools, speakerBrand);
    }
	}
	
	private static final class AvailabilityMapper implements RowMapper<BlockedData> {
    	
	    	private final JdbcTemplate jdbcTemplate;
	    	public AvailabilityMapper(final JdbcTemplate jdbcTemplate) {
				this.jdbcTemplate = jdbcTemplate;
		}
	
	    public String schema() {
		    	final StringBuilder sqlBuilder = new StringBuilder();
	    		sqlBuilder.append(" TIME_FORMAT(TIME(timeFrom), '%H:%i') as timeFrom, TIME_FORMAT(TIME(timeTo), '%H:%i') as timeTo " + 
	    				"from `order` " + 
	    				"where orderStatus in (200,300,400) ");
	    		return sqlBuilder.toString();
	    		
	    }
	
	    @Override
	    public BlockedData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	    		final String timeFrom = rs.getString("timeFrom");
	        final String timeTo = rs.getString("timeTo");
	        
	        
	        
	        return BlockedData.instance(timeFrom, timeTo, null);
	    }
	}
	
	private static final class PartnerAvailabilityMapper implements RowMapper<BlockedData> {
    	
	    	private final JdbcTemplate jdbcTemplate;
	    	
	    	public PartnerAvailabilityMapper(final JdbcTemplate jdbcTemplate) {
				this.jdbcTemplate = jdbcTemplate;
		}
	
	    public String schema() {
		    	final StringBuilder sqlBuilder = new StringBuilder();
	    		sqlBuilder.append(" TIME_FORMAT(TIME(o.timeFrom), '%H:%i') as timeFrom, TIME_FORMAT(TIME(o.timeTo), '%H:%i') as timeTo, "); 
	    		sqlBuilder.append(" o.id as orderId, o.orderStatus as orderStatus, u.name as name, u.mobileNo as mobile "); 
	    		sqlBuilder.append(" from `order` o inner join appuser u on o.createdById=u.id where o.orderStatus in (200,300,400) ");
	    		return sqlBuilder.toString();
	    		
	    }
	
	    @Override
	    public BlockedData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	    	final String timeFrom = rs.getString("timeFrom");
	        final String timeTo = rs.getString("timeTo");
	        final Long orderId = rs.getLong("orderId");
	        final String name = rs.getString("name");
	        final String mobileNo = rs.getString("mobile");
	        final String orderStatus = OrderStatus.fromInt(rs.getInt("orderStatus"));
	        
	        
	        
	        return BlockedData.instance(timeFrom, timeTo, name, orderStatus,
	        		mobileNo, orderId);
	    }
	}
	
private static final class HostMapper implements RowMapper<HostData> {
    	
    	private final JdbcTemplate jdbcTemplate;
    	public HostMapper(final JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
	}

    public String schema() {
	    	final StringBuilder sqlBuilder = new StringBuilder();
	    	sqlBuilder.append(" h.name, h.latitude, h.longitude, h.houseNo, h.street, h.landmark, h.area, r.`name` as city, h.pinCode, h.ownerName, ") 
		.append(" org.label as orgType, h.email, h.contact, h.website, h.fbPage, h.openHours, h.openMins, h.closeHours, h.closeMins, ") 
		.append(" h.startTimeMultiples, h.slotDurationMultiples, h.accountHolderName, h.accountNo, h.ifscCode, account.label as accountType ") 
		.append(" from jamhost h inner join region r on h.cityId=r.id ")
		.append(" inner join code_value org on org.id=h.orgTypeId ")
		.append(" inner join code_value account on account.id=h.accountTypeId ");
    		return sqlBuilder.toString();
    		
    }

    @Override
    public HostData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
	    	final String name = rs.getString("name");
	    	final String latitude = rs.getString("latitude");
	    	final String longitude = rs.getString("longitude");
	    	final String houseNo = rs.getString("houseNo");
	    	final String street = rs.getString("street");
	    	final String landmark = rs.getString("landmark");
	    	final String area = rs.getString("area"); 
	    	final String city = rs.getString("city");
	    	final String pinCode = rs.getString("pinCode");
	    	final String ownerName = rs.getString("ownerName");
	    	final String orgType = rs.getString("orgType");
	    	final String email = rs.getString("email");
	    	final String contact = rs.getString("contact");
	    	final String website = rs.getString("website");
	    	final String fbPage = rs.getString("fbPage"); 
	    	final Integer openHours = rs.getInt("openHours");
	    	final Integer openMins = rs.getInt("openMins");
	    	final Integer closeHours = rs.getInt("closeHours");
	    	final Integer closeMins = rs.getInt("closeMins");
	    	final Integer startTimeMultiples = rs.getInt("startTimeMultiples");
	    	final Integer slotDurationMultiples = rs.getInt("slotDurationMultiples");
	    	final String accountHolderName = rs.getString("accountHolderName");
	    	final String accountNo = rs.getString("accountNo");
	    	final String ifscCode = rs.getString("ifscCode");
	    	final String accountType = rs.getString("accountType");
        
        
        
        return HostData.instance(name, latitude, longitude, houseNo, street, landmark,
        		area, city, pinCode, ownerName, orgType, email, contact, website,
        		fbPage, openHours, openMins, closeHours, closeMins, startTimeMultiples,
        		slotDurationMultiples, accountHolderName, accountNo, ifscCode, accountType);
    }
	}

	private static final class MicMapper implements RowMapper<MicData> {
		
		private final JdbcTemplate jdbcTemplate;
		public MicMapper(final JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
	}
	
	public String schema() {
	    	final StringBuilder sqlBuilder = new StringBuilder();
	    	sqlBuilder.append(" m.condenser as condenser, m.wireless as wireless, m.instrument as instrument") 
	    			  .append(" from mic m ");
		return sqlBuilder.toString();
	}
	
	@Override
	public MicData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
		final Boolean hasCondenser = rs.getBoolean("condenser");
		final Boolean isInstrumental = rs.getBoolean("instrument");
		final Boolean isWireless = rs.getBoolean("wireless");
		
		return MicData.instance(hasCondenser, isWireless, isInstrumental);
	}
	}

	
	
}
