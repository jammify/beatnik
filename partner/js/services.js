'use strict';

angular.module('partner.services', []).
  value('version', '0.1')
.factory('loader', ['$rootScope', function($rootScope) { 
	$rootScope.blockUI = false;
	$rootScope.requests = 0;
    var loader = {
            request: function (config) {
                $rootScope.blockUI = true;
                $rootScope.requests++;
            		return config;
            },
            requestError: function (rejection) {
            		$rootScope.requests--;
                if($rootScope.requests == 0)
                		$rootScope.blockUI = false;
                	return rejection;
            },
            response: function (response) {
	            $rootScope.requests--;
	            if($rootScope.requests == 0)
	            		$rootScope.blockUI = false;
	            	return response;
            },
            responseError: function (rejection) {
            		$rootScope.requests--;
                if($rootScope.requests == 0)
                		$rootScope.blockUI = false;
                	return rejection;
            }
        }
        return loader;
}])
.config(['$httpProvider', function($httpProvider) {  
    $httpProvider.interceptors.push('loader');
}]);
