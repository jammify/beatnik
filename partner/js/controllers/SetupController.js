angular.module('partner.controllers').controller('SetupController', ['$scope', '$rootScope', 'Restangular', '$location', function($scope, $rootScope, Restangular, $location) {
	
	Restangular.one('hosts', $location.path().split('/')[2]).get().then(function(host){
			$scope.host = host;
			$scope.rooms = host.roomIds;
	});
	
	$scope.form = {};
	$scope.form.data = {};
	$scope.onTabSelected = function(roomId) {
		Restangular.one('hosts/' + $location.path().split('/')[2] + '/rooms/' + roomId).get().then(function(room){
			$scope.form.data = room;
			console.log($scope.form.data);
		});
	}
	
	$scope.addRoom = function() {
		$location.path("createroom/" + $location.path().split('/')[2]);
	};
	

}]);