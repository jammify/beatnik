angular.module('partner.controllers').controller('CreateHostController', ['$scope', '$rootScope', 'Upload', 'Restangular', 'NgMap', '$location', function($scope, $rootScope, Upload, Restangular, NgMap, $location) {
	  
	  $scope.selected = [];
	  $scope.formData = {};
	  $scope.gapOptions = [30,60];
	  $scope.mgapOptions = [15,30,60];
	  
	  Restangular.all('codes').getList({name:'orgType'})  
    	.then(function(orgs) {
    		$scope.orgTypes = orgs[0].codeValues; 
    });
	  
	  Restangular.all('codes').getList({name:'accountType'})  
  		.then(function(accounts) {
  		$scope.accountTypes = accounts[0].codeValues; 
  		});
	  
	  $scope.setAccountHolderName = function() {
		  $scope.formData.accountHolderName = $scope.formData.ownerName;
	  }
	  
	  $scope.hourOptions = [];
	  for(var i = 0; i < 24; i++) {
		  $scope.hourOptions.push(i);
	  }
	  $scope.minOptions = [];
	  for(var i = 0; i < 60; i=i+15) {
		  $scope.minOptions.push(i);
	  }
		    
		var vm = this;
    NgMap.getMap().then(map => vm.map = map);
    vm.positions = [];
    vm.addMarker = function(event) {
        var ll = event.latLng;
        vm.positions = [{lat:ll.lat(), lng: ll.lng()}];
        $scope.formData.latitude = ll.lat();
        $scope.formData.longitude = ll.lng();
      }
    
    Restangular.all('banks').getList()  
    .then(function(banks) {
      $scope.bankOptions = banks; 
    })
    
    $scope.fetchBranches = function() {
  	  	if($scope.formData.bankName) {
  	  		$scope.branchOptions = [];
  	  		$scope.formData.ifscCode = "";
  	  		Restangular.one('banks', $scope.formData.bankName).getList().then(function(branches){
  	  			$scope.branchOptions = branches;
  	  			$scope.branchName = null;
  	  		})
  	  	}
    };
    
    $scope.fetchBranch = function() {
  	  	if($scope.formData.ifscCode.length > 6) {
  	  		Restangular.one('banks/ifsc', $scope.formData.ifscCode).get().then(function(branch){
  	  			$scope.formData.bankName = branch.bankName;
  	  			$scope.branchName = branch.branchName;
  	  		})
  	  	}
    };
    
    Restangular.all('regions').getList({levelId:'2'})  
    .then(function(states) {
      $scope.states = states; 
    })
    
    $scope.fetchCities = function() {
  	  	$scope.cityOptions = [];
  	  	if($scope.stateId) {
  	  		Restangular.all('regions').getList({parentId: $scope.stateId}).then(function(cityOptions){
  	  			$scope.cityOptions = cityOptions;
  	  		});
  	  	}
    };
    
    $scope.formData.slabs = [{"start":0, "end":4,"percent":0}];
	  $scope.addSlab = function() {
		  var len = undefined;
		  if($scope.formData.slabs.length)
			  len = $scope.formData.slabs.length;
		  var begin = undefined;
		  if(len)
			  begin = $scope.formData.slabs[len - 1].end;
		  $scope.formData.slabs.push({"start": begin,"end": begin,"percent":0})
	  };
	  
	  $scope.removeSlab = function(index) {
		  $scope.formData.slabs.splice(index, 1);
	  };
	  
	 
	  $scope.submit = function() {
		  $scope.formData.userId = $location.path().split('/')[2];
		  Restangular.all('hosts').post($scope.formData).then(function(response){
			  $location.path('setup/' + response.resourceId);
		  });
	    };

	  
	  
	  
	   
}]);