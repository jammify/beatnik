angular.module('partner.controllers').controller('DashboardController', ['$scope', '$compile', '$rootScope', 'Restangular', '$location', function($scope, $compile, $rootScope, Restangular, $location) {
	
	$scope.something = function(arr, result) {
		if(arr.values.length > 0) {
			var weeks = [1, 2, 3, 4];
			for(var j=0; j<arr.values.length; j++) {
				var index = weeks.indexOf(arr.values[j][0]);
				if(index != -1) 
					weeks.splice(index, 1);
			}
			for(var k=0; k<weeks.length; k++) {
				arr.values.push([weeks[k], 0]);
			}
			result.push(arr);
		}
	};
	
	Restangular.all('reports/TotalHoursByOpenHours/execute').post([{"paramName":"studioId", "paramValue": $rootScope.hostId}])  
    .then(function(resp) {
    		$scope.totalHoursData  = resp;
    		$scope.hoursData = [];
    		
    		var justJam = {"key": "Just Jam", "values":[]};
    		var recording = {"key": "Plain Recording", "values":[]};
    		var mixing = {"key": "Recording with Mixing and Mastering", "values":[]};
    		var voiceover = {"key": "Voiceover Recording", "values":[]};
    		for(var i=0; i<$scope.totalHoursData.length; i++) {
    			if($scope.totalHoursData[i].service == "Just Jam")
    				justJam.values.push([parseInt($scope.totalHoursData[i].weekNo), parseInt($scope.totalHoursData[i].percentOfWeek)]);
    			else if($scope.totalHoursData[i].service == "Recording with Mixing and Mastering")
    				mixing.values.push([parseInt($scope.totalHoursData[i].weekNo), parseInt($scope.totalHoursData[i].percentOfWeek)]);
    			else if($scope.totalHoursData[i].service == "Voice Over Recording")
    				voiceover.values.push([parseInt($scope.totalHoursData[i].weekNo), parseInt($scope.totalHoursData[i].percentOfWeek)]);
    			else if($scope.totalHoursData[i].service == "Jamming with Recording")
    				recording.values.push([parseInt($scope.totalHoursData[i].weekNo), parseInt($scope.totalHoursData[i].percentOfWeek)]);
    		}
    		$scope.something(justJam, $scope.hoursData);
    		$scope.something(recording, $scope.hoursData);
    		$scope.something(mixing, $scope.hoursData);
    		$scope.something(voiceover, $scope.hoursData);
    	
    		console.log($scope.hoursData);
    });
	
	Restangular.all('reports/TotalBookingsWithPercentage/execute').post([{"paramName":"studioId", "paramValue": $rootScope.hostId}])  
    .then(function(resp) {
    		$scope.totalBookingsData  = resp;  
    		$scope.bookingsData = [];
    		var justJam = {"key": "Just Jam", "values":[]};
    		var recording = {"key": "Plain Recording", "values":[]};
    		var mixing = {"key": "Recording with Mixing and Mastering", "values":[]};
    		var voiceover = {"key": "Voiceover Recording", "values":[]};
    		for(var i=0; i<$scope.totalHoursData.length; i++) {
    			if($scope.totalHoursData[i].service == "Just Jam")
    				justJam.values.push([parseInt($scope.totalBookingsData[i].weekNo), parseInt($scope.totalBookingsData[i].noOfOrders)]);
    			else if($scope.totalHoursData[i].service == "Recording with Mixing and Mastering")
    				mixing.values.push([parseInt($scope.totalBookingsData[i].weekNo), parseInt($scope.totalBookingsData[i].noOfOrders)]);
    			else if($scope.totalHoursData[i].service == "Voice Over Recording")
    				voiceover.values.push([parseInt($scope.totalBookingsData[i].weekNo), parseInt($scope.totalBookingsData[i].noOfOrders)]);
    			else if($scope.totalHoursData[i].service == "Jamming with Recording")
    				recording.values.push([parseInt($scope.totalBookingsData[i].weekNo), parseInt($scope.totalBookingsData[i].noOfOrders)]);
    		}
    		$scope.something(justJam, $scope.bookingsData);
    		$scope.something(recording, $scope.bookingsData);
    		$scope.something(mixing, $scope.bookingsData);
    		$scope.something(voiceover, $scope.bookingsData);
    	
    		console.log($scope.bookingsData);
		$scope.$apply();
    });
	
	Restangular.all('reports/BookingRevenue/execute').post([{"paramName":"studioId", "paramValue": $rootScope.hostId}])  
    .then(function(resp) {
    		$scope.totalRevenueData  = resp;  
    });
	
	Restangular.all('reports/CurrentWeekBookings/execute').post([{"paramName":"studioId", "paramValue": $rootScope.hostId}])  
    .then(function(resp) {
    		$scope.currentWeekData  = resp;
    });
	
	Restangular.all('reports/RatingsAndFeedback/execute').post([{"paramName":"studioId", "paramValue": $rootScope.hostId}])  
    .then(function(resp) {
      $scope.ratingsData = resp;
    });
	
	
	
	
	$scope.options = {
		    chart: {
		        type: 'multiBarChart',
		        height: 450,
		        margin : {
		            top: 70,
		            right: 70,
		            bottom: 70,
		            left: 70
		        },
		        x: function(d){ return d[0]; },
		        y: function(d){ return d[1]; },
                clipEdge: false,
                duration: 500,
                stacked: false,
                showControls: false,
                "xDomain": [4,3,2,1],
		        xAxis: {
		        		showMaxMin: false,
		            axisLabel: 'Past Week No.',
		            	tickFormat: function(d){
	                        return d3.format(',f')(d);
                    }
		        },
		        yAxis: {
		            axisLabel: '% of Hours',
		            tickFormat: function(d){
                        return d3.format(',f')(d);
		            }
		        },
		        barColor: function(d, i){
	                  var colors = d3.scale.category20().range();
	                  if(d["key"] == "Just Jam")
	                	  	return colors[2];
	                  else if(d["key"] == "Plain Recording")
	                	  	return colors[1];
	                  else if(d["key"] == "Recording with Mixing and Mastering")
	                	  	return colors[3];
	                  else 
	                	  	return colors[4];
                }
		    }
		};
	

	

}]);