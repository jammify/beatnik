angular.module('partner.controllers').controller('CreateRoomController', ['$scope', '$rootScope', 'Upload', 'Restangular', '$location', function($scope, $rootScope, Upload, Restangular, $location) {
	  
		$scope.formData = {otherItems:[], amplifierBrands:[], images:[]};
	  
		$scope.days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
		$scope.hours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
	  
	  $scope.changeMics = function(index) {
		  console.log(index);
		  if($scope.formData.noOfMics < 1)
			  return;
		  var micsLen = $scope.formData.mics.length;
		  var len = $scope.formData.noOfMics - micsLen;
		  for(var i=0; i< len; i++)
			  $scope.formData.mics.push({"hasCondenser": false, "isWireless": false, "forInstrument": false});
		  if(len < 0) {
			  for(var i=micsLen-1; i>= $scope.formData.noOfMics; i--) {
				  $scope.formData.mics.splice(i,1);
			  }
		  }
	  };
	  
	  $scope.hasJamming = function() {
		  if($scope.formData.hasJamming == true) {
			  $scope.formData.jamming = {};
			  $scope.formData.jamming.slabs = [{"dayOfWeekStart":$scope.days[0], "dayOfWeekEnd":$scope.days[3]}];
		  } else
			  $scope.formData.jamming.slabs = null;
	  };
	  
	  $scope.hasRecording = function() {
		  if($scope.formData.hasRecording == true) {
			  $scope.formData.recording = {};
			  $scope.formData.recording.slabs = [{"dayOfWeekStart":$scope.days[0], "dayOfWeekEnd":$scope.days[3]}];
		  } else
			  $scope.formData.recording.slabs = null;
	  };

	  $scope.hasMasterRecording = function() {
		  if($scope.formData.hasMasterRecording == true) {
			  $scope.formData.master = {};
			  $scope.formData.master.slabs = [{"dayOfWeekStart":$scope.days[0], "dayOfWeekEnd":$scope.days[3]}];
		  } else 
			  $scope.formData.master.slabs = null;
	  };
	  
	  $scope.hasVoiceoverRecording = function() {
		  if($scope.formData.hasVoiceover == true) {
			  $scope.formData.voiceover = {};
			  $scope.formData.voiceover.slabs = [{"dayOfWeekStart":$scope.days[0], "dayOfWeekEnd":$scope.days[3]}];
		  } else
			  $scope.formData.master.slabs = null;
			  
	  };
	  
	  
	  $scope.addSlab = function(slabs) {
		  var len = undefined;
		  if(slabs.length)
			  len = slabs.length;
		  var index =  $scope.days.indexOf(slabs[len - 1].dayOfWeekEnd);
		  slabs.push({"dayOfWeekStart": $scope.days[index+1],"dayOfWeekEnd": $scope.days[index+1]});
	  };
	  
	  $scope.removeSlab = function(slabs, index) {
		  slabs.splice(index, 1);
	  };
	  
	  
	  $scope.imageSelect = function(imageIndex) {
		  Upload.base64DataUrl($scope.formData.images[imageIndex]).then(function(urls){
			  $scope.formData.images[imageIndex] = urls;
		  });
	  };
	 
	  $scope.submit = function() {
		  Restangular.all('hosts/' +  $location.path().split('/')[2] + '/rooms').post($scope.formData).then(function(response){
			  if(response.resourceId) {
				  $location.path('setup/' + $location.path().split('/')[2]);
			  }
		  });
	    };
	   
}]);