angular.module('partner.controllers').controller('LoginController', ['$scope', '$route', '$rootScope', '$mdSidenav', 'Restangular', '$firebaseAuth', '$location', function($scope, $route, $rootScope, $mdSidenav, Restangular, $firebaseAuth, $location) {
	
	$rootScope.openSideNavPanel = function() {
        $mdSidenav('left').open();
    };
    $rootScope.closeSideNavPanel = function() {
        $mdSidenav('left').close();
    };
    
    $rootScope.gotoDashboard = function() {
    		$location.path("dashboard/" + $rootScope.hostId);
    }
	
	
		var authObject = firebase.auth();
	  var auth = $firebaseAuth(authObject);
	  authObject.onAuthStateChanged(function(user) {
		  if (user && $rootScope.displayName) {
			  $rootScope.displayName = user.displayName;
			  $scope.formData = {"firebaseUserId": user.pa};
	          $scope.platformLogin();
		  } else {
		    // No user is signed in.
		  }
		});
	  
	  if($rootScope.ui != undefined){
		    $rootScope.ui.reset();
		}else{
		    $rootScope.ui = new firebaseui.auth.AuthUI(firebase.auth());
		}
		
	  $scope.firebaseUser = null;
      $scope.error = null;
      $scope.loggedIn = false;
      $rootScope.ui.start('#firebaseui-auth-container', {
    	  signInOptions : [
    		  firebase.auth.GoogleAuthProvider.PROVIDER_ID
    	  ],
    	  'callbacks': {
    	        'signInSuccess': function(currentUser, credential, redirectUrl) {
    	          $scope.formData = {"firebaseUserId": currentUser._lat};
    	          $rootScope.displayName = currentUser.displayName;
    	          $scope.platformLogin();
    	          return false;
    	        }
      }
      });
      
      $scope.platformLogin = function() {
	    	  Restangular.all('login/partner').post($scope.formData).then(function(response){
	    	  	  $rootScope.loggedIn = true;
	    	  	  $scope.loggedIn = true;
	    	  	  if(response.changes && response.changes.hostId) {
	    	  		  Restangular.setDefaultHeaders({ 'Authorization': 'Custom ' + response.changes.sessionToken});
	    	  		  $rootScope.hostId = response.changes.hostId;
	    	  		  $location.path("dashboard/" + response.changes.hostId);
	    	  	  } else {
	    	  		Restangular.setDefaultHeaders({ 'Authorization': 'Custom ' + response.changes.sessionToken});
	    	  		  $location.path("createhost/" + response.resourceId);
	    	  	  }
		  });
      };
      
      $rootScope.logout = function() {
    	  	firebase.auth().signOut().then(function() {
    	  		$rootScope.displayName = undefined;
    	  		$rootScope.loggedIn = false;
    	  		$location.path("login/");
//    	  		$location.reload();
    	  		$route.reload();
    		}, function(error) {
    		  console.log(error);
    		});
      };
      
 }]);