angular.module('partner.controllers').controller('TxnController', ['$scope', 'moment', '$rootScope', 'Restangular', '$location', function($scope, moment, $rootScope, Restangular, $location) {
	
	Date.prototype.addDays = function(days) {
	    this.setDate(this.getDate() + parseInt(days));
	    return this;
	};
	
	$scope.to = new Date().addDays(7);
	$scope.from = new Date();
	
	$scope.search = function() {
		var formattedToDate = moment($scope.to).format('DDMMMYYYY');
		var formattedFromDate = moment($scope.from).format('DDMMMYYYY');
		Restangular.all('orders').getList({dateTo:formattedToDate, dateFrom: formattedFromDate})  
	    .then(function(orders) {
	      $scope.orders = orders; 
	    })
	};
	
	$scope.search();
	

}]);