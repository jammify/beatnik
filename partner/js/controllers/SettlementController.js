angular.module('partner.controllers').controller('SettlementController', ['$scope', '$rootScope', 'Restangular', '$location', function($scope, $rootScope, Restangular, $location) {
	
	Date.prototype.addDays = function(days) {
	    this.setDate(this.getDate() + parseInt(days));
	    return this;
	};
	
	$scope.to = new Date().addDays(7);
	$scope.from = new Date();
	
	$scope.search = function() {
		var formattedToDate = moment($scope.from).format('DDMMMYYYY');
		var formattedFromDate = moment($scope.to).format('DDMMMYYYY');
		Restangular.all('orders/settlements').getList({dateTo:formattedToDate, dateFrom: formattedFromDate})  
	    .then(function(orders) {
	      $scope.orders = orders; 
	    })
	};
	
	$scope.search();
	

}]);