'use strict';

angular.module('partner', ['ngRoute', 'restangular', 'ngMap', 'ngFileUpload', 'localytics.directives', 'ngMaterial', 'md.data.table','firebase', 'angularMoment', 'moment-picker', 'nvd3', 'partner.services', 'partner.controllers']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
//	  $locationProvider.html5Mode(true);
    $routeProvider.when('/createhost/:userId', {templateUrl: 'views/createhost.html'});
    $routeProvider.when('/createroom/:hostId', {templateUrl: 'views/createroom.html'});
    $routeProvider.when('/setup/:hostId', {templateUrl: 'views/setup.html'});
    $routeProvider.when('/dashboard/:hostId', {templateUrl: 'views/dashboard.html'});
    $routeProvider.when('/transactions/:hostId', {templateUrl: 'views/transactions.html'});
    $routeProvider.when('/settlements/:hostId', {templateUrl: 'views/settlements.html'});
    $routeProvider.when('/login', {templateUrl: 'views/login.html'});
	$routeProvider.otherwise({redirectTo: '/login'});
	
  }]).
  config(['RestangularProvider', '$httpProvider', 'momentPickerProvider', function(RestangularProvider, $httpProvider, momentPickerProvider) {
      RestangularProvider.setBaseUrl('https://jammate.in/beatnik-core/'); 
	  RestangularProvider.setDefaultHeaders({ 'Accept': 'text/html,application/json;q=0.9,*/*;q=0.8'});
	  delete $httpProvider.defaults.headers.common['X-Requested-With'];
      RestangularProvider.setRestangularFields({
        id: '_id.$oid'
      });
      
      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      });
      
  }]);