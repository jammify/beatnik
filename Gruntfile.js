'use strict';

module.exports = function (grunt) {
	 // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Project settings
        beatnik: {
            // configurable paths
            app: require('./partner/bower.json').appPath || 'partner',
            dist: 'dist',
            target: 'beatnik'
        },
        concat: {
            pcss: {
                src: [
                    'partner/css/*.css'
                ],
                dest: 'dist/beatnik.css'
            },
            pjs: {
                src: [
                    'partner/js/*.js',
                    'partner/js/controllers/*.js'
                ],
                dest: 'partner/dist/beatnik.js'
            },
            acss: {
                src: [
                    'app/css/*.css'
                ],
                dest: 'dist/beatnik.css'
            },
            ajs: {
                src: [
                    'app/js/*.js',
                    'app/js/controllers/*.js'
                ],
                dest: 'app/dist/beatnik.js'
            }
        },
        cssmin: {
            css: {
                src: 'dist/beatnik.css',
                dest: 'dist/beatnik.min.css'
            }
        },
        uglify: {
            pjs: {
                files: {
                    'dist/beatnik.min.js': ['partner/dist/beatnik.js']
                }
            },
            ajs: {
                files: {
                    'dist/beatnik.min.js': ['app/dist/beatnik.js']
                }
            }
        },
       watch: {
       	  partner: {
       	  	files: ['partner/*'],
       	  	tasks: ['concat', 'cssmin', 'uglify']
       	  },
       	  app: {
       	  	files: ['app/*'],
          	tasks: ['concat', 'cssmin', 'uglify']
       	  }
       },
       // Empties folders to start fresh
       clean: {
           dist: {
               files: [{
                   dot: true,
                   src: [
                       '.tmp',
                       '<%= beatnik.dist %>/*',
                       '!<%= beatnik.dist %>/.git*'
                   ]
               }]
           },
           server: '.tmp'
       },
       connect: {
       		//Unable to separate connect:options for partnerserve and appserve
           options: {
               port:  9005,
               hostname: 'localhost',
               livereload: 35729,
               open:'http://<%= connect.options.hostname %>:<%= connect.options.port %>/app'
           },
           livereload: {
               options: {
                   base: [
                       '<%= beatnik.partner %>'
                   ],
                   middleware: function(connect, options, middlewares) {
		            var modRewrite = require('connect-modrewrite');
		            middlewares.unshift(modRewrite(['!\\.html|\\.js|\\.svg|\\.css|\\.png$ /index.html [L]']));
		            return middlewares;
		          }
               }
           }
       },
       copy: {
    	   		server: {
               expand: true,
               dot: true,
               src: '**/**'
           }
       }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['concat:pcss', 'cssmin:css', 'concat:pjs', 'uglify:js']);
    grunt.registerTask('partnerserve', ['clean', 'concat:pjs', 'connect:livereload', 'watch:partner']);
    grunt.registerTask('partnerprod', ['clean', 'concat:pjs', 'uglify:pjs']);
    grunt.registerTask('appserve', ['clean', 'concat:ajs', 'connect:livereload', 'watch:app']);
    grunt.registerTask('appprod', ['clean', 'concat:ajs', 'uglify:ajs']);
};